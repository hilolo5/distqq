import { Component, OnInit, Input, Inject } from '@angular/core';
import { Historique } from 'app/Models/Entities/Commun/Historique';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ActionHistorique } from './../../Enums/ActionHistorique.Enum';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { PlanComptableModel } from 'app/Models/Entities/Parametres/PlanComptableModel';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { ClientType } from 'app/Enums/TypeClient.enum';
import { User } from 'app/Models/Entities/User';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { HeaderService } from 'app/services/header/header.service';
import { LocalElements } from 'app/shared/utils/local-elements';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'common-historiques',
	templateUrl: './historiques.component.html',
	styleUrls: ['./historiques.component.scss'],
	providers: []
})
export class HistoriquesComponent implements OnInit {


	// tslint:disable-next-line:no-input-rename
	@Input('historique') historique: Historique[] = [];
	details: any;
	labelsTranslated;
	processIsStarting = false;
	isFilledObject = false;
	isUpdated = false;
	role;
	listChantiers = [];
	actionHistorique: ActionHistorique = new ActionHistorique();
	ListeCategorie = [];
	listGroupes: Groupe[] = [];
	listTechnicians: User[] = [];
	docType = '';
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
	) { }

	ngOnInit() {
		this.getRole();
		this.getListeTechnician();
		// this.ListGroupe();
		// this.ListChantier();
		this.getListeCategorie();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('labels').subscribe(labels => {
			this.labelsTranslated = labels;
		});
		this.serializeObjects();
		setInterval(() => {
			try {
				this.historique.sort((one, two) => {
					const x = new Date(one.dateAction);
					const y = new Date(two.dateAction);
					return (x.getTime() > y.getTime() ? -1 : 1);
				});
			} catch (ex) { }
		}, 1000);
	}

	getRole() {
		this.service.getAll(ApiUrl.role).subscribe(res => {
			this.role = res.value;
		});
	}

	showInConsole(champs) {
		this.processIsStarting = true;
		this.details = champs;
		this.processIsStarting = false;
	}

	getProfils(id) {
		let res = '';
		this.translate.get('labels').subscribe(labels => {
			switch (this.getRoleTyp(id)) {
				case UserProfile.admin:
					res = labels.admin;
					break;
				case UserProfile.technicien:
					res = labels.technicien;
					break;
				case UserProfile.manager:
					res = labels.manager;
					break;
				case UserProfile.technicienChantier:
					res = labels.technicienChantier;
					break;
				case UserProfile.technicienMaintenance:
					res = labels.technicienmaintenace;
					break;
			}
		});
		return res;
	}

	getRoleTyp(id) {
		return this.role.filter(x => x.id === id)[0].type
	}

	serializeObjects() {

		setInterval(() => {
			this.isFilledObject = (this.historique !== undefined);
			if (this.isFilledObject && !this.isUpdated) {
				this.historique.forEach((element) => {
					const champsLast = [];
					element.fields.forEach((item) => {
						if (item.champ === 'RoleId') {
							item.valeurInitial = this.getProfils(item.valeurInitial);
							item.valeurFinal = this.getProfils(item.valeurFinal);
						}
						if (
							item.champ === 'ProductSuppliers' || item.champ === 'Labels' || item.champ === 'PublishingContracts') {
							item.valeurFinal = 'L\'un des éléments de la liste a changé';
						}
						if (item.champ === 'ContactInformations') {
							item.champ = 'Contact informations'
							item.valeurFinal = 'L\'un des éléments de la liste a changé';
						}
						if (item.champ === 'PaymentCondition') {
							item.champ = 'Conditions'

						}
						if (item.champ === 'PhoneNumber') {
							item.champ = 'Téléphone'

						}
						if (item.champ === 'Website') {
							item.champ = 'Site web'

						}
						if (item.champ === 'LandLine') {
							item.champ = 'Fax'

						}
						if (item.champ == 'IntraCommunityVAT') {
							item.champ = 'TVA Intra-Communautaire'

						}
						if (item.champ === 'AccountingCode') {
							item.champ = 'Code comptable'
						}
						if (item.champ === 'AccountingCode') {
							item.champ = 'Code comptable'
						}
						if (item.champ === 'LastName') {
							item.champ = 'Nom'

						}
						if (item.champ === 'FirstName') {
							item.champ = 'Prénom'
						}
						if (item.champ === 'Addresses') {
							item.champ = 'Adresses';
							item.valeurFinal = 'L\'un des éléments de la liste a changé';
						}
						if (item.champ === 'WorkshopId') {
							item.valeurInitial = this.getChantier(item.valeurInitial);
							item.valeurFinal = this.getChantier(item.valeurFinal);
						}
						if (item.champ === 'CategoryId') {
							item.valeurInitial = this.getCategorie(item.valeurInitial);
							item.valeurFinal = this.getCategorie(item.valeurFinal);
						}
						if (item.champ === 'GroupeId') {
							item.valeurInitial = this.getGroupe(item.valeurInitial);
							item.valeurFinal = this.getGroupe(item.valeurFinal);
						}
						if (item.champ === 'status') {
							item.valeurInitial = this.getTranslateStaus(item.valeurInitial);
							item.valeurFinal = this.getTranslateStaus(item.valeurFinal);
						}
						if (item.champ === 'ExpirationAlertPeriodType') {
							item.valeurInitial = this.getTranslateTypeP(item.valeurInitial);
							item.valeurFinal = this.getTranslateTypeP(item.valeurFinal);
						}
						if (item.champ === 'enableAutoRenewal' || item.champ === 'ExpirationAlertEnabled') {
							item.valeurInitial = item.valeurInitial ? 'Oui' : 'Non';
							item.valeurFinal = item.valeurFinal ? 'Oui' : 'Non';
						}
						if (item.champ === 'Type') {
							item.valeurInitial = this.getType(item.valeurInitial);
							item.valeurFinal = this.getType(item.valeurFinal);
						}
						if (item.champ === 'TechnicianId') {
							item.valeurInitial = this.getTechnician(item.valeurInitial);
							item.valeurFinal = this.getTechnician(item.valeurFinal);
						}
						champsLast.push(item);
					});
					element.fields = champsLast;
				});
				this.isUpdated = true;
			}
		}, 1000);
	}

	formatBooleanValue(value): string {

		if (typeof (value) === 'boolean') {
			return value ? 'oui' : 'non';
		} else {
			return value
		}
	}

	isNotEquivalent(a, b) {

		// Create arrays of property names
		const aProps = Object.getOwnPropertyNames(a);
		const bProps = Object.getOwnPropertyNames(b);

		// If number of properties is different,
		// objects are not equivalent
		if (aProps.length !== bProps.length) {
			return true;
		}

		for (let i = 0; i < aProps.length; i++) {
			const propName = aProps[i];

			// If values of same property are not equal,
			// objects are not equivalent
			if (a[propName] !== b[propName]) {
				return true;
			}
		}

		// If we made it this far, objects
		// are considered equivalent
		return false;
	}

	getTranslate(name) {
		for (const i in this.labelsTranslated) {
			if (i.toLowerCase() === name.toLowerCase()) {
				return this.labelsTranslated[i];
			}
		}
		return name
	}


	ListGroupe() {
		this.service.getAll(ApiUrl.Groupe).subscribe((res) => {
			this.listGroupes = res.value;
		});
	}

	ListChantier() {
		this.service.getAll(ApiUrl.Chantier).subscribe((res) => {
			this.listChantiers = res.value;
		});

	}

	getListeCategorie(): void {
		this.service.getAll(ApiUrl.configClassification).subscribe(res => {
			const list = res.value.filter(x => x.type === 1);
			list.forEach(element => {
				this.ListeCategorie = [...this.ListeCategorie, ...element.subCategories]
			});
		});
	}

	getListeTechnician(): void {
		const filter = {
			roleIds: [],
			SearchQuery: '',
			Page: 1,
			PageSize: 500,
			OrderBy: 'firstName',
			SortDirection: 'Descending',
			ignorePagination: true
		};
		this.service.getAllPagination(ApiUrl.User, filter).subscribe(res => {
			this.listTechnicians = res.value;

		});
	}

	getTechnician(id) {
		if (this.listTechnicians) {
			const result = this.listTechnicians.filter(x => x.id === id)[0];
			return result !== undefined ? (result.firstName + ' ' + result.lastName) : '';
		}
	}

	getChantier(id) {
		if (this.listChantiers) {
			const result = this.listChantiers.filter(x => x.id === id)[0];
			return result !== undefined ? result.client.firstName : '';
		}
	}

	getCategorie(id) {
		if (this.ListeCategorie) {
			const result = this.ListeCategorie.find(x => x.id === id);
			return result !== undefined ? result.label : '';
		}
	}

	display(item) {
		const list = ['HourlyCost', 'WarrantyExpirationDate', 'Id', 'LineItems', 'OperationSheetsIds', 'Attachments', 'EquipmentMaintenance'];
		return !list.includes(item);
	}

	getGroupe(id) {
		if (this.listGroupes) {
			const result = this.listGroupes.filter(x => x.id === +id)[0];
			return result !== undefined ? result.name : '';
		}
	}

	getTranslateStaus(item) {
		return this.translate.instant(item);
	}

	getTranslateTypeP(item) {
		return this.translate.instant('periodeType.' + item);
	}

	getType(type) {
		let res = '';
		this.translate.get('labels').subscribe(labels => {
			switch (type) {
				case ClientType.Particular:
					res = labels.particular;
					break;
				case ClientType.Professional:
					res = labels.professional;
					break;
				default:
					res = type;
			}
		});
		return res;
	}

}
