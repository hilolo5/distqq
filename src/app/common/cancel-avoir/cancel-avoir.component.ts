import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Router } from '@angular/router';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
import { Constants } from 'app/shared/utils/constants';
import { AppSettings } from 'app/app-settings/app-settings';

declare var swal: any;
declare var toastr: any;

@Component({
	selector: 'app-cancel-avoir',
	templateUrl: './cancel-avoir.component.html',
	styleUrls: ['./cancel-avoir.component.scss']
})
export class CancelAvoirComponent implements OnInit, OnChanges {

	memos: Memo[] = [];
	memo: Memo = new Memo();
	commentaire = '';
	files = null;
	avoirdoc: Memo[] = [];
	processIsStarting = false;
	statuts: { id: number; label: string; color: string }[];
	articles = [];
	articlesInfo: any = {};
	processing = false;
	uploadStatus;
	document;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<CancelAvoirComponent>,
		public router: Router,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, document },
		private translate: TranslateService) {
		this.document = this.data.document;
	}

	ngOnInit() {
		if (this.data) {
		}
	}

	ngOnChanges() {
	}

	startUpload(event: FileList): void {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = Constants.getContentType(file.name.substring(file.name.lastIndexOf('.') + 1)).Mime;
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString()
			this.memo.attachments.unshift(pieceJoin)
			this.files = null;
		}
	}

	onFileChange(event) {
		this.uploadStatus = 0;
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
		}
	}

	deleteFile(i) {
		this.translate.get('commun.deleteMemo').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					const id = this.memos[i].id;
					const deleteId = { 'memosIds': [id] };
					this.service.create(ApiUrl.File + '/' + this.document.id + '/Delete/Memo', deleteId)
						.subscribe(res => {
							this.processIsStarting = false;
							// this.refresh();
						}, err => { console.log(err); this.processIsStarting = false; });
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	saveDocAvoir() {
		if (this.commentaire !== '' || this.memo.attachments.length > 0) {

			this.memo.id = AppSettings.guid();
			this.memo.comment = this.commentaire
			this.memo.createdOn = new Date();

			this.service.create(ApiUrl.Depense + '/' + this.document.id + '/Cancel', this.memo)
				.subscribe(res => {
					this.memo = new Memo();
					this.commentaire = '';
					this.processIsStarting = false;
					this.close();
				},
					err => {
						console.log(err)
					}
				);
		}
	}

	close() {
		this.dialogRef.close();
	}

}
