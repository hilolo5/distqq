import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { DocumentHelper } from 'app/libraries/document-helper';

export declare var swal: any;
declare var jQuery: any;

@Component({
	selector: 'app-create-depense',
	templateUrl: './create-depense.component.html',
	styleUrls: ['./create-depense.component.scss']
})
export class CreateDepenseComponent implements OnInit, OnChanges {

	search = '';
	articlesDepanseSelected: any
	articlesBc: any;
	document;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<CreateDepenseComponent>,
		public router: Router,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, document },
		private translate: TranslateService) {
		this.document = AppSettings.cloneObject(data.document);
	}

	ngOnInit() {
		if (this.data) {
			this.selectBcArticles(this.document);
		}
	}

	getPlaceholder() { return 'Recherche' }

	ngOnChanges() {
	}

	selectBcArticles(bonCommandeFournisseur) {
		this.articlesDepanseSelected = [];
		this.articlesBc = [];
		this.articlesBc = this.getArticlesBc(bonCommandeFournisseur);
	}

	getArticlesBc(bonCommandeFournisseur) {

		const prestationsBc: any[] = bonCommandeFournisseur.orderDetails.lineItems;
		if (bonCommandeFournisseur.expenses.length === 0) {
			return prestationsBc;
		}

		if (bonCommandeFournisseur.expenses.length !== 0) {
			const listArticlesUnSelected = [];
			const depenses = bonCommandeFournisseur.expenses;
			//  let depenses:Depenses = bonCommandeFournisseur.depenseBonCommandeFournisseurs.map(x=> return x.depense);

			let depensesTmp: any[] = [];

			depenses.forEach(item => {
				const prestations = item.orderDetails.lineItems;
				depensesTmp = depensesTmp.concat(prestations);
			});

			prestationsBc.forEach(prestation => {
				const depensesPresCount = depensesTmp.filter(x => {
					// x.data.tva == prestation.data.tva
					// &&
					if (x.product.id === prestation.product.id && x.product.vat === prestation.product.vat &&
						x.product.quantity === prestation.product.quantity) {
						return true;
					}
					// && x.data.qte == prestation.data.qte
				}).length;

				const bcPresCount = prestationsBc.filter(x => {
					if (x.product.id === prestation.product.id && x.product.vat === prestation.product.vat &&
						x.product.quantity === prestation.product.quantity) {
						return true;
					}
				}).length;

				const selectedPresCount = listArticlesUnSelected.filter(x => {
					if (x.product.id === prestation.product.id && x.product.vat === prestation.product.vat &&
						x.product.quantity === prestation.product.quantity) {
						return true;
					}
				}).length;

				let ifExists = false;

				if ((bcPresCount > depensesPresCount) && (selectedPresCount < (bcPresCount - depensesPresCount))) {
					ifExists = true;
				}

				if (ifExists) {
					listArticlesUnSelected.unshift(prestation);
				}
			});

			return listArticlesUnSelected;
		}
	}

	/**
	 * Selectioné les articles
	 */
	addArticle(index) {
		const article = this.articlesBc[index]
		this.articlesDepanseSelected.push(article)
		this.articlesBc.splice(index, 1)
	}
	/**
	 * Déselectioné
	 */
	removeArticle(index) {
		const article = this.articlesDepanseSelected[index]
		this.articlesBc.push(article)
		this.articlesDepanseSelected.splice(index, 1)
	}

	saveArticle() {
		if (this.articlesDepanseSelected.length > 0) {
			this.document['orderDetails']['lineItems'] = this.articlesDepanseSelected;
			localStorage.setItem(LocalElements.docType, ApiUrl.Depense);
			localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.BonCommandeF);
			this.router.navigate(['/depense/generer-create/', this.document.id]);
			localStorage.setItem('articlesDepanseSelected' , JSON.stringify(this.document) );
			this.dialogRef.close(this.articlesDepanseSelected);
		}
	}

	close() {
		this.dialogRef.close();
	}

	searchArticle() {

	}

}
