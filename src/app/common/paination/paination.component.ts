import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'paination',
	templateUrl: './paination.component.html',
	styleUrls: ['./paination.component.scss']
})

export class PainationComponent {
	// tslint:disable-next-line:no-input-rename
	@Input('painationInfo') painationInfo: painationModel = {
		value: [],
		currentPage: 1,
		rowsCount: 1,
		firstRowOnPage: 1,
		lastRowOnPage: 2,
		pageSize: 10,
		count: 1,
		pagesCount: 1,
	};

	// tslint:disable-next-line:no-input-rename
	@Input('pageSizes') pageSizes: number[] = [10, 25, 50, 100];
	// tslint:disable-next-line:no-output-rename
	@Output('chagePageNamber') chagePageNamber = new EventEmitter();
	// tslint:disable-next-line:no-output-rename
	@Output('changePageSize') changePageSize = new EventEmitter();


	constructor() { }

	Premier() {
		if (this.painationInfo.currentPage !== 1) {
			this.chagePageNamber.emit(1);
		}
	}

	Precedent() {
		if (this.painationInfo.currentPage > 1) {
			const pageNumber: number = this.painationInfo.currentPage - 1;
			this.chagePageNamber.emit(pageNumber);
		}
	}

	Suivant() {
		if (this.painationInfo.currentPage < this.painationInfo.pagesCount) {
			this.chagePageNamber.emit(this.painationInfo.currentPage + 1);
		}
	}

	Dernier() {
		if (this.painationInfo.currentPage !== this.painationInfo.pagesCount) {
			this.chagePageNamber.emit(this.painationInfo.pagesCount);
		}
	}
	changePage(pageNb) {
		this.chagePageNamber.emit(pageNb);
	}

	PageSize(size) {
		this.changePageSize.emit(size);
	}

	getFirstElement = (): number => this.painationInfo && this.painationInfo.rowsCount > 0
		? ((this.painationInfo.currentPage - 1) * this.painationInfo.pageSize) + 1
		: 0;

	getLastElement() {
		return this.painationInfo
		? this.painationInfo.currentPage >= this.painationInfo.pagesCount
			? this.painationInfo.rowsCount
			: this.painationInfo.currentPage * this.painationInfo.pageSize
		: 0;
	}
}

// tslint:disable-next-line:class-name
class painationModel {
	value: [];
	currentPage: number;
	rowsCount: number;
	firstRowOnPage: number;
	lastRowOnPage: number;
	pageSize: number;
	count: number;
	pagesCount: number;
}





