import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	selector: 'historique-emails',
	templateUrl: './historique-email.component.html',
	styleUrls: ['./historique-email.component.scss']
})
export class HistoriqueEmailComponent implements OnInit {

	// tslint:disable-next-line:no-input-rename
	@Input('emails') emails: any[] = [];
	docType = '';

	constructor(private translate: TranslateService,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

	}
	ngOnInit() {

	}

}
