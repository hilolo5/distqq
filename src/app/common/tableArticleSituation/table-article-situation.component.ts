import { Component, Input, OnChanges, OnInit, Inject } from '@angular/core';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import * as _ from 'lodash';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { TypeParametrage } from 'app/Enums/TypeParametrage.Enum';
import { DelaiGaranties } from 'app/Enums/DelaiGaranties.Enum';
import { element } from 'protractor';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { QuoteSituations, Quote } from 'app/Models/Entities/Documents/Quote';
import { CalculTva } from 'app/Models/Model/calcul-tva';
import { ArticleType } from 'app/Models/Entities/Commun/article-type';
import { ICalcule } from 'app/calcule/ICalcule';
import { Calcule } from 'app/calcule/Calcule';


interface CalculResult {
	totaGenerallHt: number,
	totalHt: number,
	totalTtc: number,
	prorata: number,
	tva: () => number,
	puc: number,
	totalAcomptes: number,
	situationCumullee: number,
	resteAPayer: number
}

@Component({
	selector: 'table-article-situation',
	templateUrl: './table-article-situation.component.html',
	styleUrls: ['./table-article-situation.component.scss'],
})

export class TableArticleSituationComponent implements OnInit, OnChanges {

	@Input('devis') devisInfos: Quote;
	@Input('pourcentage') pourcentage = 0;
	@Input('pourcentageInDb') pourcentageInDb = 0;
	@Input('factureCloture') factureCloture = false;
	@Input('load') load: { getDateToSave };
	@Input('readOnly') readOnly = false;
	@Input('facture') facture: Invoice = null;
	@Input('userForAcompt') userForAcompt: boolean = false;
	@Input('retenueGarantieValue') retenueGarantieValue: number = 0;
	@Input('delaiGarantie') delaiGarantie: DelaiGaranties = 12;
	retenueGarantie = 0;
	delaiGarantiesEnum: typeof DelaiGaranties = DelaiGaranties;
	typeFacture: typeof TypeFacture = TypeFacture;
	statutFacture: typeof StatutFacture = StatutFacture;
	listFactureSituation: QuoteSituations[] = [];
	listFactureAcompte: QuoteSituations[] = [];
	totalTTcSotuation = 0;
	totalHTSituation = 0;
	totalTTcAcompte = 0;
	totalHTAcompte = 0;
	articleType: ArticleType = new ArticleType();
	articles = [];
	calculeRemise: ICalcule = new Calcule();
	// retenueGarantie: any;
	calcul: {
		prixHT: number,
		prixTTC: number,
		tva: number,
		situationCumulleeTTC: number,
		situationCumulleeHT: number,
		totalAcomptesHT: number,
		totalAcomptesTTC: number,
		resteAPayerTTC: number,
		resteAPayerHT: number,
		montantHT: number,

		partProrata: number
	};
	factureSituationCount = ['1'];
	prestation: any;
	groupeTVA = [];
	calculResult: CalculResult
	factureSituation: QuoteSituations[] = [];
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
	) {
		this.calcul = {
			prixHT: 0,
			prixTTC: 0,
			resteAPayerTTC: 0,
			resteAPayerHT: 0,
			situationCumulleeTTC: 0,
			situationCumulleeHT: 0,
			totalAcomptesHT: 0,
			totalAcomptesTTC: 0,
			tva: 0,
			montantHT: 0,
			partProrata: 0
		}

	}
	ngOnInit() {

		//  this.retenueGarantie = (this.retenueGarantieValue === null || this.retenueGarantieValue === 0) ? 0 : 1;

	}

	getNewHt(htFacture) {
		if (this.factureCloture === false) {
			return htFacture
		} else {
			let ht;

			// tslint:disable-next-line:no-shadowed-variable
			this.factureSituation.forEach(element =>
				ht = element.totalHT += element.totalHT
			)
			return ht
		}
	}

	calcultHt() {
		this.totalHTSituation = this.totalHTSituation === undefined ? 0 : this.totalHTSituation;
		this.totalHTAcompte = this.totalHTAcompte === undefined ? 0 : this.totalHTAcompte;
		return (this.calcul.montantHT - (this.totalHTSituation + this.totalHTAcompte))
	}
	calculTTC() {
		this.totalTTcSotuation = this.totalTTcSotuation === undefined ? 0 : this.totalTTcSotuation;
		this.totalTTcAcompte = this.totalTTcAcompte === undefined ? 0 : this.totalTTcAcompte;
		return (this.calcul.prixTTC - (this.totalTTcSotuation + this.totalTTcAcompte))
	}
	getListFacture(facture: QuoteSituations[]) {
		if (facture.length > 0) {
			this.listFactureSituation = facture.filter(x => x.typeInvoice === this.typeFacture.Situation);

			this.listFactureAcompte = facture.filter(x => x.typeInvoice === this.typeFacture.Acompte);

			if (this.factureCloture) {
				if (this.listFactureSituation.length !== 0) {
					this.listFactureSituation.forEach(situation => {
						this.totalTTcSotuation += situation.totalTTC,
							this.totalHTSituation += situation.totalHT
					});
				}
				if (this.listFactureAcompte.length !== 0) {
					this.listFactureAcompte.forEach(acompte => {
						this.totalTTcAcompte += acompte.totalTTC,
							this.totalHTAcompte += acompte.totalHT
					});
				}

			}
		}
	}

	conditionCloture() {
		// const situations = JSON.parse(this.devisInfos.situation) as { idFacture: string, pourcentage: number, resteAPayer: number }[];
		// if (situations !== null) {
		// const lastFacture = situations[situations.length - 1]
		if (this.facture.status === this.statutFacture.Cloture) {
			// if (this.facture.status === this.statutFacture.Cloture && lastFacture.idFacture === this.facture.id) {
			return true
		} else {
			return false
		}
		// } else {
		// 	return false
		// }

	}
	ngOnChanges() {
		if (this.devisInfos) {
			if (this.devisInfos.situations !== undefined && this.facture === null) {
				this.getListFacture(this.devisInfos.situations);
			}
			this.retenueGarantieValue = this.devisInfos.orderDetails.holdbackDetails.holdback;
			if (this.devisInfos.orderDetails.holdbackDetails.holdback !== null) {
				this.retenueGarantie = (this.retenueGarantieValue === null || this.retenueGarantieValue === 0) ? 0 : 1;
			}
			if (this.facture !== null) {
				this.prestation = this.getProductsOfArticles(this.facture.orderDetails.lineItems);
				let factureList: QuoteSituations[];
				factureList = this.devisInfos.situations !== null ? this.devisInfos.situations : [];
				factureList.filter(e => e.invoiceId !== this.facture.id)
				if (this.facture.status === this.statutFacture.Cloture) {
					this.factureCloture = true;
				}
				this.getListFacture(factureList);
			}
			if (this.readOnly) {
				this.calcul.tva = ((this.facture.orderDetails.totalTTC - this.facture.orderDetails.totalHT) / this.facture.orderDetails.totalHT)
					* 100
			}

			// this.factureSituationCount = !this.devisInfos.situations ? 0 :
			// 	this.devisInfos.situations.filter(x => x.typeInvoice === TypeFacture.Situation).length;
			this.articles = this.getProductsOfArticles(this.devisInfos.orderDetails.lineItems);
			//  validation
			if (!this.checkDevisInfosIsValidToDispaly(this.devisInfos)) { return; }
			this.initData(this.pourcentage);
		}
	}

	getProductsOfArticles(articles) {
		const articlesTmp = [];
		articles.forEach(article => {

			if (article.type === this.articleType.produit) {
				article.product.quantity = article.quantity;
				articlesTmp.push(article.product);
			}

			if (article.type === this.articleType.lot) {
				article.product.products.map(e => {
					e.productDetails.quantity = e.quantity;
					articlesTmp.push(e.productDetails);
				});
			}
		});
		return articlesTmp;
	}

	groupTVA(): CalculTva[] {
		if (this.devisInfos) {
			const calculTvas = this.calculeRemise.calculVentilationRemise(
				this.articles,
				this.clalcTotalGeneral(),
				this.devisInfos.orderDetails.globalDiscount.value,
				this.devisInfos.orderDetails.globalDiscount.type
			);
			this.groupeTVA = [];
			//	const pourccentage = (this.factureSituationCount === 0 && !this.userForAcompt) ? (this.pourcentage - this.pourcentageInDb) : this.pourcentage;
			const pourccentage = this.pourcentage
			calculTvas.forEach(calcule => {
				calcule.tva = calcule.tva;
				calcule.totalHT = calcule.totalHT * ((pourccentage === null ? 0 : pourccentage) / 100);
				calcule.totalTVA = calcule.totalHT * (+calcule.tva / 100);
				calcule.totalTTC = calcule.totalTVA + calcule.totalHT;
				this.groupeTVA.push(calcule);
			});
			return this.groupeTVA;
		}
	}

	groupTVARead(): CalculTva[] {
		this.prestation = this.getProductsOfArticles(this.facture.orderDetails.lineItems);
		if (this.prestation) {
			const calculTvas = this.calculeRemise.calculVentilationRemise(
				this.prestation,
				this.clalcTotalGeneralRead(),
				0,
				TypeValue.Amount
			);
			return calculTvas;
		}
	}
	clalcTotalGeneral(): number {
		let totalHt = 0;
		this.articles.forEach(article => {
			totalHt = totalHt + this.TotalHt(article, article.quantity);
		});
		return totalHt;
	}

	clalcTotalGeneralRead(): number {
		let totalHt = 0;
		this.prestation.forEach(article => {
			totalHt = totalHt + article.totalHT * article.quantity;
		});
		return totalHt;
	}


	calculate_cout_horaire(Nomber_heure, Cout_vente): any {
		return parseFloat(Nomber_heure) * parseFloat(Cout_vente);
	}


	PrixHt(article) {
		return (this.calculate_cout_horaire(article.totalHours, article.hourlyCost) + article.materialCost);
	}

	TotalHt(article, qte) {
		const totalHt = this.PrixHt(article);
		if (+totalHt === 0) {
			return article.totalHT;
		}
		let remise = 0;
		if (article.discount !== null && article.discount.value !== 0 && article.discount.value !== undefined) {
			if (article.discount.type === TypeValue.Amount) {
				remise = article.discount.value;
			} else {
				remise = totalHt * (article.discount.value / 100);
			}
		}
		const res = qte !== undefined ? qte * (totalHt - remise) : (totalHt - remise);
		const result = parseFloat(res.toFixed(10));
		return isNaN(result) ? 0 : result;
	}


	initData(pourcentage) {

		const situationCumullee: { cumulleeHt: number, cumulleeTTC: number } =
			this.getSituationsCumullee(this.devisInfos.situations);

		this.calcul.situationCumulleeHT = situationCumullee.cumulleeHt;
		this.calcul.situationCumulleeTTC = situationCumullee.cumulleeTTC;


		// total Acomptes
		const totalAcomptes = this.getTotalAcomptes(this.devisInfos.situations);
		this.calcul.totalAcomptesHT = totalAcomptes.totalHT;
		this.calcul.totalAcomptesTTC = totalAcomptes.totalTTC;

		// tva
		this.calcul.tva = ((this.devisInfos.orderDetails.totalTTC - this.devisInfos.orderDetails.totalHT)
			/ this.devisInfos.orderDetails.totalHT) * 100

		// récuperer le nomber des facture situation
		// const factureSituationCount = !this.devisInfos.situations ? 0 :
		// 	this.devisInfos.situations.filter(x => x.typeInvoice === TypeFacture.Situation).length;

		// prixTTC
		let restTtc = 0;

		// si cette facture est premiére facture calculer à partir du mantant du devis est n'est pas du reste à payer
		// if (factureSituationCount === 0 && !this.userForAcompt) {
		// 	restTtc = this.devisInfos.orderDetails.totalTTC - (situationCumullee.cumulleeTTC + this.calcul.totalAcomptesTTC);
		// 	this.calcul.prixTTC = (this.devisInfos.orderDetails.totalTTC *
		// 		((pourcentage === null ? 0 : pourcentage) / 100)) - totalAcomptes.totalTTC;
		// }

		// if (factureSituationCount !== 0 || this.userForAcompt) {
		// 	restTtc = this.devisInfos.orderDetails.totalTTC - (situationCumullee.cumulleeTTC + this.calcul.totalAcomptesTTC);
		// 	this.calcul.prixTTC = (this.devisInfos.orderDetails.totalTTC * ((pourcentage === null ? 0 : pourcentage) / 100));
		// }
		restTtc = this.devisInfos.orderDetails.totalTTC - (situationCumullee.cumulleeTTC + this.calcul.totalAcomptesTTC);
		this.calcul.prixTTC = (this.devisInfos.orderDetails.totalTTC * ((pourcentage === null ? 0 : pourcentage) / 100));

		if (pourcentage !== null) {
			if (this.userForAcompt) {
				this.calcul.totalAcomptesTTC = this.calcul.totalAcomptesTTC + this.calcul.prixTTC;
			} else {
				this.calcul.situationCumulleeTTC = this.calcul.situationCumulleeTTC + this.calcul.prixTTC;
			}
		}
		// is facture cloture
		if (this.factureCloture === true) {
			restTtc = this.devisInfos.orderDetails.totalTTC;
			this.calcul.prixTTC = (this.devisInfos.orderDetails.totalTTC * ((pourcentage === null ? 0 : pourcentage) / 100));
		}

		// prixHT
		let restHt = 0;
		// if (factureSituationCount === 0 && !this.userForAcompt) {
		// 	restHt = this.devisInfos.orderDetails.totalHT;
		// 	this.calcul.prixHT = (restHt * ((pourcentage === null ? 0 : pourcentage) / 100)) - totalAcomptes.totalHT;
		// }
		// if (factureSituationCount === 0 && !this.userForAcompt) {
		// 	restHt = this.devisInfos.orderDetails.totalHT - (situationCumullee.cumulleeHt + totalAcomptes.totalHT);
		// 	this.calcul.prixHT = 0;
		// }

		// if (factureSituationCount !== 0 || this.userForAcompt) {
		// 	restHt = this.devisInfos.orderDetails.totalHT - (situationCumullee.cumulleeHt + totalAcomptes.totalHT);
		// 	this.calcul.prixHT = (this.devisInfos.orderDetails.totalHT * ((pourcentage === null ? 0 : pourcentage) / 100));
		// }
		restHt = this.devisInfos.orderDetails.totalHT - (situationCumullee.cumulleeHt + totalAcomptes.totalHT);
		this.calcul.prixHT = (this.devisInfos.orderDetails.totalHT * ((pourcentage === null ? 0 : pourcentage) / 100));
		if (this.factureCloture === true) {
			restHt = this.devisInfos.orderDetails.totalHT;
			this.calcul.prixHT = (restHt * ((pourcentage === null ? 0 : pourcentage) / 100)) - totalAcomptes.totalHT;
		}

		if (pourcentage !== null) {
			if (this.userForAcompt) {
				this.calcul.totalAcomptesHT = this.calcul.totalAcomptesHT + this.calcul.prixHT;
			} else {
				this.calcul.situationCumulleeHT = this.calcul.situationCumulleeHT + this.calcul.prixHT;
			}
		}

		// resteAPayerTTC
		if (this.factureCloture === true) {
			this.calcul.resteAPayerTTC = (this.devisInfos.orderDetails.totalTTC) - this.calcul.prixTTC;

		} else {
			this.calcul.resteAPayerTTC = (this.devisInfos.orderDetails.totalTTC -
				(situationCumullee.cumulleeTTC + totalAcomptes.totalTTC)) - this.calcul.prixTTC;

		}

		// resteAPayerHT
		if (this.factureCloture === true) {
			this.calcul.resteAPayerHT = this.devisInfos.orderDetails.totalHT - this.calcul.prixHT;

		} else {
			this.calcul.resteAPayerHT = (this.devisInfos.orderDetails.totalHT -
				(situationCumullee.cumulleeHt + totalAcomptes.totalHT)) - this.calcul.prixHT;

		}
		// montantHT
		this.calcul.montantHT = this.clalcMontantHt(this.calcul.prixHT, this.devisInfos.orderDetails.proportion);

		this.calcul.partProrata = this.clalcPartProrata(this.calcul.prixHT, this.devisInfos.orderDetails.proportion);

		this.factureSituation = this.devisInfos.situations.filter(X => X.typeInvoice === this.typeFacture.Situation);

		if (this.load === undefined) { return; }
		this.load.getDateToSave = this.getDateToSave.bind(this);
	}

	/**
	 * @description check DevisInfos Is Valid To Dispaly
	 */
	checkDevisInfosIsValidToDispaly(devis: Quote)/*: boolean*/ {
		const checkobjetEmpty: () => boolean = () => {
			let objetEmpty = false;
			// tslint:disable-next-line:forin
			for (const key in devis) {
				objetEmpty = true;
			}
			return objetEmpty;
		}
		const condition1 = devis !== undefined;
		const condition2 = checkobjetEmpty()
		return condition1 && condition2;
	}

	clalcMontantHt(prixHT: number, prorata: number): number {
		return prixHT - (prixHT * (prorata / 100));
	}

	clalcPartProrata(prixHT: number, prorata: number): number {
		return prixHT * (prorata / 100);
	}

	/**
	 * @description calculer le total des ancien 'factures en situation'
	 */
	getSituationsCumullee(factures: QuoteSituations[]): { cumulleeHt: number, cumulleeTTC: number } {

		let cumulleeHt = 0, cumulleeTTC = 0;

		//  récupérer le montant de chaque 'facture en situation'
		factures.forEach(facture => {
			if (facture.typeInvoice === this.typeFacture.Situation && facture.status !== StatutFacture.Brouillon &&
				facture.status !== StatutFacture.Annule) {
				cumulleeHt = cumulleeHt + facture.totalHT;
				cumulleeTTC = cumulleeTTC + facture.totalTTC;
			}
		});
		return { cumulleeHt, cumulleeTTC }
	}

	/**
	 * @description calculer le montant total des acomptes
	 */
	getTotalAcomptes(factures: QuoteSituations[]): { totalTTC: number, totalHT: number } {
		//  validation
		if (factures === undefined || factures.length === 0) {
			return { totalTTC: 0, totalHT: 0 };
		}
		let totalTTC = 0, totalHT = 0;

		factures.forEach(facture => {
			if (facture.typeInvoice === this.typeFacture.Acompte && facture.status !== StatutFacture.Brouillon
				&& facture.status !== StatutFacture.Annule) {
				totalTTC = totalTTC + facture.totalTTC;
				totalHT = totalHT + facture.totalHT;
			}
		});
		return { totalTTC, totalHT };
	}

	/**
	 * @description return object of data to save
	 */
	getDateToSave(status: StatutFacture, callBack: any): void {
		if (status === this.statutFacture.Cloture) {
			this.initData(100);
		}
		const pourcentage = status === this.statutFacture.Cloture ? 100 : this.pourcentage;
		const prestations = [];
		this.groupeTVA.forEach(element => {
			prestations.push({
				'product': {
					discount: {
						'value': 0,
						'type': TypeValue.Amount,
					},
					'description': '',

					'designation':
						`Facture  ${this.userForAcompt ? 'd\'acompte' : 'situation'} d'avancement de  ${pourcentage}% sur le devis ${this.devisInfos.reference} de tva ${element.tva}  %`,

					'lotProduits': null,
					'name': `Facture ${this.userForAcompt ? 'd\'acompte' : 'situation'} d'avancement de ${pourcentage}% sur le devis ${this.devisInfos.reference} de tva ${element.tva} %`,
					'totalHT': element.totalHT,
					'cout_vente': 0,
					'materialCost': 0,
					'hourlyCost': 0,
					'prixHt': element.totalHT,
					'vat': element.tva,
					'totalTTC': element.totalTTC,
					'categorieId': 10,
					'quantity': 1,
					'qte': 1
				},
				'quantity': 1,
				'type': 1,
				'remise': 0
			});
		});
		callBack({
			prestations: prestations,
			puc: this.devisInfos.orderDetails.puc,
			prorata: this.devisInfos.orderDetails.proportion,
			tva: this.calcul.tva,
			retenueGarantie: this.retenueGarantie === 1 ? this.retenueGarantieValue : 0,
			delaiGarantie: this.delaiGarantie,
			totalHt: this.factureCloture ? this.calcultHt() : this.calcul.prixHT,
			totalTtc: this.factureCloture ? this.calculTTC() : this.calcul.prixTTC,
			resteAPayerTTC: this.calcul.resteAPayerTTC,
			resteAPayerHT: this.calcul.resteAPayerHT,
			situationCumulleeTTC: this.calcul.situationCumulleeTTC,
			situationCumulleeHT: this.calcul.situationCumulleeHT,
			acomptesCumulleeHT: this.calcul.totalAcomptesHT,
			acomptesCumulleeTTC: this.calcul.totalAcomptesTTC
		});
	}
	getCouteVenteFromParamerage() {

		this.service.getAll(ApiUrl.configDocument).subscribe(res => {

			// tslint:disable-next-line:no-shadowed-variable
			const parametrage = JSON.parse(res).find(element => element.documentType === TypeNumerotation.devis);

			if (this.devisInfos.orderDetails.holdbackDetails && this.devisInfos.orderDetails.holdbackDetails.holdback !== undefined) {


			}
			if (this.devisInfos.orderDetails.holdbackDetails === undefined) {
				this.retenueGarantieValue = parametrage.retenueGarantie;

			} else {
				this.retenueGarantieValue = parametrage.retenueGarantie;

			}
		});
	}

	CalculRetenue(velueRetenue) {
		const calcul = this.calcul.prixHT * (velueRetenue / 100);
		return calcul === (undefined || null) ? 0 : calcul;
	}
}
