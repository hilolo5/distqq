import { Component, OnInit, OnChanges, Input, Inject, AfterViewInit, AfterViewChecked, SimpleChanges } from '@angular/core';
import { ICalcule } from 'app/calcule/ICalcule';
import { Calcule } from 'app/calcule/Calcule';
import { CalculTva } from 'app/Models/Model/calcul-tva';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { DelaiGaranties } from 'app/Enums/DelaiGaranties.Enum';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SelectLotComponent } from './select-lot/select-lot.component';
import { SelectProduitComponent } from './select-produit/select-produit.component';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ChangeDetectorRef } from '@angular/core';
import { QuoteSituations } from 'app/Models/Entities/Documents/Quote';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ColumnPrestation } from '../../Enums/Prestation/ColumnPrestation';
import { AdddLigneComponent } from 'app/components/add-ligne/add-ligne.component';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { DialogHelper } from 'app/libraries/dialog';
import { ProduitFormComponent } from './produit-form/produit-form.component';

class ArticleType {
	produit = 1;
	lot = 2;
	ligne = 3;
}

declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'table-article',
	templateUrl: './table-article.component.html',
	styleUrls: ['./table-article.component.scss'],
})
export class TableArticleComponent implements OnInit, OnChanges, AfterViewInit {

	@Input('checkTypePres') checkTypePres;
	@Input('docType') docType;
	// tslint:disable-next-line:no-input-rename
	@Input('factureCloture') factureCloture = false;
	@Input('isCloture') isCloture = false;
	@Input('document') document;
	// tslint:disable-next-line:no-input-rename
	@Input('ListFactureInDevis') ListFactureInDevis: QuoteSituations[] = [];
	// tslint:disable-next-line:no-input-rename
	@Input('load') load: { getDateToSave };
	// tslint:disable-next-line:no-input-rename
	@Input('showPrices') showPrices = true;
	// tslint:disable-next-line:no-input-rename
	@Input('displaysGeneralCalculation') displaysGeneralCalculation = true;
	// tslint:disable-next-line:no-input-rename
	@Input('showAdditionalInfos') showAdditionalInfos = true;
	// tslint:disable-next-line:no-input-rename
	@Input('negativeMode') negativeMode = false;
	// tslint:disable-next-line:no-input-rename
	@Input('puc') puc = 0;
	// tslint:disable-next-line:no-input-rename
	@Input('partProrata') prorata = 0;
	// tslint:disable-next-line:no-input-rename
	@Input('remiseGloabl') remiseGloabl = 0;
	// tslint:disable-next-line:no-input-rename
	@Input('retenueGarantieValue') retenueGarantieValueParam = 0;
	// tslint:disable-next-line:no-input-rename
	@Input('delaiGarantie') delaiGarantie: DelaiGaranties = 12;
	// tslint:disable-next-line:no-input-rename
	@Input('FournisseurList') FournisseurList: Supplier[] = null;
	// tslint:disable-next-line:no-input-rename
	@Input('typeRemiseGloabl') typeRemiseGloabl = TypeValue.Amount;
	// tslint:disable-next-line:no-input-rename
	@Input('tvaGlobal') tvaGlobal = null;
	// tslint:disable-next-line:no-input-rename
	@Input('readOnly') readOnly = true;
	// tslint:disable-next-line:no-input-rename
	@Input('isAvoir') isAvoir = false;
	// tslint:disable-next-line:no-input-rename
	@Input('isIntervention') isIntervention = false;
	// tslint:disable-next-line:no-input-rename
	@Input('articles') articles: {
		product: any;
		quantity: number;
		type: number;
		remise: number;
		idPres: string
	}[] = [];
	@Input('devis') devisInitiale = null;


	proarataVisible = false;
	pucVisible = false;
	garantiVisible = false;
	remiseGlobaleVisible = false;


	// hasParticipation: boolean;

	// hasGuarante: boolean;

	articlesdata: {
		product: any;
		quantity: number;
		type: number;
		remise: number;
	}[] = [];

	dragging = false;
	detailShown = false;
	detailObj = {};

	produits = [];
	produitLot = [];
	lotsProduitsTmp: { product: any; quantity: number; type: number; remise: number }[] = [];
	list = [];
	loadingFinished = false; // si la liste chargé
	search = ''; // zone de recherche
	totalPage = 1; // total des pages
	page = 1; // Current page des
	articleType: ArticleType = new ArticleType();
	prod;
	produirres: any;
	res: { id: number; nom: string; produits: any[] };
	collapseClosed: number[] = [];
	AllCollapsesIsClosed = true;
	lotP = [];
	pres: any;
	pres2: any;
	ProduitinLot: any;
	Lot: number;
	cachedIdlot = 0;
	emitter: any = {};
	idLotUsedForAddProduit: number = null;
	lastUpdatedProduit: number = null;
	montantHT = 0;
	totalGeneral = 0;
	globalTotalHTRemise = 0;
	globalTotalTTC = 0;
	MontantHt = 0;
	calcule: ICalcule = new Calcule();
	calculTvas: CalculTva[] = [];
	TotalTva = 0;
	totalTTC = 0;
	montantTva = 0;
	NouveauTotalGeneral = 0;
	listFactureSituation: QuoteSituations[] = []
	listFactureAcompte: QuoteSituations[] = [];
	articleInLot: any;
	indexLotModified = null;
	indexProduitInLotModified = null;
	articleInLotn: {
		product: any;
		quantity: number;
		type: number;
		remise: number;
	};
	listProduitInlOT: any;
	totalTTcSotuation = 0;
	totalHTSituation = 0;
	totalTTcAcompte = 0;
	totalHTAcompte = 0;
	labels: any;
	newLots = '';
	newLigne = '';
	newLigneDesignation = '';
	newLigneDescription = '';
	columns = [];
	ids = ['parent'];
	restTtcDevis = 0;
	restHtDevis = 0;
	totalDevisTTc;
	totalDevisHt;
	referenceDevis = null;
	retenueGarantieValue;
	ShowRemise = false
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {
		this.getShowRemise()
		this.getCouteVenteFromParamerage();
		window.addEventListener('resize', () => this.alignTables());
	}

	ngAfterViewInit(): void {
		this.alignTables();
	}


	addProrata() {
		this.proarataVisible = !this.proarataVisible;
	}

	addRemiseGlobal() {
		this.remiseGlobaleVisible = !this.remiseGlobaleVisible;
	}

	addParticipationPuc() {
		this.pucVisible = !this.pucVisible;
	}


	addRetenuGaranti() {
		this.garantiVisible = !this.garantiVisible;
	}


	isValid(data) { return data && data !== null }

	LoadListLots() {
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '1500px';
		dialogLotConfig.height = '500px';
		dialogLotConfig.data = { isAvoir: this.isAvoir };
		const dialogRef = this.dialog.open(SelectLotComponent, dialogLotConfig);

		dialogRef.afterClosed().subscribe((data) => {

			if (data && Array.from(data).length) {
				data = data
				data.forEach(lot => {
					lot.product.products.forEach(p => {
						p.quantity = p.quantity * lot.quantity;
						p.productDetails.discount = this.SetRemise(p.productDetails);
						p.productDetails.discount.type = (p.productDetails.discount === undefined) ? TypeValue.Amount : p.productDetails.discount.type;
						p.productDetails.totalHT = this.PrixHt(p.productDetails);
						p['product'] = p.productDetails;
					});
					lot.idPres = this.generateIdItemPrestation();
					this.ids.push(lot.idPres);
					this.articles.push(lot);
				});
			}
			this.getShowRemise();
			this.alignTables();
		});
	}
	totalTtcAcompteSituation = 0;
	getListFacture(facture: QuoteSituations[]) {
		if (facture) {
			let ht = 0;
			let ttc = 0;
			this.listFactureSituation = facture.filter(x => x.typeInvoice === TypeFacture.Situation && x.status != StatutFacture.Annule)
			ttc = this.listFactureSituation.reduce((x, y) => x + y.totalTTC, 0);
			ht = this.listFactureSituation.reduce((x, y) => x + y.totalHT, 0);
			this.listFactureAcompte = facture.filter(x => x.typeInvoice === TypeFacture.Acompte && x.status != StatutFacture.Annule)

			ttc += this.listFactureAcompte.reduce((x, y) => x + y.totalTTC, 0);
			ht += this.listFactureAcompte.reduce((x, y) => x + y.totalHT, 0);
			this.restHtDevis = this.devisInitiale.orderDetails.totalHT - ht;
			this.restTtcDevis = this.devisInitiale.orderDetails.totalTTC - ttc;
			this.totalTtcAcompteSituation += ttc;

			if (this.factureCloture) {
				this.listFactureSituation.forEach(situation => {
					this.totalTTcSotuation += situation.totalTTC,
						this.totalHTSituation += situation.totalHT
				});
				this.listFactureAcompte.forEach(acompte => {
					this.totalTTcAcompte += acompte.totalTTC,
						this.totalHTAcompte += acompte.totalHT
				});
			}
		}
	}
	getShowRemise() {
		if (this.articles.length === 0) {
			this.ShowRemise = false;
		} else {
			const remiseInProduit = this.articles.filter(x => x.type === this.articleType.produit && x.product.discount.value > 0).length;
			const lot = this.articles.filter(x => x.type === this.articleType.lot).map(x => x.product.products);
			let remiseInLot = 0;
			lot.forEach(element => {
				remiseInLot = element.filter(x => x.product.discount.value > 0).length;
			});
			const res = remiseInProduit + remiseInLot
			this.ShowRemise = res > 0 ? true : false;
		}
		this.columns = ColumnPrestation.getColumns(this.showPrices, this.ShowRemise);
	}
	getColspan() {
		if (this.readOnly && this.ShowRemise) {
			return 10
		}
		if (this.readOnly && !this.ShowRemise) {
			return 9
		}
		if (!this.readOnly && !this.ShowRemise) {
			return 11
		}
		if (!this.readOnly && this.ShowRemise) {
			return 12
		}
	}
	ngOnChanges(changes: SimpleChanges) {
		this.getShowRemise();
		if (this.devisInitiale && this.devisInitiale != null) {
			this.totalDevisTTc = this.devisInitiale.orderDetails.totalTTC;
			this.totalDevisHt = this.devisInitiale.orderDetails.totalHT;
			this.referenceDevis = this.devisInitiale.reference;
		}

		if (this.document && this.document !== null) {

			this.retenueGarantieValue = this.retenueGarantieValueParam;


			this.retenueGarantieValue = this.isValid(this.document.orderDetails.holdbackDetails) ? this.document.orderDetails.holdbackDetails.holdback : this.retenueGarantieValueParam;

			this.puc = this.isValid(this.document.orderDetails.puc) ? this.document.orderDetails.puc : 0;
			this.remiseGloabl = this.isValid(this.document.orderDetails.globalDiscount) ? this.document.orderDetails.globalDiscount.value : 0;
			this.typeRemiseGloabl = this.isValid(this.document.orderDetails.globalDiscount) ? this.document.orderDetails.globalDiscount.type : TypeValue.Amount;
			this.delaiGarantie = this.isValid(this.document.orderDetails.holdbackDetails) ? this.document.orderDetails.holdbackDetails.warrantyPeriod : 12;
			this.prorata = this.isValid(this.document.orderDetails.proportion) ? this.document.orderDetails.proportion : 0;
			this.tvaGlobal = this.isValid(this.document.orderDetails.globalVAT_Value) && this.document.orderDetails.globalVAT_Value !== -1 ? this.document.orderDetails.globalVAT_Value : null;
			this.pucVisible = (this.document['orderDetails']['puc'] > 0);
			this.proarataVisible = (this.document['orderDetails']['proportion'] > 0);
			this.garantiVisible = (this.document['orderDetails']['holdbackDetails'] !== null &&
				this.document['orderDetails']['holdbackDetails']['holdback'] > 0);
			this.remiseGlobaleVisible = this.remiseGloabl > 0;
		}
		if (this.factureCloture || this.isCloture) {
			this.getListFacture(this.ListFactureInDevis)
		}
		// this.garantiVisible = (this.retenueGarantieValue == null || this.retenueGarantieValue == undefined ||
		// 	+this.retenueGarantieValue === 0) ? false : true;
		if (this.load === undefined) { return; }
		this.load.getDateToSave = this.getDateToSave.bind(this);

		if ('articles' in changes) {
			this.ids = ['parent'];

			this.articles.forEach(e => {
				const idPres = this.generateIdItemPrestation();

				e.idPres = idPres;
				this.ids.push(idPres);
			});

			this.alignTables();
		}
	}

	setInCollapseClosed(index) {
		if (this.collapseClosed.filter(id => id === index).length === 0) {
			this.collapseClosed.push(index);
		} else {
			this.collapseClosed = this.collapseClosed.filter(id => id !== index);
		}
	}

	checkCollapseClosed(index) {
		return this.collapseClosed.filter(id => id === index).length === 0;
	}

	setAllCollapsesClosed() {
		this.collapseClosed = [];
		this.AllCollapsesIsClosed = false;
		this.articles.forEach((article, index) => {
			if (article.type === this.articleType.lot) {
				this.collapseClosed.push(index);
			}
		});
	}


	addProduit(article?) {
		DialogHelper.openDialog(
			this.dialog,
			ProduitFormComponent,
			DialogHelper.SIZE_LARGE,
			{ isIntervention: this.isIntervention, reload: this.emitter, article: article }
		).subscribe(async data => {
			if (data) {
				this.insertNewProduit(data);
				this.getShowRemise();
			}
		});
	}


	CalculTotalHTLOT(LotArticls) {
		const totalHtLot = LotArticls.map(article => {
			return this.TotalHt(article.product, article.quantity)
		})
		let totalHt = 0;
		if (totalHtLot.length === 0) {
			totalHt = 0;
		} else {
			totalHt = totalHtLot.reduce((total, num) => {
				return total + num;
			});
		}
		return totalHt;
	}

	CalculTotalTtcLOT(LotArticls) {
		const totalTtcLot = LotArticls.map(article => {
			if ((this.tvaGlobal === null || this.tvaGlobal === -1) && article.product !== null) {
				return this.prixTtc(article.product, article.quantity, article.product.vat)
			} else {
				return this.prixTtc(article.product, article.quantity, this.tvaGlobal)
			}
		})
		if (totalTtcLot.length === 0) {
			return 0
		} else {
			return totalTtcLot.reduce((total, num) => {
				return total + num;
			});
		}

	}

	cout_horaire(nomber_heure, cout_vente) {
		return nomber_heure * cout_vente;
	}

	calculate_cout_horaire(Nomber_heure, Cout_vente): any {
		return parseFloat(Nomber_heure) * parseFloat(Cout_vente);
	}

	prixTtc(article, qte, tva) {
		const tvaEnPr = 1 + parseFloat(tva) / 100;
		return this.TotalHt(article, qte) * tvaEnPr;
	}


	TotalHt(article, qte) {
		const totalHt = this.PrixHt(article);
		if (+totalHt === 0) {
			return +article.totalHT;
		}
		let remise = 0;
		if (article.discount !== null && article.discount.value !== 0 && article.discount.value !== undefined) {
			if (article.discount.type === TypeValue.Amount) {
				remise = article.discount.value;
			} else {
				remise = totalHt * (article.discount.value / 100);
			}
		}
		const res = qte !== undefined ? qte * (totalHt - remise) : (totalHt - remise);
		const result = parseFloat(res.toFixed(10));
		return isNaN(result) ? 0 : result;
	}

	PrixHt(article) {
		return (this.calculate_cout_horaire(article.totalHours, article.hourlyCost) + article.materialCost);
	}


	LoadListProduit() {
		const dialogProduitConfig = new MatDialogConfig();
		dialogProduitConfig.width = '1500px';
		dialogProduitConfig.height = '500px';
		dialogProduitConfig.data = { isAvoir: this.isAvoir, isIntervention: this.isIntervention };
		const dialogRef = this.dialog.open(SelectProduitComponent, dialogProduitConfig);

		dialogRef.afterClosed().subscribe((data) => {
			if (data && Array.from(data).length) {
				data.forEach(produit => {

					produit.product.totalHT = this.PrixHt(produit.product);
					produit.product.discount = this.SetRemise(produit.product);
					if (this.idLotUsedForAddProduit === null) {
						// produit.idPres = this.generateIdItemPrestation();
						this.articles.push(produit);
					} else {

						this.articles.map(article => {
							if (article.product.id === this.idLotUsedForAddProduit) {
								const lotProduits = {
									productId: produit.product.id,
									quantity: produit.quantity,
									product: produit.product,
									type: produit.type
								};

								if (!article.product.products || !Array.isArray(article.product.products)) {
									article.product.products = [];
								}

								article.product.products.push(lotProduits);

								return;
							}
						});
					}
				});
			}
			this.getShowRemise();


			this.alignTables();
		});
	}


	SetRemise(product) {
		const discount = {
			'type': TypeValue.Amount,
			'value': 0
		};
		if (product.discount !== undefined && product.discount !== null) {
			return product.discount;
		} else {
			return discount;
		}
	}

	createProduitForm(article) {

		if (this.idLotUsedForAddProduit != null) {
			article = {
				product: article == null ? null : article.data,
				quantity: article == null ? null : article.quantity,
				remise: article == null ? null : article.discount.value,
				type: null,
			};
		}
		return article;
	}

	createProduitFormLot(article, qte) {

		this.articleInLot = {
			product: article == null ? null : article,
			quantity: article == null ? null : qte,
			remise: article == null ? null : article.discount.value === undefined ? 0 : article.discount.value,
			type: null,
		};
		return this.articleInLot;
	}

	reloadPrixParFournisseur() {
		this.emitter.reloadPrixParFournisseur();
	}



	async insertNewProduit(produit) {
		if (this.isAvoir && produit.product !== null) {
			produit.product.totalHT = produit.product.totalHT * (-1)
			produit.product.materialCost = produit.product.materialCost * (-1)
			produit.product.hourlyCost = produit.product.hourlyCost * (-1);
		}

		if (this.idLotUsedForAddProduit === null) {
			if (this.lastUpdatedProduit === null) {
				this.articles.push(produit);
				this.alignTables();
				return;
			}
			if (this.lastUpdatedProduit !== null) {
				this.articles[this.lastUpdatedProduit] = produit;
				return;
			}
		}
		if (this.idLotUsedForAddProduit !== null) {

			this.articles.map(article => {
				if (article.product.id === this.idLotUsedForAddProduit) {
					const lotProduits = {
						productId: produit.product.id,
						quantity: produit.quantity,
						product: produit.product,
					};

					if (this.lastUpdatedProduit === null) {
						article.product.products.push(lotProduits);
						return;
					}
					if (this.lastUpdatedProduit !== null) {
						article.product.products[this.lastUpdatedProduit] = lotProduits;
						return;
					}
				}
			});
		}
	}

	updateLot(index, event) {
		this.articles[index].product.name = event.target.value
	}

	clalcTotalGeneral(): number {
		const articles = this.getProductsOfArticles(this.articles);
		let totalHt = 0;
		if (!this.factureCloture && !this.isCloture) {
			articles.forEach(article => {
				totalHt = totalHt + this.TotalHt(article, article.quantity);
			});
		}
		if (this.isCloture) {
			totalHt = this.document.orderDetails.totalHT;
		}

		if (this.factureCloture) {
			totalHt = this.restHtDevis;
		}

		this.MontantHt = totalHt;
		return totalHt;
	}

	clalcNouveauTotalGeneral() {
		if (Number(this.typeRemiseGloabl) === TypeValue.Amount) {
			this.NouveauTotalGeneral = this.clalcTotalGeneral() - this.remiseGloabl;
		} else if (Number(this.typeRemiseGloabl) === TypeValue.Percentage) {
			this.NouveauTotalGeneral = this.clalcTotalGeneral() - (this.remiseGloabl / 100) * this.clalcTotalGeneral();
		}
		return this.NouveauTotalGeneral;
	}

	clalcMontantHt() {
		const prorataEnPourcentage = this.prorata / 100 + 1;
		const TotalGeneral =
			this.clalcNouveauTotalGeneral() !== 0 ? this.clalcNouveauTotalGeneral() : this.clalcTotalGeneral();
		this.MontantHt = (TotalGeneral * 1) / prorataEnPourcentage;
		return this.MontantHt;
	}

	clalcPartProrata() {
		const TotalGeneral =
			this.clalcNouveauTotalGeneral() !== 0 ? this.clalcNouveauTotalGeneral() : this.clalcTotalGeneral();
		return TotalGeneral - this.MontantHt;
	}

	totalHtRemise(): number {
		this.globalTotalHTRemise = this.clalcTotalGeneral() - this.remiseGloabl;
		return this.globalTotalHTRemise;
	}

	getProductsOfArticles(articles) {
		const articlesTmp = [];
		articles.forEach(article => {

			if (article.type === this.articleType.produit) {
				article.product.quantity = article.quantity;
				articlesTmp.push(article.product);
			}

			if (article.type === this.articleType.lot) {
				article.product.products.map(e => {
					e.product.quantity = e.quantity;
					articlesTmp.push(e.product);
				});
			}
		});
		return articlesTmp;
	}


	groupTVA(): CalculTva[] {
		this.calculTvas = [];
		if (this.tvaGlobal === null || this.tvaGlobal === -1) {
			const articles = this.getProductsOfArticles(this.articles);
			this.calculTvas = this.calcule.calculVentilationRemise(
				articles,
				this.clalcTotalGeneral(),
				this.remiseGloabl,
				this.typeRemiseGloabl
			);
		} else {
			let totalTTC = 0;
			if (this.factureCloture) {
				totalTTC = this.restTtcDevis;
			}
			if (this.isCloture) {
				totalTTC = this.document.orderDetails.totalTTC;
			} else {
				totalTTC = this.clalcTotalGeneral() * (this.tvaGlobal / 100 + 1);
			}
			this.calculTvas.push({
				tva: this.tvaGlobal,
				totalHT: this.clalcTotalGeneral(),
				totalTTC,
				totalTVA: totalTTC - this.clalcTotalGeneral(),
				finalValue: 0,
				percente: 0,
				value: 0,
			});
		}
		return this.calculTvas;
	}

	calcTotalTva(): { totalTva: number; totalTTC: number; montantTva: number } {
		try {
			const groupTVA = this.groupTVA()
			const montantTva = groupTVA.reduce((x, y) => x + y.totalTVA, 0);
			const total = this.NouveauTotalGeneral === 0 ? this.clalcTotalGeneral() : this.NouveauTotalGeneral;
			const TotalTva = (montantTva / total) * 100;
			const totalTTC = total + montantTva;
			return { totalTva: TotalTva, totalTTC: totalTTC, montantTva: montantTva };
		} catch (err) {
			console.log(err)
		}
	}

	calcParticipationPuc() {
		const puc = parseFloat((this.puc / 100).toFixed(10));
		return puc * parseFloat(this.MontantHt.toFixed(10));
	}

	calcTotalGeneralTtc() {
		if (this.factureCloture) {
			return this.restTtcDevis
		}
		if (this.isCloture) {
			return this.document.orderDetails.totalTTC
		} else {
			return this.calcTotalTva().totalTTC;
		}
	}

	calculRestApayeGenerale() {
		const calculRest = this.calcTotalTva().totalTTC - (this.totalTTcAcompte + this.totalTTcSotuation)
		return calculRest;
	}

	calcMontantHT(totalHt, prorata) {
		prorata = prorata / 100 + 1;
		this.montantHT = (totalHt * 1) / prorata;
		return this.montantHT;
	}

	getDateToSave(callBack) {
		this.articles.map(x => x.product['categorieId'] === undefined && x.product.category !== null && x.product.category !== undefined ?
			x.product['categorieId'] = x.product['categorieId'] : null);
		const calcTotalTva = this.calcTotalTva();
		callBack({
			prestation: this.articles,
			totalHt: this.factureCloture ? this.restHtDevis : this.MontantHt,
			totalTtc: this.factureCloture ? this.restTtcDevis : calcTotalTva.totalTTC,
			tva: JSON.stringify(this.groupTVA()),
			remise: this.remiseGloabl,
			typeRemise: this.typeRemiseGloabl,
			prorata: this.proarataVisible === true ? this.prorata : 0,
			puc: this.pucVisible === true ? this.puc : 0,
			tvaGlobal: this.tvaGlobal == null ? -1 : this.tvaGlobal,
			retenueGarantie: this.garantiVisible === true ? this.retenueGarantieValue : 0,
			delaiGarantie: this.delaiGarantie,
			totalTVA: calcTotalTva.totalTva,
		});

	}

	removeArticle(i, ii) {
		if (ii == null) {
			this.articles.splice(i, 1);
		} else {
			this.articles[i].product.products.splice(ii, 1);
		}
		this.getShowRemise();
	}

	getCouteVenteFromParamerage() {
		// this.parameteresService.Get(TypeParametrage.parametrageDevis).subscribe(res => {
		// 	const parametrage = JSON.parse(res.contenu);
		// 	if (!this.readOnly && !this.retenueGarantie) {
		// 		this.retenueGarantieValue = parametrage.retenueGarantie;
		// 	}
		// });
	}

	addNewLots() {
		const data = {
			name: this.newLots,
			description: '',
			products: [],
			id: 0,
		};
		const idPres = this.generateIdItemPrestation();
		this.articles.push({
			product: data,
			quantity: 0,
			remise: 0,
			type: this.articleType.lot,
			idPres: idPres
		});
		this.ids.push(idPres);
		this.alignTables();
		jQuery('#addLot').modal('hide');
	}

	annulerTVAGlobal() {
		this.tvaGlobal = null;
	}

	setTvaArticle(articleIndex, event) {
		try {
			if (event.target.value === '') {
				this.articles[articleIndex].product.vat = 0
			} else {
				this.articles[articleIndex].product.vat = + event.target.value;
			}
		} catch (ex) {
			console.log(ex)
		}
	}


	AddLigne() {
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '650px';
		dialogLotConfig.height = 'auto';
		dialogLotConfig.data = {};
		const dialogRef = this.dialog.open(AdddLigneComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((res) => {
			if (res) {
				const data = {
					description: res.description,
					designation: res.designation,
					id: AppSettings.guid(),
				};
				this.articles.push({
					product: data,
					quantity: 0,
					remise: 0,
					type: this.articleType.ligne,
					idPres: this.generateIdItemPrestation()
				});
				this.alignTables();
			}

		});
	}

	setQuantiteArticle(product, event) {
		try {
			if (event.target.value === '') {
			} else {
				product.quantity = parseFloat(event.target.value);
			}
		} catch (ex) {
			console.log(ex)
		}
	}

	settotalHoursArticle(articleIndex, event) {
		try {
			if (event.target.value === '') {
				this.articles[articleIndex].product.totalHours = 0
			} else {
				this.articles[articleIndex].product.totalHours = parseFloat(event.target.value);
				this.PrixHt(this.articles[articleIndex]);
			}
			this.articles[articleIndex].product.totalHT = this.PrixHt(this.articles[articleIndex].product);
			this.clalcTotalGeneral();
		} catch (ex) {
			console.log(ex)
		}
	}

	setTvaArticleLot(lotIndex, articleIndex, event) {
		try {
			if (event.target.value === '') {
				this.articles[lotIndex].product.products[articleIndex].vat = 0
			} else {
				this.articles[lotIndex].product.products[articleIndex].product.vat = + event.target.value;
			}
		} catch (ex) {
			console.log(ex)
		}
	}

	onDragStarted() {
		this.dragging = true;
	}

	onDragEnded() {
		this.dragging = false;
	}

	onHover(e: any, prest: any) {
		if (this.readOnly) {
			this.detailShown = true;

			setTimeout(() => {
				const ele = e.fromElement;
				const detail = document.getElementById('prest-detail');

				if (ele && ele.getClientRects().item(0) && detail) {
					this.detailObj = {
						description: prest.product.description.replace(/<[^>]*>/g, ''),
						category: prest.product.category,
						hours: prest.product.totalHours || 0,
						tva: prest.product.vat || 0,
						materialCost: prest.product.materialCost || 0,
						totalHT: prest.product.totalHT || 0
					};

					detail.style.setProperty('top', `${ele.getClientRects().item(0).top}px`, 'important');
					detail.style.setProperty('left', `${ele.getClientRects().item(0).left}px`, 'important');
				}
			}, 0);
		}
	}

	onLeave() {
		this.detailShown = false;
	}

	/** Function Drag&Drop */
	drop(event: CdkDragDrop<string[]>) {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			const itemType = event.item.element.nativeElement['dataset']['type'];

			if (itemType !== 'lot') {
				transferArrayItem(event.previousContainer.data,
					event.container.data,
					event.previousIndex,
					event.currentIndex);
			}
		}
	}

	generateIdItemPrestation() {
		return Math.floor((1 + Math.random()) * 0x10000).toString();
	}

	getConnectedIds(id: string) {
		return this.ids.slice(0).filter((_id) => _id !== id);
		// return id === 'parent'
		// 	? this.ids.slice(0).filter((_id) => _id !== id)
		// 	: true ? id : this.ids.slice(0).filter((_id) => _id !== id);
	}

	/**
	 * Aligns the nested tables with the top header
	 */
	alignTables(): void {
		setTimeout(() => {
			$('.nested-cell').each((index, cell) => {
				const id = $(cell).attr('data-header');
				const width = document.getElementById(id).getClientRects().item(0).width;

				$(cell)[0].style.setProperty('width', `${width}px`, 'important');
			});
		}, 0);
	}
}
