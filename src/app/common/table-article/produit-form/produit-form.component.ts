import { Component, OnInit, Input, Output, EventEmitter, OnChanges, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { ArticleType } from 'app/Models/Entities/Commun/article-type';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { conversion } from 'app/common/prix-conversion/conversion';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { PlanComptableModel } from 'app/Models/Entities/Parametres/PlanComptableModel';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { ChartAccountCategoryType } from 'app/Enums/Parameters/ChartAccountCategoryType.enums';
import { DefaultValue } from '../../../Models/Entities/Parametres/DefaultValue';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var toastr: any;
declare var swal: any;
@Component({
	selector: 'form-produit',
	templateUrl: './produit-form.component.html',
	styleUrls: ['./produit-form.component.scss'],
})
export class ProduitFormComponent implements OnInit, OnChanges {
	reload: { createForm, reloadPrixParFournisseur };
	isIntervention = false;
	category;
	public ListeTva = [];
	public ListeUnite = [];
	public ListeFournisseurs = [];
	public ListeCategorie;
	public PrixParFournisseur = [];
	public form = null;
	public conversion = new conversion();
	public addInDb = true;
	public produit = null;
	public articleType: ArticleType = new ArticleType();
	public typeRemiseGloabl = TypeValue.Amount;
	public usedForAdd = false;
	public showBtnAddProduit = true;
	public loadData: any = {};
	public processing = false;
	globalCategorie = [];
	labels = [];
	public editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '10rem',
		placeholder: 'Description',
	};
	public initialisation = {
		unite: false,
		categorie: false,
		tva: false,
		fournisseur: false
	};
	chartAccountCategoryType: typeof ChartAccountCategoryType = ChartAccountCategoryType;
	idProduitsSave = 0;
	typeProduct = [];

	constructor(
		public dialogRef: MatDialogRef<ProduitFormComponent>,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		@Inject(MAT_DIALOG_DATA) public data: { isIntervention: any, article, reload: { createForm, reloadPrixParFournisseur } },
		private fb?: FormBuilder,
		private translate?: TranslateService
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getTypeProduct();
		this.getListeCategorie();
		this.getListeUnite();
		this.createForm(this.data.article);

		if (!this.data.article) {
			this.getCouteVenteFromParamerage();
		}
	}

	ngOnChanges() {
		this.reload = this.data.reload;
		// this.reload.createForm = this.createForm.bind(this);
		this.reload.reloadPrixParFournisseur = this.reloadPrixParFournisseur.bind(this);
	}

	reloadPrixParFournisseur() {
		this.loadData.return('reload', (response) => { });
	}

	getTypeProduct() {
		this.service.getAll(ApiUrl.typeProduct).subscribe(typeProduct => {
			this.typeProduct = typeProduct.value;
			if (this.data.article && this.data.article.product.productCategoryType) {
				this.form.controls['productCategoryType'].setValue(this.typeProduct.find(x => +x.id === +this.data.article.product.productCategoryType.id));
			}
		});
	}

	close() {
		this.dialogRef.close();
	}

	createForm(article: { product: any; quantity: number; type: number; remise: number } | any) {
		this.showBtnAddProduit = true;
		this.addInDb = true;
		if (article == null || article.product == null) {
			this.form = this.fb.group({
				reference: [null, [Validators.required], this.CheckUniqueReference.bind(this)],
				description: [''],
				designation: ['', [Validators.minLength(2), Validators.required]],
				totalHours: [0],
				materialCost: [0],
				hourlyCost: [0],
				vat: [0],
				unite: [null],
				categoryId: [null, [Validators.required]],
				quantity: [0],
				remise: [0],
				labels: [[]],
				productCategoryType: []

			});

			this.usedForAdd = true;
			this.PrixParFournisseur = [];
		}

		if (article != null && article.product != null) {
			this.showBtnAddProduit = false;
			this.addInDb = false
			this.form = this.fb.group({
				reference: [
					article.product.reference,
					[Validators.required],
					this.CheckUniqueReference.bind(this),
				],
				description: [article.product.description],
				designation: [article.product.designation, [Validators.minLength(2), Validators.required]],
				totalHours: [article.product.totalHours],
				materialCost: [article.product.materialCost],
				hourlyCost: [article.product.hourlyCost],
				vat: [article.product.vat],
				unite: [article.product.unite],
				categoryId: [this.getCategorie(article.product)],
				quantity: [article.quantity],
				remise: [article.product.discount.value],
				labels: [article.product.labels],
				productCategoryType: [this.typeProduct.find(x => +x.id === +article.product.productCategoryType.id)]
			});
			this.PrixParFournisseur = article.product.productSuppliers;
			this.produit = article.product;
			this.typeRemiseGloabl = article.product.discount.type;
			this.usedForAdd = false;
		}
	}

	getCategorie(data) {
		if (data.category !== undefined && data.category !== null) {
			return data.category.id;
		}
		else if (data.categoryId !== undefined) {
			return data.categoryId;
		} else {
			return '';
		}
	}

	get f() {
		return this.form.controls;
	}

	CheckUniqueReference(control: FormControl): Promise<{}> {
		const promise = new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Produit + ApiUrl.CheckReference, control.value).subscribe(res => {
				if (
					control.value !== '' &&
					(this.produit == null ? true : control.value !== this.produit.reference)
				) {
					if (!res) {
						resolve({ CheckUniqueReference: true });
					} else {
						resolve(null);
					}
				} else {
					resolve(null);
				}
			});
		});
		return promise;
	}

	getListeUnite(): void {
		if (!this.initialisation.unite) {
			this.service.getAll(ApiUrl.configurationUnit).subscribe(Unite => {
				this.ListeUnite = Unite.value;
				this.initialisation.unite = true
			});
		}
	}

	getListeFournisseurs(): void {
		if (!this.initialisation.fournisseur) {
			this.service.getAll(ApiUrl.Fournisseur).subscribe(res => {
				this.ListeFournisseurs = res.value;
				this.initialisation.fournisseur = true
			});
		}
	}

	getListeCategorie(): void {

		if (!this.initialisation.categorie) {
			this.service.getAll(ApiUrl.configClassificationSales).subscribe(res => {
				this.ListeCategorie = res.value;
				this.ListeCategorie.forEach(element => {
					this.globalCategorie = [...this.globalCategorie, ...element.subClassification]
				});
				if (this.isIntervention) {
					this.ListeCategorie = this.ListeCategorie.filter(x => x.id === 44)
				} else {
				} this.initialisation.categorie = true;
			});
		}
	}

	dispalyMsgError(): void {
		const test = "Veuillez s'il vous plait choisir un fournisseur et indiquer un prix d'achat";
		toastr.warning(test, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}


	sauvegarder(formValues) {
		formValues.discount = {
			'value': formValues.remise,
			'type': this.typeRemiseGloabl
		}
		const article = {
			product: formValues,
			quantity: formValues.quantity,
			remise: formValues.remise,
			type: this.articleType.produit,
		};
		if (this.addInDb) {
			formValues['productCategoryTypeId'] = formValues['productCategoryType'].id;
			this.addProduit(formValues);
			article.product.id = this.idProduitsSave !== 0 ? this.idProduitsSave : 0;
		} else {
			this.translate.get('addproduit').subscribe(text => {
				toastr.success(text.msg, text.title, {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
		}

		this.PrixParFournisseur = [];


		this.dialogRef.close(article);
		this.createForm(null);
	}

	swalWarningConfig = (text: any) => {
		return {
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		};
	};

	async save() {
		if (this.checkFormIsValid()) {
			let formValues = this.form.value;
			if (!formValues) {
				return;
			}
			formValues['id'] = this.produit == null ? 0 : this.produit.id;
			formValues['labels'] = [];
			if (this.PrixParFournisseur.length === 0 && this.form.value.materialCost !== 0) {
				this.dispalyMsgError();
				return false
			}
			formValues['categoryId'] = formValues.categoryId;
			formValues['category'] = this.globalCategorie.find(x => +x.id === +formValues.categoryId);
			formValues['productSuppliers'] = this.PrixParFournisseur;
			formValues['totalHT'] = this.TotalHt();

			if (this.PrixParFournisseur.length > 0 && this.form.value.materialCost === 0) {
				this.translate.get('PrixParFournisseur.prix').subscribe(async text => {
					swal(this.swalWarningConfig(text)).then(isConfirm => {
						if (isConfirm) {
							this.sauvegarder(formValues)
						} else {
						}
					});
				});
			} else {
				this.sauvegarder(formValues);
			}


		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
		}
	}

	addProduit(body) {
		this.service.create(ApiUrl.Produit + ACTION_API.create, body).subscribe(
			res => {
				if (res) {
					this.idProduitsSave = res.value.id;
					this.translate.get('addproduit').subscribe(text => {
						toastr.success(text.msg, text.title, {
							positionClass: 'toast-top-center',
							containerId: 'toast-top-center',
						});
					});
				}
			},
			err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.fillAll, '', {
						positionClass: 'toast-top-center',
						containerId: 'toast-top-center',
					});
				});
			}
		);
	}

	checkFormIsValid(): boolean {
		let valid = true;
		for (let key in this.form.controls) {
			if (this.form.controls[key].errors != null) {
				valid = false;
			}
		}
		return valid;
	}

	getPrixParFournisseurList(): Promise<any> {
		return new Promise((resolve, reject) => {
			try {
				this.loadData.return('getList', (response) => {
					resolve(response);
				});
			} catch (err) {
				reject(err);
			}
		});
	};

	GetParametrageTva() {
		if (!this.initialisation.tva) {
			this.service.getAll(ApiUrl.configCategory + '/type/' + this.chartAccountCategoryType.VAT).subscribe(res => {
				const data = res.value[0];
				const tva =
					(this.form.controls['vat'].value != null && this.form.controls['vat'].value != 0)
						? this.form.controls['vat'].value
						: parseFloat(data['vatValue']);
				this.form.controls['vat'].setValue(tva);
				this.initialisation.tva = true;
			});
		}
	}


	calculate_cout_horaire(): any {
		return parseFloat(this.form.value.totalHours) * parseFloat(this.form.value.hourlyCost);
	}

	calculHtTTc(qte: number) {
		this.TotalHt(qte);
		this.prixTtc(qte);
	}

	prixTtc(qte?) {
		const tva = this.form.controls.vat.value / 100 + 1;
		const TotalHt = qte !== undefined ? this.TotalHt(qte) : this.TotalHt();
		return TotalHt * tva;
	}

	TotalHt(qte?) {
		const totalHt = this.calculate_cout_horaire() + this.form.value.materialCost;
		let remise = 0;
		if (this.form.value.remise !== 0) {
			if (this.typeRemiseGloabl === TypeValue.Amount) {
				remise = this.form.value.remise;
			} else {
				remise = totalHt * (this.form.value.remise / 100);
			}
		}
		const res = qte !== undefined ? qte * (totalHt - remise) : (totalHt - remise);
		const result = parseFloat(res.toFixed(2));
		return isNaN(result) ? 0 : result;
	}

	getCouteVenteFromParamerage() {
		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			const PrixParDefault: DefaultValue = JSON.parse(res);
			if (!this.data.article) {
				this.form.controls['hourlyCost'].setValue(PrixParDefault.salesPrice);
				this.form.controls['vat'].setValue(PrixParDefault.defaultVAT);
			}
		});
	}
}
