import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { CommonModules } from '../common.module';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TableArticleComponent } from './table-article.component';
import { SelectProduitComponent } from './select-produit/select-produit.component';
import { SelectLotComponent } from './select-lot/select-lot.component';
import { ProduitFormComponent } from './produit-form/produit-form.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatDialogModule, MatIconModule } from '@angular/material';
import { InputTextareaModule } from '../input-textarea/input-textarea.module';
import { MatSelectModule, MatMenuModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from 'app/shared/shared.module';
import { FournisseurCardModule } from 'app/shared/fournisseur-card/fournisseur-card.module';
import { TagsInputModule } from 'app/shared/tags-input/tags-input.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { SplitButtonModule } from 'app/custom-module/primeng/primeng';

export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/ficheintervention/', suffix: '.json' },
		{ prefix: './assets/i18n/ficheinterventionmaintenance/', suffix: '.json' },
		{ prefix: './assets/i18n/selectProduit/', suffix: '.json' },
		//prestation
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },

	]);
}


@NgModule({
	// tslint:disable-next-line:max-line-length
	providers: [
		PreviousRouteService,
	],
	imports: [
		SplitButtonModule,
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		MatIconModule,
		ReactiveFormsModule,
		DataTablesModule,
		CommonModules,
		MatSelectModule,
		OverlayModule,
		DragDropModule,
		SharedModule,
		FournisseurCardModule,
		TagsInputModule,
		MatMenuModule,
		NgSelectModule,
		NgbTooltipModule,
		// GroupesModule
		InfiniteScrollModule,
		AngularEditorModule,
		MatDialogModule,
		InputTextareaModule
	],
	exports: [TableArticleComponent , ProduitFormComponent],
	declarations: [
		TableArticleComponent,
		SelectProduitComponent,
		SelectLotComponent,
		ProduitFormComponent,
	],
	entryComponents: [SelectLotComponent, SelectProduitComponent , ProduitFormComponent]
})
export class TableArticleModule { }
