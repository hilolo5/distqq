import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'app-time-picker',
	templateUrl: './time-picker.component.html',
	styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnInit, OnChanges {

	//#region Properties

	@Input() hours: any = 0;
	@Input() minutes: any = 0;
	@Input() readOnly = false;

	@Output() hoursUpdated = new EventEmitter<any>();
	@Output() minutesUpdated = new EventEmitter<any>();

	//#endregion

	//#region Lifecycle

	constructor() { }

	ngOnInit(): void {
		this.onHoursBlur({ target: { innerText: this.hours } });
		this.onMinutesBlur({ target: { innerText: this.minutes } });
	}

	ngOnChanges(): void {
		this.onHoursBlur({ target: { innerText: this.hours } });
		this.onMinutesBlur({ target: { innerText: this.minutes } });
	}

	//#endregion

	//#region Methods

	formayDisplay = (input: number) => `${input > 9 ? '' : '0'}${input}`;

	//#endregion


	//#region Events

	onHourIncrement(): void {
		this.hours = this.formayDisplay(parseInt(this.hours, 10) + 1);
		this.hoursUpdated.emit(parseInt(this.hours, 10));
	}

	onHoursDecrement(): void {
		this.hours = this.formayDisplay(Math.max(0, parseInt(this.hours, 10) - 1));
		this.hoursUpdated.emit(parseInt(this.hours, 10));
	}

	onMinutesIncrement(): void {
		this.minutes = this.formayDisplay(parseInt(this.minutes, 10) + 1 === 60 ? 0 : parseInt(this.minutes, 10) + 1);
		this.minutesUpdated.emit(parseInt(this.minutes, 10));
	}
	onMinutesDecrement(): void {
		this.minutes = this.formayDisplay(parseInt(this.minutes, 10) - 1 === -1 ? 59 : parseInt(this.minutes, 10) - 1);
		this.minutesUpdated.emit(parseInt(this.minutes, 10));
	}

	onHoursBlur = (e: any) => {
		this.hours = this.formayDisplay(Math.max(parseInt(e.target.innerText, 10), 0));
		this.hoursUpdated.emit(parseInt(this.hours, 10));
	}

	onMinutesBlur = (e: any) => {
		const res: number = parseInt(e.target.innerText, 10);

		this.minutes = this.formayDisplay(Math.min(Math.max(res, 0), 59));
		this.minutesUpdated.emit(parseInt(this.minutes, 10));
	}

	//#endregion
}
