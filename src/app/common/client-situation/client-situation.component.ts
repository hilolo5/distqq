import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import * as Chart from 'chart.js';

@Component({
	selector: 'client-situation',
	templateUrl: './client-situation.component.html',
	styleUrls: ['./client-situation.component.scss']
})
export class ClientSituationComponent implements OnInit, AfterViewInit {

	//#region Properties

	years = [];
	currentYear = 0;

	labels = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
	chart: any;
	data: any = {};

	devisDetails: any = {};
	factureDetails: any = {};
	interventionDetails: any = {};
	chantierDetails: any = {};
	contratsDetails: any = {};

	@ViewChild('chart') chartEle: ElementRef;

	//#endregion

	//#region Lifecycle

	constructor(
		public route: ActivatedRoute,
		@Inject('IGenericRepository') private service: IGenericRepository<any>
	) { }

	ngOnInit(): void {

		// Generating a list of years ranging between (current year) to 1900
		this.years = this.prepareYears();
		this.currentYear = new Date().getFullYear();
	}

	ngAfterViewInit(): void {
		const id = this.route.snapshot.params.id;

		this.service.getAll(`${ApiUrl.Client}/${id}/documentDetails`).subscribe((result: any) => {
			if (result && result.value) {
				this.data = result.value;

				this.devisDetails = {
					in_progress: this.getDocuments(this.data.quotesDetails, 'in_progress').length || 0,
					billed: this.getDocuments(this.data.quotesDetails, 'billed').length || 0,
					signed: this.getDocuments(this.data.quotesDetails, 'signed').length || 0
				}

				this.factureDetails = {
					in_progress: this.getDocuments(this.data.invoicesDetails, 'in_progress').length || 0,
					late: this.getDocuments(this.data.invoicesDetails, 'late').length || 0,
					signed: this.getDocuments(this.data.invoicesDetails, 'signed').length || 0
				}

				this.interventionDetails = {
					planned: this.getDocuments(this.data.operationSheetDetails, 'planned').length || 0,
					realized: this.getDocuments(this.data.operationSheetDetails, 'realized').length || 0,
					billed: this.getDocuments(this.data.operationSheetDetails, 'billed').length || 0
				}

				this.chantierDetails = {
					study: this.getDocuments(this.data.workshopDetails, 'study').length || 0,
					accepted: this.getDocuments(this.data.workshopDetails, 'accepted').length || 0,
					finished: this.getDocuments(this.data.workshopDetails, 'finished').length || 0,
					refused: this.getDocuments(this.data.workshopDetails, 'refused').length || 0
				}

				this.contratsDetails = {
					waiting: this.getDocuments(this.data.contractDetails, 'waiting').length || 0,
					finished: this.getDocuments(this.data.contractDetails, 'finished').length || 0,
					in_progress: this.getDocuments(this.data.contractDetails, 'in_progress').length || 0
				}

			}
		});

		this.prepareChart();
	}

	//#endregion

	//#region Events

	onYearChanged(): void {
		this.prepareChart(this.currentYear);
	}

	//#endregion

	//#region Methods

	getCurrentYear = () => new Date().getFullYear();

	prepareYears = () => new Array(this.getCurrentYear() - 1900 + 1).fill(0).map((_, i: number) => this.getCurrentYear() - i);

	prepareChart(year: number = this.getCurrentYear()): void {
		const id = this.route.snapshot.params.id;
		let ca = new Array(12).fill(0);

		this.service.getAll(`${ApiUrl.Client}/${id}/TurnOver/${year}`).subscribe((result: any) => {
			if (result && result.value && result.value.perMonthTurnOver) {
				ca = [];

				result.value.perMonthTurnOver.forEach((e: any) => {
					ca.push(e.total);
				});
			}
		});

		this.chart = new Chart(this.chartEle.nativeElement, {
			type: 'line',
			data: {
				labels: this.labels,
				datasets: [{
					backgroundColor: '#F53C56',
					borderColor: '#F53C56',
					data: ca,
					fill: false,
					borderWidth: 2
				}, {
					backgroundColor: '#05658D',
					borderColor: '#05658D',
					data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					fill: false,
					borderWidth: 2
				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							padding: 40
						},
						gridLines: {
							drawTicks: false,
							display: false
						}
					}],
					xAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							padding: 40
						},
						gridLines: {
							drawTicks: false,
							display: true
						}
					}]
				}
			}
		});
	}

	getTurnOverPercent() {
		const data = this.data.turnOverDetails || { totalTurnOver: 1, totalPayments: 0 };
		return ((data.totalPayments || 0) / (data.totalTurnOver || 1)) * 100;
	}

	getDocuments(obj: any, status: string) {
		const docs = obj.documentByStatus.find((e: any) => e.status === status);
		return docs ? docs.documents : [];
	}

	//#endregion
}
