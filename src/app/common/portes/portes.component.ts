import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
  selector: 'app-porte',
  templateUrl: './portes.component.html',
  styleUrls: ['./portes.component.scss'],
  providers: []
})
export class PortesComponent {
  @Input('name') name: any =".";
  @Output('open') open = new EventEmitter();
  openPorte() {
    this.open.emit();
  }
}