import { PersonaliseReccurenteComponent } from './personalise-reccurente.component';
import { MatDialogModule, MatDatepickerModule, MatIconModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModules } from '../common.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ScrollingModule } from '@angular/cdk/scrolling';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/facture-reccurentes/', '.json');
}

@NgModule({
	providers: [

	],

	imports: [
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		ReactiveFormsModule,
		CommonModules,
		NgSelectModule,
		NgbTooltipModule,
		InfiniteScrollModule,
		MatDialogModule,
		MatDatepickerModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatNativeDateModule,

		ScrollingModule,
	],
	exports: [PersonaliseReccurenteComponent],
	declarations: [
		PersonaliseReccurenteComponent
	],
	entryComponents: [PersonaliseReccurenteComponent]
})
export class PrsonaliseReccurenteModule { }
