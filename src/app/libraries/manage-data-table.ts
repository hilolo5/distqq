// #region imports
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ColumnType } from 'app/components/form-data/data-table/data-table.component';
import { SortDirection, IFilterOption } from 'app/Models/Model/filter-option';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { PagedResult } from 'app/Models/Model/ListModel';
import { GlobalInstances } from 'app/app.module';
import { SwalConfiguration } from './swal-configuration';
import { DialogHelper } from './dialog';
import { ChangePasswordComponent } from 'app/common/change-password/change-password.component';
import { MatDialog } from '@angular/material';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { ShowComponentGroupe } from 'app/pages/Groupe/show/show.component';
import { EditGroupeComponent } from 'app/pages/Groupe/edit/edit.component';
import { AddGroupeComponent } from 'app/pages/Groupe/add/add.component';
import { lotsFromComponent } from 'app/pages/lots/lots-form/lotsFrom.component';
import { IFormType } from 'app/Enums/IFormType.enum';
import { StatusManagement, DocumentHelper } from './document-helper';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { moisEnum } from 'app/Enums/Maintenance/mois.enum';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';
import { chantierFromComponent } from 'app/common/chantier-form/from.component';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { VisualiserPdfComponent } from 'app/common/visualiser-pdf/visualiser-pdf.component';
import { SendMailComponent } from 'app/common/send-email/send-email.component';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { LocalElements } from 'app/shared/utils/local-elements';
import { StatutBonCommandeFournisseur } from 'app/Enums/Statut/StatutBonCommandeFournisseur.Enum';
import { CreateDepenseComponent } from 'app/common/create-depense/create-depense.component';
import { CancelAvoirComponent } from 'app/common/cancel-avoir/cancel-avoir.component';
import { StatutDepense } from 'app/Enums/Statut/StatutDepense.Enum';
import { AddmodeReglementComponent } from 'app/components/forms/parametres/add-modeReglement/add-modeReglement.component';
import { AddlabelComponent } from 'app/components/forms/parametres/add-label/add-label.component';
import { AddObservationComponent } from 'app/common/add-observation/add-observation.component';
import { AddtypeDComponent } from 'app/components/forms/parametres/add-typeD/add-typeD.component';
import { AreplanifierFormComponent } from 'app/components/areplanifier-form/areplanifier-form.component';
import { StatutFicheInterventionMaintenance } from 'app/Enums/Statut/StatutFicheInterventionMain.enum';
import { CreatePdf } from './PDF/create-pdf';
import { ActionPdf } from '../Enums/typeValue.Enums';
import { AgendaFormComponent } from 'app/shared/agenda-form/agenda-form/agenda-form.component';
import { MissionKind } from 'app/Enums/missionKinds';
import { PaiementGroupeComponent } from 'app/pages/paiements/index/paiement-groupe/paiement-groupe.component';
import { MouvementCompteComponent } from 'app/pages/paiements/index/mouvement-compte/mouvement-compte.component';
import { AddTypekindComponent } from 'app/components/add-typekind/add-typekind.component';

declare var toastr: any;

export interface DataTableEntry {
	name: string | string[];
	keyTranslate?: string;
	nameTranslate: string;
	isOrder?: boolean;
	type?: ColumnType;
	appear?: boolean;
	labels?: string;
	status?: {
		value: (item) => string;
		appear: (item) => boolean;
	};
	list?: DataTableEntry | DataTableEntry[];
	textColor?: {
		color: (item) => string;
		appear: (item) => boolean;
	};
	src?: {
		value: (item) => string;
		appear: (item) => boolean;
	};
}
export interface DataTableRowActions {
	id?: number;
	icon: string;
	iconDetails?: string;
	icon1?: string;
	label: string;
	color: string;
	default?: boolean;
	appear: (item?: any) => boolean;
	appearDetail?: (item?: any) => boolean;
	action?: (item?: any, index?: number) => void;
}

export class ManageDataTable {

	public static userProfile: typeof UserProfile = UserProfile;

	public static service: IGenericRepository<any>;

	public static docType: string;

	public static header: Array<DataTableEntry | DataTableEntry[]> = [];

	public static filterOptions: IFilterOption;

	public static data: PagedResult;

	public static base64 = null;

	public static profils = [];

	public static role = [];

	public static listChantiers = [];

	public static listClient = [];

	public static listSupplier = [];

	public static comptes = [];

	public static listMois = [];

	public static listTechnicien = [];

	public static periodes = [];

	public static dialog: MatDialog;

	public static idChantier = null;

	public static isPlanification = null;

	public static typeJournal;

	public static typeKinds;

	public static isAgendaGlobal = '';


	static canDuplacate(doc) {
		// tslint:disable-next-line: max-line-length
		if (ManageDataTable.docType === ApiUrl.Invoice && doc.typeInvoice !== TypeFacture.None) {
			return false;
		} else {
			return true;

		}
	}
	public static getHeader(): Array<DataTableEntry | DataTableEntry[]> {
		switch (ManageDataTable.docType) {
			case ApiUrl.typesMission: return [
				{ name: 'value', nameTranslate: 'LABELS.value', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			// #region contacts
			case ApiUrl.configurationLabel: return [
				{ name: 'value', nameTranslate: 'LABELS.NOM', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'nobmbreDEFois', nameTranslate: 'LABELS.NbrFois', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			case ApiUrl.configDocumentType: return [
				{ name: 'value', nameTranslate: 'LABELS.NOM', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			case ApiUrl.configModeRegelemnt: return [
				{ name: 'value', nameTranslate: 'LABELS.NOM', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			case ApiUrl.Client: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'name', nameTranslate: 'LABELS.NAME', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'email', nameTranslate: 'LABELS.EMAIL', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'phoneNumber', nameTranslate: 'LABELS.PHONE_NUMBER', isOrder: true, type: ColumnType.any, appear: true }
			]; break;
			case ApiUrl.Fournisseur: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'name', nameTranslate: 'LABELS.NAME', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'email', nameTranslate: 'LABELS.EMAIL', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'phoneNumber', nameTranslate: 'LABELS.PHONE_NUMBER', isOrder: true, type: ColumnType.any, appear: true }
			]; break;
			case ApiUrl.User: return [
				{ name: 'userName', nameTranslate: 'LABELS.NOMUSER', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'firstName', nameTranslate: 'LABELS.FIRSTNAME', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'lastName', nameTranslate: 'LABELS.LASTNAME', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'statut', nameTranslate: 'LABELS.ACTIF', keyTranslate: 'ACTIVE.{val}', isOrder: false, type: ColumnType.any, appear: true },
				{ name: 'role.type', keyTranslate: 'PROFILE.{val}', nameTranslate: 'LABELS.TYPE', isOrder: false, type: ColumnType.any, appear: true },
				{ name: 'email', nameTranslate: 'LABELS.EMAIL', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'phoneNumber', nameTranslate: 'LABELS.PHONE_NUMBER', isOrder: true, type: ColumnType.any, appear: true }
			]; break;
			case ApiUrl.Groupe: return [
				{ name: 'name', nameTranslate: 'LABELS.FIRSTNAME', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'clients.length', nameTranslate: 'LABELS.NBCLIENT', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			case ApiUrl.Mission: return [
				{ name: 'title', nameTranslate: 'Titre', isOrder: true, type: ColumnType.any, appear: true },


				{
					name: 'missionKind', nameTranslate: 'Type', isOrder: true, type: ColumnType.tache, appear: true, labels: ''
					, textColor: {
						color: (doc) => {
							if (doc.type === MissionKind.Appointment) { return ' #38b475'; }
							if (doc.type === MissionKind.Call) { return '#feb969'; }
							if (doc.type === MissionKind.TechnicianTask) { return '#47a2c1'; }
						},
						appear: () => true
					}
				},
				{ name: 'client.name', nameTranslate: 'Client', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'technician.userName', nameTranslate: 'Technicien', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'startingDate', nameTranslate: 'Date début', isOrder: true, type: ColumnType.DateTime, appear: true },
				// { name: 'status', nameTranslate: 'Status', isOrder: true, type: ColumnType.Status, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
			]
			case ApiUrl.Lot: return [
				{ name: 'name', nameTranslate: 'LABELS.LASTNAME', isOrder: true, type: ColumnType.any, appear: true },
			]; break;
			case ApiUrl.Devis: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
					appear: !this.IsChantier()
				},
				{
					name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: false, type: ColumnType.span, appear: true
				},
				{
					name: 'workshop', nameTranslate: 'LABELS.WORKSHOP', isOrder: false, type: ColumnType.any,
					appear: !this.IsChantier()
				},
				{
					name: 'purpose', nameTranslate: 'LABELS.OBJET', isOrder: false, type: ColumnType.any,
					appear: !this.IsChantier()
				},
				{
					name: 'dueDate', nameTranslate: 'LABELS.DATE_ECHEANCE', isOrder: true, type: ColumnType.Date,
					appear: !this.IsChantier()
				},
				{ name: 'totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
				{ name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, appear: true },
				{
					name: 'situation', nameTranslate: 'LABELS.FACTURATION', isOrder: false, type: ColumnType.Progressbar,
					appear: !this.IsChantier()
				}
			]; break;
			case ApiUrl.Invoice: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: false, type: ColumnType.span, list: [
						{ name: 'workshop', nameTranslate: 'LABELS.CHANTIER', isOrder: false, type: ColumnType.any },
						{ name: 'operationSheetMaintenanceReference', nameTranslate: 'LABELS.FIM', isOrder: false, type: ColumnType.any },
						{ name: 'contractReference', nameTranslate: 'LABELS.CONTRATE', isOrder: false, type: ColumnType.any },
					],
					appear: true
				},
				{
					name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'dueDate', nameTranslate: 'LABELS.DATE_ECHEANCE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'purpose', nameTranslate: 'LABELS.OBJET', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{ name: 'totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
				{
					name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, list: [
						{ name: 'restToPay', nameTranslate: 'LABELS.RESTEPAYER', isOrder: false, type: ColumnType.Currency }], appear: true
				},
			]; break;
			case ApiUrl.Recurring: return [
				{ name: 'document.clientName', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'maintenanceContract.reference', nameTranslate: 'LABELS.CONTRAT', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{ name: 'document.orderDetails.totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, appear: true },
			]; break;
			case ApiUrl.avoir: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{
					name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'dueDate', nameTranslate: 'LABELS.DATE_ECHEANCE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{ name: 'totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
				{ name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, appear: true },
			]; break;
			case ApiUrl.BonCommandeF: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'workshop.name', nameTranslate: 'LABELS.WORKSHOP', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{
					name: 'supplier.name', nameTranslate: 'LABELS.FOURNISSEUR', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{ name: 'orderDetails.totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
				{ name: 'orderDetails.totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, appear: true },
			]; break;
			case ApiUrl.Depense: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'dueDate', nameTranslate: 'LABELS.DATE_EXPERATION', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'workshop', nameTranslate: 'LABELS.WORKSHOP', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{
					name: 'supplier', nameTranslate: 'LABELS.FOURNISSEUR', isOrder: false, type: ColumnType.any,
					appear: true
				},
				{ name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, appear: true },
			]; break;
			case ApiUrl.Payment: return [
				{ name: 'description', nameTranslate: 'LABELS.DESCRIPTION', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'account', nameTranslate: 'LABELS.COMPTE', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'operation', nameTranslate: 'LABELS.TYPE', isOrder: true, type: ColumnType.Translate, labels: 'typePaiment', appear: true },
				{
					name: 'datePayment', nameTranslate: 'LABELS.DATE', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'amount', nameTranslate: 'LABELS.MONTANT', isOrder: true, type: ColumnType.Currency, appear: true,
					textColor: {
						color: (paiement) => {
							return this.colorAmountPayment(paiement);
						},
						appear: () => true
					}
				},
			]; break;
			case ApiUrl.GME: return [
				{ name: 'equipmentName', nameTranslate: 'LABELS.FIRSTNAME', isOrder: true, type: ColumnType.any, appear: true }
			]; break;
			case ApiUrl.MaintenanceContrat: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'client.name', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'startDate', nameTranslate: 'LABELS.DATE_DEBUT', isOrder: true, type: ColumnType.Date,
					appear: true
				},
				{
					name: 'endDate', nameTranslate: 'LABELS.DATE_FIN', isOrder: true, type: ColumnType.Date,
					appear: true
				}
			]; break;
			case ApiUrl.VisitMaintenance: return [
				{ name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{
					name: 'year', nameTranslate: 'LABELS.YEAR', isOrder: true, type: ColumnType.any,
					appear: true
				},
				{
					name: 'month', nameTranslate: 'LABELS.MOIS', isOrder: true, type: ColumnType.Translate, labels: 'mois',
					appear: true
				},
				{
					name: 'maintenanceOperationSheetReference', nameTranslate: 'LABELS.REFERENCEFI', isOrder: true, type: ColumnType.any,
					appear: true
				}
			]; break;
			case ApiUrl.MaintenanceOperationSheet: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{ name: 'startDate', nameTranslate: 'LABELS.DATE_DEBUT', isOrder: true, type: ColumnType.Date, appear: true },
				{ name: 'endDate', nameTranslate: 'LABELS.DATE_FIN', isOrder: true, type: ColumnType.Date, appear: true },
				{ name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'technician', nameTranslate: 'LABELS.TECHNICIEN', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'type', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.Translate, appear: true, labels: 'typeIM'
					, textColor: {
						color: (doc) => {
							return doc.type === TypeInterventionMaintenance.AfterSalesService ? '#f20775' : '#51358c';
						},
						appear: () => true
					}
				},
				{
					name: 'smileSatisfication', nameTranslate: 'LABELS.SATISFACTION', isOrder: true, type: ColumnType.image, appear: true, src: {
						value: (doc) => {
							return this.smileSatisfication(doc.smileSatisfication);
						},
						appear: (doc) => doc.smileSatisfication !== 0
					}
				}
			]; break;
			case ApiUrl.Chantier: return [
				{ name: 'name', nameTranslate: 'LABELS.WORKSHOP', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'client.name', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{ name: 'createdOn', nameTranslate: 'LABELS.DATE_CREATION', isOrder: true, type: ColumnType.Date, appear: true },
				{ name: 'progressRate', nameTranslate: 'LABELS.TAUX', isOrder: true, type: ColumnType.Progressbar, appear: true },
				{ name: 'turnover', nameTranslate: 'LABELS.ChifreA', isOrder: true, type: ColumnType.Currency, appear: true },
			]; break;
			case ApiUrl.FicheIntervention: return [
				{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
						value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
						appear: (doc) => true
					}, appear: true
				},
				{ name: 'startDate', nameTranslate: 'LABELS.DATE_DEBUT', isOrder: true, type: ColumnType.DateTime, appear: true },
				{ name: 'endDate', nameTranslate: 'LABELS.DATE_FIN', isOrder: true, type: ColumnType.DateTime, appear: true },
				{ name: 'purpose', nameTranslate: 'LABELS.OBJET', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'workshop', nameTranslate: 'LABELS.WORKSHOP', isOrder: true, type: ColumnType.any, appear: true },
				{ name: 'client', nameTranslate: 'LABELS.CLIENT', isOrder: true, type: ColumnType.any, appear: true },
				{
					name: 'smileSatisfication', nameTranslate: 'LABELS.SATISFACTION', isOrder: true, type: ColumnType.image, appear: true, src: {
						value: (doc) => {
							return this.smileSatisfication(doc.smileSatisfication);
						},
						appear: (doc) => doc.smileSatisfication !== 0
					}
				}
			]; break;
			case ApiUrl.journal: return [
				{ name: 'journalCode', nameTranslate: 'LABELS.CODEJOURNAL', isOrder: false, type: ColumnType.any, appear: true },
				{ name: 'entryDate', nameTranslate: 'LABELS.DATE', isOrder: true, type: ColumnType.Date, appear: true },
				{ name: 'chartAccount', nameTranslate: 'LABELS.NUMCOMPTE', isOrder: false, type: ColumnType.any, appear: true },
				{ name: 'pieceNumber', nameTranslate: 'LABELS.PIECENUMBER', isOrder: false, type: ColumnType.any, appear: true },
				{ name: 'externalPartner', nameTranslate: this.typeJournal === 'JournalVente' ? 'LABELS.CLIENT' : 'LABELS.FOURNISSEUR', isOrder: false, type: ColumnType.any, appear: this.typeJournal === 'JournalAchat' || this.typeJournal === 'JournalVente' },
				{ name: 'tiers', nameTranslate: 'LABELS.TIERS', isOrder: false, type: ColumnType.any, appear: this.typeJournal === 'journal_caisse' || this.typeJournal === 'journal_banque' },
				{ name: 'debit', nameTranslate: 'LABELS.DEBIT', isOrder: false, type: ColumnType.Currency, appear: true },
				{ name: 'credit', nameTranslate: 'LABELS.CREDIT', isOrder: false, type: ColumnType.Currency, appear: true },
				{ name: 'typePaiement', nameTranslate: 'LABELS.TYPEPAIEMENT', isOrder: false, type: ColumnType.any, appear: this.typeJournal === 'journal_caisse' || this.typeJournal === 'journal_banque' },
			]; break;
			// #endregion
		}
	}

	public static colorAmountPayment(paiement) {
		if (paiement.type === 0 || paiement.type === 2 && paiement.operation === TypePaiement.Transfer_From) { return '#dd4b39'; }
		if (paiement.type === 1 || paiement.type === 2 && paiement.operation === TypePaiement.Transfer_To) { return '#00a65a'; }
	}

	public static IsChantier() {
		return this.idChantier && this.idChantier != null;
	}

	public static initFilters() {
		ManageDataTable.data = null;
		// ManageDataTable.currentPage = 0;

		this.filterOptions = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			SearchQuery: '',
			SortDirection: SortDirection.Descending,
			OrderBy: 'reference',
		};
	}

	public static InstanseService() {
		DocumentHelper.service = ManageDataTable.service;
	}

	public static smileSatisfication(smile) {
		if (smile === 0) { return '' }
		if (smile === 1) { return 'assets/app/imgs/frown.png' }
		if (smile === 2) { return 'assets/app/imgs/meh.png' }
		if (smile === 3) { return 'assets/app/imgs/smile.png' }
	}

	/**
	 * request data
	 */
	public static getData(cache: boolean = true): Promise<any> {
		return new Promise((resolve, reject) => {

			if (ManageDataTable.docType === ApiUrl.Mission) {
				ManageDataTable.filterOptions['OrderBy'] = 'startingDate';
			}
			const filters: IFilterOption = Object.assign(ManageDataTable.filterOptions);
			if (ManageDataTable.service) {
				ManageDataTable.service.getAllPagination(ManageDataTable.docType, filters)
					.subscribe((data) => {
						if (data.isSuccess) {
							if (ManageDataTable.docType !== ApiUrl.journal) {
								ManageDataTable.data = data;
							} else {
								data.value = ManageDataTable.FomaterData(data.value);
								ManageDataTable.data = data;
							}
							resolve(data);
						}
					}, (err: any) => {
						reject(err);
					});
			}

		});
	}

	public static getRouteDetails(element) {
		switch (this.docType) {
			// contacts
			case ApiUrl.Client:
				return `/clients/detail/${element.id}`;
				break;
			case ApiUrl.Fournisseur:
				return `/fournisseurs/detail/${element.id}`; break;
			case ApiUrl.User:
				return `/utilisateurs/detail/${element.id}`; break;
			case ApiUrl.Produit:
				return `/produits/detail/${element.id}`; break;
			case ApiUrl.Devis: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/detail/${element.id}` : `/devis/detail/${element.id}`; break;
			case ApiUrl.Invoice: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5/detail/${element.id}` : `/factures/detail/${element.id}`;
				break;
			case ApiUrl.Recurring: return `/factureReccurente/detail/${element.id}`; break;
			case ApiUrl.avoir: return `/avoirs/detail/${element.id}`; break;
			case ApiUrl.BonCommandeF: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/detail/${element.id}` : `/bonCommandeFournisseur/detail/${element.id}`;
				; break;
			case ApiUrl.Depense: return `/depense/detail/${element.id}`; break;
			case ApiUrl.Payment: return `/paiements/detail/${element.id}`; break;
			case ApiUrl.GME: return `/gammemaintenanceequipements/detail/${element.id}`;
				break;
			case ApiUrl.VisitMaintenance: return DocumentHelper.NavigateDetailsVisiteMaintenance(element); break;
			case ApiUrl.MaintenanceContrat: return `/contratentretiens/detail/${element.id}`; break;
			case ApiUrl.MaintenanceOperationSheet: return this.isPlanification != null ? `/planification/detail/${element.id}` : `/ficheinterventionmaintenance/detail/${element.id}`; break;
			case ApiUrl.FicheIntervention: return this.isAgendaGlobal != null ? `/agendaglobal/detail/${element.id}` : `/ficheintervention/detail/${element.id}`; break;
			default: return '';
		}
	}


	public static getDetails(element) {
		switch (this.docType) {
			// contacts
			case ApiUrl.Client:
				return GlobalInstances.router.navigateByUrl(`/clients/detail/${element.id}`);
				break;
			case ApiUrl.Fournisseur:
				return GlobalInstances.router
					.navigateByUrl(`/fournisseurs/detail/${element.id}`); break;
			case ApiUrl.User:
				return GlobalInstances.router
					.navigateByUrl(`/utilisateurs/detail/${element.id}`); break;
			case ApiUrl.Groupe: return DialogHelper.openDialog(
				this.dialog,
				ShowComponentGroupe,
				DialogHelper.SIZE_MEDIUM,
				{ data: element.id }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Lot: return DialogHelper.openDialog(
				this.dialog,
				lotsFromComponent,
				DialogHelper.SIZE_LARGE,
				{ type: IFormType.preview, defaultData: element }
			).subscribe(async response => {
				this.initData();
			}); break;

			case ApiUrl.Devis: return GlobalInstances.router
				.navigateByUrl(this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/detail/${element.id}` : `/devis/detail/${element.id}`); break;
			case ApiUrl.Invoice: return GlobalInstances.router
				.navigateByUrl(this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5/detail/${element.id}` : `/factures/detail/${element.id}`);
				break;
			case ApiUrl.Recurring: return GlobalInstances.router
				.navigateByUrl(`/factureReccurente/detail/${element.id}`); break;
			case ApiUrl.avoir: return GlobalInstances.router
				.navigateByUrl(`/avoirs/detail/${element.id}`); break;
			case ApiUrl.BonCommandeF: return GlobalInstances.router
				.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/detail/${element.id}` : `/bonCommandeFournisseur/detail/${element.id}`]);
				; break;
			case ApiUrl.Depense: return GlobalInstances.router
				.navigate([`/depense/detail/${element.id}`]); break;
			case ApiUrl.Payment: return GlobalInstances.router
				.navigate([`/paiements/detail/${element.id}`]); break;
			case ApiUrl.Chantier: return this.getChantier(element); break;
			case ApiUrl.GME: return GlobalInstances.router
				.navigate([`/gammemaintenanceequipements/detail/${element.id}`]);
				break;
			case ApiUrl.VisitMaintenance: return DocumentHelper.NavigateDetailsVisiteMaintenance(element); break;
			case ApiUrl.MaintenanceContrat: return GlobalInstances.router
				.navigate([`/contratentretiens/detail/${element.id}`]); break;
			case ApiUrl.MaintenanceOperationSheet: return GlobalInstances.router
				.navigate([this.isPlanification != null ? `/planification/detail/${element.id}` : `/ficheinterventionmaintenance/detail/${element.id}`]); break;
			case ApiUrl.FicheIntervention: return GlobalInstances.router
				.navigateByUrl(this.isAgendaGlobal != null ? `/agendaglobal/detail/${element.id}` : `/ficheintervention/detail/${element.id}`); break;
			case ApiUrl.Mission: return DialogHelper.openDialog(
				this.dialog,
				AgendaFormComponent,
				DialogHelper.SIZE_SMALLTache,
				{ hasBackdrop: false, res: element, readOnly: true, isMission: true }
			).subscribe(async response => {
				this.initData();
			}); break;
			default: return '';
		}
	}

	public static getUpdate(element) {
		switch (this.docType) {

			// contacts
			case ApiUrl.Client:
				return GlobalInstances.router.navigateByUrl(`/clients/edit/${element.id}`);
				break;
			case ApiUrl.Fournisseur:
				return GlobalInstances.router
					.navigateByUrl(`/fournisseurs/edit/${element.id}`); break;
			case ApiUrl.User:
				return GlobalInstances.router
					.navigateByUrl(`/utilisateurs/edit/${element.id}`); break;
			case ApiUrl.Groupe: return DialogHelper.openDialog(
				this.dialog,
				EditGroupeComponent,
				DialogHelper.SIZE_MEDIUM,
				{ data: element }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Chantier: return DialogHelper.openDialog(
				this.dialog,
				chantierFromComponent,
				DialogHelper.SIZE_LARGE,
				{ data: { defaultData: element, type: IFormType.update } }
			).subscribe(async response => {
				//this.initData();
				GlobalInstances.router.navigateByUrl(`/chantiers/detail/${element.id}`)
			}); break;
			case ApiUrl.Lot: return DialogHelper.openDialog(
				this.dialog,
				lotsFromComponent,
				DialogHelper.SIZE_LARGE,
				{ type: IFormType.update, defaultData: element }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Devis: return GlobalInstances.router
				.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/edit/${element.id}` : `/devis/edit/${element.id}`]);
			case ApiUrl.Invoice: return GlobalInstances.router
				.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5/edit/${element.id}` : `/factures/edit/${element.id}`]);
				break;
			case ApiUrl.Recurring: return GlobalInstances.router
				.navigateByUrl(`/factureReccurente/modifier/${element.id}`); break;
			case ApiUrl.avoir: return GlobalInstances.router
				.navigateByUrl(`/avoirs/edit/${element.id}`); break;
			case ApiUrl.BonCommandeF: return GlobalInstances.router
				.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/edit/${element.id}` : `/bonCommandeFournisseur/edit/${element.id}`]);
				break;
			case ApiUrl.Depense: return GlobalInstances.router
				.navigate([`/depense/edit/${element.id}`]); break;
			// case ApiUrl.Chantier: return DialogHelper.openDialog(
			// 	this.dialog,
			// 	chantierFromComponent,
			// 	DialogHelper.SIZE_LARGE,
			// 	{ data: { defaultData: element, type: IFormType.update } }
			// ).subscribe(async response => {
			// 	this.initData();
			// }); break;
			case ApiUrl.GME: return GlobalInstances.router
				.navigate([`/gammemaintenanceequipements/edit/${element.id}`]);
				break;
			case ApiUrl.MaintenanceContrat: return GlobalInstances.router
				.navigate([`/contratentretiens/edit/${element.id}`]); break;
			case ApiUrl.MaintenanceOperationSheet: return GlobalInstances.router
				.navigate([this.isPlanification != null ? `/planification/edit/${element.id}` : `/ficheinterventionmaintenance/edit/${element.id}`]); break;
			case ApiUrl.FicheIntervention: return GlobalInstances.router
				.navigateByUrl(this.isAgendaGlobal != null ? `/agendaglobal/edit/${element.id}` : `/ficheintervention/edit/${element.id}`); break;
			case ApiUrl.Mission: return DialogHelper.openDialog(
				this.dialog,
				AgendaFormComponent,
				DialogHelper.SIZE_SMALLTache,
				{ hasBackdrop: false, res: element, isMission: true }
			).subscribe(async response => {
				this.initData();
			}); break;

			default: return '';
		}
	}

	public static getChantier(element) {
		localStorage.removeItem('activetab');
		const url = `/chantiers/detail/${element.id}`;
		GlobalInstances.router.navigate([url]);
	}

	public static getActions(detail: boolean): DataTableRowActions[] {
		ManageDataTable.InstanseService();

		switch (this.docType) {
			case ApiUrl.configDocumentType: return [
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						DialogHelper.openDialog(
							this.dialog,
							AddtypeDComponent,
							DialogHelper.SIZE_MEDIUM,
							{ selected: element, show: true }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.configurationLabel: return [
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						DialogHelper.openDialog(
							this.dialog,
							AddlabelComponent,
							DialogHelper.SIZE_MEDIUM,
							{ selected: element, show: true }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.configModeRegelemnt: return [
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						DialogHelper.openDialog(
							this.dialog,
							AddmodeReglementComponent,
							DialogHelper.SIZE_MEDIUM,
							{ selected: element, show: true }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.typesMission: return [
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						DialogHelper.openDialog(
							this.dialog,
							AddTypekindComponent,
							DialogHelper.SIZE_MEDIUM,
							{ show: false, missionKind: ManageDataTable.typeKinds, selected: element }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			//  #region Contact

			case ApiUrl.Client: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([`/clients/edit/${element.id}`]);
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.Fournisseur: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([`/fournisseurs/edit/${element.id}`]);
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.User: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([`/utilisateurs/edit/${element.id}`]);
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				},
				{
					id: 5,
					icon: './assets/app/images/Icons/key.svg',
					label: 'LABELS.CHANGE_PASSWORD',
					color: '',
					action: (element) => {
						ManageDataTable.changePassword(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.Groupe: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						DialogHelper.openDialog(
							this.dialog,
							EditGroupeComponent,
							DialogHelper.SIZE_MEDIUM,
							{ data: element }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.Lot: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						DialogHelper.openDialog(
							this.dialog,
							lotsFromComponent,
							DialogHelper.SIZE_LARGE,
							{ type: IFormType.update, defaultData: element }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.Devis: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					icon1: 'eye',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail,
				},
				{
					id: 2,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/actions/editgreen.svg',
					icon1: 'edit',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/edit/${element.id}` : `/devis/edit/${element.id}`]);
					},
					appear: (element) => !detail && element !== undefined && element.canUpdate
				},

				{
					id: 4,
					icon: './assets/app/images/Icons/file-text-bl.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacture.svg',
					label: 'LABELS.CREER_FACTURE',
					color: 'green',
					action: (element) => {
						DocumentHelper.createFacture(element.id, this.idChantier);
					},
					appear: (item) => item !== undefined && item.canGenerateInvoice && item.status != 'draft'
				},
				{
					id: 5,
					icon: './assets/app/images/Icons/factureAcompte-01.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfactureac.svg',

					label: 'LABELS.CREER_FACTUREACOMPTE',
					color: 'green',
					action: (element) => {
						DocumentHelper.navigateToFactureAcompte(element.id, this.idChantier);
					},
					appear: (item) => item !== undefined && item.canGenerateDownPaymentInvoice && item.status != 'draft'
				},
				{
					id: 6,
					icon: './assets/app/images/Icons/factureSituation.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacturesit.svg',

					label: 'LABELS.CREER_FACTURESITUATION',
					color: 'green',
					action: (element) => {
						DocumentHelper.navigateToFactureSituation(element.id, this.idChantier);
					},
					appear: (item) => item !== undefined && item.canGenerateSituationInvoice && item.status != 'draft'
				},

				{
					id: 9,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/factureCloture.svg',
					label: 'LABELS.CREER_FC',
					color: 'warn',
					action: (element) => {
						DocumentHelper.navigateToCloture(element.id, this.idChantier);
					},
					appear: (item) => detail && item !== undefined && this.conditionCloture(item) && item.status != 'draft'
				},
				{
					id: 7,
					icon: './assets/app/images/Icons/Bon de commande Fournisseur.svg',
					iconDetails: './assets/app/images/Icons/actions/Bon de commande fournisseur.svg',
					label: 'LABELS.CREER_BCF',
					color: 'accent',
					action: (element) => {
						this.generateBC(element.id);
					},
					appear: (item) => item !== undefined && item.canGenerateSupplierOrder
				},

				{
					id: 8,
					icon: './assets/app/images/Icons/Fiche intervention.svg',
					iconDetails: './assets/app/images/Icons/actions/Fiche intervention.svg',

					label: 'LABELS.CREER_FI',
					color: 'warn',
					action: (element) => {
						DocumentHelper.generateFicheIntervention(element.id);
					},
					appear: (item) => item !== undefined && item.canGenerateOperationSheet && item.status != 'draft'
				},
				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/annuler.svg',

					label: 'LABELS.CANCEL',
					color: 'warn',
					action: (element) => {
						this.cancelRequest(element);
					},
					appear: (item) => detail && item !== undefined && this.conditionCancel(item)
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/actions/dupliquer.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 12,
					icon: './assets/app/images/Icons/actions/pdf.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',

					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/actions/mail.svg',
					iconDetails: './assets/app/images/Icons/actions/mail.svg',
					label: 'LABELS.SENDEMAIL',
					color: 'warn',
					action: (element) => {
						this.SendMail(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',
					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',
					icon1: 'delete',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (item) => item !== undefined && item.canDelete
				},
			]; break;
			case ApiUrl.Invoice: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 2,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5/edit/${element.id}` : `/factures/edit/${element.id}`]);
					},
					appear: (item) => !detail && item !== undefined && item.canUpdate
				},

				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => item && detail && this.canDuplacate(item),
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',

					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},

				{
					id: 12,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/mail.svg',

					label: 'LABELS.SENDEMAIL',
					color: 'warn',
					action: (element) => {
						this.SendMail(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',
					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 14,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/annuler.svg',

					label: 'LABELS.CANCEL',
					color: 'warn',
					action: (element) => {
						this.cancelRequest(element);
					},
					appear: (item) => detail && this.annullerFactureCondition(item)
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (item) => item !== undefined && item.canDelete
				},
			]; break;
			case ApiUrl.Recurring: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigateByUrl(`/factureReccurente/modifier/${element.id}`);
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.avoir: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/edit.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigateByUrl(`/avoirs/edit/${element.id}`);
					},
					appear: (document) => !detail && document !== undefined && document.status === 'draft'
				},

				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',

					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 12,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/mail.svg',

					label: 'LABELS.SENDEMAIL',
					color: 'warn',
					action: (element) => {
						this.SendMail(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',
					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (document) => document !== undefined && document.status === 'draft'
				},
			]; break;
			case ApiUrl.BonCommandeF: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/edit/${element.id}` : `/bonCommandeFournisseur/edit/${element.id}`]);
					},
					appear: (document) => !detail && document !== undefined && document.status !== StatutBonCommandeFournisseur.Facturee
				},
				{
					id: 15,
					icon: './assets/app/images/Icons/file-text-bl.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacture.svg',

					label: 'LABELS.CREATEDEPENSE',
					color: 'warn',
					action: (element) => {
						this.creerDepense(element);
					},
					appear: (item) => detail && item !== undefined && item.status !== StatutBonCommandeFournisseur.Brouillon
				},
				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 9,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/annuler.svg',

					label: 'LABELS.CANCEL',
					color: 'warn',
					action: (element) => {
						this.cancelRequest(element);
					},
					appear: (item) => detail && item !== undefined && item.status !== StatutBonCommandeFournisseur.Facturee &&
						item.status !== StatutBonCommandeFournisseur.Annule
				},


				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (document) => document !== undefined && document.status !== StatutBonCommandeFournisseur.Facturee
				},
			]; break;
			case ApiUrl.Depense: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigate([`/depense/edit/${element.id}`]);
					},
					appear: (document) => !detail && document !== undefined && document.canUpdate
				},

				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 20,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/annuler.svg',
					label: 'LABELS.CANCEL',
					color: 'warn',
					action: (element) => {
						this.cancelRequest(element);
					},
					appear: (item) => detail && item !== undefined && item.canCancel
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (document) => document !== undefined && document.canDelete
				},

			]; break;
			case ApiUrl.Payment: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.GME: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigate([`/gammemaintenanceequipements/edit/${element.id}`]);
					},
					appear: () => !detail
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.MaintenanceContrat: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					iconDetails: './assets/app/images/Icons/eye.svg',

					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail,
				},
				{
					id: 5,
					icon: './assets/app/images/Icons/external-link.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacture.svg',

					label: 'LABELS.CREER_FACTURE',
					color: 'green',
					action: (element) => {
						DocumentHelper.createFactureToMainteneContract(element.id);
					},
					appear: (item) => item && item.status != 'draft'
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigate([`/contratentretiens/edit/${element.id}`]);
					},
					appear: () => !detail
				},

				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',

					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',
					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 178,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/excelExp.svg',
					label: 'LABELS.exportExel',
					color: 'warn',
					action: (element) => {
						this.exportExel(element);
					},
					appear: (item) => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (document) => document !== undefined && document.status === 'draft'
				},
			]; break;
			case ApiUrl.VisitMaintenance: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.MaintenanceOperationSheet: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 2,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						GlobalInstances.router
							.navigate([`/ficheinterventionmaintenance/edit/${element.id}`]);
					},
					appear: (document) => !detail && document !== undefined && document.status !== 'billed' && document.status !== 'realized'
				},

				{
					id: 5,
					icon: './assets/app/images/Icons/file-text-bl.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacture.svg',

					label: 'LABELS.CREER_FACTURE',
					color: 'green',
					action: (element) => {
						DocumentHelper.createFactureinMOS(element.id);
					},
					appear: (document) => document !== undefined && document.status === 'realized'
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',

					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',
					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 12,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/mail.svg',
					label: 'LABELS.SENDEMAIL',
					color: 'warn',
					action: (element) => {
						this.SendMail(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/actions/mail.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfactureac.svg',
					label: 'LABELS.AREPLANIFIER',
					color: 'warn',
					action: (element) => {
						this.changeStatutAreplanifier(element);
					},
					appear: (item) => detail && this.condiPlanifie(item)
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (document) => document !== undefined && document.status !== 'billed' && document.status !== 'realized'
				},

			]; break;
			case ApiUrl.Chantier: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						DialogHelper.openDialog(
							this.dialog,
							chantierFromComponent,
							DialogHelper.SIZE_LARGE,
							{ data: { defaultData: element, type: IFormType.update } }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
			case ApiUrl.FicheIntervention: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => !detail
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					iconDetails: './assets/app/images/Icons/editgreen.svg',

					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element) => {
						GlobalInstances.router
							.navigate([`/ficheintervention/edit/${element.id}`]);
					},
					appear: (doc) => !detail && doc !== undefined && doc.status !== StatutFicheIntervention.Realisee &&
						doc.status !== StatutFicheIntervention.Facturee
				},

				{
					id: 5,
					icon: './assets/app/images/Icons/file-text.svg',
					iconDetails: './assets/app/images/Icons/actions/Creerfacture.svg',

					label: 'LABELS.CREER_FACTURE',
					color: 'green',
					action: (element) => {
						DocumentHelper.createFacturinFI(element.id);
					},
					appear: (doc) => doc !== undefined && (doc.status === StatutFicheIntervention.Realisee || doc.status === StatutFicheIntervention.Brouillon)
				},
				{
					id: 10,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/dupliquer.svg',

					label: 'LABELS.DUPLIQUER',
					color: 'warn',
					action: (element) => {
						this.duppliquer(element);
					},
					appear: (item) => detail
				},
				{
					id: 11,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/pdf.svg',
					label: 'LABELS.VPDF',
					color: 'warn',
					action: (element) => {
						this.genererPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 13,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/Print.svg',

					label: 'LABELS.IMPRIMER',
					color: 'warn',
					action: (element) => {
						this.imprimerPDF(element);
					},
					appear: (item) => detail
				},
				{
					id: 12,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/mail.svg',
					label: 'LABELS.SENDEMAIL',
					color: 'warn',
					action: (element) => {
						this.SendMail(element);
					},
					appear: (item) => detail
				},
				{
					id: 9,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/annuler.svg',
					label: 'LABELS.CANCEL',
					color: 'warn',
					action: (element) => {
						this.cancelRequest(element);
					},
					appear: (item) => detail && item !== undefined && item.status !== StatutFicheIntervention.Annulee
				},
				{
					id: 30,
					icon: './assets/app/images/Icons/play-circle.svg',
					iconDetails: './assets/app/images/Icons/actions/factureCloture.svg',
					label: 'LABELS.devisetablir',
					color: 'warn',
					action: (element) => {
						this.devisAetablir(element);
					},
					appear: (item) => detail && item !== undefined && (item.status === StatutFicheIntervention.Realisee || item.status === StatutFicheIntervention.Brouillon)
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					iconDetails: './assets/app/images/Icons/actions/del.svg',

					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: (doc) => doc !== undefined && doc.status !== StatutFicheIntervention.Realisee &&
						doc.status !== StatutFicheIntervention.Facturee
				},
			];

				break;

			case ApiUrl.Mission: return [
				{
					id: 1,
					icon: './assets/app/images/Icons/eye.svg',
					label: 'LABELS.SHOW',
					default: true,
					color: 'accent',
					action: (element: any) => {
						ManageDataTable.getDetails(element);
					},
					appear: () => true
				},
				{
					id: 3,
					icon: './assets/app/images/Icons/edit.svg',
					label: 'LABELS.EDIT',
					color: 'primary',
					action: (element: Groupe) => {
						DialogHelper.openDialog(
							this.dialog,
							AgendaFormComponent,
							DialogHelper.SIZE_SMALLTache,
							{ hasBackdrop: true, res: element, isMission: true }
						).subscribe(async response => {
							this.initData();
						});
					},
					appear: () => true
				},
				{
					id: 4,
					icon: './assets/app/images/Icons/trash-2.svg',
					label: 'LABELS.DELETE',
					color: 'warn',
					action: (element) => {
						ManageDataTable.deleteRequest(element);
					},
					appear: () => true
				}
			]; break;
		}
	}

	public static devisAetablir(element) {
		DialogHelper.openDialog(
			this.dialog,
			AddObservationComponent,
			DialogHelper.SIZE_MEDIUM,
			{ docType: this.docType, document: element }
		).subscribe(async response => {
			// this.getDetails(element);
		});
	}

	public static exportExel(document) {
		this.service.getById(ApiUrl.MaintenanceContrat + '/Export', document.id).subscribe(
			value => {
				// Get time stamp for fileName.
				const stamp = new Date().getTime();

				// file type
				const fileType = 'application/vnd.ms-excel';

				// file data
				const fileData = AppSettings._base64ToArrayBuffer(value.value);

				// file extension
				const extension = 'xlsx';
				AppSettings.setFile(fileData, ('GammeMaintenanceEquipement') + stamp + '.' + extension, fileType, extension);
			},
			err => {
				console.log(err);
			}
		)
	}

	public static generateBC(id) {

		this.service.getById(ApiUrl.Devis + '/Generate/SuppliersOrders', id).subscribe(res => {
			if (res.isSuccess) {
				//	GlobalInstances.router.navigate(['/devis']);
				this.initData();
				toastr.success('Ajout avec succès', 'Bon de commandes fournisseur', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

			}
		});
	}
	public static creerDepense(element) {
		DialogHelper.openDialog(
			this.dialog,
			CreateDepenseComponent,
			DialogHelper.SIZE_MEDIUM,
			{ docType: this.docType, document: element }
		).subscribe(async response => {
			// this.getDetails(element);
		});
	}

	public static conditionCancel(doc) {
		return doc.status === StatutDevis.EnAttente || doc.status === StatutDevis.Acceptee
			|| doc.status === StatutDevis.NonAcceptee
	}

	public static conditionCloture(elem) {
		const res = elem.status !== StatutDevis.Annulee && elem.orderDetails.productsDetailsType === OrderProductsDetailsType.List;
		if (elem.situations === null || elem.situations.length === 0) { return true; }
		if (elem.situations.length !== 0) {
			const list = elem.situations.filter(e => e.typeInvoice === TypeFacture.None ||
				e.typeInvoice === TypeFacture.Cloturer)
			return res && list.length === 0 ? true : false;
		}
		return false;
	}

	public static annullerFactureCondition(facture) {
		if (facture.typeInvoice === TypeFacture.Acompte || facture.typeInvoice === TypeFacture.Situation) {
			return facture.payments.length === 0 &&
				(facture.status === StatutFacture.Encours || facture.status === StatutFacture.Enretard) && facture.canCancel === false
		} else {
			return (!facture.payments || facture.payments.length === 0) &&
				(facture.status === StatutFacture.Encours || facture.status === StatutFacture.Enretard);
		}

	}

	public static imprimerPDF(eleme) {

		CreatePdf.generatePdf(eleme, this.docType, ActionPdf.print);
		/* this.service.visualPdf(this.docType, eleme.id).subscribe(res => {
			const stamp = new Date().getTime();

			//  file type
			const fileType = 'application/pdf';

			//  file data
			const fileData = AppSettings._base64ToArrayBuffer(res.value);

			//  file extension
			const extension = 'pdf';
			const pdfSrc = AppSettings.printPdf(fileData, eleme.reference + '.' + extension, fileType, extension);

			const objFra = document.createElement('iframe');
			objFra.style.visibility = 'hidden';
			objFra.src = pdfSrc;
			document.body.appendChild(objFra);
			objFra.contentWindow.focus();
			objFra.contentWindow.print();
		}); */

	}

	public static getSearchTerms(): any {
		switch (this.docType) {
			case ApiUrl.Invoice:
				return ['reference', 'objet', 'montant', 'nomclient']; break;
			case ApiUrl.User:
				return ['nom', 'prenom', 'userName']; break;
			case ApiUrl.Client:
			case ApiUrl.Fournisseur:
				return ['reference', 'nom']; break;
			case ApiUrl.Groupe:
			case ApiUrl.Lot:
			case ApiUrl.GME:
			case ApiUrl.configModeRegelemnt:
			case ApiUrl.configurationLabel:
			case ApiUrl.configDocumentType:
				return ['nom']
			case ApiUrl.Devis:
			case ApiUrl.avoir:
			case ApiUrl.BonCommandeF:
			case ApiUrl.Depense:
			case ApiUrl.FicheIntervention:
			case ApiUrl.MaintenanceContrat:
			case ApiUrl.VisitMaintenance:
				return ['reference']; break;
			case ApiUrl.Payment:
				return ['description']; break;
			case ApiUrl.typesMission:
				return ['value']
			default: return null;
		}
	}

	public static getCreateRoute() {
		switch (this.docType) {
			// contacts
			case ApiUrl.Client: return `/clients/create`; break;
			case ApiUrl.Fournisseur: return `/fournisseurs/create`; break;
			case ApiUrl.User: return `/utilisateurs/create`; break;
			case ApiUrl.avoir: return `/avoirs/create`; break;
			case ApiUrl.Depense: return `/depense/create`; break;
			case ApiUrl.Payment: return `/paiements/ajouter`; break;
			case ApiUrl.GME: return `/gammemaintenanceequipements/create`; break;
			case ApiUrl.FicheIntervention: return `/ficheintervention/create`; break;
			case ApiUrl.MaintenanceContrat: return `/contratentretiens/create`; break;
			case ApiUrl.MaintenanceOperationSheet: return this.isPlanification != null ? `/planification/create` : `/ficheinterventionmaintenance/create`; break;
			case ApiUrl.Groupe: return DialogHelper.openDialog(
				this.dialog,
				AddGroupeComponent,
				DialogHelper.SIZE_MEDIUM,
				{}
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Chantier: return DialogHelper.openDialog(
				this.dialog,
				chantierFromComponent,
				DialogHelper.SIZE_LARGE,
				{ data: { defaultData: null, type: IFormType.add } }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Lot: return DialogHelper.openDialog(
				this.dialog,
				lotsFromComponent,
				DialogHelper.SIZE_LARGE,
				{ type: IFormType.add, defaultData: null }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.configModeRegelemnt: return DialogHelper.openDialog(
				this.dialog,
				AddmodeReglementComponent,
				DialogHelper.SIZE_LARGE,
				{ selected: null, show: false }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.configurationLabel: return DialogHelper.openDialog(
				this.dialog,
				AddlabelComponent,
				DialogHelper.SIZE_LARGE,
				{ selected: null, show: false }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.configDocumentType: return DialogHelper.openDialog(
				this.dialog,
				AddtypeDComponent,
				DialogHelper.SIZE_LARGE,
				{ selected: null, show: false }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.Devis: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/1/create/devis` : `/devis/create`; break;
			case ApiUrl.Invoice: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/create/facture` : `/factures/create`; break;
			case ApiUrl.BonCommandeF: return this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/create/commandes` : `/bonCommandeFournisseur/create`; break;
			case ApiUrl.Recurring: return `/factureReccurente/ajouter`; break;
			case ApiUrl.Mission: return DialogHelper.openDialog(
				this.dialog,
				AgendaFormComponent,
				DialogHelper.SIZE_SMALLTache,
				{ hasBackdrop: true, dateSelectd: new Date(), isMission: true }
			).subscribe(async response => {
				this.initData();
			}); break;
			case ApiUrl.typesMission: return DialogHelper.openDialog(
				this.dialog,
				AddTypekindComponent,
				DialogHelper.SIZE_SMALLTache,
				{ show: false, missionKind: ManageDataTable.typeKinds }
			).subscribe(async response => {
				if (response) {
					this.initData();
				}

			}); break;
			default: return '';
		}
	}

	public static duppliquer(doc) {
		switch (this.docType) {
			case ApiUrl.Devis: return GlobalInstances.router.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3/dupliquer-create/${doc.id}` : `/devis/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.Invoice: return GlobalInstances.router.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5/dupliquer-create/${doc.id}` : `/factures/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.avoir: return GlobalInstances.router.navigate([`/avoirs/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.MaintenanceContrat: return GlobalInstances.router.navigate([`/contratentretiens/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.Depense: return GlobalInstances.router.navigate([`/depense/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.FicheIntervention: return GlobalInstances.router.navigate([`/ficheintervention/dupliquer-create/${doc.id}`]); break;
			case ApiUrl.BonCommandeF: return GlobalInstances.router.navigate([this.idChantier && this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2/dupliquer-create/${doc.id}` : `/bonCommandeFournisseur/dupliquer-create/${doc.id}`]); break;
			default: return '';
		}
	}

	public static genererPDF(doc) {
		CreatePdf.generatePdf(doc, this.docType, ActionPdf.download);

		// this.service.visualPdf(this.docType, doc.id).subscribe(res => {
		// 	this.base64 = res.value;
		// 	DialogHelper.openDialog(
		// 		this.dialog,
		// 		VisualiserPdfComponent,
		// 		DialogHelper.SIZE_MEDIUM,
		// 		{ base64: this.base64, docType: this.docType, doc }
		// 	).subscribe(async response => {
		// 		this.initData();
		// 	});
		// });

	}

	public static condiPlanifie(doc) {
		if (doc.status === StatutFicheInterventionMaintenance.Planned || doc.status === StatutFicheInterventionMaintenance.Late) {
			return true;
		} else {
			return false;
		}
	}
	public static changeStatutAreplanifier(doc) {
		DialogHelper.openDialog(
			this.dialog,
			AreplanifierFormComponent,
			DialogHelper.SIZE_MEDIUM,
			{ docType: this.docType, doc }
		).subscribe(async response => {
			// this.getDetails(doc)
			GlobalInstances.router
				.navigate([this.isPlanification != null ? `/planification/detail/${doc.id}` : `/ficheinterventionmaintenance/detail/${doc.id}`, { refresh: (new Date).getTime() }]);


		});
	}
	public static SendMail(doc) {
		DialogHelper.openDialog(
			this.dialog,
			SendMailComponent,
			DialogHelper.SIZE_MEDIUM,
			{ docType: this.docType, doc }
		).subscribe(async response => {
		});
	}

	public static getListRoute() {
		switch (this.docType) {
			// contacts
			case ApiUrl.configDocumentType: return '/parameteres/typedocument'; break;
			case ApiUrl.configurationLabel: return '/parameteres/labels'; break;
			case ApiUrl.configModeRegelemnt: return '/parameteres/modelregelement'; break;
			case ApiUrl.VisitMaintenance: return '/visitesMaintenance'; break;
			case ApiUrl.Client: return `/clients`; break;
			case ApiUrl.Fournisseur: return `/fournisseurs`; break;
			case ApiUrl.User: return `/utilisateurs`; break;
			case ApiUrl.Groupe: return `/groupes`; break;
			case ApiUrl.Produit: return `/produits`; break;
			case ApiUrl.Lot: return `/lots`; break;
			case ApiUrl.Mission: return `/tache`; break;
			case ApiUrl.typesMission: return '/parameteres/agenda'; break;
			case ApiUrl.Devis: return this.idChantier != null ? `/chantiers/${this.idChantier}/documents/3` : `/devis`; break;
			case ApiUrl.Invoice: return this.idChantier != null ? `/chantiers/${this.idChantier}/documents/5` : `/factures`; break;
			case ApiUrl.Recurring: return `/factureReccurente`; break;
			case ApiUrl.avoir: return `/avoirs`; break;
			case ApiUrl.BonCommandeF: return this.idChantier != null ? `/chantiers/${this.idChantier}/documents/2` : `/bonCommandeFournisseur`; break;
			case ApiUrl.Depense: return `/depense`; break;
			case ApiUrl.Payment: return `/paiements`; break;
			case ApiUrl.Chantier: return `/chantiers`; break;
			case ApiUrl.GME: return `/gammemaintenanceequipements`; break;
			case ApiUrl.FicheIntervention: return this.isAgendaGlobal != null ? `/agendaglobal` : `/ficheintervention`; break;
			case ApiUrl.MaintenanceContrat: return `/contratentretiens`; break;
			case ApiUrl.MaintenanceOperationSheet: return this.isPlanification != null ? `/planification` : `/ficheinterventionmaintenance`; break;
			default: return '';
		}
	}

	public static deleteRequest(element) {
		var ref = ApiUrl.Groupe !== ManageDataTable.docType && ApiUrl.Lot !== ManageDataTable.docType ? element.reference : element.name;
		if (!ref) { ref = 'document'; }
		const text = {
			title: GlobalInstances.translateService.instant('suppression.header', { reference: ref }),
			question: GlobalInstances.translateService.instant('suppression.question'),
			cancel: GlobalInstances.translateService.instant('suppression.annuler'),
			confirm: GlobalInstances.translateService.instant('suppression.supprimer'),
		}

		SwalConfiguration.Information(this.delete.bind(this), element, text);
	}

	public static cancelRequest(element) {
		const text = {
			title: GlobalInstances.translateService.instant('annuler.header', { reference: element.reference }),
			question: GlobalInstances.translateService.instant('annuler.question'),
			cancel: GlobalInstances.translateService.instant('annuler.annuler'),
			confirm: GlobalInstances.translateService.instant('annuler.supprimer'),
		}

		SwalConfiguration.Information(this.annuler.bind(this), element, text);
	}


	public static changePassword(element) {

		DialogHelper.openDialog(
			this.dialog,
			ChangePasswordComponent,
			DialogHelper.SIZE_MEDIUM,
			{ userId: element.id }
		).subscribe(async response => {
			this.initData();
		});
	}

	public static initData() {
		ManageDataTable.initFilters();

		if (ManageDataTable.docType === ApiUrl.typesMission) {
			ManageDataTable.filterOptions['missionKinds'] = [ManageDataTable.typeKinds];
		}

		ManageDataTable.getData();
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	public static delete(element) {
		var ref = ApiUrl.Groupe !== ManageDataTable.docType && ApiUrl.Lot !== ManageDataTable.docType ? element.reference : element.name;
		if (!ref) { ref = 'document'; }
		ManageDataTable.service.delete(ManageDataTable.docType, element.id)
			.subscribe((data) => {
				if (data.isSuccess) {
					SwalConfiguration.Success(GlobalInstances.translateService.instant('suppression.done', { reference: ref }));
					this.initData();
				}
			}, (err: any) => {
				if (ApiUrl.User === ManageDataTable.docType) {
					SwalConfiguration.Error(GlobalInstances.translateService.instant('suppression.user'));
				} else if (ApiUrl.configModeRegelemnt === ManageDataTable.docType) {
					if (err.error.messageCode === 100) {
						SwalConfiguration.Error('Impossible de supprimer ce mode de réglement car il est lié à un paiement');
					}
				} else {
					SwalConfiguration.Error(GlobalInstances.translateService.instant('suppression.failed', { reference: ref }));
				}
			});
	}

	public static annuler(element) {

		if (this.docType === ApiUrl.Invoice) {
			this.service.getAll(ApiUrl.Invoice + '/' + element.id + '/Cancel').subscribe(res => {
				if (res) {
					SwalConfiguration.Success(GlobalInstances.translateService.instant('annuler.done', { reference: element.reference }));
					// localStorage.setItem(LocalElements.docType, ApiUrl.avoir);
					GlobalInstances.router.navigate(['/factures/detail', element.id, { refresh: (new Date).getTime() }])
				}
			})
		} else if (this.docType === ApiUrl.Depense) {
			DialogHelper.openDialog(
				this.dialog,
				CancelAvoirComponent,
				DialogHelper.SIZE_MEDIUM,
				{ docType: this.docType, document: element }
			).subscribe(async response => {
				this.getDetails(element);
			});
		} else if (this.docType === ApiUrl.BonCommandeF) {
			element.status = StatutBonCommandeFournisseur.Annule;
			element.workshopId = element.workshop !== null ? element.workshop.id : null;
			element.supplierId = element.supplier !== null ? element.supplier.id : null;
			ManageDataTable.service.update(ApiUrl.BonCommandeF, element, element.id).subscribe(res => {
				SwalConfiguration.Success(GlobalInstances.translateService.instant('annuler.done', { reference: element.reference }));
				this.getDetails(element);
			}, err => { SwalConfiguration.Error(GlobalInstances.translateService.instant('annuler.failed', { reference: element.reference })); });
		} else {
			element.status = StatutDevis.Annulee;
			element.clientId = element.client.id;
			if (this.docType === ApiUrl.FicheIntervention) {
				element['technicians'] = element['technicians'].map(technicien => {
					return (technicien.id);
				});
			}
			ManageDataTable.service.update(this.docType, element, element.id).subscribe(res => {
				SwalConfiguration.Success(GlobalInstances.translateService.instant('annuler.done', { reference: element.reference }));
				this.getDetails(element);
			}, err => { SwalConfiguration.Error(GlobalInstances.translateService.instant('annuler.failed', { reference: element.reference })); });
		}
	}

	public static getRole() {
		ManageDataTable.service.getAll(ApiUrl.role).subscribe(res => {
			ManageDataTable.role = res.value;
			ManageDataTable.getProfils();
		});
	}

	public static getProfils() {
		GlobalInstances.translateService.get('LABELS').subscribe(labels => {
			ManageDataTable.profils.push({ id: ManageDataTable.role.filter(x => x.type === ManageDataTable.userProfile.admin)[0].id, libelle: labels.ADMIN });
			ManageDataTable.profils.push({ id: ManageDataTable.role.filter(x => x.type === ManageDataTable.userProfile.technicien)[0].id, libelle: labels.TECHNICIEN });
			ManageDataTable.profils.push({ id: ManageDataTable.role.filter(x => x.type === ManageDataTable.userProfile.manager)[0].id, libelle: labels.MANAGER });
			ManageDataTable.profils.push({
				id: ManageDataTable.role.filter(x => x.type ===
					ManageDataTable.userProfile.technicienChantier)[0].id, libelle: labels.TECHNICIENCHANTIER
			});
			ManageDataTable.profils.push({
				id: ManageDataTable.role.filter(x => x.type ===
					ManageDataTable.userProfile.technicienMaintenance)[0].id, libelle: labels.TECHNICIENMAINTENANCE
			})
		});
	}

	public static getChantiers() {
		ManageDataTable.service.getAll(ApiUrl.Chantier).subscribe(res => {
			ManageDataTable.listChantiers = res.value;
		});
	}

	public static getClients() {
		ManageDataTable.service.getAll(ApiUrl.Client).subscribe(res => {
			ManageDataTable.listClient = res.value;
		});
	}

	public static getSupplier() {
		ManageDataTable.service.getAll(ApiUrl.Fournisseur).subscribe(res => {
			ManageDataTable.listSupplier = res.value;
		});
	}

	public static GetCompte() {
		ManageDataTable.service.getAll(ApiUrl.configCompte).subscribe(res => {
			ManageDataTable.comptes = res.value;
		});
	}

	public static async getlistMois() {
		ManageDataTable.listMois = Object.entries(moisEnum).map(doc => ({
			label:
				GlobalInstances.translateService.instant('mois.' + doc[0]), value: doc[0]
		})).filter(e => Number.isInteger(+ e.value) === true);
	}

	public static async getPeriodes() {
		const values = Object.keys(PeriodType);
		ManageDataTable.periodes = Object.entries(PeriodType).map(doc => ({
			label:
				GlobalInstances.translateService.instant('periode.' + doc[0]), value: doc[1]
		})).filter(e => Number.isInteger(e.value) === true);
	}

	public static getTechniciens() {
		this.service.getAll(ApiUrl.role).subscribe(res => {
			const role = res.value;
			const listTechnicien = [role.find(x => x.type === UserProfile.technicien).id, role.find(x => x.type ===
				UserProfile.technicienMaintenance).id];
			const filter = {
				roleIds: listTechnicien,
				SearchQuery: '',
				Page: 1,
				PageSize: 500,
				OrderBy: 'firstName',
				SortDirection: 'Descending',
				ignorePagination: true
			};
			this.service.getAllPagination(ApiUrl.User, filter)
				.subscribe(res => {
					ManageDataTable.listTechnicien = res.value.map(doc => ({
						label: doc['firstName'] + ' ' + doc['lastName'], value: doc['id']
					}));
				}, err => { });
		});
	}

	public static FomaterData(data) {
		let list = [];
		data.forEach(element => {
			if (element.credits && element.credits.length !== 0) {
				element['debit'] = element['amount'];
				element['credit'] = 0;
				list = [...list, ...element];
				element.credits.forEach(ele => {
					ele['debit'] = 0;
					ele['credit'] = ele['amount'];
					list.push(ele);
				});
			} else {
				element['debit'] = element['amount'];
				element['credit'] = 0;
				list = [...list, ...element];
			}
		});
		return list;
	}

	public static addPaiementGroupe() {
		DialogHelper.openDialog(
			this.dialog,
			PaiementGroupeComponent,
			DialogHelper.SIZE_MEDIUM,
			{ data: { defaultData: null, type: IFormType.add } }
		).subscribe(async response => {
			if (response) {
				this.initData();

			}
		});
	}
	public static MouvementCompte() {
		DialogHelper.openDialog(
			this.dialog,
			MouvementCompteComponent,
			DialogHelper.SIZE_MEDIUM,
			{ data: { defaultData: null, type: IFormType.add } }
		).subscribe(async response => {
			if (response) {
				this.initData();

			}
		});
	}

}
