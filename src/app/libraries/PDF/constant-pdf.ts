export class ConstantPdf {

	public static widthsTable = ['52.5%', '9.5%', '11%', '8%', '8%', '11%'];
	public static widthsTableFI = ['80%', '20%'];
	public static widthTableFIM = ['45%', '15%', '40%'];


	public static columndesignationArticle = {
		text: 'Désignation',
		bold: true,
		alignment: 'left',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnprixHtArticle = {
		text: 'Prix Ht',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnquantite = {
		text: 'Quantité',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnprixU = {
		text: 'Prix U.',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnremiseTable = {
		text: 'Remise',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columntvaTable = {
		text: 'TVA (%)',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnprixHt = {
		text: 'Total HT',
		bold: true,
		alignment: 'right',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};


	public static columnGammeMaintenance = {
		text: 'Gamme maintenance',
		bold: true,
		alignment: 'center',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnObservation = {
		text: 'Observation',
		bold: true,
		alignment: 'center',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
	public static columnGammeMaintenanceEtat = {
		text: 'Etat',
		bold: true,
		alignment: 'center',
		fontSize: 11,
		fillColor: '#47A2C1',
		color: '#FFFFFF',
		margin: [0, 5, 0, 5]
	};
}
