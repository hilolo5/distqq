import { GlobalInstances } from '../../app.module';
import { Calcule } from '../../calcule/Calcule';
import { ICalcule } from '../../calcule/ICalcule';
import { RoudingNumberPipe } from '../../common/Pipes/rounding-number.pipe';
import { TypeArticle, TypeValue } from '../../Enums/typeValue.Enums';
import { CalculTva } from '../../Models/Model/calcul-tva';

export class CalculPdf {

	calcule: ICalcule = new Calcule();
	roudingNumberPipe = new RoudingNumberPipe()

	RoundNumber(total) {
		return GlobalInstances.currencyPipe.transform(this.roudingNumberPipe.transform(total));
	}

	calculTotalHtProduct(product) {
		const remise = product.discount.type === TypeValue.Percentage ?
		product.totalHT * product.quantity (product.discount.value / 100) : product.discount.value;

		return product.totalHT * product.quantity - remise
	}

	groupTVA(orderDetails): CalculTva[] {
		let calculTvas = [];
		// if (orderDetails.globalVAT_Value === -1) {
			const articles = this.getProductsOfArticles(orderDetails.lineItems);
			calculTvas = this.calcule.calculVentilationRemise(
				articles,
				orderDetails.totalHT,
				orderDetails.globalDiscount.value,
				orderDetails.globalDiscount.type
			);
		// }
		// else {
		// 	const totalTTC = this.clalcTotalGeneral() * (this.tvaGlobal / 100 + 1);
		// 	this.calculTvas.push({
		// 		tva: this.tvaGlobal,
		// 		totalHT: this.clalcTotalGeneral(),
		// 		totalTTC,
		// 		totalTVA: totalTTC - this.clalcTotalGeneral(),
		// 		finalValue: 0,
		// 		percente: 0,
		// 		value: 0,
		// 	});
		// }
		return calculTvas;
	}
	getProductsOfArticles(articles) {
		const articlesTmp = [];
		articles.forEach(article => {

			if (article.type === TypeArticle.produit) {
				article.product.quantity = article.quantity;
				articlesTmp.push(article.product);
			}

			if (article.type === TypeArticle.lot) {
				article.product.products.map(e => {
					e.product.quantity = e.quantity;
					articlesTmp.push(e.product);
				});
			}
		});
		return articlesTmp;
	}
}
