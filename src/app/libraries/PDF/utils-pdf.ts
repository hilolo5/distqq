export class UtilsPDf {
	static removeNullText(text: string) {
		return text.replace('null', '');
	}
}