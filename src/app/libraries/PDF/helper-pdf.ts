import { ConstantPdf } from './constant-pdf';
import { TypeValue, TypeArticle, EtatIntervention } from '../../Enums/typeValue.Enums';
import { Calcul } from '../../utils/calcul';
import { ApiUrl } from '../../Enums/Configuration/api-url.enum';
import htmlToPdfmake from 'html-to-pdfmake';
import { GlobalInstances } from '../../app.module';
import { RoudingNumberPipe } from '../../common/Pipes/rounding-number.pipe';
import { data } from 'jquery';
import { ConstantInofsSupp } from './constant-infos-supp';
import { CalculPdf } from './calcul-pdf';
import { UtilsPDf } from './utils-pdf';
import { TypeFacture } from '../../Enums/TypeFacture.enum';
import { element } from 'protractor';

export class HelperPdf {

	static BodyTable = [];

	static calcul = new CalculPdf();


	public static getNamedocType(docType) {
		switch (docType) {
			case ApiUrl.Devis:
				return 'Devis'
			case ApiUrl.Invoice:
				return 'Facture'
			case ApiUrl.avoir:
				return 'Avoir'
			case ApiUrl.MaintenanceOperationSheet:
			case ApiUrl.FicheIntervention:
				return 'Intervention'

			default:
				break;
		}
	}

	public static getReference(document, docType) {
		return [
			{
				colSpan: 2,
				table: {
					widths: ['auto', '2%', 'auto'],
					body: [
						[
							{
								colSpan: 3, text: `${this.getNamedocType(docType)}  ${document.reference}`, fontSize: 12,
								margin: [5, 0, 0, 0], bold: true,
							}, {}, {}
						],
						// columnAssocie,
						[
							{
								text: 'Date de création', fontSize: 10,
								margin: [5, -4, 0, 0],
							},
							{
								text: ':', fontSize: 10,
								margin: [0, -4, 0, 0]
							},
							{
								text: GlobalInstances.datePipe.transform(new Date(
									(document.creationDate || document.startDate)
								), 'dd-MM-yyyy'), fontSize: 10, alignment: 'right',
								margin: [0, -4, 3, 0]
							}
						],
						[
							{
								text: 'Date d\'échéance', fontSize: 10,
								margin: [5, -4, 0, 0],
							},
							{
								text: ':', fontSize: 10,
								margin: [0, -4, 0, 0]
							},
							{
								text: GlobalInstances.datePipe.transform(new Date(
									(document.creationDate || document.endDate)
								), 'dd-MM-yyyy'), fontSize: 10, alignment: 'right',
								margin: [0, -4, 3, 0]
							}
						]
					]
				},
				layout: 'noBorders',
				margin: [0, 0, 3, 0]
			},
			{}
		];
	}

	public static infoClient(client) {
		return [
			[
				{
					text: client.name,
					fontSize: 12,
					bold: true,
					margin: [5, 0, 0, 0]
				}
			],
			[
				{
					text: UtilsPDf.removeNullText(`${client.billingAddress.street}
						${client.billingAddress.postalCode} ${client.billingAddress.city}
						${client.billingAddress.countryCode}`),
					fontSize: 10, margin: [5, 0, 0, 0]
				}
			]
		];

	}

	public static generateColRefAndClient(document, docType) {
		return {
			columns: [
				{
					width: '65%',
					table: {
						headerRows: 1,
						widths: ['80%', '25%'],
						body: [this.getReference(document, docType)]
					},
					layout: 'noBorders',
				},
				{
					width: '35%',
					table: {
						headerRows: 1,
						widths: ['auto'],
						body: this.infoClient(document.client)
					},
					layout: 'noBorders',
					margin: [-30, 0, 0, 0]
				}
			],
			columnGap: 10,
			margin: [-20, 0, -20, 0]
		}

	}

	public static getObjet(purpose) {
		return {
			id: 'objet',
			table: {
				width: ['auto', '*'],
				body: [
					[
						{ text: 'Objet : ', bold: true, fontSize: 12 },
						{ text: purpose, fontSize: 10, margin: [0, 2, 0, 0] }
					]
				]
			},
			layout: 'noBorders',
			margin: [-20, 15, 0, 0]
		};
	}

	public static getTable(document, docType) {

		/* document.orderDetails.lineItems */
		// product type Lot/article/ligne

		const headerTable = [];
		let widthTable = ConstantPdf.widthsTableFI;
		this.BodyTable = [];
		headerTable.push(
			ConstantPdf.columndesignationArticle,
			ConstantPdf.columnquantite);
		if ([ApiUrl.Invoice, ApiUrl.Devis, ApiUrl.avoir].includes(docType)) {
			headerTable.push(
				ConstantPdf.columnprixHtArticle,
				ConstantPdf.columnremiseTable,
				ConstantPdf.columntvaTable,
				ConstantPdf.columnprixHt,
			)
			widthTable = ConstantPdf.widthsTable;
		}
		this.BodyTable.push(headerTable);
		document.orderDetails.lineItems.forEach(product => {
			if (product.type === TypeArticle.produit) {
				this.addArticleToTable(product.product, docType, false)
			} else {
				this.AddLigneOrLotDesignation(product.product, product.type);
				if (product.type === TypeArticle.lot) {
					product.product.products.forEach(article => {
						this.addArticleToTable(article.product, docType, true);
					});
				}
			}
		});
		return {
			id: 'tableContent',
			table: {
				headerRows: 0,
				widths: widthTable,
				body: this.BodyTable
			},
			layout: {
				hLineWidth: (i, node) => {
					return (i === 0 || i === 1 || i === node.table.body.length) ? 1 : 0;
				}
			},
			margin: [-20, 15, -20, 5]
		};
	}


	public static addArticleToTable(product, docType, isProductLot: boolean) {

		const marginLeft = isProductLot ? 15 : 2
		const dataPDF = [];
		dataPDF.push({
			text: `- ${product.designation || product.name}`,
			margin: [marginLeft, 5, 0, 5],
			fontSize: 10,
			bold: true
		});

		dataPDF.push({
			text: product.quantity,
			alignment: 'right',
			margin: [marginLeft, 2, 0, 2],
			fontSize: 9
		});
		if ([ApiUrl.Invoice, ApiUrl.Devis, ApiUrl.avoir].includes(docType)) {

			dataPDF.push({
				text: this.calcul.RoundNumber(product.totalHT),
				alignment: 'right',
				margin: [marginLeft, 2, 0, 2],
				fontSize: 9
			});

			let valeurRemise = product.discount.value;
			if (product.discount.type === TypeValue.Percentage) {
				valeurRemise = (product.totalHT * 1 * (product.discount.value / 100));
			}

			dataPDF.push({
				text: valeurRemise,
				alignment: 'right',
				margin: [marginLeft, 2, 0, 2],
				fontSize: 9
			});

			dataPDF.push({
				text: product.vat,
				alignment: 'right',
				margin: [marginLeft, 2, 0, 2],
				fontSize: 9
			});

			dataPDF.push({
				text: this.calcul.RoundNumber(this.calcul.calculTotalHtProduct(product)),
				alignment: 'right',
				margin: [marginLeft, 2, 0, 2],
				fontSize: 9
			});
		}
		this.BodyTable.push(dataPDF);
		if (product.description) {
			const description = [];
			const htmlDescription = this.htmlToPdf(product.description);
			if (Array.isArray(htmlDescription)) {
				for (let index = 0; index < htmlDescription.length; index++) {
					// tslint:disable-next-line:no-shadowed-variable
					const element = htmlDescription[index];
					element.margin = [marginLeft + 20, -5, 5, 3];
					element.italics = true;
					element.fontSize = 9;
				}
			}
			description.push(
				htmlDescription
			);
			for (let i = 1; i <= dataPDF.length - 1; i++) {
				description.push({ text: '', });
			}
			this.BodyTable.push(description);
		}

	}

	public static AddLigneOrLotDesignation(product, type) {

		const ProductInfos = [];
		if (type === TypeArticle.lot) {
			ProductInfos.push({
				text: product.name, margin: [0, 5, 0, 5],
				bold: true
			});
		} else {
			ProductInfos.push({
				text: `${product.designation} - ${product.description}`, margin: [0, 5, 0, 5],
			});
		}



		for (let i = 1; i <= ConstantPdf.widthsTable.length - 1; i++) {
			ProductInfos.push({ text: '', });
		}

		this.BodyTable.push(ProductInfos)
	}


	/** content table Total et Condition Regelment note */
	public static TableCalculTotal(document) {
		const orderDetails = document.orderDetails;
		const totalHtSansRemise = new Calcul().calculGeneralHt(orderDetails.lineItems);
		const remiseValueGlobal = orderDetails.globalDiscount.type === TypeValue.Percentage ?
			totalHtSansRemise * (orderDetails.globalDiscount.value / 100) : orderDetails.globalDiscount.value;

		const TotalHtTable = [
			{ text: 'Sous Total HT', margin: [5, 3, 2, 3], fontSize: 9 },
			{
				text: this.calcul.RoundNumber(totalHtSansRemise),
				alignment: 'right',
				margin: [5, 3, 2, 3],
				fontSize: 9
			}
		];

		const remiseGlobal = [
			{ text: 'Remise Globale', margin: [5, 3, 2, 3], fontSize: 9 },
			{
				text: this.calcul.RoundNumber(remiseValueGlobal),
				alignment: 'right',

				margin: [5, 3, 2, 3],
				fontSize: 9
			}
		];

		const MontantHt = [
			{ text: 'Montant HT', margin: [5, 3, 2, 3], fontSize: 9 },
			{
				text: this.calcul.RoundNumber(totalHtSansRemise - remiseValueGlobal),
				alignment: 'right',

				margin: [5, 3, 2, 3],
				fontSize: 9
			}
		];

		const body = [];
		if (remiseValueGlobal > 0) {
			body.push(TotalHtTable);
			body.push(remiseGlobal);
		}
		body.push(MontantHt);
		const gourpTVA = this.calcul.groupTVA(orderDetails);
		let totalTva = 0;
		if (gourpTVA.length > 0) {
			gourpTVA.forEach(tva => {
				totalTva += tva.totalTVA;
				body.push(
					[{ text: `TVA ${tva.tva}%`, margin: [5, 3, 2, 3], fontSize: 9 },
					{
						text: this.calcul.RoundNumber(tva.totalTVA),
						alignment: 'right',
						margin: [5, 3, 2, 3],
						fontSize: 9
					}]
				);
			});
		}

		const totalTTc = [
			{
				text: 'Total TTC',
				fillColor: '#47A2C1',
				color: 'white',
				margin: [5, 3, 2, 3],
				bold: true,
				fontSize: 13
			},
			{
				text: this.calcul.RoundNumber(totalHtSansRemise + totalTva - remiseValueGlobal),
				alignment: 'right',
				fillColor: '#47A2C1',
				color: 'white',
				margin: [5, 3, 2, 3],
				bold: true,
				fontSize: 14
			}
		];
		body.push(totalTTc);



		if (document.typeInvoice === TypeFacture.Cloturer) {
			const refDevis = document.associatedDocuments.find(x => x.id === document.quoteId).reference;
			body.push([
				{
					text: `Devis initiale - ${refDevis}`,
					margin: [5, 3, 2, 3],
					fontSize: 9
				},
				{
					text: this.calcul.RoundNumber(totalHtSansRemise + totalTva - remiseValueGlobal),
					alignment: 'right',
					margin: [5, 3, 2, 3],
					fontSize: 9
				}
			]);
			document.quote.situations.forEach(ele => {
				let text = (ele.typeInvoice === TypeFacture.Acompte ? 'Acompte' : 'Situation') + ` (${ele.situation}%) - ${ele.reference}`;
				let montant = ele.totalTTC;
				if (ele.typeInvoice === TypeFacture.Cloturer) {
					text = 'Reste à payer';
					montant = orderDetails.totalTTC
				}
				body.push([
					{
						text: text,
						margin: [5, 3, 2, 3],
						fontSize: 9
					},
					{
						text: this.calcul.RoundNumber(montant),
						alignment: 'right',
						margin: [5, 3, 2, 3],
						fontSize: 9
					}
				]);
			});
		}

		return {
			id: 'totalTtc',
			headerRows: body.length,
			widths: ['*', 'auto'],
			body: body
		};
	}

	public static ColmunconditionReglement(paymentCondition) {
		return !paymentCondition ? '' : {
			width: '50%',
			table: {
				headerRows: 1,
				widths: ['auto'],
				body: [
					[
						{
							text: 'Conditions de règlement',
							bold: true,
						}
					],
					[
						this.htmlToPdf(paymentCondition)
					]
				]
			},
			layout: 'noBorders', margin: [0, 10, 0, 0]
		};
	}

	public static ConditioRegAndTableTotal(document) {
		return {
			id: 'conditionReglement',
			columns: [
				this.ColmunconditionReglement(document.paymentCondition),
				{
					width: '50%',
					table: this.TableCalculTotal(document),
					layout: {
						vLineWidth: (i, node) => {
							return (i === 0 || i === node.table.widths.length) ? 1 : 0;
						},
						hLineWidth: (i, node) => {
							return (i === 0 || i === node.table.body.length) ? 1 : 0;
						}
					},
					margin: [18, 0, -20, 0]
				}
			]
		};
	}

	public static Note(note) {
		return {
			id: 'note',
			table: {
				widths: ['100%'],
				body: [
					[
						{ text: 'Note', bold: true }
					],
					[
						this.htmlToPdf(note)
					]
				]
			},
			layout: 'noBorders',
			margin: [0, 10, 0, 20]
		};
	}

	/** signature Cachet */
	public static CachetEtSiganture(document, docType, parametragePdf) {
		let tableSign;
		let luApprover;
		const siganture = document.clientSignature;
		const imgCachet = parametragePdf.images.cachet ?
			{ image: parametragePdf.images.cachet, width: 150, margin: [30, 10, 0, 0] } :
			{ text: ' ' };

		if (siganture) {

			luApprover = {
				text: 'Lu et approuvé le : ' + GlobalInstances.datePipe.transform(new Date(
					(siganture.signatureDate)
				), 'dd-MM-yyyy')
			};
		}

		if (docType === ApiUrl.Devis) {
			tableSign = {
				width: '40%',
				stack: [
					{ text: 'Signature Client', bold: true }
				]
			};
			if (siganture) {
				tableSign = {
					width: '40%',
					stack: [
						{ text: 'Signature Client', bold: true },
						{ image: siganture.imageContent, width: 117, height: 95, margin: [0, 5, 0, 0] },
						luApprover
					]
				};
			}
			return {
				id: 'signature',
				columns: [
					{
						id: 'cachet', width: '60%', stack: [
							imgCachet
						]
					},
					tableSign
				], margin: [0, 15, 0, 0]
			};
		} else if (docType === ApiUrl.Invoice) {
			return {
				id: 'signature',
				columns: [
					{ width: '60%', text: '' },
					{
						id: 'cachet',
						width: '40%',
						stack: [
							imgCachet
						]
					}
				], margin: [0, 15, 0, 0]
			};
		}
	}



	/** Contrat maintenance */
	public static generateTableGME(document, docType) {
		const headerTable = [];
		this.BodyTable = [];
		headerTable.push(
			ConstantPdf.columnGammeMaintenance,
			ConstantPdf.columnGammeMaintenanceEtat,
			ConstantPdf.columnObservation,
		)
		this.BodyTable.push(headerTable);
		document.equipmentDetails.forEach(gme => {
			this.addGmeToTable(gme, document.observations);
		});
		return {
			id: 'tableContent',
			table: {
				headerRows: 0,
				widths: ConstantPdf.widthTableFIM,
				body: this.BodyTable
			},
			layout: {
				hLineWidth: (i, node) => {
					return (i === 0 || i === 1 || i === node.table.body.length || docType === ApiUrl.MaintenanceOperationSheet) ? 1 : 0;
				}
			},
			margin: [-20, 15, -20, 5]
		};
	}
	public static generateOperation(subOper, observations, isSubOper) {
		const dataPDF = []
		const observation = observations.find(x => x.name === subOper.name);
		if (observation) {
			dataPDF.push({
				text: (isSubOper ? '- ' : '') + observation.name,
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'left',
				italic: isSubOper
			});
			dataPDF.push({
				text: observation.status === EtatIntervention.OK ? 'OK' : 'NOK',
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'center',
				italic: isSubOper
			});
			dataPDF.push({
				text: observation.observation,
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'left',
				italic: isSubOper
			});
			this.BodyTable.push(dataPDF);
		} else {
			dataPDF.push({
				text: (isSubOper ? '- ' : '') + subOper.name,
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'left',
				italic: isSubOper
			});
			dataPDF.push({
				text: '',
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'center',
				italic: isSubOper
			});
			dataPDF.push({
				text: '',
				margin: [20, 5, 0, 5],
				fontSize: 10,
				alignment: 'left',
				italic: isSubOper
			});
			this.BodyTable.push(dataPDF);
		}

	}
	public static addGmeToTable(gme, observations) {
		let dataPDF = [];
		dataPDF.push({
			text: gme.equipmentName,
			margin: [2, 5, 0, 5],
			fontSize: 10,
			bold: true,
			colSpan: 3,
			alignment: 'center'
		}, {}, {});
		this.BodyTable.push(dataPDF);
		dataPDF = [];
		gme.maintenanceOperations.forEach(operation => {
			if (operation.periodicity.length > 0) {
				this.generateOperation(operation, observations, false);
			} else {
				dataPDF.push({
					text: '- ' + operation.name,
					margin: [15, 5, 0, 5],
					fontSize: 10,
					colspan: 3,
					alignment: 'left'
				}, {}, {});
				this.BodyTable.push(dataPDF);
				dataPDF = [];
				operation.subOperations.forEach(subOper => {

					this.generateOperation(subOper, observations, true);

				});
			}
		});

	}

	public static rapportContratMaintenance(report) {
		return {
			id: 'note',
			table: {
				widths: ['100%'],
				body: [
					[
						{ text: 'Rapport', bold: true }
					],
					[
						this.htmlToPdf(report)
					]
				]
			},
			layout: 'noBorders',
			margin: [0, 10, 0, 20]
		};
	}

	public static siganturesContratMaintenance(document, marginTop) {
		const signatureTech = document.technicianSignature ?
			{ image: document.technicianSignature.imageContent, width: 117, height: 95, margin: [0, 5, 0, 0] } :
			{ text: ' ' };
		const signatureClient = document.clientSignature ?
			{ image: document.clientSignature.imageContent, width: 117, height: 95, margin: [0, 5, 0, 0] } :
			{ text: ' ' };
		return {
			id: 'signature',
			columns: [
				{
					width: '50%',
					stack: [
						{ text: 'Signature technicien', bold: true },
						signatureTech,
						{
							text: document.technicianSignature ? 'Lu et approuvé le : ' + GlobalInstances.datePipe.transform(new Date(
								(document.technicianSignature.signatureDate)
							), 'dd-MM-yyyy') : ''
						}
					]
				},
				{
					width: '50%',
					stack: [
						{ text: 'Signature client', bold: true },
						signatureClient,
						{
							text: document.clientSignature ? 'Lu et approuvé le : ' + GlobalInstances.datePipe.transform(new Date(
								(document.clientSignature.signatureDate)
							), 'dd-MM-yyyy') : ''
						}
					]
				}
			], margin: [0, marginTop, 0, 0]
		};
	}


	/**
	 * convert html text to PDF
	 */
	public static htmlToPdf = (text: string) => {

		try {
			const ret = htmlToPdfmake(text);

			if (typeof ret !== 'string') {
				ret.forEach((e: any) => {
					delete e.style;
					delete e.margin;
				});
			}

			return ret;
		} catch (e) {

		}
	}

	/**
	 * Add certif Borel
	 */
	public static certifBorel() {
		return {
			table: {
				width: ['*'],
				body: [
					[
						{ image: ConstantInofsSupp.LOGOCERTIFBOREL, width: 220, margin: [0, 5, 0, 0] },
					]
				]
			},
			layout: 'noBorders',
			margin: [-20, 15, 0, 0]
		};
	}

	public static RemerciementBorel() {
		return {
			table: {
				widths: ['100%'],
				body: [
					[
						{
							text: `Vous remerciant de la confiance que vous m'avez accordé, veuillez agréer, Monsieur, l'expression de mes sentiments respectueux`,
							fontSize: 9
						}
					],
					[
						{
							image: ConstantInofsSupp.signatureBorel, width: 100, height: 50,
							margin: [0, 15, 0, 0], alignment: 'center'

						},
					]
				]
			},
			layout: 'noBorders',
			margin: [0, 80, 0, 20]
		};
	}
}
