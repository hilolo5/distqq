
/**
 * general configuration
 */
declare var swal: any;
declare var toastr: any;

export class SwalConfiguration {

	public static Success(message: string) {
		swal(message, '', 'success');
	}

	public static Error(message: string) {
		swal(message, '', 'error');
	}


	public static Message(message: string) {
	}


	public static Information(success , id , text) {

			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm && success ) {
						success(id);
				} else {
					// toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			// });
		});
	}

}
