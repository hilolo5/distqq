
export class HelperFunctions {

	/**
	 * arrondir number
	 */
	static toFixed = (number: string, toFix: number) => Number.parseFloat(number).toFixed(toFix);

	/**
	 * spaces number
	 */
	static numberWithSpaces(x) {
		const parts = x.toString().split('.');
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
		return parts.join('.');
	}

	/**
	 * remove undefined values from list
	 */
	static removeUndefinedFromList = (items: any) => items.filter(function (el) {
		return el !== undefined;
	})

	static removeHtmlTags = (html): string => html ? html.toString().replace(/<(.|\n)*?>/g, '') : '';

	/**
	 * //FIXME: must change the way of detecting documents
	 */
	// static isAvoir = () => localStorage.getItem(LocalElements.DOCUMENT) === Identificator.avoirDocId;

	/**
	 * Generates a color based off of a string
	 *
	 * @param str The input string
	 */
	public static generateColor(str: string = '') {
		let hash = 0;
		let color = '#';

		for (let i = 0; i < str.length; i++) {
			// tslint:disable-next-line:no-bitwise
			hash = str.charCodeAt(i) + ((hash << 5) - hash);
		}

		for (let i = 0; i < 3; i++) {
			// tslint:disable-next-line:no-bitwise
			const value = (hash >> (i * 8)) & 0xff;
			color += ('00' + value.toString(16)).substr(-2);
		}

		return color;
	}
}
