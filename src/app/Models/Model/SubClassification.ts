export class SubClassification {
	id?: string;
	type: string;
    label: string;
    description:Text;
	chartAccountItemId: string;
	parentId?: Number;
}
