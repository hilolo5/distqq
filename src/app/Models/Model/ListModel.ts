import { User } from '../Entities/User';
import { Client } from '../Entities/Contacts/Client';
import { Groupe } from '../Entities/Contacts/Groupe';
import { Supplier } from '../Entities/Contacts/Supplier';
import { Quote } from '../Entities/Documents/Quote';
import { Workshop } from '../Entities/Documents/Workshop';
import { OperationSheet } from '../Entities/Documents/OperationSheet';
import { Invoice } from '../Entities/Documents/Invoice';
import { SupplierOrder } from '../Entities/Documents/SupplierOrder';
import { Payment } from '../Entities/Documents/Payments';
import { GammeMaintenanceEquipement } from '../Entities/Maintenance/GammeMaintenanceEquipement';

export class ListModel {
	public currentPage: number;
	public pagesCount: number;
	public pageSize: number;
	public rowsCount: number;
	public firstRowOnPage: number;
	public lastRowOnPage: number;
	public count: number;
	public isListEmpty: boolean;
	public hasValue: boolean;
	public status: number;
	public message: string;
	public messageCode: number;
	public error: any
	public hasError: boolean;
	public isSuccess: boolean;
}

export class PagedResult extends ListModel {
	public value: any[];
}

export class ListUserModule extends ListModel {
	public value: User[];
}

export class ClientListModel extends ListModel {
	public value: Client[];
}

export class GroupeListModel extends ListModel {
	public value: Groupe[];
}

export class FournisseurListModel extends ListModel {
	public value: Supplier[];
}

export class WorkShopListModel extends ListModel {
	public value: Workshop[];
}

export class QuoteListModel extends ListModel {
	public value: Quote[];
}

export class OperationSheetListModel extends ListModel {
	public value: OperationSheet[];
}

export class InvoiceListModel extends ListModel {
	public value: Invoice[];
}

export class CreditListModel extends ListModel {
	public value: Invoice[];
}

export class SupplierOrderListModel extends ListModel {
	public value: SupplierOrder[];
}

export class ExpenseListModel extends ListModel {
	public value: SupplierOrder[];
}

export class PaymentListModel extends ListModel {
	public value: Payment[];
}

export class GammeMEListModel extends ListModel {
	public value: GammeMaintenanceEquipement[];
}



















