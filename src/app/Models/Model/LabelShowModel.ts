export class LabelShowModel {
	id: string;
	name: string;
	disabled: boolean;
}
