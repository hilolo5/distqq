import { StatutFicheIntervention } from "app/Enums/Statut/StatutFicheIntervention.enum";

export class ChangeStatutBodyRequestIntervention{
    idFicheIntervention:number;
    statutFicheIntervention:StatutFicheIntervention;
}