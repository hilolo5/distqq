export class ConfigMessagerie {
	id: number;
	username: string;
	password: string;
	server: string;
	port: string;
	ssl: number;
}
