import { Adresse } from '../Commun/Adresse';

export class DashboardChantier {
	workshopsByStatus: ChantierParStatus[];
	operationSheetsByStatus: FicheInterventionParStatus[];
	topWorkshopsByTurnover: ChantierDetail[];
	topWorkshopsByWorkingHours: ChantierDetail[];
	marginDetails: EvolutionMarge;
}
export class FicheInterventionParStatus {
	status: string;
	operationSheets: InterventionDetail[];
}

export class InterventionDetail {
	id: string;
	reference: string;
	status: string;
	workshops: ChantierDetail[];
}
export class GroupeDetail {
	idGroupe: number;
	name: string;
}

export class ClientDetails {
	id: string;
	reference: string;
	name: string;
	billingAddress: Adresse;
	accountingCode: string;
	code: string;
	// groupe: GroupeDetail;
}



export class ChantierDetail {
	id: string;
	status: string;
	name: string;
	// ca: number
	client: ClientDetails;
	turnover: 0
	workingHours: 0
}

export class ChantierParStatus {
	// nbrChantier: number;
	status: string;
	// totalCA: number;
	workshops: ChantierDetail[];
	totalTurnover: number;
	totalWorkingHours: number;
}





export class Top10ChantierParCA {
	idChantier: number;
	name: string;
	cA: number;
	clientDetails: ClientDetails
}

export class Top10ChantierParMainOeuvre {
	idChantier: number;
	name: string;
	CA: number;
	clientDetails: ClientDetails;
}

export class EvolutionMarge {
	wokshopMargin: number
	workforceMargin: number
	subcontracting: number
	// chantierList: ChantierList[];
}

export class ChantierList {
	iChantier: number
	nom: string;
}
