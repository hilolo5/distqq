import { DetailArticle } from "./dashboardAchat";


export class DashboardVenteModel {
	turnoverDetails;
	topClients: Top10CAparClients[];
	productsDetails;
	quoteDetails;
	devisParStatus: DevisParStatus[];
	tauxDevis: TauxDevis;
	objectifAnnuelCA: ObjectifsAnnuelCA;
}

export class ObjectifsAnnuelCA {
	percentage: string;
	caYears: number;
	monthlyDetails: ObjectifsMensuelleCA[];
	type: number;
}

export class ObjectifsMensuelleCA {
	month: number
	total: number
	percentage: number
}

export class TauxDevis {
	datailsDevis: DevisDetail[];
	tauxDevis: number;
}
export class EvolutionMensuelleCAMarge {
	ca: number;
	marge: number;
	mois: number;
}

export class FactureDetail {
	id: string;
	reference: string;
	dateCreation: Date;
	totalHt: number;
	status: number
}


export class Top10CAparClients {
	id: string;
	name: string;
	reference: string;
	total: number;
}

export class Top10CAparLabel {
	ca: number;
	articleDetails: DetailArticle;
}

export class LabelDetails {
	ca: number;
	id: number;
	name: string;
}


export class GroupeDetailModel {
	idGroupe: number;
	name: string;
}

export class ClientDetailsModel {
	idClient: number;
	name: string;
	groupe: GroupeDetailModel;
}

export class DevisParStatusCA {
	totalCA: number;
	devisParStatus: DevisParStatus;
}

export class DevisParStatus {
	status: string;
	percent: number;
	quotes: DevisDetail[];
}


export class DevisDetail {
	documentType: number;
	id: string;
	reference: string;
	status: string;
	totalHT: number;
	totalTTC: number;
}
