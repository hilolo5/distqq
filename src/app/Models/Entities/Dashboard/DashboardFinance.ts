

export class DashboardFinanceModel {
	turnoverDetails: TournoverDetails;
	turnoverIncreaseDetails;
	turnoverAchievementDetails;
}

export class ProgressionCANMunsuelle {
	type: number;
	years: number;
	evolutionCANMunsuelle: EvolutionCANMunsuelle[];

}

export class EvolutionCANMunsuelle {
	ca: number;
	marge: number;
	mois: number;
}

export class ProgressionCANAnnuelle {
	type: number;
	ca: number;
	marge: number;
	year: number;
}

export class TournoverDetails {
	totalIncome: number;
	totalUnpaidIncome: number;
	totalExpenses: number;
	totalUnpaidExpenses: number;
}

export class ChiffresAffaireFinance {
	montantEncaissements: number;
	cArestantencaisser: number;
	achatrestantencaisser: number;
	soldeFinancier: number;
}