
export class DashboardAchatModel {
	topSuppliers: FournisseurDetail[];
	selectedProductPurchaseDetails;
	topProductsByQuantity: DetailArticle[];
	topProductsBySupplierPrice: DetailArticle[];
}

export class FournisseurDetail {
	supplier: Supplier;
	countTransaction: number;
	totalTransaction: number;
	expensesDetails: DetailDepense[];
}

export class Supplier {
	id: string;
	reference: string;
	name: string;
}

export class DetailDepense {
	documentType: string;
	id: string;
	reference: string;
	status: string;
	totalHT: number;
	totalTTC: number;
}

export class DetailArticle {
	id: string;
	name: string;
	reference: string;
	totalOrders: number;
	countOrders: number;
	category: any;
	labels: any;
	suppliers: SupplierAchat[]
}

export class ReparationAchatParFournisseur {

	id: string;
	name: string;
	reference: string;
	totalOrders: number;
	countOrders: number;
	category: any;
	labels: any;
	suppliers: SupplierAchat[]
}

export class SupplierAchat {
	id: string;
	name: string;
	reference: string;
	price: number;
	quantity: number;
}



