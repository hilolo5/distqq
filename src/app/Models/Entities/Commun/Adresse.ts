export class Adresse {
	designation: string;
	department: string;
	street: string;
	complement: string;
	city: string;
	postalCode: string;
	countryCode: string;
	isDefault: boolean
}
