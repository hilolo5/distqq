import { AssociatedDoc, ClientSignature, Technicians } from '../Commun/Commun';
import { OrderDetails } from '../Commun/OrderDetails';
import { Adresse } from '../Commun/Adresse';
import { Client } from '../Contacts/Client';
import { Historique } from '../Commun/Historique';
import { Workshop } from './Workshop';
import { Quote } from './Quote';

export class OperationSheet {
	id: string;
	reference: string;
	status: string;
	orderDetails: OrderDetails;
	addressIntervention: Adresse;
	startDate: Date;
	endDate: Date;
	report: string;
	purpose: string;
	visitsCount: number;
	totalBasketConsumption: number;
	clientId: number;
	client: Client;

	clientSignature: ClientSignature;
	technicianSignature: ClientSignature;
	workshop: Workshop;
	memos: [];
	technicians: Technicians[];
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[];
	quoteId: number;
	devis?: Quote;

}
