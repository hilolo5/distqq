import { MinimalProductDetails } from '../Commun/MinimalProductDetails';

export class MinimalLotDetails {
	id: string;
	description: string;
	name: string;
	products: MinimalLotProductDetails[];
}

export class MinimalLotProductDetails {
	productId: string;
	lotId: string;
	quantity: number;
	productDetails: MinimalProductDetails[]
}
