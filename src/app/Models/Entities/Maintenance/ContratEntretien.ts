import { Client } from '../Contacts/Client';
import { Attachement } from '../Commun/DocumentAttacher';
import { OperationMaintenance } from './GammeMaintenanceEquipement';
import { Adresse } from '../Commun/Adresse';
import { Memo } from '../Commun/Memo';
import { Historique } from '../Commun/Historique';
import { PeriodType } from 'app/Enums/Maintenance/typePeriode.Enum';
import { OrderDetails } from '../Commun/OrderDetails';
import { PublishingContract } from '../publishing-contract/publishing-contract';


export class ContratEntretien {
	id: string;
	reference: string;
	status: string;
	site: Adresse;
	startDate: Date | string;
	endDate: Date | string;
	expirationAlertEnabled: boolean;
	expirationAlertPeriod: number;
	expirationAlertPeriodType: PeriodType;
	client: Client;
	memos: Memo[];
	attachments: Attachement[];
	changesHistory: Historique[];
	enableAutoRenewal: boolean;
	equipmentMaintenance: MaintenanceEquipement[];
	orderDetails: OrderDetails;
	associatedDocuments?: any;
	publishingContracts?: PublishingContract[];
}


export class ContratEntretienPut {
	reference: string;
	status: string;
	site: Adresse;
	startDate: any;
	endDate: any;
	clientId: string;
	client: Client;
	expirationAlertEnabled: boolean;
	expirationAlertPeriod: number;
	expirationAlertPeriodType: PeriodType;
	enableAutoRenewal: boolean;
	attachments: Attachement[];
	equipmentMaintenance: MaintenanceEquipement[];
}

export class MaintenanceEquipement {
	id: string;
	equipmentName: string;
	model?: string;
	brand?: string;
	serialNumber?: string;
	idMaterial?: string;
	installationDate?: Date;
	maintenanceOperations: OperationMaintenance[]
}


export class VisiteMaintenance {
	id: string;
	status: string;
	site: Adresse;
	year: number;
	month: number;
	client: any;
	clientId: string;
	contractId: string;
	equipmentDetails: MaintenanceEquipement[];
	maintenanceOperationSheetReference: string;
	previousMaintenanceVisits?: any;
}

export class VisiteMaintenanceViewModel {
	year: number;
	visitesMaintenances: VisiteMaintenance[];
}
