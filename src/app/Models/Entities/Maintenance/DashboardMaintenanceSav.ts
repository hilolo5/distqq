import { Client } from '../Contacts/Client';

export class DashbordContratEntretien {
	contratParStatut: ContratParStatut[];
	contratParDate: ContratParDate;
	technicienParHeure: TechnicienParHeure[];
	ficheInterventionParType: FicheInterventionParType[];
	top10ContratParCA: ContratDetail[];
}

export class FicheInterventionParType {
	type: number;
	nbrIntervention: number;
	interventionDetails: InterventionMaintenanceDetail[];
}
export class InterventionMaintenanceDetail {
	idIntervention: number;
	reference: string;
	idVisite: number;
	idTechnicien: number;
	clientDetails: Client;
}


export class ContratParStatut {
	status: string;
	nbrContrat: number;
	detailsContrat: DetailContrat[];

}

export class DetailContrat {
	idContrat: number;
	idClient: number;
	nameClient: string;
	dite: string;
	dateDebut: Date;
	dateFin: Date;
}

export class ContratParDate {
	nbrContrat: number;
	detailsContrat: DetailContrat;
}


export class TechnicienParHeure {
	nbrHeure: number;
	detailsTechnicien: DetailTechnicien;
}


export class DetailTechnicien {
	idTechnicien: number;
	nom: string;
	prenom: string;
	matricule: string
	idProfile: number;

}

export class ContratDetail {
	ca: number;
	idcontrat: number;
	referenceContrat: string;


}