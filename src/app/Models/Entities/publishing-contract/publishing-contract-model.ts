/**
 * a publishing contract model
 */
export interface PublishingContractModel {

	/**
     * the title of publishing contract
     */
	title: string;

	/**
     * the tags of publishing contract
     */
	tags: any;

	/**
     * the filename
     */
	fileName: string;

	/**
     * the original file name
     */
	orginalFileName: string;

}
