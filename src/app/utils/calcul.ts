import { TypeArticle, TypeValue } from '../Enums/typeValue.Enums';

export class Calcul {

	
	calculGeneralHt(lintItems) {
		const articles = this.getProductsOfArticles(lintItems);
		let totalHt = 0;
		articles.forEach(article => {
			totalHt = totalHt + this.TotalHtArticle(article, article.quantity);
		});

		return totalHt;
	}

	getProductsOfArticles(articles) {
		const articlesTmp = [];
		articles.forEach(article => {

			if (article.type === TypeArticle.produit) {
				article.product.quantity = article.quantity;
				articlesTmp.push(article.product);
			}

			if (article.type === TypeArticle.lot) {
				article.product.products.map(e => {
					e.product.quantity = e.quantity;
					articlesTmp.push(e.product);
				});
			}
		});
		return articlesTmp;
	}
	PrixHt(article) {
		return (this.calculate_cout_horaire(article.totalHours ? article.totalHours : 0 , article.hourlyCost) + article.materialCost).toFixed(2);
	}
	calculate_cout_horaire(Nomber_heure, Cout_vente): any {
		return parseFloat(Nomber_heure) * parseFloat(Cout_vente);
	}
	TotalHtArticle(article, qte) {
		const totalHt = this.PrixHt(article);
		if (+totalHt === 0) {
			return article.totalHT;
		}
		let remise = 0;
		if (article.discount !== null && article.discount.value !== 0 && article.discount.value !== undefined) {
			if (article.discount.type === TypeValue.Amount) {
				remise = article.discount.value;
			} else {
				remise = totalHt * (article.discount.value / 100);
			}
		}
		const res = qte !== undefined ? qte * (totalHt - remise) : (totalHt - remise);
		const result = parseFloat(res.toFixed(2));
		return isNaN(result) ? 0 : result;
	}
}
