export enum RequestType {
	JSON = 'application/json',
	TEXT = 'text/plain',
	FORM = 'application/x-www-form-urlencoded'
}
