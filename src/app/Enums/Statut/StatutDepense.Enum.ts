export enum StatutDepense {
	Brouillon = 'draft',
	Encours = 'in_progress',
	Enretard = 'late',
	Cloture = 'closed',
	Annule = 'canceled'
}
export enum CategorieDepense {
	Achat = 1,
	SousTraitant = 2,
}
