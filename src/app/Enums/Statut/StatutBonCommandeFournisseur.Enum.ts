
export enum StatutBonCommandeFournisseur {
	Brouillon = 'draft',
	Encours = 'in_progress',
	Annule = 'canceled',
	Facturee = 'billed',
}
