

export enum StatutChantier {
	EnEtude = 'study',
	Accepte = 'accepted',
	Termine = 'finished',
	NonAcceptee = 'not_accepted'
}
