export enum StatutDevis {
	EnAttente = 'in_progress',
	Acceptee = 'accepted',
	NonAcceptee = 'refused',
	Annulee = 'canceled',
	Facture = 'billed',
	Signed = 'signed',
	Late = 'late',
	Draft = 'draft'
}

