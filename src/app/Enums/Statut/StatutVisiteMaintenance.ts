export enum StatutVisiteMaintenance {
	Planned = 'planned',
	Unplanned = 'Unplanned',
	Canceled = 'canceled',
	Realized = 'realized'
}
