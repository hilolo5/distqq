export enum StatutFicheInterventionMaintenance {

	/// <summary>
	/// the Maintenance Operation Sheet has been drafted
	/// </summary>
	Draft = 'draft',

	/// <summary>
	/// the Maintenance Operation Sheet has been planned for it.
	/// </summary>
	Planned = 'planned',

	/// <summary>
	/// the Maintenance Operation Sheet been realized
	/// </summary>
	Realized = 'realized',

	/// <summary>
	/// the Maintenance Operation Sheet has been canceled
	/// </summary>
	Canceled = 'canceled',

	/// <summary>
	/// the Maintenance Operation Sheet has been billed
	/// </summary>
	Billed = 'billed',

	/// <summary>
	/// the Maintenance Operation Sheet has been undone
	/// </summary>
	UnDone = 'un_done',

	Late = 'late',

}



