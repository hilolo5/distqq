export enum StatutAvoir {
	Brouillon = 'draft',
	Encours = 'in_progress',
	Utilise = 'used',
	Expire = 'expired'
}
