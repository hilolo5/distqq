export enum DelaiGaranties {
	TroisMois = 3,
	SixMois = 6,
	UnAns = 12,
	DeuxAns = 24,
}

export enum StatutRetenueGarantie {
	nonrecuperer = 'not_recovered',
	recuperer = 'recovered',
	encours = 'in_progress',
	enretard = 'late'
}
