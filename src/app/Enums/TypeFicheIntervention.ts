export enum TypeFicheIntervention {
  None = 0,
  Maintenance = 1,
  Chantier = 2,
}