export enum ClientType {
	Particular = 'particular',
	Professional = 'professional',
}