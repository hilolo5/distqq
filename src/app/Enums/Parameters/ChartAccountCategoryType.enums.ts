export enum ChartAccountCategoryType {
	/// <summary>
	/// the Category repressing a default value
	/// </summary>
	Default = 0,
	/// <summary>
	/// expenses Category
	/// </summary>
	Expense,
	/// <summary>
	/// sales Category
	/// </summary>
	Sales,
	/// <summary>
	/// VAT Category
	/// </summary>
	VAT
}