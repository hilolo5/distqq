import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaGlobalComponent } from './agenda-global.component';

describe('AgendaGlobalComponent', () => {
  let component: AgendaGlobalComponent;
  let fixture: ComponentFixture<AgendaGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
