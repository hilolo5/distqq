import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Modereglement } from 'app/Models/Entities/Parametres/Modereglement';
import { ParametrageCompte } from 'app/Models/Entities/Parametres/ParametrageCompte';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { StaticModeReglement } from 'app/Enums/StaticModeReglement.Enum';
import { TypeCompte } from 'app/Enums/Parameters/TypeCompte.enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { Typepaiement } from 'app/components/paiement-facture/paiement.component';
import { PaymentAdd, Invoices } from 'app/Models/Model/PaymentAdd';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogHelper } from 'app/libraries/dialog';
import { SelectFactureComponent } from './select-facture/select-facture.component';

declare var toastr: any;
declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'paiement-groupe',
	templateUrl: './paiement-groupe.component.html',
	styleUrls: ['./paiement-groupe.component.scss']
})
export class PaiementGroupeComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('refresh') refresh = new EventEmitter();

	// Ajouter paiement
	form
	modesRegelement: Modereglement[] = null;
	comptes: ParametrageCompte[] = null;
	paiement: PaymentAdd;
	dateLang
	isClient = true;
	// choix de client
	idClient;
	idChantier
	// choix des factures
	search = ''
	factures: Invoice[] = []
	finished = true;
	page = 1;
	totalPages = 0;
	montant = 0;
	totalMontantFacture = 0

	loading = false;
	clients: Client[] = []
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<PaiementGroupeComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
		private dialog: MatDialog,

	) {
		this.form = this.fb.group({
			'montant': [null, [Validators.required]],
			'idCaisse': [null, [Validators.required]],
			'datePaiement': [new Date(), [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
			'description': ['Paiement factures'],
		})
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
		this.getClients('');
		this.GetModeRegelement('')
		this.GetCompte('');
	}

	/**
	* Pour Récupérer la liste des Clients
	*/
	getClients(search) {
		this.service.getAll(ApiUrl.Client).subscribe((res) => {
			this.clients = res.value;
		});
	}

	// Récuperer la liste des modes régelements
	GetModeRegelement(search) {
		if (this.modesRegelement == null) {
			this.service.getAll(ApiUrl.configModeRegelemnt)
				.subscribe(
					res => {
						this.modesRegelement = res.value.filter(x => x.id !== StaticModeReglement.Avoir)
					}
				)
		}
	}

	// Récuperer la liste des comptes
	GetCompte(search) {
		if (this.comptes == null) {
			this.service.getAll(ApiUrl.configCompte)
				.subscribe(res => {
					this.comptes = res.value
				})
		}
	}

	// Get information de form
	get f() { return this.form.controls; }

	// Save paiement
	savePaiement() {
		if (this.form.valid) {
			let values = this.form.value;
			values = AppSettings.ConvertEmptyValueToNull(values);
			const paiement: PaymentAdd = new PaymentAdd();
			paiement.amount = values['montant'];
			paiement.datePayment = new Date(AppSettings.formaterDatetime(values['datePaiement']));
			paiement.description = 'Paiement factures';
			paiement.type = Typepaiement.Invoice;
			paiement.creditNoteId = '';
			paiement.invoices = [];
			paiement.operation = TypePaiement.GroupedPayment;
			paiement.paymentMethodId = +values['idModePaiement'];
			paiement.accountId = +values['idCaisse'];
			this.paiement = paiement;
			this.montant = this.paiement.amount
			this.idChantier = null
			jQuery('#choixClient').modal('show')
			//jQuery('#ajouterPaiement').modal('hide')
			//this.close();

		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}
	/**
	 *
	 * @param info choisir chantier
	 */

	// choisirChantier(info: { idChantier: number, idFournisseur: number }): void {
	//   this.idChantier = info.idChantier;
	//   this.factures = []
	//   this.GetFactures()
	//   jQuery('#choixFacture').modal('show')

	// }

	choisirClient(id) {
		this.idClient = id;
		this.factures = []
		//this.GetFactures()
		this.openListFacture();
		//jQuery('#choixFacture').modal('show')
	}
	openListFacture() {
		const dialogListFcatureConfig = new MatDialogConfig();
		dialogListFcatureConfig.width = '600px';
		dialogListFcatureConfig.height = '850px';
		dialogListFcatureConfig.data = { idClient: this.idClient, paiement: this.paiement, montant: this.montant };
		const dialogRef = this.dialog.open(SelectFactureComponent, dialogListFcatureConfig);

		dialogRef.afterClosed().subscribe((data) => {
			if (data) {
				this.dialogRef.close(data);
			}
		});

	}

	onChangeMoyenPaiement(value) {
		if (value === StaticModeReglement.Espece) {
			const caisse = this.comptes.filter(x => x.type === TypeCompte.caisse)[0];
			if (caisse != null) {
				this.form.controls['idCaisse'].setValue(caisse.id.toString())
			}
		}
	}


	close() { this.dialogRef.close(); }


}
