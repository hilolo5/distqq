import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectFactureComponent } from './select-facture.component';

describe('SelectFactureComponent', () => {
  let component: SelectFactureComponent;
  let fixture: ComponentFixture<SelectFactureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectFactureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFactureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
