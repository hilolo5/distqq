import { Component, OnInit, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { OptionSpDF, Header, adresseCompany, Footer, Images } from 'app/Models/Entities/optionPdf';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
import { HeaderService } from 'app/services/header/header.service';
declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'app-parametrage-pdf',
	templateUrl: './parametrage-pdf.component.html',
	styleUrls: ['./parametrage-pdf.component.scss']
})
export class ParametragePdfComponent implements OnInit {

	form;
	data: OptionSpDF = null;
	logo = null;
	fileName = null;
	cachet = null;
	fileNameCachet = null;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder,
		private header: HeaderService
	) {
		this.form = this.fb.group({
			'companyName': [''],
			'phoneNumber': [''],
			'email': [''],
			'street': [''],
			'city': [''],
			'postalCode': [''],
			'countryCode': [''],
			'line1': [''],
			'line2': [''],
			'line3': [''],
		});
		this.header.breadcrumb = [
			{ label: 'Paramétrage PDF', route: '/parameteres/parametragePdf' }
		];
	}

	inistalizeForm() {
		this.form.patchValue({
			companyName: this.data.header.companyName,
			phoneNumber: this.data.header.phoneNumber,
			email: this.data.header.email,
			street: this.data.header.address.street,
			city: this.data.header.address.city,
			postalCode: this.data.header.address.postalCode,
			countryCode: this.data.header.address.countryCode,
			line1: this.data.footer.line_1,
			line2: this.data.footer.line_2,
			line3: this.data.footer.line_3
		});
	}
	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.GetParametragePdf();

	}
	GetParametragePdf() {
		this.service.getAll(ApiUrl.configParametragePDF).subscribe(res => {
			this.data = JSON.parse(res);
			// tslint:disable-next-line: triple-equals
			if (this.data.images.logo != '' && this.data.images.logo != null) {
				this.logo = this.data.images.logo;
				this.cachet = this.data.images.cachet;
				this.fileName = 'logo'
				this.fileNameCachet = 'Cachet'
			}
			//this.form.patchValue(this.data);
			this.inistalizeForm();
		});
	}
	get f() { return this.form.controls; }


	update() {
		if (this.form.valid) {
			this.data = this.form.value;
			const value = this.form.value;
			const adresse: adresseCompany = {
				countryCode: value.countryCode,
				city: value.city,
				postalCode: value.postalCode,
				street: value.street,


			}
			const header: Header = {
				companyName: value.companyName,
				phoneNumber: value.phoneNumber,
				email: value.email,
				address: adresse,
			}
			const footer: Footer = {
				line_1: value.line1,
				line_2: value.line2,
				line_3: value.line3,

			}
			const logo: Images = {
				logo: this.logo,
				cachet: this.cachet,
			}

			const optionPdf: OptionSpDF = {
				header: header,
				footer: footer,
				images: logo,
			}

			this.service.updateAll(ApiUrl.configParametragePDF, { value: JSON.stringify(optionPdf) }).subscribe(res => {
				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.GetParametragePdf();
			})
		}
	}

	startUpload(event: FileList) {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString();
			if (pieceJoin.fileType === 'jpg' || pieceJoin.fileType === 'png' || pieceJoin.fileType === 'jpeg' || pieceJoin.fileType === 'PNG') {
				this.fileName = file.name;
				this.logo = pieceJoin.content;
			} else {
				this.translate.get('parametragePdf').subscribe(text => {
					toastr.warning(text.logoCompany, text.fichierAcceptable, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				return
			}

		}
	}


	startUploadCachet(event: FileList) {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = file.name.substring(file.name.lastIndexOf('.') + 1)
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString();
			if (pieceJoin.fileType === 'jpg' || pieceJoin.fileType === 'png' || pieceJoin.fileType === 'jpeg' || pieceJoin.fileType === 'PNG') {
				this.fileNameCachet = file.name;
				this.cachet = pieceJoin.content;
			} else {
				this.translate.get('parametragePdf').subscribe(text => {
					toastr.warning(text.logoCompany, text.fichierAcceptable, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				return
			}

		}
	}
	removeFile() {
		this.translate.get('file.delete').subscribe(text => {
			swal({
				title: 'Supprimer',
				text: '',
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.logo = null;
					this.fileName = null;
					swal(text.success, '', 'success');
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}

	removeFileCachet() {
		this.translate.get('file.delete').subscribe(text => {
			swal({
				title: 'Supprimer',
				text: '',
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.cachet = null;
					this.fileNameCachet = null;
					swal(text.success, '', 'success');
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});

	}
}
