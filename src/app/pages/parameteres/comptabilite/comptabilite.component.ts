import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { PeriodeComptable } from 'app/Models/Entities/Parametres/PeriodeComptable';
import { DureeComptable } from 'app/Enums/DureeComptable.enum';
import { PlanComptableModel } from 'app/Models/Entities/Parametres/PlanComptableModel';
import { PlanComptableEnum } from 'app/Enums/PlanComptable.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { async } from '@angular/core/testing';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
import { DatePipe } from '@angular/common';

declare var jQuery: any;
declare var toastr: any;
declare var swal: any;


@Component({
	selector: 'app-comptabilite',
	templateUrl: './comptabilite.component.html',
	styleUrls: ['./comptabilite.component.scss'],
	providers: [DatePipe]

})
export class ComptabiliteComponent implements OnInit {

	periodes: PeriodeComptable[] = []
	formArrayNameProviderdateLang
	periodeComptable
	loading = false;
	dureeComptables: number[] = []
	isAdd = false;
	dateLang
	periodeComptableForm;
	formPlanComptable;
	planComptableEnum: typeof PlanComptableEnum = PlanComptableEnum;
	modelName = '';
	ModelNameTva = '';
	editPlanComptable = new PlanComptableModel();
	categoryItem = new PlanComptableModel();
	tvaup
	codeComptableup
	formtva
	planComptableList;
	plancomptablecode;
	TvaList: any;
	nameCategorie = '';
	codeCategorie = '';
	categoryVente: any;
	subCategoryVente: any;
	categoryDepense: any;
	subCategoryDepense: any;
	planComptableForm: FormGroup;
	VenteForm: FormGroup;
	depenseForm: FormGroup;
	periodesComptableForm: FormGroup;
	/*
	@section code comtable tva
	*/
	formTvaType: ('add' | 'edit') = null;
	currentIndex: number = null;
	selectedTabs = 'plan-comptable';


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder,
		private header: HeaderService
		//private toast: MessageService,
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.formtva = this.fb.group({
			valeurtvaup: [''],
			codecomptableup: [''],
		});
	}


	async ngOnInit() {
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
		await this.createPlanComptableForm();
		await this.createVenteForm();
		await this.createDepenseForm();
		await this.createPeriodeComptableForm();
		this.GetPeriodeComptable();
		this.getListDureeComptable();
		this.GetParametragePlanComptable();

		this.header.breadcrumb = [
			{ label: 'Comptabilité', route: '/parameteres/comptabilite' }
		];


	}
	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}

	GetParametragePlanComptable() {
		this.service.getAll(ApiUrl.configCategory).subscribe(res => {
			this.planComptableList = res.value.filter(x => x.type !== 3) as PlanComptableModel[];
			const tva = res.value.filter(x => x.type === 3)[0];
			if (!tva) {
				return false;
			}
			const subCategories = tva.subCategories;
			this.TvaList = [...subCategories, ...tva] as PlanComptableModel[];


		});
	}

	/**
	 * Récupére les période comptables
	 */
	GetPeriodeComptable() {
		this.loading = true;
		this.service.getAll(ApiUrl.configAccountingPeriode)
			.subscribe(res => {
				this.periodes = res.value;
				this.loading = false;
			}, err => {
				this.loading = false;
			})
	}

	/**
	 * Ajouter les périodes comptable
	 */
	ajouterPeriodeComptable() {
		this.isAdd = true;
		jQuery('#modifierPeriodeComptable').modal('show')
		this.periodeComptableForm = this.fb.group({
			dateDebut: [null, [Validators.required]],
			periode: [null, [Validators.required]],
		})
	}

	/**
	 * Modifier les périodes comptable
	 */
	modifierPeriodeComptable(index) {
		this.isAdd = false;
		//jQuery('#modifierPeriodeComptable').modal('show')
		this.periodeComptable = this.periodes[index];
		this.periodeComptableForm = this.fb.group({
			dateDebut: [new Date(this.periodeComptable.startingDate), [Validators.required]],
			periode: [this.periodeComptable.period, [Validators.required]],
		})
	}
	/**
	 * Mettre à jour période comptable
	 */
	updatePeriodeComptable(periode) {
		if (this.periodeComptableForm.valid) {
			jQuery('#modifierPeriodeComptable').modal('hide');
			this.periodeComptable = periode;
			this.periodeComptable.startingDate = AppSettings.formaterDatetime(this.periodeComptableForm.value['dateDebut']);
			this.periodeComptable.period = this.periodeComptableForm.value['periode'];

			this.service.updateAll(ApiUrl.configAccountingPeriode, this.periodeComptable).subscribe(res => {
				this.GetPeriodeComptable();
				this.translate.get('parametresCompatibility.form').subscribe(text => {
					toastr.success(text.edit.updateLabel, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			})
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}
	/**
	 * Sauvgarder ajouter période comptable
	 */
	addPeriodeComptable() {
		const res = {
			'id': 0,
			'startingDate': '',
			'endingDate': '',
			'period': 0
		}

		if (this.periodeComptableForm.valid) {
			this.loading = true;
			jQuery('#modifierPeriodeComptable').modal('hide')
			this.periodeComptableForm.value['dateDebut'] = AppSettings.formaterDatetime(this.periodeComptableForm.value['dateDebut'])
			res['startingDate'] = this.periodeComptableForm.value['dateDebut'];
			res['period'] = this.periodeComptableForm.value['periode'];
			this.service.updateAll(ApiUrl.configAccountingPeriode, res).
				subscribe(res => {
					this.loading = false;
					this.GetPeriodeComptable()
					this.translate.get('form').subscribe(text => {
						toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					})
				}, err => {
					this.loading = false;
					this.translate.get('form').subscribe(text => {
						toastr.success(text.addd.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					})
				})
		} else {
			this.translate.get('comptabilite').subscribe(text => {
				toastr.warning(text.confirmCloture.error, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	/**
	 * cloturé période comptable
	 */
	cloturPeriode(id) {
		this.translate.get('comptabilite.confirmCloture').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.loading = true;
					this.service.getById(ApiUrl.configAccountingPeriode + '/Close', id).subscribe(
						res => {
							this.loading = false;
							if (res) {
								this.GetPeriodeComptable();
								this.translate.get('comptabilite').subscribe(text => {
									toastr.success(text.cloturePeriode, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
								})
							} else {
								this.translate.get('errors').subscribe(text => {
									toastr.warning(text.server, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
								})
							}
						},
						err => {
							console.log(err);
							this.loading = false;
							if (err.error.messageCode === 139) {
								const message = 'la période comptable sélectionnée n\'a pas atteint la fin de sa période';
								toastr.warning(message, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
							}

						}
					)
				}
			});
		});
	}
	/**
	 * Get liste duree comptable
	 */

	getListDureeComptable() {
		for (let n in DureeComptable) {
			if (typeof DureeComptable[n] === 'number') {
				this.dureeComptables.push(<any>DureeComptable[n])
			}
		}
	}


	async OpenPopUpPlanComptable(item) {
		this.editPlanComptable = item;
		this.plancomptablecode = item.code;
		this.modelName = await this.getmodelName(this.editPlanComptable.label);
		jQuery('#modifierPlanComptable').modal('show');
	}


	async OpenPopUpAdCategorie(item?) {
		this.modelName = 'Ajout'
		this.categoryItem = item;
		//	this.modelName = await this.getmodelName(this.categoryItem.label);
		jQuery('#addCategorie').modal('show');
	}

	getmodelName(name): Promise<string> {
		return new Promise((resolve, reject) => {
			this.translate.get('parametrageplanComptable').subscribe(labels => {
				resolve(labels[name]);
			});
		})

	}

	updatePlanComptable() {
		if (this.plancomptablecode) {
			this.editPlanComptable.code = this.plancomptablecode;
		}

		this.service.updateAll(ApiUrl.configCategory, [this.editPlanComptable])
			.subscribe(res => {
				this.GetPeriodeComptable();
				this.GetParametragePlanComptable();
				this.translate.get('parametrageplanComptable').subscribe(text => {
					toastr.success(text.msgUpdate, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				jQuery('#modifierPlanComptable').modal('hide');

				this.editPlanComptable = new PlanComptableModel();
			});
	}

	addCategorie() {
		const resCatgorie = {
			'id': '',
			'type': this.categoryItem.type,
			'categoryType': 0,
			'label': this.nameCategorie,
			'code': this.codeCategorie,
			'description': '',
			'vatValue': 0,
			'parentId': this.categoryItem.id,
			'subCategories': []
		}
		this.service.updateAll(ApiUrl.configCategory, [resCatgorie])
			.subscribe(res => {
				this.GetPeriodeComptable();
				this.GetParametragePlanComptable();
				this.createVenteForm();
				this.createDepenseForm();
				this.translate.get('parametrageplanComptable').subscribe(text => {
					toastr.success(text.msgUpdate, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				jQuery('#addCategorie').modal('hide');

				this.nameCategorie = '';
				this.codeCategorie = '';
			});
	}

	saveFormTva() {
		this.editPlanComptable.vatValue = this.formtva.value.valeurtvaup;
		this.editPlanComptable.code = this.formtva.value.codecomptableup;
		this.editPlanComptable.subCategories = [];
		const tvaDefault = this.TvaList.filter(x => x.parentId === null)[0];
		this.editPlanComptable.parentId = tvaDefault !== undefined ? tvaDefault.id : null;
		this.editPlanComptable.id = this.editPlanComptable.id === null ? null : this.editPlanComptable.id;
		this.updatePlanComptable();
		this.closeFormTva();
	}

	setdata(type) {
		return {
			'id': null,
			'type': type,
			'label': '',
			'code': '',
			'description': '',
			'vatValue': 0,
			'parentId': 0,
			'subCategories': []
		}
	}


	/**
	 * supprimer code comptable d'une tva par son @param index
	 */
	deleteTva(index) {
		this.translate.get('parametrageplanComptable.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.document.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.editPlanComptable.code = this.TvaList;
					this.service.delete(ApiUrl.configCategory, this.TvaList[index].id)
						.subscribe(res => {
							if (res) {
								this.GetPeriodeComptable();
								this.GetParametragePlanComptable();
								swal(text.success, '', 'success');
							} else {
								swal(text.error, '', 'error');
							}

						});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}


	async OpenFromTva(indexCPTva?: number, element?) {
		this.editPlanComptable = element ? element : this.setdata(3);
		if (element) {
			this.currentIndex = indexCPTva;
			this.ModelNameTva = await this.getmodelName('modifierTvaCodeComptable');
			this.formtva = this.fb.group({
				valeurtvaup: [this.TvaList[indexCPTva].vatValue, [Validators.required]],
				codecomptableup: [this.TvaList[indexCPTva].code]
			});
		} else {
			this.ModelNameTva = await this.getmodelName('addTvaCodeComptable');
			this.currentIndex = null;
			this.formtva.reset();
		}

		jQuery('#formTva').modal('show')
	}


	closeFormTva() {
		this.formtva.reset();
		jQuery('#formTva').modal('hide');
	}

	clearform() {
		this.formtva.reset();
	}

	/**
	 * Create form Plan Comptable
	**/
	async createPlanComptableForm() {

		const result = (await this.service.getAll(ApiUrl.configCategory).toPromise()).value;
		this.planComptableList = result.filter(x => x.type === 0) as PlanComptableModel[];

		this.planComptableForm = this.fb.group({});
		this.planComptableList.forEach(item => {
			this.planComptableForm.addControl(item.id, new FormControl(item.code, [Validators.pattern(/^\d+$/), Validators.required]))
		});
	}



	/**
	 * Create form Vente
	**/
	async createVenteForm() {

		const result = (await this.service.getAll(ApiUrl.configCategory).toPromise()).value;
		this.categoryVente = result.filter(x => x.type === 2)[0];
		this.subCategoryVente = this.categoryVente.subCategories;
		this.VenteForm = this.fb.group({});
		this.subCategoryVente.forEach(item => {
			this.VenteForm.addControl(item.id, new FormControl(item.code, [Validators.pattern(/^\d+$/), Validators.required]))
		});
	}



	removeProduit(subCategories) {
		swal({
			title: this.translate.instant('deleteCompteProduit.title'),
			text: this.translate.instant('deleteCompteProduit.question'),
			icon: 'warning',
			buttons: {
				cancel: {
					text: this.translate.instant('deleteCompteProduit.cancel'),
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: this.translate.instant('deleteCompteProduit.confirm'),
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		}).then(isConfirm => {
			if (isConfirm) {
				const url = ApiUrl.configCategory + '/' + subCategories.id + '/Delete'
				this.service.deleteAll(url).subscribe(res => {
					if (res.isSuccess) {
						this.createVenteForm();
					}
					if (!res.isSuccess && res.messageCode === 203) {
						const text = 'Vous ne pouvez pas supprimer cette catégorie car elle est associée à une ou plusieurs classifications'
						toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					}

				}, err => {
					console.log(err);
				});
			} else {
				// tslint:disable-next-line: max-line-length
				toastr.warning(this.translate.instant('deleteCompteProduit.failed'), this.translate.instant('deleteCompteProduit.title'), { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		});

	}



	/**
	 * Create form Depense
	**/
	async createDepenseForm() {
		const result = (await this.service.getAll(ApiUrl.configCategory).toPromise()).value;
		this.categoryDepense = result.filter(x => x.type === 1)[0];
		this.subCategoryDepense = this.categoryDepense.subCategories;
		this.depenseForm = this.fb.group({});
		this.subCategoryDepense.forEach(item => {
			this.depenseForm.addControl(item.id, new FormControl(item.code, [Validators.pattern(/^\d+$/), Validators.required]))
		});
	}

	/**
	 * Save form Plan Comptable
	**/
	saveFormPlanComptable() {
		if (!this.planComptableForm.valid) {
			return false;
		}

		const dirtyValues = this.getDirtyValues(this.planComptableForm);

		this.service.updateAll(ApiUrl.configCategory, this.convertData(dirtyValues)).subscribe(res => {
			this.GetPeriodeComptable();
			this.translate.get('parametresCompatibility.form').subscribe(text => {
				toastr.success(text.edit.updateLabel, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		})

	}


	/**
	* Save form Plan Vente
	**/
	saveFormVente() {
		if (!this.VenteForm.valid) {
			return false;
		}

		const dirtyValues = this.getDirtyValues(this.VenteForm);
		this.service.updateAll(ApiUrl.configCategory, this.convertData(dirtyValues)).subscribe(res => {
			this.translate.get('parametresCompatibility.form').subscribe(text => {
				toastr.success(text.edit.updateLabel, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		})
	}


	/**
	* Save form Plan Depense
	**/
	saveFormDepense() {

		if (!this.depenseForm.valid) {
			return false;
		}

		const dirtyValues = this.getDirtyValues(this.depenseForm);
		this.service.updateAll(ApiUrl.configCategory, this.convertData(dirtyValues)).subscribe(res => {
			this.translate.get('parametresCompatibility.form').subscribe(text => {
				toastr.success(text.edit.updateLabel, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		})
	}

	/**
	 * Create form periode comptable
	**/
	async createPeriodeComptableForm() {

		let startDate = '';
		let period = '';

		const periodesData = (await this.service.getAll(ApiUrl.configAccountingPeriode).toPromise()).value;
		this.periodeComptable = periodesData.filter(x => x.endingDate === null)[0];

		if (!this.periodeComptable)
			return;

		if (this.periodeComptable.id) {
			startDate = this.periodeComptable.startingDate;
			period = this.periodeComptable.period;
		}

		this.periodeComptableForm = this.fb.group({
			dateDebut: [new Date(startDate), [Validators.required]],
			periode: [period, [Validators.required]],
		});
	}

	getDirtyValues(form: any) {
		let dirtyValues = {};

		Object.keys(form.controls)
			.forEach(key => {
				let currentControl = form.controls[key];

				if (currentControl.dirty) {
					if (currentControl.controls)
						dirtyValues[key] = this.getDirtyValues(currentControl);
					else
						dirtyValues[key] = currentControl.value;
				}
			});

		return dirtyValues;

	}


	convertData(data) {
		var result = Object.keys(data).map(function (key) {
			return { 'id': key, 'code': data[key] };
		});

		return result;
	}

}
