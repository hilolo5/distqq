import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationVenteComponent } from './classification-vente.component';

describe('ClassificationVenteComponent', () => {
  let component: ClassificationVenteComponent;
  let fixture: ComponentFixture<ClassificationVenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassificationVenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationVenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
