import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { EditParametersComponent } from './edit-parameters.component';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { MatExpansionModule, MatSelectModule } from '@angular/material';


export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/parametres/', '.json');
}

@NgModule({
	declarations: [EditParametersComponent],
	imports: [
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		InputTextareaModule,
		FormsModule,
		ReactiveFormsModule,
		DataTablesModule,
		NgSelectModule,
		NgbTooltipModule,
		AngularEditorModule,
		NgSelectModule,
		MatSelectModule,
		MatExpansionModule,
	],
	exports: [EditParametersComponent]
})
export class EditParametersModule { }

