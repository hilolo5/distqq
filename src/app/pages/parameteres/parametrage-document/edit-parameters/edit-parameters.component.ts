import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';

@Component({
	selector: 'app-edit-parameters',
	templateUrl: './edit-parameters.component.html',
	styleUrls: ['./edit-parameters.component.scss']
})

export class EditParametersComponent implements OnInit {

	@Input() document;
	@Input() data;
	@Output() items = new EventEmitter();
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	documentForm;
	detailL = false;
	editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '8rem',
		translate: 'yes',

	};

	panelOpenState = false;

	constructor(private formBuilder: FormBuilder,
		private translate: TranslateService) { }

	ngOnInit() {
		this.createEmptyForm();
		this.createForm();
	}

	ngOnChanges() {
		this.createEmptyForm();
		this.createForm();
	}

	createEmptyForm(): void {
		this.documentForm = this.formBuilder.group({
			documentType: [''],
			Holdback: [''],
			validity: [''],
			object: [''],
			note: [''],
			paymentTerms: [''],
			header: [''],
			subjectEmail: [''],
			contentEmail: [''],
			subjectRelance: [''],
			contentRelance: [''],
			detaillot: [false]
		});
	}

	createForm(): void {

		this.documentForm = this.formBuilder.group({
			documentType: [this.document.documentType ? this.document.documentType : ''],
			Holdback: [this.document.Holdback ? this.document.Holdback : ''],
			validity: [this.document.validity ? this.document.validity : ''],
			object: [this.document.object ? this.document.object : ''],
			note: [this.document.note ? this.document.note : ''],
			paymentTerms: [this.document.paymentTerms ? this.document.paymentTerms : ''],
			header: [this.document.header ? this.document.header : ''],
			subjectEmail: [this.document.Email ? this.document.Email.subject : ''],
			contentEmail: [this.document.Email ? this.document.Email.content : ''],
			subjectRelance: [this.document.reflationEmail ? this.document.reflationEmail.subject : ''],
			contentRelance: [this.document.reflationEmail ? this.document.reflationEmail.content : ''],
			detaillot: [this.document.LotDetails],
		});
		this.detailL = this.document.LotDetails;

		this.documentForm.statusChanges.subscribe(
			result => this.updateDocumentParameters()
		);

	}


	setTitle(docType: string) {
		const title = this.translate.instant('parametresDocument.'.concat(this.typeNumerotation[docType]));
		return title;
	}

	delaiValidity() {
		return [this.typeNumerotation.facture, this.typeNumerotation.devis, this.typeNumerotation.avoir,
		this.typeNumerotation.boncommande_fournisseur].includes(this.document.documentType);
	}

	header() {
		const type = this.document.documentType
		return type !== this.typeNumerotation.depense;
	}

	email() {
		const type = this.document.documentType
		return type !== this.typeNumerotation.depense &&
			type !== this.typeNumerotation.boncommande_fournisseur;
	}

	relance() {
		const type = this.document.documentType
		return type === this.typeNumerotation.facture || type === this.typeNumerotation.devis || type === this.typeNumerotation.avoir;
	}

	detailLot() {
		const type = this.document.documentType
		return type === this.typeNumerotation.facture || type === this.typeNumerotation.devis;
	}


	retenuGarantie() {
		const type = this.document.documentType
		return type === this.typeNumerotation.facture || type === this.typeNumerotation.devis || type === this.typeNumerotation.avoir;
	}

	updateDocumentParameters(): void {

		this.document.object = this.documentForm.value.object;

		if (this.delaiValidity()) {
			this.document.validity = this.documentForm.value.validity;
		}

		if (this.retenuGarantie()) {
			this.document.Holdback = this.documentForm.value.Holdback;
		}

		if (this.detailLot()) {
			this.document.LotDetails = this.detailL;
		}

		if (this.document.documentType !== this.typeNumerotation.fiche_intervention) {
			this.document.note = this.documentForm.value.note;
			this.document.paymentTerms = this.documentForm.value.paymentTerms;
		}

		if (this.header()) {
			this.document.header = this.documentForm.value.header;
		}

		if (this.email()) {
			this.document.Email = {
				subject: this.documentForm.value.subjectEmail,
				content: this.documentForm.value.contentEmail
			};
		}

		if (this.relance()) {
			this.document.reflationEmail = {
				subject: this.documentForm.value.subjectRelance,
				content: this.documentForm.value.contentRelance
			};
		}

		const index = this.data.findIndex(e => e.documentType === this.document.documentType);
		this.data[index] = this.document;
		this.items.emit(this.data);

	}


	click(e) {
		this.detailL = e;
	}


}
