import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DefaultValue } from 'app/Models/Entities/Parametres/DefaultValue';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;
@Component({
	selector: 'app-horaire-travail',
	templateUrl: './horaire-travail.component.html',
	styleUrls: ['./horaire-travail.component.scss']
})
export class HoraireTravailComponent implements OnInit {

	form: any;
	loading;
	data: DefaultValue;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private header: HeaderService,
		private fb: FormBuilder) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.form = this.fb.group({
			heureDebut: [''],
			heureFin: ['']
		})
	}


	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetParametrageHoraireTravail();

	}


	prepareBreadcrumb(): void {
		this.translate.get(`parametrageHoraireTravail.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('parametrageHoraireTravail.title'), route: '/parameteres/horairetravail' }
			];
		});
	}
	GetParametrageHoraireTravail() {
		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			this.data = JSON.parse(res);
			this.form.controls['heureDebut'].setValue(this.data['startingHour']);
			this.form.controls['heureFin'].setValue(this.data['endingHour']);
		});
	}

	update() {

		if (this.form.valid) {
			this.data.startingHour = this.form.value.heureDebut;
			this.data.endingHour = this.form.value.heureFin;
			this.service.updateAll(ApiUrl.configDefaultValue, { value: JSON.stringify(this.data) }).subscribe(res => {
				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.GetParametrageHoraireTravail();
			})
		}

	}
}
