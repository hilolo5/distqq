import { HttpEventType } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { SwalUtils } from '../../../common/Swal/swal';
// import { LoaderService } from 'app/services/loader/loader.service';
import { AppSettings } from '../../../app-settings/app-settings';
import { NameModelComponent } from './name-model/name-model.component';
import { FillTagsComponent } from '../../../common/fill-tags/fill-tags.component';
import { PublishingContract } from '../../../Models/Entities/publishing-contract/publishing-contract';
import { PublishingContractModel } from '../../../Models/Entities/publishing-contract/publishing-contract-model';
import { IGenericRepository } from '../../../shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from '../../../Enums/Configuration/api-url.enum';
import { UploadFileDialogComponent } from '../../../common/upload-file-dialog/upload-file-dialog.component';
import { SortDirection } from '../../../Models/Model/filter-option';
import { PublishingService } from '../../../services/publishing/publishing.service';
import { FileUtils } from '../../../shared/utils/file';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-generate-contract',
	templateUrl: './generate-contract.component.html',
	styleUrls: ['./generate-contract.component.scss']
})
export class GenerateContractComponent implements OnInit {

	/**
	* the enumeration define the permissions in all application
	*/
	contracts: PublishingContract[] = [];
	acceptFormats = ['docx', 'doc'];
	items = [];
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private servicePublishing: PublishingService,
		private translate: TranslateService,
		private modalService: NgbModal,
		private header: HeaderService
		// private loaderService: LoaderService
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.items = [{ label: '#NomClient#', isLabel: true },
		{ label: '#AdresseClient#', isLabel: true },
		{ label: '#NumContrat#', isLabel: true },
		{ label: '#TotalTTC#', isLabel: true },
		{ label: '#DateDébut#', isLabel: true },
		{ label: '#DateFin#', isLabel: true },
		{ label: '#TotalHT#', isLabel: true },
		{ label: '#signature_...#', isLabel: true }
	];
	}


	ngOnInit() {
		this.getContracts();
		this.prepareBreadcrumb();
	}

	prepareBreadcrumb(): void {
		this.translate.get(`publishingContract.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('publishingContract.label.title'), route: '/parameteres/editique' }

			];
		});
	}
	// #region actions

	/**
	 * upload a new model
	 */
	async uploadNewModelAction() {
		const resultFile = await this.uploadModel();
		if (resultFile !== null) {
			this.extractTags(resultFile, async (extractResult) => {
				if (extractResult.isSuccess) {
					const tags = await this.fillTags(extractResult.value);
					const renameResult = await this.chooseName();
					const modelName = renameResult.name;
					this.save(resultFile, tags, modelName);
				} else {
					toastr.error(this.translate.instant('errors.' + extractResult.messageCode));
				}
			});
		}
	}

	/**
	* delete model contract
	*/
	deleteAction(id) {
		SwalUtils.confirm({
			title: 'Supprimer',
			cancel: 'Annuler',
			confirm: 'Oui, supprimez-le',
			question: 'Êtes-vous sûr supprimer ce modèle ?'
		}, () => {
			this.service.delete(ApiUrl.Publishing, id).subscribe(result => {
				if (result.isSuccess) {
					toastr.success(this.translate.instant('success.delte'));
					this.contracts = this.contracts.filter(e => e.id !== id);
				} else {
					toastr.error(this.translate.instant('errors.server'));
				}
			});
		})
	}

	/**
	* display a contract action
	*/
	displayAction(id: number) {
		const contract = this.contracts.find(e => e.id === id);
		this.servicePublishing.download(ApiUrl.PublishingDownload, id).subscribe(data => {
			let progress = 0;
			if (data.type === HttpEventType.UploadProgress) {
				progress = Math.round(100 * data.loaded / data.total);
				// this.loaderService.show(progress)
			} else if (data.type === HttpEventType.Response) {
				// this.loaderService.hide()
				FileUtils.downloadBlob(data.body, contract.orginalFileName)
			}
		})
	}

	async changeNameAction(index: number) {
		const contract = this.contracts[index];
		const renameResult = await this.chooseName(contract.title);
		contract.title = renameResult.name;
		this.updateContract(contract, () => {
			this.contracts[index].title = renameResult.name;
		})
	}

	async fillTagsAction(index: number) {
		const contract = this.contracts[index];
		const tags = contract.tags;
		const fillTagsResult = await this.fillTags(Object.keys(tags), tags);
		contract.tags = fillTagsResult;
		this.updateContract(contract, () => {
			this.contracts[index].tags = fillTagsResult;
		})
	}

	async changeModelAction(index: number) {
		const model = this.contracts[index];
		const resultFile = await this.uploadModel();
		if (resultFile != null) {
			this.extractTags(resultFile, async (resultTags) => {
				if (resultTags.isSuccess) {
					const tags = await this.fillTags(resultTags.value, model.tags);
					this.uploadContract(resultFile, (resultUpload) => {
						if (resultUpload != null) {
							model.tags = tags;
							model.fileName = resultUpload.value;
							model.orginalFileName = resultFile.name;
							this.updateContract(model, () => {
								this.contracts[index] = model;
							})
						}
					})
				}
			})
		}
	}

	// #endregion

	// #region services

	/**
	* get list of contracts
	*/
	getContracts() {
		this.service.getAllPagination(ApiUrl.Publishing, {
			SearchQuery: '',
			OrderBy: 'title',
			Page: 1,
			SortDirection: SortDirection.Ascending,
			PageSize: 10
		}).subscribe(result => {
			if (result.isSuccess) {
				this.contracts = result.value;
			} else {
				toastr.error(this.translate.instant('errors.server'));
			}
		})
	}

	/**
	* update contract
	*/
	updateContract(contract: PublishingContract, successCallBack: any) {
		this.service.update(ApiUrl.Publishing, contract, contract.id)
			.subscribe(result => {
				if (result.isSuccess) {
					toastr.success('Mettre à jour avec succès', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					successCallBack();
				} else {
					toastr.error(this.translate.instant('errors.server'))
				}
			});
	}

	/**
	 * upload contract
	 * @param file the file we want to upload
	 * @param callback the callback of success uploaded
	 */
	uploadContract(file: File, callback) {
		this.servicePublishing.UploadContract(ApiUrl.PublishingUpload, file).subscribe(event => {
			let progress = 0
			if (event.type === HttpEventType.UploadProgress) {
				progress = Math.round(100 * event.loaded / event.total);
				// this.loaderService.show(progress)
			} else if (event.type === HttpEventType.Response) {
				// this.loaderService.hide()
				const body = event.body;
				callback(body);
			}
		});
	}

	/**
	 * extract tags from document
	 * @param file the file we want to extract form it tags
	 */
	extractTags(file: File, callback) {
		this.servicePublishing.ExtractTagsFromDocument(ApiUrl.ExtractTagsFromDocument, file).subscribe(async event => {
			let progress = 0;
			if (event.type === HttpEventType.UploadProgress) {
				progress = Math.round(100 * event.loaded / event.total);
				// this.loaderService.show(progress)
			} else if (event.type === HttpEventType.Response) {
				// this.loaderService.hide()
				const body = event.body;
				callback(body);
			}
		})
	}

	/**
	 * save contract
	 */
	save(file: File, tags: any, modelName: string) {
		this.uploadContract(file, (result) => {
			const publishingContract: PublishingContractModel = {
				fileName: result.value,
				orginalFileName: file.name,
				tags: tags,
				title: modelName
			}
			this.service.create(ApiUrl.Publishing + ACTION_API.create, publishingContract)
				.subscribe(res => {
					if (res.isSuccess) {
						toastr.success('Ajouter avec succès', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						this.getContracts();
					} else {
						toastr.error(this.translate.instant('errors.add'));
					}
				})
		});
	}
	// #endregion

	// #region popups

	/**
	* upload popup
	*/
	async uploadModel() {
		const referenceModal = this.modalService.open(UploadFileDialogComponent, { centered: true });
		referenceModal.componentInstance.requiredExtensions = this.acceptFormats;
		return await referenceModal.result;
	}

	/**
	* fill tags of contract
	*/
	async fillTags(tags: string[], defaultValue: any = null) {
		const referenceModal = this.modalService.open(FillTagsComponent, { centered: true });
		referenceModal.componentInstance.tags = tags;
		referenceModal.componentInstance.defaultValue = defaultValue;
		return await referenceModal.result;
	}

	/**
	* choose a name for a contract
	*/
	async chooseName(name: string = null): Promise<{ name: string }> {
		const referenceModal = this.modalService.open(NameModelComponent, { centered: true });
		referenceModal.componentInstance.name = name;
		return await referenceModal.result;
	}

	// #endregion

}
