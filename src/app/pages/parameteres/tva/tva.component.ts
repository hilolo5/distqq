import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DefaultValue } from 'app/Models/Entities/Parametres/DefaultValue';
import { ChartAccountCategoryType } from 'app/Enums/Parameters/ChartAccountCategoryType.enums';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-tva-config',
	templateUrl: './tva.component.html',
	styleUrls: ['./tva.component.scss']
})
export class TvaComponent implements OnInit {
	form: any;
	loading;
	data;
	chartAccountCategoryType: typeof ChartAccountCategoryType = ChartAccountCategoryType;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private header: HeaderService,
		private fb: FormBuilder) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.form = this.fb.group({
			tvaDefaut: ['']
		})
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetParametrageTva();

	}
	prepareBreadcrumb(): void {
		this.translate.get(`parametrageTva.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('parametrageTva.title'), route: '/parameteres/prix' }
			];
		});
	}
	GetParametrageTva() {
		this.service.getAll(ApiUrl.configCategory + '/type/' + this.chartAccountCategoryType.VAT).subscribe(res => {
			this.data = res.value[0];
			this.form.controls['tvaDefaut'].setValue(this.data['vatValue']);
		});
	}
	update() {

		if (this.form.valid) {
			this.data.vatValue = this.form.value.tvaDefaut;
			this.data.subCategories = [];
			this.service.updateAll(ApiUrl.configCategory, [this.data]).subscribe(res => {
				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.GetParametrageTva();
			})
		}

	}

}
