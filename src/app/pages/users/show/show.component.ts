import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { User } from 'app/Models/Entities/User';
import { HeaderService } from 'app/services/header/header.service';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
import { TranslateConfiguration } from 'app/libraries/translation';

@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

	docType = '';
	apiUrl: typeof ApiUrl = ApiUrl;
	selectedTabs = 'information';
	id;
	user: User;
	historique = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private header: HeaderService,
		private route: ActivatedRoute, private translate: TranslateService) {
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		this.route.params.subscribe(params => {
			this.id = params['id'];
			this.service.getById(ApiUrl.User, this.id).subscribe(
				res => {
					this.user = res.value;
					this.historique = this.user.changesHistory;
					this.prepareBreadcrumb();
				}
			)
		});

	}
	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.user.reference, route: ManageDataTable.getRouteDetails(this.user) }
			];
		});
	}

	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}


}
