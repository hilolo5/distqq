import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilisateurRoutingModule } from './users-routing.module';
import { AddComponent } from './add/add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UsersProfil } from './../../Enums/UserProfil.Enum';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModules } from './../../common/common.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
// import { changePasswordModule } from 'app/common/change-password/change-password.module';

import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { MatTabsModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { InformationComponent } from './show/information/information.component';
import { ShowComponent } from './show/show.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' }
	]);
}


@NgModule({
	providers: [
		UsersProfil,
		PreviousRouteService
	],
	imports: [
		CommonModules,
		CommonModule,
		MatSelectModule,
		MatCheckboxModule,
		UtilisateurRoutingModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		MatTabsModule,
		ReactiveFormsModule,
		DataTablesModule,
		NgSelectModule,
		NgbTooltipModule,
		ShowHidePasswordModule.forRoot(),
		// changePasswordModule
	],
	declarations: [ShowComponent, InformationComponent, AddComponent]
})
export class UtilisateurModule { }
