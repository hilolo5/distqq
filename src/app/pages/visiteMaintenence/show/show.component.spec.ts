import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowComponentVisiteMaintenance } from './show.component';

describe('ShowComponent', () => {
  let component: ShowComponentVisiteMaintenance;
  let fixture: ComponentFixture<ShowComponentVisiteMaintenance>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowComponentVisiteMaintenance]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowComponentVisiteMaintenance);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
