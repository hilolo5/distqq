import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { InformationComponent } from './show/information/information.component';
import { CommonModules } from 'app/common/common.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { CalendarModule, SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { ContratEntretienRoutingModule } from './contratEntretien-routing.module';
import { MatDialogModule, MatDatepickerModule, MatIconModule, MatInputModule, MatNativeDateModule, MatFormFieldModule, MatTabsModule, MatMenuModule, MatSelectModule, MatCheckboxModule } from "@angular/material";
import { GammeMaintenanceEquipementModules } from 'app/common/gamme-maintenance-equipement/gamme-maintenance-equipement.module';
import { ListeGammeMaintenanceEquipementModules } from 'app/common/liste-gamme-maintenance-equipement/liste-gamme-maintenance-equipement.module';
import { ListeGammeMaintenanceEquipementComponent } from 'app/common/liste-gamme-maintenance-equipement/liste-gamme-maintenance-equipement.component';
import { gammeMaintenanceEquipementContainerComponent } from 'app/common/gamme-maintenance-equipement-container/gamme-maintenance-equipement-container.component';
import { GammeMaintenanceEquipementContainerModules } from 'app/common/gamme-maintenance-equipement-container/gamme-maintenance-equipement-container.module';
import { VisiteMaintenanceComponent } from './show/visite-maintenance/visite-maintenance.component';
import { VisiteMaintenanceModule } from '../visiteMaintenence/visiteMaintenance.module';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { PrsonaliseReccurenteModule } from 'app/common/personalise-reccurente/personalise-reccurente.module';
import { ContractComponent } from './show/contract/contract.component';
import { SelectContractComponent } from './show/contract/select-contract/select-contract.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';
import { ActionsModule } from 'app/shared/actions/actions.module';


export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/contratentretien/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' }
	]);
}


@NgModule({
	declarations: [
		AddComponent,
		InformationComponent,
		ShowComponent,
		VisiteMaintenanceComponent,
		ContractComponent,
		SelectContractComponent
	],
	imports: [
		ContratEntretienRoutingModule,
		CommonModule,
		NgxMatSelectSearchModule,
		MatCheckboxModule,
		MatSelectModule,
		CommonModules,
		AddChantierFormModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		DataTablesModule,
		MatTabsModule,
		CalendarModule,
		MatMenuModule,
		SplitButtonModule,
		MatDialogModule,
		TableArticleModule,
		ListeGammeMaintenanceEquipementModules,
		GammeMaintenanceEquipementContainerModules,
		GammeMaintenanceEquipementModules,
		MatDatepickerModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatNativeDateModule,
		VisiteMaintenanceModule,
		PrsonaliseReccurenteModule,
		ActionsModule,

	],
	providers: [
		PreviousRouteService,
		MatDatepickerModule
	],
	entryComponents: [ListeGammeMaintenanceEquipementComponent, gammeMaintenanceEquipementContainerComponent, SelectContractComponent]
})
export class ContratEntretienModule { }


