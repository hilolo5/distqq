import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ContratEntretien } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { GammeMaintenanceEquipement } from 'app/Models/Entities/Maintenance/GammeMaintenanceEquipement';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { PeriodType } from 'app/Enums/Maintenance/typePeriode.Enum';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { PeriodicityType, PeriodicityEndingType } from 'app/Enums/TypeRepetition.Enum';
import { PersonaliseReccurenteComponent } from 'app/common/personalise-reccurente/personalise-reccurente.component';
import { ReccurenteModel } from 'app/Models/Model/ReccurenteModel';
import { DatePipe } from '@angular/common';
import { DialogHelper } from 'app/libraries/dialog';
import { VisualiserPdfComponent } from 'app/common/visualiser-pdf/visualiser-pdf.component';
import { Color } from 'app/Enums/color';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ActivatedRoute, Router } from '@angular/router';

export declare var swal: any;
declare var toastr: any;
declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'information-contratEntretien',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.scss'],
	providers: [DatePipe]
})
export class InformationComponent implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('contratEntretien') contratEntretien: ContratEntretien = null;
	// tslint:disable-next-line:no-input-rename
	@Input('recurrente') reccurente = null;
	gamme_maintenance_equipement_Selected: GammeMaintenanceEquipement[] = [];
	selectedGammeMaintenanceEquipement = 0;
	statutFicheIntervention: typeof StatutFicheIntervention = StatutFicheIntervention;
	// listFicheIntervention: FicheInterventionMaintenance[] = [];
	listFicheIntervention = [];
	form: FormGroup;
	base64 = null;
	PeriodType: typeof PeriodType = PeriodType;
	// facture: Facture[] = [];
	statutFacture: typeof StatutFacture = StatutFacture;
	typeFacture;
	articles = [];
	emitterArticle: any = {};
	periodicityType: typeof PeriodicityType = PeriodicityType;
	periodicityEndingType: typeof PeriodicityEndingType = PeriodicityEndingType;
	dataDialog: ReccurenteModel = null;
	result: ReccurenteModel = null;
	selcted = true;
	color: typeof Color = Color;
	processing = true;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog,
		private formBuilder: FormBuilder,
		private datepipe: DatePipe,
		private route: ActivatedRoute,
		private router: Router,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnChanges() {
		if (this.contratEntretien !== undefined && this.contratEntretien !== null && this.selcted) {
			this.gamme_maintenance_equipement_Selected = this.formatGamme_maintenance_equipement_Selected(this.contratEntretien.equipmentMaintenance);
			this.articles = this.contratEntretien.orderDetails !== null && this.contratEntretien.orderDetails !== undefined ?
				this.contratEntretien.orderDetails.lineItems : [];
			this.selcted = false;
			this.processing = false;
		}

		// if (this.contratEntretien != undefined && Object.keys(this.contratEntretien).length > 0) {
		//   this.listFicheIntervention = this.contratEntretien.visiteMaintenance.filter
		// (element => element.ficheInterventionMaintenance != null).map(element => element.ficheInterventionMaintenance);
		this.reccurente = this.reccurente;
		//   this.facture = this.contratEntretien.factureReccurent.facture;
		// }
	}

	formatGamme_maintenance_equipement_Selected(equipementContrat) {
		if (equipementContrat !== undefined) {
			equipementContrat.forEach((elem, index) => {
				if (elem.installationDate !== undefined && elem.installationDate !== null) {
					equipementContrat[index].installationDate = new Date(elem.installationDate);
				}
				equipementContrat[index].maintenanceOperations = this.formaterPeriodiciet(elem.maintenanceOperations);
			});
			return equipementContrat;
		}
	}

	formaterPeriodiciet(result) {
		result.forEach((elem, index) => {
			result[index].periodicity = result[index].periodicity.length !== 0 ? this.getPeriodicity(elem.periodicity) : [];
			result[index].subOperations.forEach((ele, i) => {
				result[index].subOperations[i].periodicity = result[index].subOperations[i].periodicity.length !== 0 ?
					this.getPeriodicity(ele.periodicity) : [];
			});
		});
		return result;
	}

	getPeriodicity(periode) {
		const periodicite = [];
		for (let i = 1; i <= 12; i++) {
			periodicite.push({ mois: i, value: periode.includes(i) })
		}
		return periodicite;
	}

	generatePDFBase64(doc): void {
		this.service.visualPdf(ApiUrl.MaintenanceContrat, doc.id).subscribe(res => {
			this.base64 = res.value;
			this.genererPDF(doc)
		}, err => {
			this.translate.get('errors').subscribe(text => {
				toastr.warning('', text.serveur, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});
	}

	genererPDF(doc) {
		DialogHelper.openDialog(
			this.dialog,
			VisualiserPdfComponent,
			DialogHelper.SIZE_MEDIUM,
			{ base64: this.base64, docType: ApiUrl.MaintenanceContrat, doc: doc }
		).subscribe(async response => {
		});
	}

	//  Imprimer pdf
	imprimerPDF() {

		this.service.visualPdf(ApiUrl.MaintenanceContrat, this.contratEntretien.id).subscribe(res => {
			const stamp = new Date().getTime();

			//  file type
			const fileType = 'application/pdf';

			//  file data
			const fileData = AppSettings._base64ToArrayBuffer(res.value);

			//  file extension
			const extension = 'pdf';
			const pdfSrc = AppSettings.printPdf(fileData, this.contratEntretien.reference + '.' + extension, fileType, extension);

			const objFra = document.createElement('iframe');
			objFra.style.visibility = 'hidden';
			objFra.src = pdfSrc;
			document.body.appendChild(objFra);
			objFra.contentWindow.focus();
			objFra.contentWindow.print();
		});

	}


	// Generate pdf
	generatePDF(id) {

		this.service.visualPdf(ApiUrl.MaintenanceContrat, id).subscribe(res => {
			// Get time stamp for fileName.
			const stamp = new Date().getTime();

			// file type
			const fileType = 'application/pdf';

			// file extension
			const extension = 'pdf';

			AppSettings.downloadBase64(',' + res.value, this.contratEntretien.reference + '.' + extension, fileType, extension);

		});
	}

	getColor(status) { return this.color[status === 'finished' ? 'late' : status] };


	downloadPieceJointe(piece) {
		this.service.getById(ApiUrl.File, piece.fileId).subscribe(value => {
			AppSettings.downloadBase64(
				value.value,
				piece.fileName,
				value.value.substring('data:'.length, value.value.indexOf(';base64')),
				piece.fileType
			);
		});
	}

	//#region facture reccurent

	get f() {
		return this.form.controls;
	}

	getDate(date) {
		return this.datepipe.transform(new Date(date), 'dd/MM/yyyy');
	}
	getValid(res) {
		if (res !== null && res !== undefined) {
			return true
		}
		return false;
	}

	LoadPersonaliseReccurente() {

		let dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '500px';
		dialogLotConfig.data = {};
		const res = new ReccurenteModel();
		res.typeRepetition = this.reccurente.periodicityOptions.recurringType;
		res.nbrRepetition = this.reccurente.periodicityOptions.repeatEvery;
		res.jour = this.getValid(this.reccurente.periodicityOptions) ? this.reccurente.periodicityOptions.dayOfMonth : null;
		res.jourSemaine = this.getValid(this.reccurente.periodicityOptions) ? this.reccurente.periodicityOptions.daysOfWeek : null;

		dialogLotConfig.data = { listes: res, readOnly: true };

		const dialogRef = this.dialog.open(PersonaliseReccurenteComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});

	}
	readolyChecked() { return false; }

	getFacture(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		this.router.navigate([`/factures/detail/${id}`]);
	}

	//#endregion

}
