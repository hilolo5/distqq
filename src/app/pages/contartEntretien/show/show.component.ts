import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionHistorique } from 'app/Enums/ActionHistorique.Enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { ContratEntretienState } from '../contratEntretien-state';
import { CreateContratEntretien } from 'app/Enums/CreateContratEntretien.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { ContratEntretien, VisiteMaintenance } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { StatutContratEntretien } from 'app/Enums/Statut/StatutContratEntretien.Enum';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeDocument } from '../../../Enums/TypeDocument.enums';
import { MenuItem } from 'app/custom-module/primeng/api';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
import { MatDialog } from '@angular/material';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;
export declare var swal: any;
@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {
	dateLang: any;
	processing = false;
	adresses: Adresse[] = [];
	client: Client = new Client();
	contratEntretien = null;
	historique;
	actionsItems: MenuItem[] = [];
	statutContratEntretien: typeof StatutContratEntretien = StatutContratEntretien;
	memos: Memo[] = [];
	actionHistorique: ActionHistorique = new ActionHistorique();
	statuts: { id: number; label: string; color: string }[];
	id
	labels: any = null;
	visiteMain: VisiteMaintenance;
	selectedTabs = 'information';
	recurrente: any;
	docType = '';
	actionsOpened = false;
	items = [];
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private router: Router,
		private dialog: MatDialog,
		private header: HeaderService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		ManageDataTable.service = service;
		ManageDataTable.docType = this.docType;
		ManageDataTable.dialog = this.dialog;
	}

	ngOnInit() {
		this.processing = true;
		//	this.selectLanguage();
		this.getItems();
		this.translate.get('labels').subscribe(text => {
			this.labels = text;
		});
		this.route.params.subscribe(async params => {
			this.id = params['id'];
			this.translate.get('statuts').subscribe((statuts: { id: number; label: string; color: string }[]) => this.statuts = statuts);
			await this.refresh();
			this.prepareBreadcrumb();
			await this.visiteM();
			this.processing = false;
		});
		this.processing = false;
	}


	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.contratEntretien.reference, route: ManageDataTable.getRouteDetails(this.contratEntretien) }

			];
		});
	}
	async init(): Promise<void> {
		await this.refresh();
	}


	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}


	modifier() {
		ManageDataTable.getUpdate(this.contratEntretien);
	}

	onToggle(e: boolean): void {
		this.actionsOpened = e;
	}

	async setActionsItems() {
		const getTransaltion = () => {
			return new Promise((resolve, reject) => {
				this.translate.get('labels').subscribe(text => {
					resolve(text);
				});
			});
		}
		const labels: any = await getTransaltion();

		//  Modifier
		this.actionsItems.push({
			label: labels.modifier,
			icon: 'ft-edit',
			command: () => {
				this.navigateToEditComponent(this.contratEntretien.id);
			},
		});

		//  dupliquer
		this.actionsItems.push({
			label: labels.dupliquer,
			icon: 'pi pi-copy',
			command: () => {
				this.dupliquerContratEntretien();
			},
		});

		// print
		this.actionsItems.push({
			label: labels.print,
			icon: 'pi pi-print',
			command: () => {
				this.imprimerPDF();
			},
		});

		// export
		this.actionsItems.push({
			label: labels.exportExel,
			icon: 'pi pi-upload',
			command: () => {
				this.exportExel();
			},
		});

		// telecharger
		this.actionsItems.push({
			label: labels.telecharger,
			icon: 'fa fa-cloud-download',
			command: () => {
				this.generatePDF(this.id);
			},
		});

	}

	navigateToEditComponent(id): void {
		const url = `/contratentretiens/modifier/${id}`;
		this.router.navigate([url])
	}

	// Generate pdf

	generatePDF(id) {

		this.service.visualPdf(ApiUrl.Invoice, id).subscribe(res => {
			// Get time stamp for fileName.
			const stamp = new Date().getTime();

			// file type
			const fileType = 'application/pdf';

			// file extension
			const extension = 'pdf';

			AppSettings.downloadBase64(',' + res.value, this.contratEntretien.reference + '.' + extension, fileType, extension);

		});
	}

	visiteM() {
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.VisitMaintenance + '/contract/' + this.id).subscribe(async res => {
				this.visiteMain = res.value;
			}, err => {
			}, () => {
				reslove();
			});
		});
	}

	//  Imprimer pdf
	imprimerPDF() {

		this.service.visualPdf(ApiUrl.MaintenanceContrat, this.id).subscribe(res => {
			const stamp = new Date().getTime();

			//  file type
			const fileType = 'application/pdf';

			//  file data
			const fileData = AppSettings._base64ToArrayBuffer(res.value);

			//  file extension
			const extension = 'pdf';
			const pdfSrc = AppSettings.printPdf(fileData, this.contratEntretien.reference + '.' + extension, fileType, extension);

			const objFra = document.createElement('iframe');
			objFra.style.visibility = 'hidden';
			objFra.src = pdfSrc;
			document.body.appendChild(objFra);
			objFra.contentWindow.focus();
			objFra.contentWindow.print();
		});

	}

	// Refresh facture aprés l'ajout de paiement ou init component
	refresh(): Promise<void> {
		return new Promise((reslove, reject) => {
			this.service.getById(ApiUrl.MaintenanceContrat, this.id).subscribe(async res => {
				this.contratEntretien = res.value;
				this.historique = res.value.changesHistory;
				this.memos = this.contratEntretien.memos;
				await this.getReccurente(this.contratEntretien['recurringDocumentId']);
				this.actionsItems = [];
				this.setActionsItems();
			}, err => {

			}, () => {
				reslove();
			});
		});
	}


	getItems() {
		this.items = ManageDataTable.getActions(true);
	}


	getReccurente(id): Promise<void> {
		return new Promise((reslove, reject) => {
			this.service.getById(ApiUrl.Recurring, id).subscribe(async res => {
				this.recurrente = res.value;
			}, err => {

			}, () => {
				reslove();
			});
		});
	}


	/** --------------------------------------------------------
	 @description définir la langue utilisée dans le composant
	 --------------------------------------------------------*/
	selectLanguage(): void {
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	downloadPieceJointe(event) {
		const pieceJointe = event;
		this.service.getById(ApiUrl.File, pieceJointe.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, pieceJointe.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					pieceJointe.fileType)
			}
		)
	}

	async saveMemo(memo: Memo) {
		// if (memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		this.processing = true;
		memo['id'] = AppSettings.guid();
		this.service.create(ApiUrl.File + '/' + this.id + '/Create/Memo', memo)
			.subscribe(res => {
				this.processing = false;
				this.refresh();
			}, err => { console.log(err); this.processing = false; });
	}

	getLabelleByStatut(statut: StatutContratEntretien) {

		if (this.labels == null) {
			return
		}
		switch (statut) {
			case this.statutContratEntretien.Encours:
				return this.labels.encours;
				break;
			case this.statutContratEntretien.Enattente:
				return this.labels.enattente;
				break;
			case this.statutContratEntretien.Annule:
				return this.labels.annulee;
				break;
			case this.statutContratEntretien.Termine:
				return this.labels.termine;
				break;
			case this.statutContratEntretien.Brouillon:
				return this.labels.brouillon;
				break;
			//
		}
	}

	changeStatut(statut: StatutContratEntretien) {
		this.service.updateAll(ApiUrl.MaintenanceContrat + '/' + this.contratEntretien.id +
			'/Update/Status', { 'status': statut }).subscribe(res => {
				if (res) {
					this.refresh();
				} else {
				}
			}, err => {
				swal('', '', 'error');
			});
	}

	/**
	 * Dupliquer contrat entretien
	 */
	dupliquerContratEntretien() {
		ContratEntretienState.contratEntretien = this.contratEntretien;
		this.router.navigate(['/contratentretiens/ajouter', CreateContratEntretien.DUPLIQUER])
	}

	exportExel() {
		this.processing = true;
		this.service.getById(ApiUrl.MaintenanceContrat + '/Export', this.contratEntretien.id).subscribe(
			value => {
				this.processing = false;
				// Get time stamp for fileName.
				const stamp = new Date().getTime();

				// file type
				const fileType = 'application/vnd.ms-excel';

				// file data
				const fileData = AppSettings._base64ToArrayBuffer(value.value);

				// file extension
				const extension = 'xlsx';
				AppSettings.setFile(fileData, ('GammeMaintenanceEquipement') + stamp + '.' + extension, fileType, extension);
			},
			err => {
				console.log(err);
				this.processing = false;
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.server, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			}
		)
	}

}
