import { Component, Input, Output, EventEmitter, OnInit, OnChanges, Inject } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IFormType } from './IFormType.enum';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateConfiguration } from 'app/libraries/translation';
import { SortDirection } from 'app/Models/Model/filter-option';
import * as _ from 'lodash';
declare var toastr: any;
declare var jQuery: any;

@Component({
	selector: 'lots-from',
	templateUrl: './lotsFrom.component.html',
	styleUrls: ['./lotsFrom.component.scss']
})
export class lotsFromComponent implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	type;
	// tslint:disable-next-line:no-input-rename
	defaultData;
	// lotName = "";
	// description = "";
	produits = [];
	formType: typeof IFormType = IFormType;
	search = '';
	labels: any;
	produitsSelectionneeProvisoirement = [];
	// produitsSelectionnee: Produit[] = [];
	loading = false;
	form;
	page = 1;
	totalPage = 5;
	listP = [];
	searchQuery = '';
	pageNamber = 1;
	pageSize = 50;

	constructor(
		public dialogRef: MatDialogRef<lotsFromComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { type: any, defaultData: any },
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb?: FormBuilder,
		private translate?: TranslateService) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	async ngOnInit() {
		this.form = this.fb.group({
			lotName: ['', [Validators.required, Validators.minLength(3)], this.CheckUniqueName.bind(this)],
			description: [null],
		});
		this.type = this.data.type;
		this.getProduitss();
		this.defaultData = this.data.defaultData;
		await this.initialiser();
	}

	CheckUniqueName(control: FormControl) {
		const promise = new Promise((resolve, reject) => {
			this.service
				.getById(ApiUrl.Lot + ApiUrl.CheckName, control.value)
				.subscribe(res => {
					if (res === false) {
						resolve({ CheckUniqueReference: true });
					} else {
						resolve(null);
					}
				});
		});
		return promise
	}

	clearform() {
		this.form.reset();
	}

	async ngOnChanges() {
		this.type = this.data.type;
		this.defaultData = this.data.defaultData;
		await this.initialiser();
	}

	async initialiser() {
		this.form = this.fb.group({
			lotName: ['', [Validators.required, Validators.minLength(3)], this.CheckUniqueName.bind(this)],
			description: [null],
		});

		this.produitsSelectionneeProvisoirement = []
		// this.produits = ;
		this.form.controls['lotName'].setValue(this.defaultData == null ? '' : this.defaultData.name);
		this.form.controls['description'].setValue(this.defaultData == null ? '' : this.defaultData.description);

	}

	async close() {
		this.dialogRef.close();
	}

	async setProduitFromDefaultDataToProduitTmp(): Promise<void> {

		if (this.defaultData == null) {
			return;
		}
		this.produitsSelectionneeProvisoirement = [];
		this.defaultData.products.forEach((lotProduits) => {
			if (this.type === IFormType.preview) {
				const produit = lotProduits.productDetails;
				produit.qte = lotProduits.quantity;
				this.produitsSelectionneeProvisoirement.push(produit);
			} else if (this.listP && this.listP.length > 0) {
				const produit = this.listP.find(P => P.id === lotProduits.productId);
				if (produit) {
					produit.qte = lotProduits.quantity;
					this.produitsSelectionneeProvisoirement.push(produit);
				} else {
					const res = lotProduits.productDetails;
					res.qte = lotProduits.quantity;
					this.produitsSelectionneeProvisoirement.unshift(res);
				}
			}
		});

		this.produitsSelectionneeProvisoirement = _.uniq(this.produitsSelectionneeProvisoirement, 'id'); 

		if (this.type !== IFormType.preview) {
			this.produitsSelectionneeProvisoirement.forEach((produit) => {
				this.produits = this.produits.filter(P => P.id !== produit.id)
			});
		}
	}



	addProduit(index: number, value?): void {
		/** si non retiré à partir du @var produits el l'ajouter dans @var produitTmp*/
		const produit = this.produits[index];
		const qte = (produit.qte === NaN || produit.qte === undefined) ? 0 : produit.qte;
		produit.qte = parseInt(value) > 0 ? qte + parseInt(value) : (qte > 0 ? (qte - parseInt(value)) : 0);
		this.produitsSelectionneeProvisoirement.unshift(produit);
		this.produits.splice(index, 1);
	}

	removeProduit(index) {
		const produit = this.produitsSelectionneeProvisoirement[index];
		produit.qte = 0;
		this.produits.unshift(produit);
		this.produitsSelectionneeProvisoirement.splice(index, 1);
	}

	changetQte(index, qte, type?: boolean) {

		const produit = this.produitsSelectionneeProvisoirement[index];
		if (qte === '') {
			produit.qte = 0;
			return;
		}
		if ((produit.qte === 1 && parseInt(qte) < 0) || parseInt(qte) === 0) {
			this.removeProduit(index);
			return;
		}
		produit.qte = type ? parseInt(qte) : produit.qte + parseInt(qte);
		return;
	}

	async searche() {

		if (this.search !== '' && this.search !== null) {
			this.produits = Object.assign([], this.produits).filter(
				item => item.reference.toLowerCase().indexOf(this.search.toLowerCase()) > -1 ||
					item.designation.toLowerCase().indexOf(this.search.toLowerCase()) > -1
			)
			this.produitsSelectionneeProvisoirement.forEach((produit) => {
				this.produits = this.produits.filter(P => P.id !== produit.id)
			});
		} else {
			this.produits = [];
			this.getProduitss();
		}
	}

	/**
	 * créer l'objet du 'lot' et le retourner
	 */
	submit(): void {
		if (this.form.value.lotName.length >= 3 && this.produitsSelectionneeProvisoirement.length > 0) {
			const lotProducts: { productId: number, quantity: number }[] = [];
			this.produitsSelectionneeProvisoirement.forEach(produit => {
				lotProducts.push({ productId: produit.id, quantity: produit.qte });
			});
			const lot = {
				id: this.defaultData == null ? 0 : this.defaultData.id,
				name: this.form.value.lotName,
				description: this.form.value.description,
				products: lotProducts
			}
			this.form.controls['lotName'].setValue('');
			this.form.controls['description'].setValue('');
			this.produitsSelectionneeProvisoirement = [];
			this.defaultData = null;
			this.OnSave(lot);
		} else {
			this.translate.get('lotFromValidationErrorMsg').subscribe(text => {
				setTimeout(toastr.success(text.msg, text.title, {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				}), 0.5);
			});
		}
	}

	OnSave(lot) {
		if (this.type === IFormType.add) {
			delete lot.id;
			this.add(lot);
		}
		if (this.type === IFormType.update) {
			this.update(lot);
		}
	}
	add(lot) {
		this.service.create(ApiUrl.Lot + ACTION_API.create, lot).subscribe(res => {
			const text = this.translate.instant('toast.add-sucsess')
			this.dialogRef.close(res.value);
			toastr.success('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}, err => {
			this.dialogRef.close();
			const text = this.translate.instant('errors.serveur')
			toastr.warning('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

		});
	}

	update(lot) {
		this.service.update(ApiUrl.Lot, lot, lot.id).subscribe(res => {
			const text = this.translate.instant('toast.update-sucsess')
			this.dialogRef.close(res.value);
			toastr.success('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

		}, err => {
			const text = this.translate.instant('errors.serveur')
			toastr.warning('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

		});
	}

	/**
	 *  vérifier si un produit est déja sélectionnée,
	 * par verifie si il exisists dans le tableau @var produitTmp
	 * */
	checkProduitIsSelectedOrNot(idProduit: number): boolean {
		try {
			return this.produitsSelectionneeProvisoirement.filter(P => P.id === idProduit).length > 0 ? true : false;
		} catch (ex) {
			return false;
		}
	}

	getmodelName() {
		const lot = this.translate.instant('LABELS.LOT');
		if (this.type === IFormType.add) {
			return this.translate.instant('LABELS.ADD') + ' ' + lot;
		}
		if (this.type === IFormType.preview) {
			return this.translate.instant('LABELS.SHOW') + ' ' + lot;

		}
		if (this.type === IFormType.update) {
			return this.translate.instant('LABELS.EDIT') + ' ' + lot;

		}
	}

	validate(): boolean {
		return this.produitsSelectionneeProvisoirement.length > 0 && this.form.valid
	}


	calculate_cout_horaire(nomber_heure, cout_vente): number {
		return parseFloat(nomber_heure) * parseFloat(cout_vente);
	}

	TotalHt(item) {
		const calculate_cout_horaire = item.totalHours * item.hourlyCost;
		return (calculate_cout_horaire + item.materialCost).toFixed(2)
	}

	prixTtc(produit): number | 0 {
		if (produit === undefined) { return; }
		return this.TotalHt(produit) * ((produit.vat / 100) + 1);
	}

	get f() { return this.form.controls; }

	onScroll() {
		if (this.totalPage !== this.page) {
			this.page++;
			this.getProduitss();
		}
	}

	getProduitss() {
		this.service.getAllPagination(ApiUrl.Produit, {
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 5,
			OrderBy: 'reference',
			SortDirection: SortDirection.Descending
		}).subscribe(async res => {
			if (res.value.length > 0) {
				this.produits = [...this.produits, ...res.value];
				this.listP = [...this.produits, ...res.value];
				await this.setProduitFromDefaultDataToProduitTmp();
			}
			this.totalPage = res.pagesCount;
		}, err => {
			console.log(err)
		});
	}
}
