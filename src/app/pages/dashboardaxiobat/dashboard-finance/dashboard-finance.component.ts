import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { DashboardFinanceModel } from 'app/Models/Entities/Dashboard/DashboardFinance';
import * as Chart from 'chart.js';
import { ObjectifsAnnuelCA } from 'app/Models/Entities/Dashboard/DashbordVente';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;

@Component({
	selector: 'app-dashboard-finance',
	templateUrl: './dashboard-finance.component.html',
	styleUrls: ['./dashboard-finance.component.scss']
})
export class DashboardFinanceComponent implements OnInit {
	@ViewChild('barChartMunsuelle') barChartMunsuelle;
	@ViewChild('barMargeMunsuelle') barMargeMunsuelle;

	@ViewChild('barMargeAnnuelle') barMargeAnnuelle;
	@ViewChild('barCAAnnuelle') barCAAnnuelle;
	@ViewChild('selecteClients') selecteClients: NgSelectComponent;
	@ViewChild('barObjectifAnnuelle') barObjectifAnnuelle;
	@ViewChild('lineObjectifMunsuelle') lineObjectifMunsuelle;

	periode = PeriodType.None
	periodeEnum: typeof PeriodType = PeriodType;
	dateMinimal
	dateMaximal
	idClient: [] = [];
	dashboardFinance: DashboardFinanceModel = null;
	montantencaissements = 0;
	CArestantencaisser = 0;
	achatrestantencaisser = 0;
	soldeFinance = 0;
	filterlistClient = [];
	objectifAnnuelCA: ObjectifsAnnuelCA;
	objectifsAnnuelCAN: ObjectifsAnnuelCA;
	listClients: Client[] = null;
	dateLang;
	labelsYears: any;
	labels = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
	datasetsCA1: any = [];
	datasetsMarge1: any = [];
	datasetsCA2: any = [];
	datasetsMarge2: any = [];
	chartMunsuelle: any;
	datasetCaAnnuelle1: any = [];
	datasetCaAnnuelle2: any = [];

	datasetMargeAnnuelle1: any = [];
	datasetMargeAnnuelle2: any = [];
	yearCurrent = new Date().getFullYear();
	yearLast = new Date().getFullYear() - 1;
	chartMargeMunsuelle: any;
	chartcaAnnuelle: any;
	chartobjectifsAnnuelle: any;
	chartmargeAnnuelle: any;
	chartLine: any;


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService) {
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	ngOnInit() {
		this.dashboardVente();
	}
	filter() {
		this.dashboardVente();

	}

	getClients() {

		if (this.listClients == null) {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
				this.filterlistClient = this.formaterClient(this.listClients);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteClients.isOpen = true;
			});
		}
	}

	formaterClient(list: Client[]) {
		const listClient = [];
		list.forEach((client, index) => {
			listClient.push({ id: client.id, name: client.firstName, disabled: false });
		});
		return listClient;
	}

	dashboardVente() {

		const res = {
			externalPartnerId: this.idClient,
			dateStart: this.dateMinimal,
			dateEnd: this.dateMinimal,
			periodType: this.periode,
		};
		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;
		this.service.create(ApiUrl.analytic + '/Financial', res).subscribe(result => {
			this.dashboardFinance = result.value;
			this.datasetsCA1 = this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge1 = this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.monthlyIncreaseDetails.map(x => x.margin);

			this.datasetsCA2 = this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge2 = this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.monthlyIncreaseDetails.map(x => x.margin);

			this.datasetCaAnnuelle1 = [this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.totalIncome];
			this.datasetCaAnnuelle2 = [this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.totalIncome];

			this.datasetMargeAnnuelle1 = [this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.margin];
			this.datasetMargeAnnuelle2 = [this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.margin];

			this.labelsYears = this.getLabels(this.yearLast, this.yearCurrent);

			this.objectifAnnuelCA = this.dashboardFinance.turnoverAchievementDetails.currentPeriod;
			this.objectifsAnnuelCAN = this.dashboardFinance.turnoverAchievementDetails.preCurrentPeriod;

			this.grapheProgressionMunsuelle();
			this.grapheProgressionMargeMunsuelle();
			this.grapheCAannuelle();
			this.grapheMargeannuelle();

			this.montantencaissements = this.dashboardFinance.turnoverDetails.totalIncome;
			this.CArestantencaisser = this.dashboardFinance.turnoverDetails.totalUnpaidIncome;
			this.achatrestantencaisser = this.dashboardFinance.turnoverDetails.totalUnpaidExpenses;
			this.soldeFinance = this.CArestantencaisser - this.achatrestantencaisser;

			this.grapheObjectifsannuelle(Math.min(this.ifNan(this.objectifsAnnuelCAN.percentage)).toFixed(2),
				Math.min(this.ifNan(this.objectifAnnuelCA.percentage)).toFixed(2)
			);
			const data1 = this.objectifsAnnuelCAN.monthlyDetails.map(x =>
				Math.min(this.ifNan(x.percentage)).toFixed(2))

			const data2 = this.objectifAnnuelCA.monthlyDetails.map(x =>
				Math.min(this.ifNan(x.percentage)).toFixed(2))
			this.chartObjectCALine(data1, data2);
		}
		);
	}

	ifNan(perce) {
		if (perce === 'Infinity') { return 100; }
		if (perce === 'NaN') { return 0; }
		// tslint:disable-next-line:radix
		return parseInt(perce);
	}

	getLabels(yearLast, yearCurrent) {
		return [yearLast.toString(), yearCurrent.toString()]
	}

	grapheProgressionMunsuelle() {

		if (this.chartMunsuelle) {
			this.chartMunsuelle.destroy();
		}
		this.chartMunsuelle = new Chart(this.barChartMunsuelle.nativeElement, {
			type: 'bar',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'CA' + this.yearLast,
						data: this.datasetsCA2,
						backgroundColor: '#dbdcdf',
						hoverBackgroundColor: '#dbdcdf',
						fill: false,
						lineTension: 0.2,
						borderWidth: 1
					},
					{
						label: 'CA' + this.yearCurrent,
						data: this.datasetsCA1,
						backgroundColor: '#46b5e1',
						hoverBackgroundColor: '#46b5e1',
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					},
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	}


	grapheProgressionMargeMunsuelle() {

		if (this.chartMargeMunsuelle) {
			this.chartMargeMunsuelle.destroy();
		}

		this.chartMargeMunsuelle = new Chart(this.barMargeMunsuelle.nativeElement, {
			type: 'bar',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'Marge ' + this.yearLast,
						data: this.datasetsMarge2,
						backgroundColor: '#dbdcdf',
						hoverBackgroundColor: '#dbdcdf',

						fill: false,
						lineTension: 0.2,
						borderWidth: 1
					},
					{
						label: 'Marge ' + this.yearCurrent,
						data: this.datasetsMarge1,
						backgroundColor: '#46b5e1',
						hoverBackgroundColor: '#46b5e1',
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					},
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	}

	grapheCAannuelle() {
		if (this.chartcaAnnuelle) {
			this.chartcaAnnuelle.destroy();
		}
		this.chartcaAnnuelle = new Chart(this.barCAAnnuelle.nativeElement, {
			type: 'bar',
			data: {
				labels: ['Progression de la CA  annuelle'],
				datasets: [
					{

						label: 'CA' + this.yearLast,
						data: this.datasetCaAnnuelle2,
						backgroundColor: '#dbdcdf',
						hoverBackgroundColor: '#dbdcdf',
						fill: false,
						lineTension: 0.2,
						borderWidth: 1
					},
					{
						label: 'CA' + this.yearCurrent,
						data: this.datasetCaAnnuelle1,
						backgroundColor: '#46b5e1',
						hoverBackgroundColor: '#46b5e1',
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					},
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	}

	grapheMargeannuelle() {

		if (this.chartmargeAnnuelle) {
			this.chartmargeAnnuelle.destroy();
		}

		this.chartmargeAnnuelle = new Chart(this.barMargeAnnuelle.nativeElement, {
			type: 'bar',
			data: {
				labels: ['Progression de la marge  annuelle'],
				datasets: [
					{
						label: 'Marge' + this.yearLast,
						data: this.datasetMargeAnnuelle2,
						backgroundColor: '#dbdcdf',
						hoverBackgroundColor: '#dbdcdf',
						fill: false,
						lineTension: 0.2,
						borderWidth: 1
					},
					{
						label: 'Marge ' + this.yearCurrent,
						data: this.datasetMargeAnnuelle1,
						backgroundColor: '#46b5e1',
						hoverBackgroundColor: '#46b5e1',
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					},
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	}


	grapheObjectifsannuelle(data1, data2) {
		if (this.chartobjectifsAnnuelle) {
			this.chartobjectifsAnnuelle.destroy();
		}

		this.chartobjectifsAnnuelle = new Chart(this.barObjectifAnnuelle.nativeElement, {
			type: 'bar',
			data: {
				labels: ['Prévision CA'],
				datasets: [
					{

						label: 'Prévision CA' + this.yearLast,
						data: [data1],
						backgroundColor: '#dbdcdf',
						hoverBackgroundColor: '#dbdcdf',

						fill: false,
						lineTension: 0.2,
						borderWidth: 1
					},
					{
						label: 'Prévision CA  ' + this.yearCurrent,
						data: [data2],
						backgroundColor: '#46b5e1',
						hoverBackgroundColor: '#46b5e1',
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					},
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});

	}


	chartObjectCALine(data1, data2) {
		if (this.chartLine) {
			this.chartLine.destroy();
		}

		this.chartLine = new Chart(this.lineObjectifMunsuelle.nativeElement, {
			type: 'line',
			data: {
				labels: this.labels,
				datasets: [{
					label: 'Prévision CA  ' + this.yearLast,
					backgroundColor: '#3e95cd',
					borderColor: '#3e95cd',
					data: data1,
					fill: false,
					borderWidth: 2
				}, {
					label: 'Prévision CA  ' + this.yearCurrent,

					backgroundColor: '#ff3300',
					borderColor: '#ff3300',
					data: data2,
					fill: false,
					borderWidth: 2
				}]
			},
			options: {
				legend: {
					position: 'bottom'
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							maxTicksLimit: 5,
							padding: 20
						},
						gridLines: {
							drawTicks: false,
							display: false
						}
					}],
					xAxes: [{
						gridLines: {
							zeroLineColor: 'transparent'
						},
						ticks: {
							padding: 20,
							fontColor: 'rgba(0,0,0,0.5)',
						}
					}]
				}
			}
		});
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}

}
