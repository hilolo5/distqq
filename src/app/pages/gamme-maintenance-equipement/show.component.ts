import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { GammeMaintenanceEquipement, OperationMaintenance } from 'app/Models/Entities/Maintenance/GammeMaintenanceEquipement';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { Action } from 'app/Enums/action';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

	form
	public processing = true;
	emitter: any = {};
	gammeMaintenanceEquipement;
	id;
	equipements = [];
	docType = '';
	readOnly = false;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder,
		private router: Router,
		private header: HeaderService,
		private route: ActivatedRoute,
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
	}

	ngOnInit() {
		this.init();
		this.route.params.subscribe(

			async params => {
				this.init();
			}
		);


	}

	init() {
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;

		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		if (EditDocumentHelper.isAction(Action.INFORMATION)) { this.readOnly = true; }

		if (!EditDocumentHelper.isAction(Action.INFORMATION)) {
			this.form = this.fb.group({
				nom: ['', [Validators.minLength(3), Validators.required], this.checkUniqueNom.bind(this)],
				description: ['']
			});
		}

		this.route.params.subscribe(params => {
			if (params['id'] !== undefined) {
				this.processing = true;
				this.id = params['id'];
				this.GetGammeMaintenanceEquipement(this.id);
			}

		});
		this.processing = false;
	}
	GetGammeMaintenanceEquipement(id) {
		this.processing = true;
		this.service.getById(ApiUrl.GME, id).subscribe(gammeMaintenanceEquipement => {
			this.gammeMaintenanceEquipement = gammeMaintenanceEquipement.value;
			this.prepareBreadcrumb();
			this.SetData(this.gammeMaintenanceEquipement);
			this.processing = false;

		});
	}

	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.gammeMaintenanceEquipement.equipmentName, route: ManageDataTable.getRouteDetails(this.gammeMaintenanceEquipement) }

			];
		});
	}

	/*-----------------------------------------------------------*/
	SetData(gammeMaintenanceEquipement: GammeMaintenanceEquipement) {
		if (!EditDocumentHelper.isAction(Action.INFORMATION)) {
			this.form.controls['nom'].setValue(gammeMaintenanceEquipement.equipmentName);
		}
		this.equipements = gammeMaintenanceEquipement.maintenanceOperations;
		this.formaterPeriodiciet();
	}


	checkUniqueNom(control: FormControl) {
		const promise = new Promise((resolve) => {
			this.service.getById(ApiUrl.GME + ApiUrl.CheckName, control.value).subscribe(
				res => {
					if (!res && this.isModif() && this.form.value.nom !== this.gammeMaintenanceEquipement.equipmentName) {
						resolve({ checkUniqueNom: true });
					} else if (!res && !this.isModif()) {
						resolve({ checkUniqueNom: true });
					} else {
						resolve(null);
					}
				}
			)
		});
		return promise;
	}

	getOperation(): OperationMaintenance[] {
		const result = this.emitter.getOperations;
		result.forEach((element, index) => {
			result[index].periodicity = this.filterPeriodicity(element.periodicity);
			result[index].subOperations.forEach((ele, i) => {
				result[index].subOperations[i].periodicity = this.filterPeriodicity(ele.periodicity);
			});
		});
		return result;
	}


	filterPeriodicity(periodicity) {
		if (periodicity !== undefined) {
			return periodicity.filter(x => x.value === true).map(y => y.mois);
		}
		return [];
	}


	submit() {
		this.processing = true;
		this.translate.get('errors').subscribe(text => {
			if (!this.form.valid) {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				return;
			}
			const operations = this.getOperation();
			if (operations.length === 0) {
				toastr.warning(text.perationsRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				return;
			}

			if (operations.filter(x => x.periodicity.length === 0 && x.subOperations.length === 0).length > 0) {
				// tslint:disable-next-line: no-shadowed-variable
				this.formaterPeriodiciet();
				const text = this.translate.instant('errors.equipement.perationsRequired')
				toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				return;
			}

			const values = this.form.value;
			const gammeMaintennaceEquipement: GammeMaintenanceEquipement = new GammeMaintenanceEquipement();
			gammeMaintennaceEquipement.equipmentName = values.nom;
			gammeMaintennaceEquipement.maintenanceOperations = operations;

			if (EditDocumentHelper.isAction(Action.MODIFIER)) {
				this.service.update(ApiUrl.GME, gammeMaintennaceEquipement, this.id).subscribe(res => {
					if (res) {
						this.processing = false;
						this.toast('add-sucsess');
						this.router.navigate(['/gammemaintenanceequipements/detail', res.value.id]);
					} else {
						this.processing = false;
						this.translate.get('errors').subscribe(textt => {
							toastr.warning(textt.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						})
					}
				})
			} else {
				this.service.create(ApiUrl.GME + ACTION_API.create, gammeMaintennaceEquipement).subscribe(res => {
					if (res) {
						this.processing = false;
						this.toast('add-sucsess');
						this.router.navigate(['/gammemaintenanceequipements/detail', res.value.id]);
					} else {
						this.processing = false;
						this.translate.get('errors').subscribe(textt => {
							toastr.warning(textt.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						})
					}
				})
			}

		});
	}

	toast(msg) {
		return toastr.success(this.translate.instant('toast.' + msg), '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}

	formaterPeriodiciet() {
		this.equipements.forEach((elem, index) => {
			this.equipements[index].periodicity = this.equipements[index].periodicity.length !== 0 ? this.getPeriodicity(elem.periodicity) : [];
			this.equipements[index].subOperations.forEach((ele, i) => {
				this.equipements[index].subOperations[i].periodicity = this.equipements[index].subOperations[i].periodicity.length !== 0 ?
					this.getPeriodicity(ele.periodicity) : [];
			});
		});
	}


	getPeriodicity(periode) {
		const periodicite = [];
		for (let i = 1; i <= 12; i++) {
			periodicite.push({ mois: i, value: periode.includes(i) })
		}
		return periodicite;
	}

	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER) }
	isCreate() { return EditDocumentHelper.isAction(Action.AJOUTER) }
	isDetail() { return EditDocumentHelper.isAction(Action.INFORMATION) }
	isValid() { return this.isModif() || EditDocumentHelper.isAction(Action.AJOUTER) }

	close() { return this.router.navigate(['/gammemaintenanceequipements']); }

	get f() { return this.form.controls; }

	delete(element) {
		ManageDataTable.docType = ApiUrl.GME;
		ManageDataTable.service = this.service;
		ManageDataTable.deleteRequest(element);
	}

}
