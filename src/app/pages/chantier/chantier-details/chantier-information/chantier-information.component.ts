import { Component, OnInit, Input, OnChanges, Inject, ViewChild, ElementRef } from '@angular/core';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Color } from 'app/Enums/color';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { MatDialog } from '@angular/material';
import { MenuService } from 'app/services/menu/menu.service';


@Component({
	// tslint:disable-next-line:component-selector
	selector: 'chantier-information',
	templateUrl: './chantier-information.component.html',
	styleUrls: ['./chantier-information.component.scss']
})
export class ChantierComponent implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('chantier') chantier;
	@ViewChild('label') labelElement: ElementRef;

	statutChantier: typeof StatutChantier = StatutChantier;
	tauxAvancement = {
		valueInDb: 0,
		currentValue: 0
	};

	title = 'materialApp';
	disabled = false;
	invert = false;
	thumbLabel = false;
	value = 0;
	vertical = false;
	color: typeof Color = Color;

	getColor(status) { return this.color[status] };

	constructor(@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private dialog: MatDialog,
		public menu: MenuService,

	) {

	}
	ngOnInit() {
		ManageDataTable.dialog = this.dialog;

		if (this.chantier !== undefined && this.chantier != null) {
			if (this.chantier.progressRate == null) {
				this.tauxAvancement.currentValue = 0;
				this.tauxAvancement.valueInDb = 0;
			} else {
				this.tauxAvancement.currentValue = this.chantier.progressRate;
				this.tauxAvancement.valueInDb = this.chantier.progressRate;
			}

		}
	}
	ngOnChanges() {
		if (this.chantier !== undefined && this.chantier != null) {
			if (this.chantier.progressRate == null) {
				this.tauxAvancement.currentValue = 0;
				this.tauxAvancement.valueInDb = 0;
			} else {
				this.tauxAvancement.currentValue = this.chantier.progressRate;
				this.tauxAvancement.valueInDb = this.chantier.progressRate;
			}
		}

	}

	ChangementTauxAvencement() {
		const result = {
			'progressRate': this.tauxAvancement.currentValue
		};
		this.service.updateAll(ApiUrl.Chantier + '/' + this.chantier.id + '/Update/ProgressRate', result).subscribe(res => {
			if (this.chantier.progressRate == null) {
				this.tauxAvancement.currentValue = 0;
				this.tauxAvancement.valueInDb = 0;
			} else {
				this.tauxAvancement.currentValue = res.value.progressRate;
				this.tauxAvancement.valueInDb = res.value.progressRate;
			}
		})
	}


	getLabelPos(): number | string {
		let res: string | number = 0;

		if (this.labelElement && this.labelElement.nativeElement) {
			const width = this.labelElement.nativeElement.offsetWidth;
			const ratio = width * (this.value / 100);

			res = ratio <= 25.39
				? `calc(${this.value}% + 25.39px)`
				: ratio >= width - 25.39
					? `calc(${this.value}% - 25.39px)`
					: `${this.value}%`;
		}

		return res;
	}

	formatDisplay = () => `${(this.tauxAvancement.currentValue || 0).toFixed(2)} %`;

}
