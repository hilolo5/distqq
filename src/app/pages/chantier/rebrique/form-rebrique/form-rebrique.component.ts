import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { IFormType } from 'app/pages/lots/lots-form/IFormType.enum';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
import { Constants } from 'app/shared/utils/constants';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;

@Component({
	selector: 'app-form-rebrique',
	templateUrl: './form-rebrique.component.html',
	styleUrls: ['./form-rebrique.component.scss']
})
export class FormRebriqueComponent implements OnInit {
	documentationForm = this.fb.group({ /*type: [],*/ designation: [], commentaire: [] });
	formType: typeof IFormType = IFormType;
	labels: any;
	files = null;
	file: any;
	listLablDocument: any
	allTagsList = [];
	listLabelTags = [];
	tagsSelected: { value: string, origine: boolean }[] = [];
	emitter: any = {};
	lIstnom: any
	readOnly = false;
	constructor(
		public dialogRef: MatDialogRef<FormRebriqueComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { defaultData: any, type: any, readOnly: boolean, typesDocuments: any },
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() {
		this.readOnly = this.data.readOnly;
		this.createEmptyForm();
		this.FilterTagsList();
	}

	createEmptyForm() {
		const designation = this.data.defaultData == null ? '' : this.data.defaultData.designation;
		const commentaire = this.data.defaultData == null ? '' : this.data.defaultData.comment;
		if (this.data.defaultData != null) {
			this.listLabelTags = [];
			this.data.defaultData.types.forEach(tag => {
				this.listLabelTags.push({ value: tag, origine: false })
			});
		}
		this.documentationForm = this.fb.group({
			designation: [designation, [Validators.minLength(2), Validators.required]],
			commentaire: [commentaire]
		});
		this.file = this.data.defaultData == null ? null : this.data.defaultData['attachments'][0];
	}
	get f() { return this.documentationForm.controls; }

	emptyList() {

		this.emitter.emptyList();
		this.documentationForm.reset();
		this.file = null;
	}

	checkIfAllFiledsIsValid(): boolean {
		if (!this.documentationForm.valid || this.file == null) {
			const text = this.translate.instant('errors.fillAll');
			toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

			return false;
		}
		return true;
	}


	close() {
		this.dialogRef.close()

	}
	save() {
		const response = {
			// id: this.type == IFormType.add ? 0 : this.defaultData.id,
			rubricId: 1,
			types: [],
			comment: this.documentationForm.value.commentaire == null ? '' : this.documentationForm.value.commentaire,
			designation: this.documentationForm.value.designation,
			attachments: [this.file],
		}

		this.tagsSelected.forEach(tag => {
			tag.value['isDefault'] = false;
			response['types'].push(tag.value);
		});
		this.dialogRef.close(response)
		this.reInitialser();

	}
	reInitialser() {
		this.documentationForm.reset();
		this.file = null;
		this.tagsSelected = [];
		this.allTagsList = [];
		this.FilterTagsList();
	}

	getmodelName() {
		let title = '';
		const doc = this.translate.instant('labels.document')
		if (+this.data.type === IFormType.add) {
			const text = this.translate.instant('labels.attacher')
			title = text + ' ' + doc;
		}
		if (+this.data.type === IFormType.preview) {
			const text = this.translate.instant('labels.afficher')
			title = text + ' ' + doc;
		}
		if (+this.data.type === IFormType.update) {
			const text = this.translate.instant('labels.modifier')
			title = text + ' ' + doc;
		}
		return title;
	}

	//#region File
	startUpload(event: FileList): void {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			pieceJoin.fileType = Constants.getContentType(file.name.substring(file.name.lastIndexOf('.') + 1)).Mime;
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString()
			this.file = pieceJoin;
		}
	}

	downloadPieceJointe() {
		this.service.getById(ApiUrl.File, this.file.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, this.file.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					this.file.fileType)
			}
		)
	}

	removeFile() {
		this.file = null;
	}
	//#endregion

	//#region Tags
	FilterTagsList() {
		if (this.data.typesDocuments != null) {
			this.data.typesDocuments.forEach(tag => {
				this.allTagsList.push({ value: tag, origine: true });
			});
		}
	}


	onTagsChange(Tags: [{ value: string, origine: boolean }]) {
		this.tagsSelected = Tags;
	}
	//#endregion

}
