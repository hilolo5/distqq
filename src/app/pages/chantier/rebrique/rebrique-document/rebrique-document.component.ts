import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { TranslateService } from '@ngx-translate/core';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { LabelShowModel } from 'app/Models/Model/LabelShowModel';
import { IFormType } from '../../../../Enums/IFormType.enum';
import { Constants } from 'app/shared/utils/constants';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { FormRebriqueComponent } from '../form-rebrique/form-rebrique.component';
import { ColumnType } from 'app/components/form-data/data-table/data-table.component';
import { DataTableRowActions } from 'app/libraries/manage-data-table';
import { IFilterOption, SortDirection } from 'app/Models/Model/filter-option';
import { FormControl } from '@angular/forms';
import { PeriodType } from 'app/Enums/Maintenance/typePeriode.Enum';
import { PagedResult } from 'app/Models/Model/ListModel';
import { GlobalInstances } from 'app/app.module';
import { StringHelper } from 'app/libraries/string';

export declare var swal: any;
declare var toastr: any;

@Component({
	selector: 'app-rebrique-document',
	templateUrl: './rebrique-document.component.html',
	styleUrls: ['./rebrique-document.component.scss'],
})
export class DocumentAttacherComponent implements OnInit {
	formType: typeof IFormType = IFormType;
	processing = false;
	tableColumnsDocuments;
	actions: DataTableRowActions[];
	selected: any;
	idChantier = null;
	typesDocuments
	module: number = null;
	pageSizeOptions = AppSettings.PAGE_SIZE_OPTIONS;
	columnType: typeof ColumnType = ColumnType;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private router: Router,
		@Inject(DOCUMENT) private document: any,
		private dialog: MatDialog,

	) { }

	async ngOnInit() {
		this.route.params.subscribe(params => {
			this.idChantier = params['id'];
			this.module = params['module'];
			this.initData()
		});

		this.tableColumnsDocuments = [

			{
				name: 'designation', nameTranslate: 'labels.designation', isOrder: false, type: ColumnType.any,
				appear: true
			},


			{
				name: 'comment', nameTranslate: 'labels.commentaire', isOrder: true, type: ColumnType.any,
				appear: true
			},
			{
				name: 'labelDocument', nameTranslate: 'labels.labelDocument', isOrder: false, type: ColumnType.Html,
				appear: true
			},
			{
				name: 'userName', nameTranslate: 'labels.user', isOrder: false, type: ColumnType.any,
				appear: true
			},

		];
	}
	public initData() {
		this.initFilters();
		this.getData();
		//GlobalInstances.router.navigateByUrl(`/chantiers`);
	}

	openFormRebrique(element?, type?) {
		//this.idrubrique = element.id;
		let defaultData = null;
		let readOnly = false;

		if (type != this.formType.add) {
			defaultData = element;
		}
		if (type === this.formType.preview) {
			readOnly = true;
		}
		const dialogRebriqueConfig = new MatDialogConfig();
		dialogRebriqueConfig.width = '850px';
		dialogRebriqueConfig.height = '650px';
		dialogRebriqueConfig.data = { defaultData: defaultData, type: type, readOnly: readOnly, typesDocuments: this.typesDocuments };
		const dialogRef = this.dialog.open(FormRebriqueComponent, dialogRebriqueConfig);
		dialogRef.afterClosed().subscribe((data) => {
			if (data) {
				this.OnSave(data, type)
			}
		});
	}
	async OnSave(data, type) {
		if (type === IFormType.add) {
			this.addDocumentAttacher(data);
		}
		if (type === IFormType.update) {
			this.updatedocumentAttacher(data);
		}
	}

	async updatedocumentAttacher(documentation) {
		this.service.updateAll(ApiUrl.Chantier + '/' + this.idChantier + '/Update/Documents/' +
			documentation.id, documentation).subscribe(async res => {
				if (res) {
					//	GlobalInstances.router.navigateByUrl(`chantiers/${this.idChantier}/documents/${this.module}`);
					this.initData();

					this.translate.get('updateDocumentAttache').subscribe(text => {
						toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					});
				}

			}, err => {
				console.log(err)
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			});

	}

	async addDocumentAttacher(documentation) {
		documentation.rubricId = this.module;
		this.service.create(ApiUrl.Chantier + '/' + this.idChantier + '/Documents', documentation).subscribe(async res => {
			if (res) {
				//	GlobalInstances.router.navigateByUrl(`chantiers/${this.idChantier}/documents/${this.module}`);
				this.initData();
				this.translate.get('adddoc').subscribe(text => {
					toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			}
		}, err => {
			this.translate.get('adddoc').subscribe(text => {
				toastr.warning(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});

	}
	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.tableColumnsDocuments.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}
	onMenuOpened(element: any): void {
		this.selected = element;
		this.actions = this.getActions(this.selected);
	}


	public getActions(ele): DataTableRowActions[] {

		return [
			{
				id: 1,
				icon: './assets/app/images/Icons/eye.svg',
				label: 'labels.voirDetails',
				default: true,
				color: 'accent',
				action: (element: any) => {
					this.openFormRebrique(element, this.formType.preview)
				},
				appear: () => true

			},
			{
				id: 2,
				icon: './assets/app/images/Icons/edit.svg',
				label: 'labels.modifier',
				color: 'warn',
				action: (element) => {
					this.openFormRebrique(element, this.formType.update)

				},
				appear: () => true
			},
			{
				id: 3,
				icon: './assets/app/images/Icons/trash-2.svg',
				label: 'labels.supprimer',
				color: 'primary',
				action: (element) => {
					this.delete(element.id)
				},
				appear: () => true
			},
		];


	}


	delete(id) {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true,
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true,
					},
				},
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.Chantier + '/' + this.idChantier + '/Documents', id).subscribe(async res => {
						if (res) {
							swal(text.success, '', 'success');
							//this.rerender();
						} else {
							swal(text.error, '', 'error');
						}
					});
				} else {
					toastr.success(text.failed, text.title, {
						positionClass: 'toast-top-center',
						containerId: 'toast-top-center',
					});
				}
			});
		});
	}
	filterOptions: IFilterOption;
	data: PagedResult;
	initFilters() {
		this.data = null;
		// ManageDataTable.currentPage = 0;

		this.filterOptions = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			SearchQuery: '',
			SortDirection: SortDirection.Descending,
			OrderBy: 'id',
		};
	}

	/**
 * request data
 */
	items: any[];
	dataitems: any;
	rowCount: number;
	checkedColumns: boolean[] = [];
	state: IFilterOption = {
		Page: 1,
		PageSize: AppSettings.DEFAULT_PAGE_SIZE,
		OrderBy: 'id',
		SortDirection: SortDirection.Descending,
		SearchQuery: ''
	};

	changeFiltersEvent(dataTableOutput: IFilterOption) {
		this.filterOptions = { ...this.filterOptions, ...dataTableOutput };
		this.getData()
			.then(() => {
				this.processing = false;
			});
	}
	getData(cache: boolean = true): Promise<any> {
		return new Promise((resolve, reject) => {
			const filters: IFilterOption = Object.assign(this.filterOptions);

			if (cache) {
				// const filterDB = ManageDataTable.getFilters();

				// if ('search' in filterDB) {
				// 	filters['searchQuery'] = filterDB['search'] + '';
				// }
				filters['rubricId'] = this.module;

			}
			this.service
				.getAllPagination(ApiUrl.Chantier + '/' + this.idChantier + '/Documents/Rubrics/Details', filters)
				.subscribe((data) => {
					if (data.isSuccess) {
						this.data = data;
						const listData = [];
						data.value.forEach(element => {
							element['labelDocument'] = this.insialazeLabels(element.types as any);
							element['userName'] = element.user.userName
							listData.push(element)
						});
						this.data.value = listData;
						this.dataitems = data;
						if (data == null) {
							this.items = [];
							this.rowCount = 0;
						} else {
							this.items = data.value;
							this.rowCount = data.rowsCount;
							this.processing = false;
						}
						resolve(data);
					}
				}, (err: any) => {
					reject(err);
				});


		});
	}
	onBackdropClicked(): void {
		this.initFilters();
		//this.initFilter();
		this.initState();
		this.saveState();
	}
	initState() {
		this.state = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			OrderBy: 'id',
			SortDirection: SortDirection.Descending,
			SearchQuery: ''
		}
	}
	chagePageNumber(event: any) {
		this.state.Page = event;
		this.saveState();
	}
	changePageSize(event: any) {
		this.state.PageSize = event;
		this.saveState();
	}
	sortChange(direction, event: any) {
		if (!StringHelper.isEmptyOrNull(event)) {
			this.state.OrderBy = event;
			this.state.SortDirection = direction;
			this.saveState();
		}
	}
	saveState() {
		localStorage.setItem(`state_rebrique`, JSON.stringify(this.state));
		this.emitChange();
	}
	emitChange() {
		this.changeFiltersEvent(this.state);
	}
	searchFormControl = new FormControl();

	retrieveState() {
		const oldState = localStorage.getItem(`state_state_rebrique`);
		if (oldState != null && oldState !== '') {
			this.state = JSON.parse(oldState);
			this.searchFormControl.setValue(this.state.SearchQuery);
		}
		this.emitChange();
	}


	insialazeLabels(labels): string {
		let list = []
		if (labels == null) {
			return
		} else {
			let text = '';
			list = labels as [];
			list.forEach(ele => {
				text = text + `<i class="ft-tag"></i> ${ele.value} <br> `;
			});
			return text;
		}
	}

}
