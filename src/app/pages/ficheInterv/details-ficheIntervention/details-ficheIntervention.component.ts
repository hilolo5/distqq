import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialog } from '@angular/material';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { AppSettings } from 'app/app-settings/app-settings';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { LocalElements } from 'app/shared/utils/local-elements';
import { HeaderService } from 'app/services/header/header.service';
import { TypeRubrique } from 'app/Enums/TypeRubrique.Enum';

declare var toastr: any;

@Component({
	selector: 'app-details-ficheIntervention',
	templateUrl: './details-ficheIntervention.component.html',
	styleUrls: [
		'./details-ficheIntervention.component.scss',
	]
})
export class DetailsFicheInterventionComponent implements OnInit, OnDestroy {

	docType = '';
	apiUrl: typeof ApiUrl = ApiUrl;
	selectedTabs = 'information';
	id = '';
	document;
	historique = [];
	memos = [];
	idChantier: number = null;
	processing = false;
	actionsOpened = false;
	items = [];
	emails = [];
	isPlanification = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private dialog: MatDialog,
		public translate: TranslateService,
		public route: ActivatedRoute,
		private header: HeaderService
	) {
		this.docType = localStorage.getItem(LocalElements.docType);
		this.isPlanification = this.route.snapshot.data['isPlanification'];
		const isAgendaGlobal = this.route.snapshot.data['isAgendaGlobal'];
		// this.translate.setDefaultLang(AppSettings.lang);
		// this.translate.use(AppSettings.lang);
		ManageDataTable.isPlanification = this.isPlanification;
		ManageDataTable.isAgendaGlobal = isAgendaGlobal;
		ManageDataTable.service = service;
		ManageDataTable.docType = this.docType;
		ManageDataTable.dialog = this.dialog;
	}

	async ngOnInit(): Promise<void> {
		this.init();
		this.route.params.subscribe(
			async params => {
				this.init();
			}
		);
	}

	async init() {
		this.id = this.route.snapshot.params.id;
		await this.getDocument();
		const res = await this.getParamsFromRoute('idChantier')
		this.idChantier = res === undefined ? null : res as number;
		ManageDataTable.idChantier = this.idChantier;
		this.getItems();
		// this.header.breadcrumb = [
		// 	{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
		// 	{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) }
		// ];
		this.prepareBreadcrumb();
	}


	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			if (this.idChantier == null) {
				this.header.breadcrumb = [
					{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
					{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) },
				];
			}
			if (this.idChantier != null) {
				const namechantier = localStorage.getItem('nomChantier');
				let moduleName = localStorage.getItem('moduleName');
				if (moduleName === TypeRubrique.Devis.toString()) {
					moduleName = 'Devis';
				}
				if (moduleName === TypeRubrique.Commandes.toString()) {
					moduleName = 'Commandes';
				}
				if (moduleName === TypeRubrique.Facturation.toString()) {
					moduleName = 'Factures';
				}
				this.header.breadcrumb = [
					{ label: 'Chantier', route: `/chantiers` },
					{ label: namechantier, route: `/chantiers/detail/${this.idChantier}` },
					{ label: moduleName, route: this.isRouteRebrique() },
					{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) },
				];
			}



		});


	}

	isRouteRebrique() {
		// const moduleName = localStorage.getItem('moduleName');
		// if (this.docType === ApiUrl.Devis) {
		// 	this.ModuleRebrique = 1;
		// }
		// if (this.docType === ApiUrl.Invoice) {
		// 	this.ModuleRebrique = 3;
		// }
		// if (this.docType === ApiUrl.BonCommandeF) {
		// 	this.ModuleRebrique = 2;
		// }
		return `/chantiers/${this.idChantier}`
	}
	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}

	modifier() {
		ManageDataTable.getUpdate(this.document);
	}

	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}

	ngOnDestroy(): void {
	}

	// #region services

	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(this.docType, this.id).subscribe(res => {
				this.document = res.value;
				this.historique = this.document.changesHistory;
				this.memos = this.document.memos;
				this.emails = this.document.emails;
				resolve(res.value);
			});
		});
	}

	onToggle(e: boolean): void {
		this.actionsOpened = e;
	}

	getItems() {
		this.items = ManageDataTable.getActions(true);
	}

	async saveMemo(memo: Memo) {
		// if (memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		this.processing = true;
		memo['id'] = AppSettings.guid();
		this.service.create(ApiUrl.File + '/' + this.id + '/Create/Memo', memo)
			.subscribe(res => {
				console.log(res);
				this.processing = false;
				this.getDocument();
			}, err => { console.log(err); this.processing = false; });
	}

	downloadPieceJointe(event) {
		const pieceJointe = event;
		this.service.getById(ApiUrl.File, pieceJointe.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, pieceJointe.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					pieceJointe.fileType)
			}
		)
	}


	//#endregion
}
