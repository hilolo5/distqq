// #region imports
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit, ChangeDetectorRef, Input, Inject, OnChanges } from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DatePipe } from '@angular/common';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { User } from 'app/Models/Entities/User';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';
import { observation } from 'app/Models/Entities/Maintenance/FicheInterventionMaintenance';
import { Statisfaction } from 'app/Enums/Satisfaction.enum';
import { TypeDocument } from 'app/Enums/TypeDocument.enums';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Color } from 'app/Enums/color';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { AppSettings } from 'app/app-settings/app-settings';



// #endregion

@Component({
	selector: 'app-fiche-ficheIntervention',
	templateUrl: './fiche-ficheIntervention.component.html',
	styleUrls: [
		'./fiche-ficheIntervention.component.scss',
	],
	providers: [DatePipe]
})


export class FicheFicheInterventionComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {

	// #region  Properties
	docType = '';
	@Input() data;
	articles = [];
	treeColapse = [];
	color: typeof Color = Color;
	listObservation = [];
	statisfaction: typeof Statisfaction = Statisfaction;
	processing = true;
	motifs = [];
	typeInterventionMaintenance: typeof TypeInterventionMaintenance = TypeInterventionMaintenance;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.docType = localStorage.getItem(LocalElements.docType);
	}

	ngOnInit(): void {
	}

	ngOnChanges(): void {

		if (this.data) {
			this.motifs = this.data.unDoneMotif;
			this.articles = this.data.orderDetails !== undefined && this.data.orderDetails !== null ? this.data.orderDetails.lineItems : [];
			this.listObservation = this.data.observations === (undefined || null) ? [] : this.data.observations;
			this.processing = false;
		}
	}


	ngAfterViewInit(): void {
		// init helper
	}

	ngOnDestroy(): void {
	}

	getObservations(id) {
		if (this.listObservation.length === 0) {
			return null;
		} else {
			const observationInList: observation = this.listObservation.filter(x => x.id === id)[0];
			return observationInList !== undefined ? (observationInList.observation == null ? null : observationInList.observation) : null;
		}
	}
	getStatutOperation(id) {

		if (this.listObservation.length === 0) {
			return null;
		} else {
			const observationInList = this.listObservation.filter(x => x.id === id)[0];
			return observationInList !== undefined ? (observationInList.status == null ? null : observationInList.status) : null;
		}
	}
	openElement(index) {

		if (this.treeColapse.filter(x => x.index === index).length === 0) {
			this.treeColapse.push({
				isOpen: true,
				index: index,
				childs: []
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === index) {
					x.isOpen = !x.isOpen
				}
				return x;
			});
		}
	}
	openChild(indexElemnet, indexChild) {

		if (this.treeColapse[indexElemnet].childs.filter(x => x.index === indexChild).length === 0) {
			this.treeColapse[indexElemnet].childs.push({
				isOpen: true,
				index: indexChild,
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === indexElemnet) {
					x.childs = x.childs.map(child => {
						if (child.index === indexChild) {
							child.isOpen = !child.isOpen;
						}
						return child;
					});
				}
				return x;
			});
		}
	}
	checkElementIsOpen(indexElemnet) {
		const element = this.treeColapse.filter(x => x.index === indexElemnet);
		return element.length === 0 ? false : element[0].isOpen;
	}

	checkChildIsOpen(indexElemnet, indexChild) {
		if (this.treeColapse.filter(x => x.index === indexElemnet && x.childs.filter(y => y.index === indexChild).length !== 0).length === 0) {
			return false;
		};
		return this.treeColapse.filter(x => x.index === indexElemnet)[0].childs.filter(y => y.index === indexChild)[0].isOpen;
	}

	getDocument(item) {
		let text = '';

		if (item.documentType === TypeDocument.Payment) {
			text = 'paiement';
		}
		if (item.documentType === TypeDocument.Expenses) {
			text = 'depense';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === TypeFacture.Acompte) {
			text = 'factureAcompte';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === TypeFacture.Cloturer) {
			text = 'factureCloture';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === TypeFacture.None) {
			text = 'factureGenerale';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === TypeFacture.Situation) {
			text = 'factureSituation';
		}
		if (item.documentType === TypeDocument.SupplierOrder) {
			text = 'bonCommandFournisseur';
		}
		if (item.documentType === TypeDocument.OperationSheet) {
			text = 'ficheintervention';
		}
		if (item.documentType === TypeDocument.Quote) {
			text = 'devis';
		}
		if (item.documentType === TypeDocument.OperationSheetMaintenance) {
			text = 'OperationSheetMaintenance';
		}
		if (item.documentType === TypeDocument.CreditNote) {
			text = 'avoir';
		}

		return this.translate.instant('typeDocAssocie.' + text);
	}

	goToDetails(ev) {
		const item = this.data.associatedDocuments[ev];
		let docType = '';
		if (item.documentType === TypeDocument.Expenses) { docType = ApiUrl.Depense; }
		if (item.documentType === TypeDocument.Quote) { docType = ApiUrl.Devis; }
		if (item.documentType === TypeDocument.Invoice) { docType = ApiUrl.Invoice; }
		if (item.documentType === TypeDocument.OperationSheet) { docType = ApiUrl.FicheIntervention; }
		if (item.documentType === TypeDocument.OperationSheetMaintenance) { docType = ApiUrl.MaintenanceOperationSheet; }
		if (item.documentType === TypeDocument.CreditNote) { docType = ApiUrl.avoir; }
		if (item.documentType === TypeDocument.SupplierOrder) { docType = ApiUrl.BonCommandeF; }

		localStorage.setItem(LocalElements.docType, docType);
		ManageDataTable.docType = docType;
		ManageDataTable.getDetails(item);
	}

	isdemandedevis() { return this.docType === ApiUrl.MaintenanceOperationSheet && this.data.quoteRequest !== null }
	getColor(status) { return this.color[status] };
	isEquipement() { return this.docType === ApiUrl.MaintenanceOperationSheet && this.data.type === TypeInterventionMaintenance.Maintenance }
	isSignature() { return this.data.clientSignature !== null }
	isFicheIntervention() { return this.docType === ApiUrl.FicheIntervention }
	isFicheInterventionM() { return this.docType === ApiUrl.MaintenanceOperationSheet }
	isPrestation() {
		return this.docType === ApiUrl.FicheIntervention
			|| (this.docType === ApiUrl.MaintenanceOperationSheet && this.data.type === TypeInterventionMaintenance.AfterSalesService)
	}

}


