// #region imports
import { Component, OnInit, OnDestroy, AfterViewInit, Inject, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute, NavigationEnd, } from '@angular/router';
import { DatePipe } from '@angular/common';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialog, MatInput } from '@angular/material';
import * as _ from 'lodash';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { Subscription } from 'rxjs';
import { LocalElements } from 'app/shared/utils/local-elements';
import { AppSettings } from 'app/app-settings/app-settings';
import { GlobalInstances } from 'app/app.module';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { filter, debounceTime } from 'rxjs/operators';
import { Action } from 'app/Enums/action';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { User } from 'app/Models/Entities/User';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { TypeInterventionMaintenance } from 'app/Enums/Maintenance/InterventionMaintenance.Enum';
import { StatutFicheInterventionMaintenance } from 'app/Enums/Statut/StatutFicheInterventionMain.enum';
import { Color } from 'app/Enums/color';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { DialogHelper } from 'app/libraries/dialog';

declare var toastr: any;
declare var jQuery: any;
export declare var swal: any;
// #endregion

@Component({
	selector: 'app-fiche-ficheIntervention',
	templateUrl: './edit-ficheIntervention.component.html',
	styleUrls: [
		'./edit-ficheIntervention.component.scss',
	],
	providers: [DatePipe]
})


export class EditFicheInterventionComponent implements OnInit, OnDestroy, AfterViewInit {

	// #region  Properties

	editHelper = EditDocumentHelper;
	treeColapse = [];
	prestations;
	subscriptions: Subscription[] = [];
	docType = '';
	articles = [];
	selectedStatus = '';
	status = [];
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	listClients: Client[] = [];
	clients: Client[] = [];
	listChantiers: Workshop[] = [];
	chantiers: Workshop[] = [];
	adressesIntervention: Adresse[] = [];
	techniciens: User[] = [];
	color: typeof Color = Color;
	techniciensFI: User[] = [];
	listTechnicien = [];
	listTechn = [];
	userProfil: typeof UserProfile = UserProfile;
	@ViewChild('picker') picker: any;
	@ViewChild('fromInput', {
		read: MatInput
	}) fromInput: MatInput;
	@ViewChild('froInput', {
		read: MatInput
	}) froInput: MatInput;

	@ViewChild('toInput', {
		read: MatInput
	}) toInput: MatInput;
	processing = true;
	emitter: any = {};
	document;
	idDevis = null;
	id = '';
	defaultVal;
	typeInterventionMaintenance: typeof TypeInterventionMaintenance = TypeInterventionMaintenance;
	isPlanification;
	listObservation = [];
	ListGammeEquipement = [];
	// #endregion

	// #region Lifecycle

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		private route: ActivatedRoute,
		private dialog: MatDialog,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
	) {
		this.docType = localStorage.getItem(LocalElements.docType);
		this.isPlanification = this.route.snapshot.data['isPlanification'];
		ManageDataTable.isPlanification = this.isPlanification;
		const isAgendaGlobal = this.route.snapshot.data['isAgendaGlobal'];
		ManageDataTable.isAgendaGlobal = isAgendaGlobal;
		EditDocumentHelper.typeAction = EditDocumentHelper.getTypeAction();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

		// Loader.show('');
		this.createForm();


		this.subscriptions.push(
			this.router.events.pipe(
				debounceTime(0),
				filter(e => e instanceof NavigationEnd))
				.subscribe(async () => {
					if (
						EditDocumentHelper.isAction(Action.AJOUTER) ||
						EditDocumentHelper.isAction(Action.DUPLIQUER) ||
						EditDocumentHelper.isAction(Action.GENERER)) {
						EditDocumentHelper.form.controls['reference'].setValue(await this.generateReference());
					}
					if (
						EditDocumentHelper.isAction(Action.MODIFIER) ||
						EditDocumentHelper.isAction(Action.DUPLIQUER) ||
						EditDocumentHelper.isAction(Action.GENERER)) {
						this.id = this.activatedRoute.snapshot.params.id;
						const doc = await this.getDocument();
						this.setData(doc);
					}
				})
		);

	}

	async ngOnInit() {
		this.processing = false;
		await this.getRole();
		await this.getClients();
		await this.getChantiers();
		await this.getTechniciens();
		await this.getDefaultsValues();
		this.getDefaultsVal();
		this.CheckStatus();
		this.processing = false;
	}

	ngAfterViewInit(): void {
		// init helper
	}

	ngOnDestroy(): void {
		localStorage.removeItem('startDate');
		if (this.subscriptions) {
			this.subscriptions.forEach(s => s.unsubscribe());
		}
	}

	setDateMinutenull() {
		if (localStorage.getItem('startDate')) {
			const date = new Date(localStorage.getItem('startDate'));
			return new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 0, 0);
		} else {
			const today = new Date();
			return new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), 0, 0);
		}
	}

	// #endregion

	// #region Events

	setData(doc) {
		this.processing = true;
		setTimeout(async () => {
			try {
				EditDocumentHelper.form.patchValue({
					purpose: doc.purpose,
					report: doc.report ? doc.report : '',
					workshopId: doc.workshop && doc.workshop !== null ? doc.workshop.id : null,
					client: doc.client,
					addressIntervention: doc.addressIntervention,
					visitsCount: doc.visitsCount,
					idTechnicien: doc.technician !== null && doc.technician !== undefined ? doc.technician.id : null
				});

				if (this.isModif()) { this.selectedStatus = doc.status }

				if (EditDocumentHelper.isAction(Action.MODIFIER)) {
					EditDocumentHelper.form.controls.reference.setValue(doc.reference);
					EditDocumentHelper.form.controls.startDate.setValue(new Date(doc.startDate));
					EditDocumentHelper.form.controls.endDate.setValue(new Date(doc.endDate));
				}

				if (EditDocumentHelper.isAction(Action.GENERER) && localStorage.getItem(LocalElements.docTypeGenerer) === ApiUrl.Devis) {
					this.idDevis = doc.id;
				}

				if (this.isFicheInterventionM()) {
					const listObservation = doc.observations === (undefined || null) ? [] : doc.observations;
					this.listObservation = listObservation == null ? [] : listObservation;
					this.ListGammeEquipement = doc.equipmentDetails;
				}
				if (doc.technicians != undefined) {
					this.techniciensFI = doc.technicians;

				}

				await this.isClientinList(doc.client);
				EditDocumentHelper.form.controls.client.setValue(this.listClients.find(x => x.id === doc.client.id));
				this.articles = doc.orderDetails !== null ? doc.orderDetails.lineItems : [];

				this.getAdressIntervention();
				this.processing = false;

				// Loader.hide();
			} catch (e) {
				console.log({ e });
				// Loader.hide();
			}
		}, 0);
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients();
				this.listClients.unshift(client);
				EditDocumentHelper.form.controls['client'].setValue(client);
				const adresses: Adresse[] = client.addresses;
				this.adressesIntervention = adresses == null ? [] : adresses;
			}
		});
	}

	async submit(statut?) {
		this.processing = true;
		const compareDate = EditDocumentHelper.form.value.endDate == null || EditDocumentHelper.form.value.startDate == null ?
			true : this.compareDate(EditDocumentHelper.form.value.endDate, EditDocumentHelper.form.value.startDate)
		if (!compareDate) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.datesIntervention, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return;
		}
		if (this.isFicheInterventionM() && EditDocumentHelper.form.value.idTechnicien === '') {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.technicienRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return;
		}
		if (this.isFicheIntervention() && this.techniciensFI.length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.technicienRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return;
		}

		if (!EditDocumentHelper.form.valid) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAllChamps, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			this.processing = false;
			return;
		}
		if (!(this.isFicheInterventionM() && this.document && this.document.type === TypeInterventionMaintenance.Maintenance)) {
			this.prestations = await this.getDataFromArticlesComponet();
			if (this.prestations.prestation.length === 0) {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.articleRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.processing = false;
				return;
			}
		}

		let bodyRequest = null;
		bodyRequest = await this.createCompletBodyRequest(statut);

		await this.insertDocument(bodyRequest);
	}

	async insertDocument(bodyRequest) {

		if (EditDocumentHelper.isAction(Action.AJOUTER) ||
			EditDocumentHelper.isAction(Action.DUPLIQUER) ||
			EditDocumentHelper.isAction(Action.GENERER)) {
			await this.create(bodyRequest);
		}

		if (EditDocumentHelper.isAction(Action.MODIFIER)) {
			await this.update(bodyRequest);
		}
	}


	// #endregion

	// #region action


	async createCompletBodyRequest(statut) {
		return new Promise(async (reslove, reject) => {

			let formValue = EditDocumentHelper.form.value;

			var object = {};

			if (EditDocumentHelper.isAction(Action.MODIFIER)) {
				object = this.document;
			}

			formValue = AppSettings.ConvertEmptyValueToNull(formValue);
			object['reference'] = formValue.reference;
			object['addressIntervention'] = formValue.addressIntervention;
			object['startDate'] = AppSettings.formaterDatetime(formValue.startDate);
			object['endDate'] = AppSettings.formaterDatetime(formValue.endDate);
			object['report'] = formValue.report;
			object['purpose'] = formValue.purpose;
			object['clientId'] = formValue.client.id;
			object['client'] = formValue.client;
			object['visitsCount'] = formValue.visitsCount;
			object['totalBasketConsumption'] = 0;

			if (!EditDocumentHelper.isAction(Action.MODIFIER)) {
				if (statut) {
					object['status'] = statut;
					object['reference'] = formValue.reference + '-' + AppSettings.generateStampHM() + '-Brouillon';

				} else {
					object['status'] = StatutFicheIntervention.Planifiee
				}
			} else {
				if (this.selectedStatus === 'draft' && !statut) {
					object['status'] = StatutFicheIntervention.Planifiee;
					object['reference'] = await this.generateReference();
				}
				if (this.selectedStatus === 'draft' && statut) {
					object['status'] = statut;

				}

				if (this.selectedStatus !== 'draft' && !statut) {
					object['status'] = this.selectedStatus;

				}

				if (this.selectedStatus === StatutFicheIntervention.Areplanifiee) {
					object['status'] = StatutFicheIntervention.Planifiee;

				}
			}
			if (this.isFicheIntervention()) {

				object['quoteId'] = this.idDevis;
				object['workshopId'] = formValue.workshopId;
				object['orderDetails'] = {
					'lineItems': this.prestations.prestation,
				};

				if (this.isModif()) {
					object['technicians'] = this.techniciensFI.map(technicien => {
						const interventionTechnicien = object['technicians'].filter(I => I.id === technicien.id);
						if (interventionTechnicien.length === 0) {
							return technicien.id;
						} else {
							return interventionTechnicien[0].id;
						}
					});
				} else {
					object['technicians'] = this.techniciensFI.map(technicien => {
						return (technicien.id);
					});
				}
			}

			if (this.isFicheInterventionM()) {
				object['technicianId'] = formValue.idTechnicien;

				if (EditDocumentHelper.isAction(Action.AJOUTER) ||
					(this.document && this.document.type === this.typeInterventionMaintenance.AfterSalesService)) {
					const order = {
						totalHT: this.prestations.totalHt,
						totalTTC: this.prestations.totalTtc,
						globalDiscount: {
							value: this.prestations.remise,
							type: this.prestations.typeRemise
						},
						lineItems: this.prestations.prestation
					}
					object['orderDetails'] = order;
				}
			}

			reslove(object)
		});

	}

	compareDate(date1, date2) {
		const dateOne = new Date(date1);
		const dateTwo = new Date(date2);
		if (dateOne > dateTwo) {
			return true;
		} else {
			return false;
		}
	}


	async createForm() {
		EditDocumentHelper.form = this.formBuilder.group({
			reference: ['', [Validators.required]],
			client: ['', [Validators.required]],
			addressIntervention: [null],
			workshopId: [null],
			idTechnicien: [''],
			startDate: [this.setDateMinutenull(), [Validators.required]],
			endDate: [this.setDateMinutenull(), [Validators.required]],
			purpose: [''],
			report: [''],
			visitsCount: [0]
		});

		if (this.isFicheInterventionM()) {
			EditDocumentHelper.form.get('idTechnicien').setValidators([Validators.required]);
		}

		// if (this.isFicheIntervention()) {
		// 	EditDocumentHelper.form.get('workshopId').setValidators([Validators.required]);
		// }

	}


	//#endregion

	// #region operation

	routour(doc) {
		ManageDataTable.docType = this.docType;
		ManageDataTable.getDetails(doc);
	}

	getDataFromArticlesComponet(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave((res) => {
				resolve(res);
			})
		})
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation)
			});
		});
	}


	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}

	get f() { return EditDocumentHelper.form.controls; }


	getTechnicienscHECK(techniciens: User[]): void {
		this.techniciensFI = techniciens;
	}

	close() {
		ManageDataTable.docType = this.docType;
		GlobalInstances.router.navigateByUrl(ManageDataTable.getListRoute());
	}

	getTypeNumer() {
		if (this.docType === ApiUrl.MaintenanceOperationSheet) { return this.typeNumerotation.fiche_interventionMaintenance };
		if (this.docType === ApiUrl.FicheIntervention) { return this.typeNumerotation.fiche_intervention };
	}

	getAdressIntervention() {
		const client = EditDocumentHelper.form.controls.client.value;
		if (client) {
			const adresses: Adresse[] = client.addresses;
			this.adressesIntervention = adresses == null ? [] : adresses;
			const result: Adresse = (adresses && adresses.length > 0 ?
				adresses.find(A => A.isDefault === true) : '') as Adresse;
			EditDocumentHelper.form.controls.addressIntervention.setValue(result);
		}
	}

	loadClientofChantier(idChantier) {
		if (this.listChantiers.length !== 0 && idChantier != null) {
			const chantierInfos = this.listChantiers.find(x => x.id === idChantier);
			const clientInfo = chantierInfos.client;
			this.isClientinList(clientInfo);
			const client = this.listClients.find(x => x.id === clientInfo.id);
			EditDocumentHelper.form.controls['client'].setValue(client);
			this.getAdressIntervention();
		}
	}

	toast(msg) {
		return toastr.success(this.translate.instant('toast.' + msg), '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
	}

	openElement(index) {

		if (this.treeColapse.filter(x => x.index === index).length === 0) {
			this.treeColapse.push({
				isOpen: true,
				index: index,
				childs: []
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === index) {
					x.isOpen = !x.isOpen
				}
				return x;
			});
		}
	}
	openChild(indexElemnet, indexChild) {

		if (this.treeColapse[indexElemnet].childs.filter(x => x.index === indexChild).length === 0) {
			this.treeColapse[indexElemnet].childs.push({
				isOpen: true,
				index: indexChild,
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === indexElemnet) {
					x.childs = x.childs.map(child => {
						if (child.index === indexChild) {
							child.isOpen = !child.isOpen;
						}
						return child;
					});
				}
				return x;
			});
		}
	}
	checkElementIsOpen(indexElemnet) {
		const element = this.treeColapse.filter(x => x.index === indexElemnet);
		return element.length === 0 ? false : element[0].isOpen;
	}

	checkChildIsOpen(indexElemnet, indexChild) {
		if (this.treeColapse.filter(x => x.index === indexElemnet && x.childs.filter(y => y.index === indexChild).length !== 0).length === 0) {
			return false;
		};
		return this.treeColapse.filter(x => x.index === indexElemnet)[0].childs.filter(y => y.index === indexChild)[0].isOpen

	}

	getStatutOperation(id) {
		if (this.listObservation.length === 0) {
			return null;
		} else {
			const observationInList = this.listObservation.filter(x => x.id === id)[0];
			return observationInList !== undefined ? (observationInList.status == null ? null : observationInList.status) : null;
		}
	}

	getObservations(id) {
		if (this.listObservation.length === 0) {
			return null;
		} else {
			const observationInList = this.listObservation.filter(x => x.id === id)[0];
			return observationInList !== undefined ? (observationInList.observation == null ? null : observationInList.observation) : null;
		}
	}

	CheckStatus() {
		if (this.docType === ApiUrl.FicheIntervention && this.selectedStatus === StatutFicheIntervention.Annulee) {
			this.status = [StatutFicheIntervention.Annulee, StatutFicheIntervention.Planifiee, StatutFicheIntervention.Brouillon];
		}
		if (this.docType === ApiUrl.FicheIntervention && this.selectedStatus === StatutFicheIntervention.Brouillon) {
			this.status = [StatutFicheIntervention.Brouillon, StatutFicheIntervention.Planifiee];
		}
		if (this.docType === ApiUrl.FicheIntervention && this.selectedStatus === StatutFicheIntervention.Planifiee) {
			this.status = [StatutFicheIntervention.Planifiee, StatutFicheIntervention.Brouillon, StatutFicheIntervention.Annulee,
			StatutFicheIntervention.Realisee];
		}
		if (this.docType === ApiUrl.MaintenanceOperationSheet && this.selectedStatus === StatutFicheInterventionMaintenance.Draft) {
			this.status = [StatutFicheInterventionMaintenance.Draft, StatutFicheInterventionMaintenance.Planned];
		}
		if (this.docType === ApiUrl.MaintenanceOperationSheet && this.selectedStatus === StatutFicheInterventionMaintenance.Canceled) {
			this.status = [StatutFicheInterventionMaintenance.Canceled, StatutFicheInterventionMaintenance.Draft,
			StatutFicheInterventionMaintenance.Planned];
		}
		if (this.docType === ApiUrl.MaintenanceOperationSheet && this.selectedStatus === StatutFicheInterventionMaintenance.Planned) {
			this.status = [StatutFicheInterventionMaintenance.Planned, StatutFicheInterventionMaintenance.Realized,
			StatutFicheInterventionMaintenance.Draft, StatutFicheInterventionMaintenance.Canceled];
		}
	}

	getColor(status) { return this.color[status] };

	// #endregion

	// #region check
	isEquipement() {
		return this.docType === ApiUrl.MaintenanceOperationSheet && this.isModif() &&
			this.document.type === TypeInterventionMaintenance.Maintenance
	}
	isPresta() {
		if (this.isFicheIntervention()) { return true };
		if (this.isFicheInterventionM() && !this.isModif()) { return true };
		if (this.isFicheInterventionM() && this.isModif() && this.document &&
			this.document.type === this.typeInterventionMaintenance.AfterSalesService) { return true };
	}
	isFicheIntervention() { return this.docType === ApiUrl.FicheIntervention }
	isFicheInterventionM() { return this.docType === ApiUrl.MaintenanceOperationSheet }
	isModif() { return EditDocumentHelper.isAction(Action.MODIFIER) }
	isClientSelect() {
		if (EditDocumentHelper.form.controls.workshopId.value === null && !this.isModif()) { return true; }
		if (EditDocumentHelper.form.controls.workshopId.value === null && this.isFicheInterventionM() && this.isModif()) {
			return this.document && this.document.type === this.typeInterventionMaintenance.AfterSalesService
		}
		if (EditDocumentHelper.form.controls.workshopId.value === null && this.isFicheIntervention() && this.isModif()) {
			return true
		}
		return false;
	}
	isClientInput() {
		if (EditDocumentHelper.form.controls.workshopId.value !== null && !this.isModif()) { return true; }
		if (EditDocumentHelper.form.controls.workshopId.value === null && this.isFicheInterventionM() && this.isModif()) {
			return this.document && this.document.type === this.typeInterventionMaintenance.Maintenance
		}
		if (EditDocumentHelper.form.controls.workshopId.value !== null && this.isFicheIntervention() && this.isModif()) { return true; }
		return false;
	}

	async isClientinList(client) {
		if (this.listClients.length === 0) { await this.getClients(); }
		const dataClient = this.listClients.find(x => x.id === client.id);
		if (dataClient === undefined) {
			this.listClients.unshift(client);
			this.clients.unshift(client);
		}
	}

	//#endregion

	// #region services


	async getChantiers(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Chantier).subscribe((res) => {
				resolve(res.value);
				this.listChantiers = res.value;
				this.chantiers = res.value;
			});
		});
	}

	async getClients(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Client).subscribe((res) => {
				resolve(res.value);
				this.listClients = res.value;
				this.clients = res.value;
			});
		});
	}

	getRole(): Promise<void> {
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.role).subscribe(async res => {
				const role = res.value;
				const rolesTechni = role.filter(x =>
					[UserProfile.technicien, UserProfile.technicienChantier, UserProfile.technicienMaintenance, UserProfile.admin].includes(x.type));

				rolesTechni.forEach(element => {
					this.listTechnicien.push(element.id);
				});
			}, err => {
			}, () => {
				reslove();
			});
		});
	}


	async getTechniciens(): Promise<any> {
		const filter = {
			roleIds: this.listTechnicien,
			SearchQuery: '',
			Page: 1,
			PageSize: 500,
			OrderBy: 'firstName',
			SortDirection: 'Descending',
			ignorePagination: true
		};
		return new Promise((resolve, reject) => {
			this.service.getAllPagination(ApiUrl.User, filter).subscribe((res) => {
				resolve(res.value);
				this.techniciens = res.value;
				this.listTechn = res.value;
			});
		});
	}


	generateReference(): Promise<string> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.configReference + this.getTypeNumer() + '/reference')
				.subscribe(response => {
					resolve(response);
				}, (error) => reject(error));
		});

	}

	create(bodyRequest): Promise<any> {

		const url = this.docType === ApiUrl.MaintenanceOperationSheet ? ApiUrl.MaintenanceOperationSheet + ACTION_API.create + '/AfterSalesService' :
			ApiUrl.FicheIntervention + ACTION_API.create;
		return new Promise((resolve, reject) => {
			this.service.create(url, bodyRequest).subscribe(async res => {
				this.toast('add-sucsess');
				this.routour(res.value);
				this.processing = false;
				resolve();
			}, async err => {
				if (err.error && err.error.messageCode === 103) {
					const reference = EditDocumentHelper.form.value.reference;
					EditDocumentHelper.form.controls['reference'].setValue(await this.generateReference());
					const message = "Il existe une fiche avec cette reference :  " + reference + "   Vous voulez créer cette fiche  par la nouevelle reference :"
						+ + EditDocumentHelper.form.value.reference;
					swal({
						title: '',
						text: message,
						icon: 'warning',
						buttons: {
							cancel: {
								text: 'Annuler',
								value: null,
								visible: true,
								className: '',
								closeModal: true
							},
							confirm: {
								text: 'Valider',
								value: true,
								visible: true,
								className: '',
								closeModal: true
							}
						}
					}).then(async isConfirm => {
						if (isConfirm) {
							bodyRequest['reference'] = EditDocumentHelper.form.value.reference;
							this.create(bodyRequest);
						} else {
						}
					});

				} else {
					const translation = await this.getTranslationByKey('errors');
					toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.processing = false;
					resolve();
				}
			});
		})
	}

	update(bodyRequest): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.update(this.docType, bodyRequest, bodyRequest.id).subscribe(async res => {
				this.toast('update-sucsess');
				this.routour(res.value);
				this.processing = false;
				resolve();
			}, async err => {
				console.log(err);
				const translation = await this.getTranslationByKey('errors');
				toastr.warning(translation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				this.processing = false;
				resolve();
			});
		})
	}

	getDocument(): Promise<any> {
		return new Promise((resolve, reject) => {
			const docType = EditDocumentHelper.isAction(Action.GENERER) ? localStorage.getItem(LocalElements.docTypeGenerer) : this.docType;
			this.service.getById(docType, this.id).subscribe(res => {
				this.document = res.value;
				resolve(res.value);
			});
		});
	}

	getDefaultsValues(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.configDocument).subscribe(res => {
				this.defaultVal = JSON.parse(res);
				resolve(JSON.parse(res));
			});
		});
	}

	filterclient(e) {
		if (e !== '') {
			this.listClients = [];
			this.listClients = this.clients.filter(x => x.name.includes(e.toUpperCase()) || x.reference.includes(e.toUpperCase()));
		} else {
			this.listClients = this.clients;
		}
	}

	filtertechnecien(e) {
		if (e !== '') {
			this.techniciens = [];
			this.techniciens = this.listTechn.filter(x => x.lastName.includes(e.toUpperCase()) || x.firstName.includes(e.toUpperCase()));
		} else {
			this.techniciens = this.listTechn;
		}
	}

	filterchantier(e) {
		if (e !== '') {
			this.listChantiers = [];
			this.listChantiers = this.chantiers.filter(x => x.name.includes(e.toUpperCase()));
		} else {
			this.listChantiers = this.chantiers;
		}
	}


	async getDefaultsVal() {
		const defaultsValues = this.defaultVal.find(x => x.documentType === this.getTypeNumer());
		if (defaultsValues && !this.isModif()) {
			EditDocumentHelper.form.controls['purpose'].setValue(defaultsValues['object']);
		}
	}


	// #endregion

}
