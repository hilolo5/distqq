import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'app/Models/Entities/User';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { NgSelectComponent } from '@ng-select/ng-select';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { CreateFacture } from 'app/Enums/CreateFacture.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { OperationSheetListModel } from 'app/Models/Model/ListModel';
import { OperationSheet } from 'app/Models/Entities/Documents/OperationSheet';
// tslint:disable-next-line: max-line-length
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService, EventSettingsModel, View, ScheduleComponent, EventRenderedArgs, ActionEventArgs, DragEventArgs, PopupOpenEventArgs } from '@syncfusion/ej2-angular-schedule';
export declare var swal: any;
declare var toastr: any;
declare let require: Function;
import { L10n, loadCldr, setCulture, addClass, remove, closest } from '@syncfusion/ej2-base';
import { MatDialog, MatOption } from '@angular/material';
import { DialogHelper } from 'app/libraries/dialog';
import { AgendaFormComponent } from 'app/shared/agenda-form/agenda-form/agenda-form.component';
import { MissionKind } from 'app/Enums/missionKinds';
import { AgendaTypeFilter } from 'app/Enums/AgendaTypeFIlter';
import { LocalElements } from 'app/shared/utils/local-elements';
setCulture('fr');
@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss'],
	// tslint:disable-next-line: max-line-length
	providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService]

})
export class IndexFIComponent implements OnInit {


	@ViewChild('allSelected') allSelected: MatOption;
	// tslint:disable-next-line:no-input-rename
	@Input('statut') statut = null;
	// tslint:disable-next-line:no-input-rename
	@Input('orderBy') orderBy;
	// tslint:disable-next-line:no-input-rename
	@Input('nameModule') nameModule;
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtOptions: DataTables.Settings;
	dtTrigger: Subject<any> = new Subject();
	dataTablesLang;
	tableColumns: any[];
	pageLength = 50;
	checkedColumns: any;
	FicheInterventions: OperationSheetListModel;
	recordsTotal = 0;
	INIT_CHEKECED_COLMUN;
	chantier: Workshop = null;
	userProfil: typeof UserProfile = UserProfile;
	initPage = 0;
	total;
	i = 0;
	ficheType = null;
	dateLang
	listStatus: any = [];
	chantiersList: any = null;
	statutFicheIntervention: typeof StatutFicheIntervention = StatutFicheIntervention;
	recordsFiltered;
	loading = true;
	dateMinimal: Date
	dateMaximal: Date
	id;
	tasks = [];
	idFicheTravail: number;
	processing = false;
	isAdmin = true;
	calendarEvents = null;
	indexChangeDateMinimal = 0;
	indexChangeDateMaximal = 0;
	FichesInterventions;
	idFichesTravail
	techniciensList: User[] = null;
	technicien: User = null;
	clientsList: Client[] = null;
	client: Client = null;
	preventSingleClick = false;
	timer: any;
	delay: Number;
	technicienId = [];
	statutFiche: StatutFicheIntervention = null;
	@ViewChild('selectTetechniciens') selectTetechniciens: NgSelectComponent;
	@ViewChild('selectChantiers') selectChantiers: NgSelectComponent;
	@ViewChild('selectClients') selectClients: NgSelectComponent;
	docType = '';
	filterlistTechnicien = [];
	isTreeItemDropped = false;
	allowMultiple = false;
	allowDragAndDrop = true;
	field: Object;
	draggedItemId = '';
	agendaTypeFilter: typeof AgendaTypeFilter = AgendaTypeFilter;
	ficheIntervention;
	//#region agenda

	@ViewChild('scheduleObj') scheduleObj: ScheduleComponent;

	// tslint:disable-next-line: member-ordering
	public selectedDate: Date = new Date();
	// tslint:disable-next-line: member-ordering
	public eventSettings: EventSettingsModel = { dataSource: [] };
	// tslint:disable-next-line: member-ordering
	public currentView: View = 'Month';
	// tslint:disable-next-line: member-ordering
	public workWeekDays: number[] = [2, 3, 4, 5, 0, 6, 1]; // is manday to sanday
	// tslint:disable-next-line: member-ordering
	public showWeekend: Boolean = true;
	// tslint:disable-next-line: member-ordering
	public startWeek = 1; // start week day is monday
	// tslint:disable-next-line: member-ordering
	data: object[] = [];
	public groupIndex: number;
	public isDrop = false;

	public currentEventId: number;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private activatedRoute: ActivatedRoute,
		private router: Router,
		public dialog: MatDialog
	) {
		this.docType = this.activatedRoute.snapshot.data.docType;
		this.ficheType = [this.agendaTypeFilter.InterventionsChantier];
	}

	async ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.traductionCalendarToFranch();
		const dataTablesTranslation = await this.getTranslationByKey('dataTables');
		this.dataTablesLang = dataTablesTranslation;
		const datePickerTransaltions = await this.getTranslationByKey('datePicker');
		this.dateLang = datePickerTransaltions;
		const labelsTransaltions = await this.getTranslationByKey('labels');
		this.tableColumns = [labelsTransaltions.reference, labelsTransaltions.status, labelsTransaltions.dateDebut,
		labelsTransaltions.dateFin, labelsTransaltions.objet, labelsTransaltions.chantier, labelsTransaltions.satisfication,
		labelsTransaltions.actions];
		const transaltions = await this.getTranslationByKey('labels');
		this.listStatus.push({ value: this.statutFicheIntervention.Brouillon, name: transaltions.brouillon });
		this.listStatus.push({ value: this.statutFicheIntervention.Planifiee, name: transaltions.planifiee });
		this.listStatus.push({ value: this.statutFicheIntervention.Realisee, name: transaltions.realisee });
		this.listStatus.push({ value: this.statutFicheIntervention.Facturee, name: transaltions.facturee });
		this.listStatus.push({ value: this.statutFicheIntervention.Annulee, name: transaltions.annulee });
		this.listStatus.push({ value: this.statutFicheIntervention.Late, name: transaltions.enretard });
		this.getListTec();
	}

	compareDate(date): boolean {
		const dateOne = new Date();
		const datefin = new Date(date);
		if (dateOne > datefin) {
			return true;
		} else {
			return false;
		}
	}

	getFiltersFicheChantier() {
		const list = this.technicienId.filter(x => x !== 0);
		return {
			'externalPartnerId': '',
			'techniciansId': this.technicienId.length === 0 || this.technicienId == null ? [] : list,
			'workshopId': '',
			'status': [],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': '',
			'page': 1,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference',
			'ignorePagination': true

		};
	}

	tosslePerOne(all) {
		if (this.allSelected.selected) {
			this.allSelected.deselect();
			this.refreshAgenda();
			return false;
		} else if (this.technicienId.length === this.filterlistTechnicien.length) {
			this.allSelected.select();
			this.refreshAgenda();
		} else if (this.technicienId.length = 0) {
			this.data = [];
			this.dataSource(this.data);
		} else {
			this.refreshAgenda();
		}
	}

	toggleAllSelection() {
		if (this.allSelected.selected) {
			this.technicienId = [...this.filterlistTechnicien.map(item => item.id), 0];
			this.refreshAgenda();
		} else {
			this.technicienId = [];
			this.data = [];
			this.dataSource(this.data);

		}
	}


	refreshAgenda() {
		this.data = [];
		if ((this.ficheType == null || this.ficheType.length === 0) || this.technicienId.length === 0) {
			this.dataSource(this.data);
		} else if (this.ficheType != null && this.ficheType.length > 0 && this.technicienId.length > 0) {
			if (this.ficheType.includes(this.agendaTypeFilter.InterventionsChantier)) {
				this.InitDataTable();
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Taches)) {
				this.getTaches(MissionKind.TechnicianTask);
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Evenements)) {
				this.getTaches(MissionKind.Appointment);
			}
			if (this.ficheType.includes(this.agendaTypeFilter.Rappels)) {
				this.getTaches(MissionKind.Call);
			}
		} else {
			this.InitDataTable();
			this.getTaches();
		}
		if (this.scheduleObj) {
			// this.scheduleObj.refreshEvents();
		}
	}


	InitDataTable() {
		this.data = [];
		this.service.getAllPagination(ApiUrl.FicheIntervention, this.getFiltersFicheChantier()).subscribe((data) => {
			{
				this.FicheInterventions = data;
				this.getDataForAgenda(data.value);
			}
		});
	}


	// this.technicien
	getFilters(dataTablesParameters) {
		return {
			'externalPartnerId': this.client !== null && this.client.id ? this.client.id : this.client,
			'techniciansId': this.technicien != null ? [this.technicien] : [],
			'workshopId': this.chantier == null ? null : this.chantier.id,
			'status': this.statut !== null && this.statut !== '' ? [this.statut] : [],
			'dateStart': this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null,
			'dateEnd': this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null,
			'searchQuery': dataTablesParameters.search.value,
			'page': dataTablesParameters.start / dataTablesParameters.length + 1,
			'pageSize': dataTablesParameters.length,
			'sortDirection': dataTablesParameters.order[0].dir === 'asc' ? 'Ascending' : 'Descending',
			'orderBy': dataTablesParameters.columns[dataTablesParameters.order[0].column].data
		};
	}

	setCalendarEvents(data) {
		const space = '\f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f \f';
		this.calendarEvents = data.map((element) => {
			const techniciens = () => {
				let text = ``;
				element.technicians.forEach((elem, count) => {
					if (count !== 0) {
						text = text + `${space} - ${elem.lastName} ${elem.firstName}
			`;
					} else {
						text = text + `- ${elem.lastName} ${elem.firstName}
			`;
					}
				});
				return text;
			}

			return {
				title: `\n Réference : ${element.reference}
		Prénom : ${element.client}
		Objet : ${element.purpose == null ? '' : (element.purpose)}
		Techniciens : ${techniciens()}`,
				start: element.startDate,
				end: element.endDate,
				color: this.getStatutColor(element.status),
				textColor: '#FFF',
			};
		});
	}

	getStatutColor(status: StatutFicheIntervention) {
		const statutColors = [
			{ status: StatutFicheIntervention.Annulee, color: '#fe0000' },
			{ status: StatutFicheIntervention.Brouillon, color: '#636e72' },
			{ status: StatutFicheIntervention.Facturee, color: '#3498db' },
			{ status: StatutFicheIntervention.Planifiee, color: '#00d2d3' },
			{ status: StatutFicheIntervention.Realisee, color: '#2ecc71' },
			{ status: StatutFicheIntervention.Late, color: '#ff0000' }
		];
		return statutColors.filter(color => color.status === status)[0].color;
	}

	changeDateMinimal() {
		if (this.dateMinimal != null) {
			this.rerender();
			this.indexChangeDateMinimal = 0;
		} else if (this.indexChangeDateMinimal === 0) {
			this.rerender();
			this.indexChangeDateMinimal++;
		}
	}

	changeDateMaximal() {
		if (this.dateMaximal != null) {
			this.rerender();
			this.indexChangeDateMaximal = 0;
		} else if (this.indexChangeDateMaximal === 0) {
			this.rerender();
			this.indexChangeDateMaximal++;
		}
	}

	delete(id) {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.delete(ApiUrl.FicheIntervention, id).subscribe(res => {
						if (res) {
							swal(text.success, '', 'success');
							this.FicheInterventions.value = this.FicheInterventions.value.filter(x => x.id !== id);
							this.refreshFicheIntervention();
							this.rerender();
						} else {
							swal(text.error, '', 'error');
						}
					});
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngAfterViewInit(): void {
		setTimeout(() => {
			this.GetInitParameters()
			this.InitDataTable();
			this.dtTrigger.next();
			this.rerender();
		}, 500);
		setTimeout(() => {
			this.loading = false;
		}, 1000);
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngOnDestroy(): void {
		this.dtTrigger.unsubscribe();
	}

	rerender(): void {
		this.loading = false;
		// this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
		// 	// Destroy the table first
		// 	dtInstance.destroy();
		// 	// Call the dtTrigger to rerender again
		// 	this.dtTrigger.next();
		// });
	}

	SetCheckedColmuns(arr) {
		localStorage.setItem('module_ficheintervention_colmuns', JSON.stringify(arr))
	}

	GetInitParameters() {
		const data = JSON.parse(localStorage.getItem('module_ficheintervention_colmuns'));
		this.checkedColumns = (data == null ? [] : data)
	}

	filterParStatut() {
		this.rerender();
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngOnChanges() {
		if (!this.loading) {
			this.rerender();
		}
	}

	saveChoices(length, search, sort, dir, start) {
		localStorage.setItem('module_ficheintervention_info', JSON.stringify({
			page_length: length, search: search,
			sort: sort, dir: dir, start: start
		}));
	}


	GetNameOfChantier(idChantier) {
		const result = (this.chantiersList as any[]).filter(c => c.id === idChantier);
		return result.length > 0 ? result[0].nom : '';
	}

	getFicheInterventions(search): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.FicheIntervention + '/Reference', search).subscribe(async (res) => {
				resolve(res.value)
			});
		});
	}

	transfertFIToFacture(ficheIntervention) {
		const facture = new Invoice();
		facture.orderDetails = {
			'lineItems': ficheIntervention.orderDetails.lineItems,
			'proportion': 0,
			'puc': 0,
			'globalVAT_Value': null,

			'globalDiscount': {
				'value': 0,
				'type': TypeValue.Amount
			},
			'holdbackDetails': {
				'holdback': 0,
				'warrantyPeriod': 12,
				'warrantyExpirationDate': new Date()
			}
		}
		facture.workshopId = ficheIntervention.workshop !== null ? ficheIntervention.workshop.id : null;
		facture.addressIntervention = ficheIntervention.addressIntervention;
		facture.typeInvoice = TypeFacture.None;

		facture.workshop = ficheIntervention.workshop;
		facture.client = ficheIntervention.client;
		return facture
	}

	getFicheIntevention(id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.FicheIntervention, id).subscribe(response => {
				resolve(response.value);
			});
		});
	}

	getTechniciens(): void {
		if (this.techniciensList == null) {
			this.processing = true;
			this.service.updateAll(ApiUrl.role + '/Users',
				{ 'roleTypes': [UserProfile.technicien, UserProfile.technicienChantier] }).subscribe(response => {
					this.techniciensList = response.value
				}, async err => {
					const transaltions = await this.getTranslationByKey('errors');
					toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}, () => {
					this.processing = false;
					this.selectTetechniciens.isOpen = true;
				});
		}
	}

	getClients(): void {
		if (this.clientsList == null) {
			this.service.getAll(ApiUrl.Client).subscribe(response => {
				this.clientsList = response.value;
			}, async err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.processing = false;
				this.selectClients.isOpen = true;
			});
		}
	}

	getChantiers(): void {
		if (this.chantiersList == null) {
			this.service.getAll(ApiUrl.Chantier).subscribe(response => {
				this.chantiersList = response.value;
			}, async err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.processing = false;
				this.selectChantiers.isOpen = true;
			});
		}
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}
	async navigateToClick(reference: string): Promise<void> {
		this.processing = true;
		const result = await this.getFicheInterventions(reference);
		this.processing = false;
		this.router.navigate(['/ficheintervention/detail', result.id]);
	}
	// rerender()
	async chantierChange() {
		if (this.chantier != null) {
			this.clientsList = [this.chantier.client];
			this.client = this.chantier.client;
			this.rerender();
		} else {
			this.clientsList = null;
			this.getClients();
			this.client = null;
			this.rerender();
		}
	}


	doubleClick(idficheintervention) {
		this.preventSingleClick = true;
		clearTimeout(this.timer);
		this.router.navigate(['/ficheintervention/detail', idficheintervention]);
	}

	getViewDate = () => this.scheduleObj && this.scheduleObj.selectedDate ? this.scheduleObj.selectedDate : '';
	getCurrentDate = () => new Date();
	getDateFormat = () => this.scheduleObj ? (this.scheduleObj.currentView === 'Month' ? 'MMMM yyyy' : 'dd MMMM yyyy') : '';

	onAgendaPrevious(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-prev button') as any;

			if (btn) {
				btn.click();
			}
		}
	}

	onAgendaNext(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-next button') as any;
			if (btn) {
				btn.click();
			}
		}
	}

	onAgendaToday(): void {
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector('.e-toolbar-item.e-today button') as any;
			if (btn) {
				btn.click();
			}
		}
	}
	onDisplayChanged(e: number): void {
		const keys = ['e-month', 'e-timeline-day', 'e-timeline-work-week'];
		if (this.scheduleObj && this.scheduleObj.element) {
			const btn = this.scheduleObj.element.querySelector(`.e-toolbar-item.e-views.${keys[e]} button`) as any;
			if (btn) {
				btn.click();
			}
		}
		// this.group = this.isMonthly()
		// 	? null
		// 	: { enableCompactView: false, resources: ['Departments', 'Consultants'] };
	}
	genereteNameTechnicien(user) {
		return `${user.lastName[0]}${user.firstName[0]}`
	}

	getFiltersMission() {
		const list = this.technicienId.filter(x => x !== 0);
		return {
			'technicianIds': this.technicienId.length === 0 || this.technicienId == null ? [] : list,
			'clientId': '',
			'types': [],
			'status': [],
			'kinds': [],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': '',
			'page': 1,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'reference',
			'ignorePagination': true

		};
	}

	addDays(date, days) {
		const copy = new Date(Number(date))
		copy.setDate(date.getDate() + days)
		return copy
	}

	getFilterKindOption(kind) {
		const res = this.getFiltersMission();
		res.kinds.push(kind);
		return res;
	}

	async getTaches(kind?) {
		this.processing = true;
		const filterMission = !kind ? this.getFiltersMission() : this.getFilterKindOption(kind);
		this.service.getAllPagination(ApiUrl.Mission, filterMission).subscribe(res => {
			this.tasks = res.value;
			if (this.tasks.length === 0) {
				this.dataSource(this.data);
				this.processing = false;
			} else {
				this.tasks.forEach(async element => {
					const adress = element.client == null || element.client.address == null || element.client.address.city === null ? ''
						: ' (' + element.client.address.city + ')';
					const clientNam = element.client == null || element.client.name == null ? '' : element.client.name;
					this.data.push({
						Id: element.id,
						Subject: clientNam + adress + element.object + ' ,' + `${element.technician.lastName} ${element.technician.firstName}`,
						Name: element.object,
						StartTime: new Date(element.startingDate),
						EndTime: this.addDays(new Date(element.startingDate), 0),
						Description: element.object, // objet
						DepartmentID: null,
						ConsultantID: null,
						DepartmentName: '',
						customClass: this.getMisionKind(element.missionKind),
						CategoryColor: this.getMisionKindColor(element.missionKind),

					});
					this.dataSource(this.data);
				});
				this.processing = false;
			}
		});
	}

	navigateToDetail(data) {

		// args.cancel = true;
		// localStorage.setItem('startDate', args.startTime);
		// this.router.navigate(['/ficheintervention/create'])
		if (data.customClass === 'cust-appointment--interventionsChantier') {
			localStorage.setItem(LocalElements.docType, ApiUrl.FicheIntervention)
			this.router.navigate(['/ficheintervention/detail', data.Id])
		}
		if (data.customClass === 'cust-appointment--taches') {
			this.dateils(data)
		}
		if (data.customClass === 'cust-appointment--evenements') {
			this.dateils(data)
		}
		if (data.customClass === 'cust-appointment--rappels') {
			this.dateils(data)
		}
	}


	dateils(data) {
		this.service.getById(ApiUrl.Mission, data.Id).subscribe(response => {
			if (response) {
				const res = response.value;
				DialogHelper.openDialog(
					this.dialog,
					AgendaFormComponent,
					'',
					{ hasBackdrop: false, res: res, readOnly: true, hasMain: false }
				).subscribe(async response => {
					if (response) {
						this.router.navigate(['/ficheintervention', { refresh: (new Date).getTime() }]);
						this.refreshAgenda();
					}
				});
			}
		});
	}

	update(data) {
		this.service.getById(ApiUrl.Mission, data.Id).subscribe(response => {
			if (response) {
				DialogHelper.openDialog(
					this.dialog,
					AgendaFormComponent,
					'',
					{ hasBackdrop: true, res: response.value, hasMain: false }
				).subscribe(async response => {
					if (response) {
						this.router.navigate(['/ficheintervention', { refresh: (new Date).getTime() }]);
						this.refreshAgenda();
					}
				});
			}
		});
	}


	ifupdate(customClass) {
		return customClass === 'cust-appointment--evenements' ||
			customClass === 'cust-appointment--rappels' || customClass === 'cust-appointment--taches'
	}


	getMisionKind(type) {
		if (type === MissionKind.Appointment) { return 'cust-appointment--evenements'; }
		if (type === MissionKind.Call) { return 'cust-appointment--rappels'; }
		if (type === MissionKind.TechnicianTask) { return 'cust-appointment--taches'; }
	}

	getMisionKindColor(type) {
		if (type === MissionKind.Appointment) { return ' #38b475'; }
		if (type === MissionKind.Call) { return '#ffffff'; }
		if (type === MissionKind.TechnicianTask) { return '#ffffff'; }
	}

	getDataForAgenda(ficheInterventionChantier) {
		if (ficheInterventionChantier.length === 0) {
			this.dataSource(this.data);
		} else {
			ficheInterventionChantier.forEach(async element => {
				let listTechnicienName = '';
				element.technicians.forEach(technicien => {
					listTechnicienName += ' ,' + this.genereteNameTechnicien(technicien);
				});
				this.data.push({
					Id: element.id,
					Subject: element.reference + ',' + element.client + listTechnicienName,
					Name: element.client, // client name
					StartTime: new Date(element.startDate),
					EndTime: new Date(element.endDate),
					Description: element.purpose,
					DepartmentID: null,
					ConsultantID: null, // element.idTechnicien,
					customClass: 'cust-appointment--interventionsChantier',
					CategoryColor: this.getColorByStatut(element.status)
				});
				this.dataSource(this.data);
				this.scheduleObj.refreshEvents();
			});
		}
	}

	listTechnicien(listTechnicien) {
		const list = [];
		listTechnicien.forEach(element => {
			list.push(element.firstName + ' ' + element.lastName + ' , ');
		});
		return list;
	}

	/**data source for event setting */
	dataSource(data) {
		if (data.length === 0) {
			this.scheduleObj.eventSettings.dataSource = [];
		} else if (this.scheduleObj) {
			this.scheduleObj.eventSettings.dataSource = data;
		}
	}
	onPopupOpen(args: PopupOpenEventArgs): void {
		if (args.type === 'QuickInfo') {
			// QuickInfo for event  vide
			if (args.target.classList.contains('e-work-cells') || args.target.classList.contains('e-header-cells')) {
				this.scheduleObj.quickPopup.quickPopupHide(true);
				args.cancel = true;
			} else if (args.target.classList.contains('e-appointment')) {
				// QuickInfo for event
				args.cancel = true;
				this.scheduleObj.quickPopup.quickPopupHide(true);

				(<HTMLElement>args.element).style.boxShadow = `1px 2px 5px 0 ${(<HTMLElement>args.target).style.backgroundColor}`;
			}
		}
		if (args.type === 'Editor') {
			args.cancel = true;
			this.scheduleObj.quickPopup.quickPopupHide(true);

		}

	}


	getListTec() {
		this.processing = true;
		const filter = {
			roleIds: [],
			SearchQuery: '',
			Page: 1,
			PageSize: 10,
			OrderBy: 'firstName',
			SortDirection: 'Descending',
			ignorePagination: true
		};
		this.service.getAllPagination(ApiUrl.User, filter)
			.subscribe(res => {
				this.processing = false;
				this.filterlistTechnicien = res.value;
				this.technicienId = this.filterlistTechnicien.map(element => element.id);
				this.technicienId.push(0);
			}, err => {
			});
	}

	onEventClick(args) {

	}
	/**
	 * Disabel cell click
	 */
	onCellClick(args) {

		if (!args.event.target.classList.contains('e-more-indicator')) {
			args.cancel = true;
			DialogHelper.openDialog(
				this.dialog,
				AgendaFormComponent,
				'',
				{ hasBackdrop: true, dateSelectd: args.startTime, hasMain: false }
			).subscribe(async response => {
				if (response) {
					// this.router.navigate(['/ficheintervention', { refresh: (new Date).getTime() }]);
					this.refreshAgenda();
				}
			});
		}
	}
	oncellDoubleClick(args) {
		args.cancel = true;
	}

	//#region color calendar
	getColorByStatut(statut: StatutFicheIntervention) {
		switch (statut) {
			case StatutFicheIntervention.Brouillon:
				return '#494948';
				break;
			case StatutFicheIntervention.Annulee:
				return '#494948';
				break;
			case StatutFicheIntervention.Planifiee:
				return '#FEB969'; // this.getColorStatutPlanifier(date)
				break;
			case StatutFicheIntervention.Realisee:
				return '#38B475';
				break;
			case StatutFicheIntervention.Facturee:
				return '#38B475';
				break;
			case StatutFicheIntervention.Areplanifiee:
				return '#FEB969'; // this.getColorStatutPlanifier(date)
				break;
			case StatutFicheIntervention.Late:
				return '#F53C56'; // this.getColorStatutPlanifier(date)
				break;
		}
	}

	/**
	*
	* @param args get color for event
	*/
	applyCategoryColor(args: EventRenderedArgs): void {
		const categoryColor: string = args.data.CategoryColor as string;
		if (!args.element || !categoryColor) {
			return;
		}
		if (this.scheduleObj.currentView === 'Agenda') {
			(args.element.firstChild as HTMLElement).style.borderLeftColor = categoryColor;
		} else {
			args.element.style.backgroundColor = categoryColor;
		}
	}

	refreshFicheIntervention() {
		this.InitDataTable();
		this.scheduleObj.refreshEvents();
		this.processing = false;
	}



	createBodyRequest(fiche): OperationSheet {
		this.ficheIntervention = fiche;
		this.ficheIntervention['technicians'] = fiche['technicians'].map(technicien => {
			const interventionTechnicien = this.ficheIntervention['technicians'].filter(I => I.id === technicien.id);
			if (interventionTechnicien.length === 0) {
				return technicien.id;
			} else {
				return interventionTechnicien[0].id;
			}
		});
		return this.ficheIntervention;

	}
	updateInterventionMAintenance(Id, StartTime, EndTime, ConsultantID) {
		this.processing = true;

		const fiche = this.FicheInterventions.value.filter(x => x.id === Id)[0] as OperationSheet;
		if (fiche.status !== StatutFicheIntervention.Realisee && fiche.status !== StatutFicheIntervention.Facturee) {
			this.translate.get('file.update').subscribe(text => {
				swal({
					title: text.title,
					text: text.question,
					icon: 'error',
					buttons: {
						cancel: {
							text: text.cancel,
							value: null,
							visible: true,
							className: '',
							closeModal: true
						},
						confirm: {
							text: text.confirm,
							value: true,
							visible: true,
							className: '',
							closeModal: true
						}
					}
				}).then(isConfirm => {
					if (isConfirm) {
						Date.prototype.addHours = function (h) {
							this.setTime(this.getTime() + (h * 60 * 60 * 1000));
							return this;
						}
						fiche.startDate = fiche.startDate !== StartTime ? (StartTime as Date).addHours(1) : fiche.startDate;
						fiche.endDate = fiche.endDate !== EndTime ? (EndTime as Date).addHours(1) : fiche.endDate;
						// 	fiche.technicianId = ConsultantID;
						const createBody = this.createBodyRequest(fiche);

						this.service.update(ApiUrl.FicheIntervention, createBody, createBody.id).subscribe(res => {
							swal(text.success, '', 'success');
							this.processing = false;

						});
						// 	(StartTime as Date).addHours(-1);
						// 	(EndTime as Date).addHours(-1);
						// 	this.scheduleObj.refreshEvents();
						this.refreshFicheIntervention();
						this.processing = false;

					} else {
						// 	this.getFicheInterventionMaintenanceList();
						toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						this.processing = false;

					}
				});
			});
		} else {
			const msgErr = this.translate.instant('errors.statutRealiseFacture');
			// toastr.warning(msgErr, "", { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			toastr.warning(msgErr, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			// 	this.getFicheInterventionMaintenanceList();

			// this.scheduleObj.refreshEvents();
			this.refreshFicheIntervention();
			this.processing = false;

		}

	}
	public onCloseClick(): void {
		this.scheduleObj.quickPopup.quickPopupHide();
	}


	DeleteMission(data) {
		let text = null;
		if (data.customClass === 'cust-appointment--evenements') {
			text = this.translate.instant('list.deleteEvenements')
		}
		if (data.customClass === 'cust-appointment--rappels') {
			text = this.translate.instant('list.deleteRappels')

		}
		if (data.customClass === 'cust-appointment--taches') {
			text = this.translate.instant('list.deleteTache')

		}

		swal({
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		}).then(isConfirm => {
			if (isConfirm) {
				this.service.delete(ApiUrl.Mission, data.Id).subscribe(res => {
					if (res) {
						swal(text.success, '', 'success');
						this.refreshAgenda();
					} else {
						swal(text.error, '', 'error');
					}
				});
			} else {
				toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		});

	}


	onItemDrag(event: any): void {
		if (this.scheduleObj.isAdaptive) {
			const classElement: HTMLElement = this.scheduleObj.element.querySelector('.e-device-hover');
			if (classElement) {
				classElement.classList.remove('e-device-hover');
			}
			if (event.target.classList.contains('e-work-cells')) {
				// 	addClass([event.target], 'e-device-hover');
			}
		}
		if (document.body.style.cursor === 'not-allowed') {
			document.body.style.cursor = '';
		}
		if (event.name === 'nodeDragging') {
			const dragElementIcon: NodeListOf<HTMLElement> =
				document.querySelectorAll('.e-drag-item.treeview-external-drag .e-icon-expandable');
			for (let i = 0; i < dragElementIcon.length; i++) {
				dragElementIcon[i].style.display = 'none';
			}
		}

	}
	onEventRendered(args) {
		// Here we added animation effect for current Appointment
		if (args.data.Id === this.currentEventId) {
			args.element.classList.add('e-animate');
		} else {
			args.element.classList.remove('e-animate');
		}
		this.applyCategoryColor(args);
	}
	onActionBegin(args) {
		if (args.requestType === 'eventCreate') {
		}
		if (args.requestType === 'eventChange') {
			const { Id, StartTime, EndTime, ConsultantID } = args.changedRecords[0] as any;
			this.updateInterventionMAintenance(Id, StartTime, EndTime, ConsultantID);
		}
	}


	traductionCalendarToFranch() {
		loadCldr(
			require('./../../planification/app-schedule/traduction/numberingSystems.json'),
			require('./../../planification/app-schedule/traduction/main/fr/ca-gregorian.json'),
			require('./../../planification/app-schedule/traduction/main/fr/currencies.json'),
			require('./../../planification/app-schedule/traduction/main/fr/numbers.json'),
			require('./../../planification/app-schedule/traduction/main/fr/timeZoneNames.json')
		);
		L10n.load({
			'fr': {
				'schedule': {
					'day': 'journée',
					'week': 'La semaine',
					'workWeek': 'Semaine de travail',
					'month': 'Mois',
					'agenda': 'Ordre du jour',
					'weekAgenda': 'Agenda de la semaine',
					'workWeekAgenda': 'Agenda de la semaine de travail',
					'monthAgenda': 'Agenda du mois',
					'today': 'Aujourd\'hui',
					'noEvents': 'Pas d\'événements',
					'emptyContainer': 'Aucun événement n\'est prévu ce jour-là.',
					'allDay': 'Toute la journée',
					'start': 'Début',
					'end': 'Fin',
					'more': 'plus',
					'close': 'Fermer',
					'cancel': 'Annuler',
					'noTitle': '(Pas de titre)',
					'delete': 'Effacer',
					'deleteEvent': 'Supprimer un événement',
					'deleteMultipleEvent': 'Supprimer plusieurs événements',
					'selectedItems': 'Articles sélectionnés',
					'deleteSeries': 'Supprimer la série',
					'edit': 'modifier',
					'editSeries': 'Modifier la série',
					'editEvent': 'Modifier l\'événement',
					'createEvent': 'Créer',
					'subject': 'Assujettir',
					'addTitle': 'Ajouter un titre',
					'moreDetails': 'Plus de détails',
					'save': 'sauvegarder',
					'editContent': 'Voulez-vous modifier uniquement cet événement ou une série entière?',
					'deleteRecurrenceContent': 'Voulez-vous supprimer uniquement cet événement ou une série entière?',
					'deleteContent': 'Êtes-vous sûr de vouloir supprimer cet événement?',
					'deleteMultipleContent': 'Êtes-vous sûr de vouloir supprimer les événements sélectionnés?',
					'newEvent': 'Ajouter fiche d\'intervention maintenance',
					'title': 'Titre',
					'location': 'Emplacement',
					'description': 'La description',
					'timezone': 'Fuseau horaire',
					'startTimezone': 'Début du fuseau horaire',
					'endTimezone': 'Fin du fuseau horaire',
					'repeat': 'Répéter',
					'saveButton': 'sauvegarder',
					'cancelButton': 'Annuler',
					'deleteButton': 'Effacer',
					'recurrence': 'Récurrence',
					'wrongPattern': 'Le modèle de récurrence n\'est pas valide.',
					'seriesChangeAlert': 'Les modifications apportées à des instances spécifiques de cette série seront annulées et ces événements correspondront à nouveau à la série.',
					'createError': 'La durée de l\'événement doit être plus courte que sa fréquence. Raccourcissez la durée ou modifiez le modèle de récurrence dans l\'éditeur d\'événement de récurrence.',
					'recurrenceDateValidation': 'Certains mois ont moins que la date sélectionnée. Pour ces mois, l\'événement se produira à la dernière date du mois.',
					'sameDayAlert': 'Deux occurrences du même événement ne peuvent pas se produire le même jour.',
					'editRecurrence': 'Modifier la récurrence',
					'repeats': 'Répète',
					'alert': 'Alerte',
					'startEndError': 'La date de fin sélectionnée se produit avant la date de début.',
					'invalidDateError': 'La valeur de date saisie est invalide.',
					'ok': 'D\'accord',
					'occurrence': 'Occurrence',
					'series': 'Séries',
					'previous': 'précédent',
					'next': 'Prochain',
					'timelineDay': 'Journée',
					'timelineWeek': 'Semaine',
					'timelineWorkWeek': 'Semaine',
					'timelineMonth': 'Mois'
				},
				'recurrenceeditor': {
					'none': 'Aucun',
					'daily': 'du quotidien',
					'weekly': 'Hebdomadaire',
					'monthly': 'Mensuel',
					'month': 'Mois',
					'yearly': 'Annuel',
					'never': 'Jamais',
					'until': 'Jusqu\'à',
					'count': 'Compter',
					'first': 'Premier',
					'second': 'Seconde',
					'third': 'Troisième',
					'fourth': 'Quatrième',
					'last': 'Dernier',
					'repeat': 'Répéter',
					'repeatEvery': 'Répéter tous les',
					'on': 'Répéter sur',
					'end': 'Fin',
					'onDay': 'journée',
					'days': 'Journées)',
					'weeks': 'Semaines)',
					'months': 'Mois)',
					'years': 'Années)',
					'every': 'chaque',
					'summaryTimes': 'fois)',
					'summaryOn': 'sur',
					'summaryUntil': 'jusqu\'à',
					'summaryRepeat': 'Répète',
					'summaryDay': 'journées)',
					'summaryWeek': 'semaines)',
					'summaryMonth': 'mois)',
					'summaryYear': 'années)',
					'monthWeek': 'Mois Semaine',
					'monthPosition': 'Position du mois',
					'monthExpander': 'Mois Expander',
					'yearExpander': 'Année Expander',
					'repeatInterval': 'Intervalle de répétition'

				},
				'dropdowns': {
					'noRecordsTemplate': 'Aucun enregistrement trouvé',
					'actionFailureTemplate': 'Modèle d\'échec d\'action',
					'overflowCountTemplate': '+${count} plus..',
					'totalCountTemplate': '${count} choisi'
				}
			}
		});
	}

	isFicheIntervention() { return this.docType === ApiUrl.FicheIntervention }
	isFicheInterventionM() { return this.docType === ApiUrl.MaintenanceOperationSheet }
	//endregion
}
