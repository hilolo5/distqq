import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { LoginComponent } from './connecter/login.component';
import { RecorverPasswordComponent } from './recorver-password/recorver-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/login/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' }
	]);
}


@NgModule({
	declarations: [LoginComponent, RecorverPasswordComponent, ChangePasswordComponent],
	imports: [
		CommonModule,
		LoginRoutingModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		ReactiveFormsModule,
		ShowHidePasswordModule,
		ShowHidePasswordModule
	]
})
export class LoginModule { }
