import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VisiteMaintenanceModule } from 'app/pages/visiteMaintenence/visiteMaintenance.module';
import { VisiteMaintenanceRoutingModule } from 'app/pages/visiteMaintenence/visite-maintenance-routing.module';
import { CommonModules } from 'app/common/common.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { DataTablesModule } from 'angular-datatables';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ShowVisiteComponent } from './show-visite.component';
export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/visitemaintenance/', '.json');
}

@NgModule({
	declarations: [ShowVisiteComponent],
	imports: [
		CommonModule,
		VisiteMaintenanceRoutingModule,
		CommonModules,
		FormsModule,
		MatDialogModule,
		VisiteMaintenanceModule,
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		DataTablesModule,
		SplitButtonModule,
		InfiniteScrollModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
	],


	entryComponents: [ShowVisiteComponent]

})
export class ShowvisiteModule { }
