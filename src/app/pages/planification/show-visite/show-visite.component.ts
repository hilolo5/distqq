import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-show-visitemaintenance',
	templateUrl: './show-visite.component.html',
	styleUrls: ['./show-visite.component.scss']
})
export class ShowVisiteComponent implements OnInit {

	constructor(private dialogRef: MatDialogRef<ShowVisiteComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { idContratVisite: string, year: number, month: number },
		private translate: TranslateService) {
	}

	ngOnInit() {
	}

	public closeMe() {
		this.data = null;
		this.dialogRef.close();
	}

}
