import { Component, OnInit } from '@angular/core';
import { LocalElements } from 'app/shared/utils/local-elements';
import { Router } from '@angular/router';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

	constructor(private router: Router) { }

	ngOnInit() {
	}

	navigateToCreate() {
		localStorage.setItem(LocalElements.docType, ApiUrl.MaintenanceOperationSheet);
		this.router.navigate(['/planification/create'])

	}

}
