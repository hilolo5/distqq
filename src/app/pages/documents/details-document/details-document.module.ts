import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsDocumentComponent } from './details-document.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MultiLoader } from '../documents.module';
import { MatTabsModule, MatMenuModule, MatIconModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FicheDocumentComponent } from './fiche-document/fiche-document.component';
import { CommonModules } from 'app/common/common.module';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TableArticleDepenseModule } from 'app/common/table-article-depense/table-article-depense.module';
import { TableArticleFactureAcompteModule } from 'app/common/table-article-facture-acompte/table-article-facture-acompte.module';
import { SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { SharedModule } from 'app/shared/shared.module';
import { FournisseurCardModule } from 'app/shared/fournisseur-card/fournisseur-card.module';
import { ActionsModule } from 'app/shared/actions/actions.module';



@NgModule({
	declarations: [
		DetailsDocumentComponent,
		FicheDocumentComponent
	],
	imports: [
		MatSelectModule,
		MatOptionModule,
		CommonModule,
		MatTabsModule,
		MatMenuModule,
		MatIconModule,
		TableArticleDepenseModule,
		TableArticleFactureAcompteModule,
		CommonModules,
		SharedModule,
		FournisseurCardModule,
		TableArticleModule,
		ActionsModule,
		NgbTooltipModule,
		SplitButtonModule,
		ReactiveFormsModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		RouterModule.forChild([
			{
				path: '',
				component: DetailsDocumentComponent,
			}
		])
	],
	providers: [],
	exports: [DetailsDocumentComponent,
		FicheDocumentComponent],
	entryComponents: []
})
export class DetailsDocumentModule { }
