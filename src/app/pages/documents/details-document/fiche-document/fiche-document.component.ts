// #region imports
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit, ChangeDetectorRef, Input, Inject, OnChanges } from '@angular/core';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { LocalElements } from 'app/shared/utils/local-elements';
import { DatePipe } from '@angular/common';
import { EditDocumentHelper } from 'app/libraries/document-helper';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { TypeDevis } from '../edit-document/edit-document.component';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Color } from 'app/Enums/color';
import { AppSettings } from 'app/app-settings/app-settings';
import { Action } from 'app/Enums/action';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { TypeDocument } from 'app/Enums/TypeDocument.enums';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { HeaderService } from 'app/services/header/header.service';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';

export declare var swal: any;
// #endregion


@Component({
	selector: 'app-fiche-document',
	templateUrl: './fiche-document.component.html',
	styleUrls: [
		'./fiche-document.component.scss',
	],
	providers: [DatePipe]
})


export class FicheDocumentComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
	status = [];
	// #region  Properties
	selectedStatus = '';
	editHelper = EditDocumentHelper;
	// #region Lifecycle
	@Input() data;
	TypeDevis: typeof TypeDevis = TypeDevis;
	apiUrl: typeof ApiUrl = ApiUrl;
	color: typeof Color = Color;
	typeDevis = TypeDevis.Complet;
	articles = [];
	docType = '';
	partProrata = false;
	participationPuc = false;
	retenuGaranti = false;
	action: typeof Action = Action;
	typeFacture: typeof TypeFacture = TypeFacture;
	TypeDocument: typeof TypeDocument = TypeDocument;
	processing = false;
	isDevis = null

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public router: Router,
		public formBuilder: FormBuilder,
		public translate: TranslateService,
		public activatedRoute: ActivatedRoute,
		private header: HeaderService,

	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.docType = localStorage.getItem(LocalElements.docType);

		// if it comes from visuel page dont update view
	}

	ngOnInit(): void {
	}

	async getDevisById(idDevis: string) {
		return await this.service.getById(ApiUrl.Devis, idDevis).toPromise();
	}
	async getDevis() {
		if (this.docType === this.apiUrl.Invoice && this.data['typeInvoice'] === TypeFacture.Cloturer) {
			this.getDevisById(this.data.quoteId).then(async (result) => {
				this.isDevis = result.value;
			}).catch(error => {
				console.error(error);
				this.processing = false;
			});
		}
	}
	factureCloture() {
		if (this.docType === this.apiUrl.Invoice && this.data) {
			if (this.data['typeInvoice'] === TypeFacture.Cloturer) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	getTransalteLocationRequest(statut): string {
		return `changeStatutdevis.${statut}`;

	}

	changeStatutDevis() {


		this.translate.get(this.getTransalteLocationRequest(this.selectedStatus)).subscribe(text => {

			swal({

				title: `${text.title} " ${this.data.reference} "`,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: 'ok',
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.data.status = this.selectedStatus;
					this.service.updateAll('Quote/' + this.data.id + '/Update/Status', this.data).subscribe(res => {
						if (res) {
							this.data = res.value;
							swal('', '', 'success');

						} else {
							swal('', '', 'error');
						}
					}, err => {
						console.log(err);
						swal('', '', 'error');
					});
				} else {
					swal('', '', 'error');
				}
			});
		});
	}


	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}

	CheckStatus() {
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.EnAttente) {
			this.status = [StatutDevis.EnAttente, StatutDevis.NonAcceptee, StatutDevis.Acceptee, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.Acceptee) {
			this.status = [StatutDevis.EnAttente, StatutDevis.NonAcceptee, StatutDevis.Acceptee, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.NonAcceptee) {
			this.status = [StatutDevis.NonAcceptee, StatutDevis.EnAttente, StatutDevis.Annulee];
		}
		if (this.docType === ApiUrl.Devis && this.selectedStatus === StatutDevis.Annulee) {
			this.status = [StatutDevis.Annulee, StatutDevis.EnAttente];
		}
	}


	getColor(status) { return this.color[status] };
	ngOnChanges(): void {
		this.processing = true;
		if (this.data) {
			this.selectedStatus = this.data.status;
			this.CheckStatus();
			this.articles = this.data.orderDetails.lineItems;
			if (!this.isDocSupplier()) {
				this.typeDevis = this.data.orderDetails.productsDetailsType;
			}
			//add condition
			this.getDevis();
			this.processing = false;
		}
	}

	goToDetails(ev) {
		const item = this.data.associatedDocuments[ev];
		let docType = '';
		if (item.documentType === TypeDocument.Expenses) { docType = ApiUrl.Depense; }
		if (item.documentType === TypeDocument.Quote) { docType = ApiUrl.Devis; }
		if (item.documentType === TypeDocument.Invoice) { docType = ApiUrl.Invoice; }
		if (item.documentType === TypeDocument.OperationSheet) { docType = ApiUrl.FicheIntervention; }
		if (item.documentType === TypeDocument.OperationSheetMaintenance) { docType = ApiUrl.MaintenanceOperationSheet; }
		if (item.documentType === TypeDocument.CreditNote) { docType = ApiUrl.avoir; }
		if (item.documentType === TypeDocument.SupplierOrder) { docType = ApiUrl.BonCommandeF; }

		localStorage.setItem(LocalElements.docType, docType);
		ManageDataTable.docType = docType;
		ManageDataTable.getDetails(item);
	}


	ngAfterViewInit(): void {
		// init helper
	}

	ngOnDestroy(): void {
	}

	isAdresseIntervention() { return this.docType !== ApiUrl.avoir && !this.isDocSupplier(); }

	isDocSupplier() { return this.docType === ApiUrl.Depense || this.docType === ApiUrl.BonCommandeF }

	isdetailLot() { return this.docType === ApiUrl.Invoice || this.docType === ApiUrl.Devis; }

	downloadPieceJointe(piece) {
		this.service.getById(ApiUrl.File, piece.fileId).subscribe(value => {
			AppSettings.downloadBase64(
				value.value,
				piece.fileName,
				value.value.substring('data:'.length, value.value.indexOf(';base64')),
				piece.fileType
			);
		});
	}

	isInvoiceNone() {
		if (this.docType === ApiUrl.Invoice && this.data) return this.data.typeInvoice === this.typeFacture.None || this.data.typeInvoice === this.typeFacture.Cloturer;
		return true;
	}

	getDocument(item) {
		let text = '';

		if (item.documentType === TypeDocument.Payment) {
			text = 'paiement';
		}
		if (item.documentType === TypeDocument.Expenses) {
			text = 'depense';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === this.typeFacture.Acompte) {
			text = 'factureAcompte';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === this.typeFacture.Cloturer) {
			text = 'factureCloture';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === this.typeFacture.None) {
			text = 'factureGenerale';
		}
		if (item.documentType === TypeDocument.Invoice && item.typeInvoice === this.typeFacture.Situation) {
			text = 'factureSituation';
		}
		if (item.documentType === TypeDocument.SupplierOrder) {
			text = 'bonCommandFournisseur';
		}
		if (item.documentType === TypeDocument.OperationSheet) {
			text = 'ficheintervention';
		}
		if (item.documentType === TypeDocument.Quote) {
			text = 'devis';
		}
		if (item.documentType === TypeDocument.OperationSheetMaintenance) {
			text = 'OperationSheetMaintenance';
		}
		if (item.documentType === TypeDocument.CreditNote) {
			text = 'avoir';
		}

		return this.translate.instant('typeDocAssocie.' + text);
	}
}


