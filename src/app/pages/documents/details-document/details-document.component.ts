import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { AppSettings } from 'app/app-settings/app-settings';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { MatDialog } from '@angular/material';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { HeaderService } from 'app/services/header/header.service';
import { MenuService } from 'app/services/menu/menu.service';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { StatutDepense } from 'app/Enums/Statut/StatutDepense.Enum';
import { TranslateConfiguration } from 'app/libraries/translation';
import { TypeRubrique } from 'app/Enums/TypeRubrique.Enum';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
declare var toastr: any;


@Component({
	selector: 'app-details-document',
	templateUrl: './details-document.component.html',
	styleUrls: [
		'./details-document.component.scss',
	]
})
export class DetailsDocumentComponent implements OnInit, OnDestroy {

	docType = '';
	apiUrl: typeof ApiUrl = ApiUrl;
	selectedTabs = 'information';
	id = '';
	document;
	historique = [];
	memos = [];
	idChantier: number = null;
	processing = false;
	actionsOpened = false;
	items = [];
	emails = [];
	actions = [];
	ModuleRebrique = null;
	canUpdateDoc = true;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private dialog: MatDialog,
		public translate: TranslateService,
		public route: ActivatedRoute,
		private menu: MenuService,
		private header: HeaderService

	) {
		this.docType = localStorage.getItem(LocalElements.docType);
		TranslateConfiguration.setCurrentLanguage(this.translate);

		// this.translate.setDefaultLang(AppSettings.lang);
		// this.translate.use(AppSettings.lang);
		ManageDataTable.service = service;
		ManageDataTable.docType = this.docType;
		ManageDataTable.dialog = this.dialog;
	}

	async ngOnInit(): Promise<void> {

		await this.init();
		this.router.events.subscribe(async (event) => {
			if (event instanceof NavigationEnd) {
				await this.init();
			}
		});

	}

	async init() {
		this.processing = true;
		this.id = this.route.snapshot.params.id;
		this.ModuleRebrique = this.route.snapshot.params.module;
		await this.getDocument();
		const res = await this.getParamsFromRoute('idChantier')
		this.idChantier = res === undefined ? null : res as number;
		ManageDataTable.idChantier = this.idChantier;
		this.getItems();
		this.processing = false;
		this.menu.opened = false;
		this.prepareBreadcrumb();
	}

	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			if (this.idChantier == null) {
				this.header.breadcrumb = [
					{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
					{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) },
				];
			}
			if (this.idChantier != null) {
				const namechantier = localStorage.getItem('nomChantier');
				let moduleName = localStorage.getItem('moduleName');
				if (moduleName === TypeRubrique.Devis.toString()) {
					moduleName = 'Devis';
				}
				if (moduleName === TypeRubrique.Commandes.toString()) {
					moduleName = 'Commandes';
				}
				if (moduleName === TypeRubrique.Facturation.toString()) {
					moduleName = 'Factures';
				}
				this.header.breadcrumb = [
					{ label: 'Chantier', route: `/chantiers` },
					{ label: namechantier, route: `/chantiers/detail/${this.idChantier}` },
					{ label: moduleName, route: this.isRouteRebrique() },
					{ label: this.document.reference, route: ManageDataTable.getRouteDetails(this.document) },
				];
			}

		});
	}

	isRouteRebrique() {
		const moduleName = localStorage.getItem('moduleName');
		if (this.docType === ApiUrl.Devis) {
			this.ModuleRebrique = 1;
		}
		if (this.docType === ApiUrl.Invoice) {
			this.ModuleRebrique = 3;
		}
		if (this.docType === ApiUrl.BonCommandeF) {
			this.ModuleRebrique = 2;
		}
		return `/chantiers/${this.idChantier}/documents/${moduleName}/${this.ModuleRebrique}`
	}
	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}

	modifier() {
		ManageDataTable.getUpdate(this.document);
	}

	conditionAcompteSituation() {
		// tslint:disable-next-line: max-line-length
		if (this.docType === ApiUrl.Invoice && this.document) {
			if (this.document['typeInvoice'] === TypeFacture.Acompte || this.document['typeInvoice'] === TypeFacture.Situation) {
				return false;
			} else {
				return true
			}
		} else {
			return true;
		}
	}
	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}

	ngOnDestroy(): void {
	}

	// #region services
	canUpdate(document) {
		if (this.docType !== ApiUrl.avoir && document && document.canUpdate === undefined) {
			return true;
		}
		if (this.docType !== ApiUrl.avoir && document && !document.canUpdate) {
			return false;
		}
		if (this.docType !== ApiUrl.avoir && document && document.canUpdate) {
			return true;
		}
		if (this.docType === ApiUrl.avoir && document && document.status === StatutAvoir.Utilise) {
			return false;
		} else {
			return true
		}

	}
	async getDocument(): Promise<any> {
		this.processing = true;
		return new Promise((resolve, reject) => {
			this.service.getById(this.docType, this.id).subscribe(async res => {
				this.document = res.value;
				this.canUpdateDoc = await this.canUpdate(this.document);
				this.historique = this.document.changesHistory;
				this.memos = this.document.memos;
				this.emails = this.document.emails;
				resolve(res.value);
				this.processing = false;
			});
		});
	}

	getItems() {
		this.items = ManageDataTable.getActions(true);
	}

	async saveMemo(memo: Memo) {
		// if (memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		this.processing = true;
		memo['id'] = AppSettings.guid();
		this.service.create(ApiUrl.File + '/' + this.id + '/Create/Memo', memo)
			.subscribe(res => {
				this.processing = false;
				this.getDocument();
			}, err => { console.log(err); this.processing = false; });
	}

	downloadPieceJointe(event) {
		const pieceJointe = event;
		this.service.getById(ApiUrl.File, pieceJointe.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, pieceJointe.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					pieceJointe.fileType)
			}
		)
	}

	onToggle(e: boolean): void {
		this.actionsOpened = e;
	}

	isPaiemnet() {
		return this.document && this.docType === this.apiUrl.Invoice && (this.document.status !== StatutFacture.Brouillon ||
			this.document.status !== StatutFacture.Annule)
	}
	isPaiemnetDepense() {
		return this.document && this.docType === this.apiUrl.Depense && (this.document.status !== StatutDepense.Brouillon ||
			this.document.status !== StatutDepense.Annule)
	}

	//#endregion
}
