import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ColumnType } from 'app/components/form-data/data-table/data-table.component';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DataTableRowActions } from 'app/libraries/manage-data-table';
import { StringHelper } from 'app/libraries/string';
import { TranslateConfiguration } from 'app/libraries/translation';
import { IFilterOption, SortDirection } from 'app/Models/Model/filter-option';
import { PagedResult } from 'app/Models/Model/ListModel';
import { HeaderService } from 'app/services/header/header.service';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'tache',
	templateUrl: './tache.component.html',
	styleUrls: ['./tache.component.scss']
})
export class TacheComponent implements OnInit {
	processing = false;
	filterMission;
	tasks = [];
	totalPage = 1;
	techniciens = [];
	technicienId: [] = [];
	items: any[];
	dataitems: any;
	rowCount: number;
	checkedColumns: boolean[] = [];
	pageSizeOptions = AppSettings.PAGE_SIZE_OPTIONS;
	state: IFilterOption = {
		Page: 1,
		PageSize: AppSettings.DEFAULT_PAGE_SIZE,
		OrderBy: 'id',
		SortDirection: SortDirection.Descending,
		SearchQuery: ''
	};
	filterOptions;
	data: PagedResult;
	search = ''
	page = 1;
	Columns: any[];
	columnType: typeof ColumnType = ColumnType;
	actions: DataTableRowActions[];
	selected: any;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate?: TranslateService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	async ngOnInit() {
	}
}