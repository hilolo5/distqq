import { registerLocaleData, CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { LOCALE_ID, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatIconModule, MatMenuModule, MatNativeDateModule, MatOptionModule, MatSelectModule, MatTableModule, MatTooltipModule, MAT_DATE_LOCALE } from "@angular/material";
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { CheckBoxAllModule, ButtonAllModule } from "@syncfusion/ej2-angular-buttons";
import { DatePickerAllModule, TimePickerAllModule, DateTimePickerAllModule } from "@syncfusion/ej2-angular-calendars";
import { DropDownListAllModule, MultiSelectAllModule } from "@syncfusion/ej2-angular-dropdowns";
import { NumericTextBoxAllModule, MaskedTextBoxModule, UploaderAllModule } from "@syncfusion/ej2-angular-inputs";
import { ToolbarAllModule, ContextMenuAllModule, TreeViewModule } from "@syncfusion/ej2-angular-navigations";
import { RecurrenceEditorAllModule, ScheduleAllModule } from "@syncfusion/ej2-angular-schedule";
import { CommonModules } from "app/common/common.module";
import { InputTextareaModule } from "app/common/input-textarea/input-textarea.module";
import { ScheduleModule } from "app/custom-module/primeng/primeng";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { FicheInterventionModule } from "../ficheInterv/ficheIntervention.module";
import localeFr from '@angular/common/locales/fr';
import { TacheComponent } from "./tache.component";
import { TacheRoutingModule } from "./tache-routing.module";

registerLocaleData(localeFr, 'fr-FR');

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/agendaglobal/', '.json');
}

@NgModule({
	declarations: [TacheComponent],
	imports: [
		CommonModule,
		TacheRoutingModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		InputTextareaModule,
		ScheduleModule,
		MatSelectModule,
		NgxMatSelectSearchModule,
		MatOptionModule,
		MatIconModule,
		RecurrenceEditorAllModule,
		FicheInterventionModule,
		HttpModule,
		ScheduleAllModule,
		RecurrenceEditorAllModule,
		MatCheckboxModule,
		NumericTextBoxAllModule,
		DatePickerAllModule,
		TimePickerAllModule,
		DateTimePickerAllModule,
		CheckBoxAllModule,
		ToolbarAllModule,
		DropDownListAllModule,
		ContextMenuAllModule,
		MaskedTextBoxModule,
		UploaderAllModule,
		MultiSelectAllModule,
		TreeViewModule,
		ButtonAllModule,
		NgSelectModule,
		CommonModules,
		NgbTooltipModule,
		FormsModule,
		ReactiveFormsModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatIconModule,
		MatMenuModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatTableModule,

	],
	providers: [

		{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
		{ provide: LOCALE_ID, useValue: 'fr-FR' }
	],
})

export class TacheModule { }
