import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';

import { PersonaliseReccurenteComponent } from 'app/common/personalise-reccurente/personalise-reccurente.component';
import { MatDialog, MatDialogConfig, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AppDateAdapter, APP_DATE_FORMATS } from 'app/shared/dateAdapter/date.adapter';
import { ReccurenteModel } from 'app/Models/Model/ReccurenteModel';
import { StatutReccurente } from 'app/Enums/Statut/StatutReccurente.Enum';
import { TypeTermine, DayOfWeek, PeriodicityRecurringType, PeriodicityType, PeriodicityEndingType } from 'app/Enums/TypeRepetition.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { DatePipe } from '@angular/common';
import { Color } from 'app/Enums/color';

@Component({
	selector: 'facture-information-reccurents',
	templateUrl: './informations.component.html',
	styleUrls: ['./informations.component.scss'],
	providers: [
		{
			provide: DateAdapter, useClass: AppDateAdapter
		},
		{
			provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
		},
		DatePipe
	]
})
export class InformationsComponent implements OnInit, OnChanges {


	@Input('reccurente') reccurente: any;
	@Input('client') client: Client;
	@Input('articles') articles = [];
	statutReccurente: typeof StatutReccurente = StatutReccurente;
	typeTermineEnum: typeof TypeTermine = TypeTermine;
	daysEnum: typeof DayOfWeek = DayOfWeek;
	reccurent: ReccurenteModel = new ReccurenteModel();
	form: FormGroup;
	typetermine = null;
	dateCreation: Date = new Date();
	workshopName;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	typeRepetitionEnum: typeof PeriodicityRecurringType = PeriodicityRecurringType;
	periodicityType: typeof PeriodicityType = PeriodicityType;
	periodicityEndingType: typeof PeriodicityEndingType = PeriodicityEndingType;
	dataDialog: ReccurenteModel = null;
	result: ReccurenteModel = null;
	partProrata = false;
	participationPuc = false;
	retenuGaranti = false;
	color: typeof Color = Color;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog,
		private dateAdapter: DateAdapter<Date>,
		private datepipe: DatePipe

	) {
		this.dateAdapter.setLocale('fr');
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}
	designationgetAdresseIntervention() {

	}
	async ngOnChanges() {
		if (this.reccurente !== undefined) {
			this.dateCreation = new Date(this.reccurent.dateValidation);
			this.typetermine = this.reccurente.typeTermine;
			this.workshopName = this.reccurente.document.workshopId !== null ?
				(await this.getdocById(ApiUrl.Chantier, this.reccurente.document.workshopId)).name : '';
			this.participationPuc = (this.reccurente.document['orderDetails']['puc'] > 0);
			this.partProrata = (this.reccurente.document['orderDetails']['proportion'] > 0);
			this.retenuGaranti = (this.reccurente.document['orderDetails']['holdbackDetails']['holdback'] > 0);
		}
	}
	readolyChecked() {
		return false
			;
	}
	getColor(status) { return this.color[status] };



	getdocById(url, id): Promise<Workshop> {
		if (id !== null) {
			return new Promise((resolve, reject) => {
				this.service.getById(url, id).subscribe(
					res => resolve(res.value),
					err => reject(err)
				);
			});
		}

	}

	async ngOnInit() {

	}

	LoadPersonaliseReccurente() {

		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '500px';
		dialogLotConfig.data = {};
		const res = new ReccurenteModel();
		res.typeRepetition = this.reccurente.periodicityOptions.recurringType;
		res.nbrRepetition = this.reccurente.periodicityOptions.repeatEvery;
		res.jour = this.getValid(this.reccurente.periodicityOptions) ? this.reccurente.periodicityOptions.dayOfMonth : null;
		res.jourSemaine = this.getValid(this.reccurente.periodicityOptions) ? this.reccurente.periodicityOptions.daysOfWeek : null;

		dialogLotConfig.data = { listes: res, readOnly: true };


		const dialogRef = this.dialog.open(PersonaliseReccurenteComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});

	}

	getValid(res) {
		if (res !== null && res !== undefined) {
			return true
		}
		return false;
	}

	getAdresseDesignation(data): string {
		try {
			const adresse = data.filter(x => x.isDefault);
			return adresse.length !== 0 ? adresse[0].designation : '';
		} catch (err) {
			return '';
		}
	}

	getAdresseIntervention(data): string {
		try {
			return data != null && data !== undefined ? data.designation : '';
		} catch (err) {
			return '';
		}
	}

	getDate(date) {
		return this.datepipe.transform(new Date(date), 'dd/MM/yyyy');
	}
}
