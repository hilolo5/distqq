import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformationsComponent } from './show/informations/informations.component';
import { HttpClient } from '@angular/common/http';
import { ShowReccurenteComponent } from './show/show.component';
import { CommonModules } from 'app/common/common.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, SplitButtonModule, InputTextareaModule } from 'app/custom-module/primeng/primeng';
import { DataTablesModule } from 'angular-datatables';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { FactureReccurenteRoutingModule } from './facture-reccurente-routing.module';
import { PrsonaliseReccurenteModule } from 'app/common/personalise-reccurente/personalise-reccurente.module';
import {
	MatDialogModule, MatDatepickerModule, MatIconModule, MatInputModule, MatNativeDateModule,
	MatFormFieldModule, MatTableModule, MatSortModule, MatCheckboxModule, MatPaginatorModule,
	MatTooltipModule, MatButtonModule, MatSelectModule, MatMenuModule, MatTabsModule
} from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { EditReccurenteComponent } from './edit-reccurente/edit-reccurente.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';

// export function createTranslateLoader(http: HttpClient) {
// 	return new TranslateHttpLoader(http, './assets/i18n/facture-reccurentes/', '.json');
// }
export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/facture-reccurentes/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },
	]);
}
@NgModule({
	declarations: [
		ShowReccurenteComponent,
		InformationsComponent,
		EditReccurenteComponent],
	imports: [
		FactureReccurenteRoutingModule,
		CommonModule,
		CommonModules,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),

		FormsModule,
		ReactiveFormsModule,
		NgxMatSelectSearchModule,
		AddChantierFormModule,
		NgSelectModule,
		NgbTooltipModule,
		DataTablesModule,
		SplitButtonModule,
		AngularEditorModule,
		TableArticleModule,
		PrsonaliseReccurenteModule,
		MatDialogModule,
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		DataTablesModule,
		SplitButtonModule,
		AngularEditorModule,
		TableArticleModule,
		MatDatepickerModule,
		MatDialogModule,
		MatIconModule,
		MatInputModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatNativeDateModule,
		InputTextareaModule,
		MatDialogModule,
		MatTableModule,
		OverlayModule,
		MatSortModule,
		NgSelectModule,
		CalendarModule,
		MatCheckboxModule,
		NgbTooltipModule,
		MatPaginatorModule,
		InfiniteScrollModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatMenuModule,
		ShowHidePasswordModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatTabsModule,
	],
	providers: [
		MatDatepickerModule,

	],
})
export class FactureReccurenteModule { }
