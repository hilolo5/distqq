import { NgModule } from '@angular/core';
import { ShowReccurenteComponent } from './show/show.component';
import { RouterModule, Routes } from '@angular/router';
import { EditReccurenteComponent } from './edit-reccurente/edit-reccurente.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				pathMatch: 'full',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			},
			{
				path: 'ajouter',
				component: EditReccurenteComponent,
			},
			{
				path: 'ajouter/:id',
				component: EditReccurenteComponent,
			},
			{
				path: 'modifier/:id',
				component: EditReccurenteComponent,
			},
			{
				path: 'detail/:id',
				component: ShowReccurenteComponent,
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})

export class FactureReccurenteRoutingModule { }
