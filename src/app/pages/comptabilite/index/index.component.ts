import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { JournalType } from 'app/Enums/typeJournal.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';

declare var toastr: any;
declare var jQuery: any;

export enum TabsComptabilite {
	JournalVente,
	JournalAchat,
	JournalBanque,
	JournalCaisse
}

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

	selectedTab: TabsComptabilite = TabsComptabilite.JournalVente;
	tabsComptabilite = TabsComptabilite;
	apiUrl: ApiUrl = ApiUrl.journal;
	loading = false;
	periode = PeriodType.None
	min = new Date();
	dateCloture = new Date();
	processing = false;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.GetPeriodeComptable();
		this.min.setDate(this.min.getDate() - 5);
	}

	GetPeriodeComptable() {
		this.loading = true;
		this.service.getAll(ApiUrl.configAccountingPeriode)
			.subscribe(res => {
				if (res.value.length !== 0) {
					const data = res.value.find(x => x.endingDate === null)
					this.dateCloture = data !== undefined ? data.startingDate : this.dateCloture;
				}
				this.loading = false;
			}, err => {
				this.loading = false;
			})
	}


	tabs(e) {
		this.selectedTab = +(e.tab.content.viewContainerRef.element.nativeElement.dataset.name);
	}


	exporterClose() {
		this.loading = true;
		this.service.getAll(ApiUrl.configAccountingPeriode + '/Close').subscribe(
			res => {
				this.loading = false;
				jQuery('#cloturePeriodeComptable').modal('hide');
				if (res) { this.exporter(); }
			},
			err => {
				if (err.error.messageCode === 139) {

					this.loading = false;
					const message = 'la période comptable sélectionnée n\'a pas atteint la fin de sa période';
					toastr.warning(message, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
				console.log(err);
			}
		)
	}

	getFiletrExport() {
		return {
			'journalType': this.typeJournaux(),
			'periodType': ManageDataTable.filterOptions['periodType'],
			'ignorePagination': false,
			'dateStart': ManageDataTable.filterOptions.DateStart,
			'dateEnd': ManageDataTable.filterOptions.DateEnd,
			'searchQuery': ManageDataTable.filterOptions.SearchQuery,
			'page': 1,
			'pageSize': 10,
			'sortDirection': 'Descending',
			'orderBy': 'entryDate'
		};
	}

	typeJournaux() {
		if (this.selectedTab === TabsComptabilite.JournalAchat) { return JournalType.Expense };
		if (this.selectedTab === TabsComptabilite.JournalVente) { return JournalType.Sales; }
		if (this.selectedTab === TabsComptabilite.JournalCaisse) { return JournalType.Cash; }
		if (this.selectedTab === TabsComptabilite.JournalBanque) { return JournalType.Bank; }
	}

	typeJournauxTranslate() {
		if (this.selectedTab === TabsComptabilite.JournalAchat) { return 'JournalAchat ' };
		if (this.selectedTab === TabsComptabilite.JournalVente) { return 'JournalVente'; }
		if (this.selectedTab === TabsComptabilite.JournalCaisse) { return 'JournalCaisse'; }
		if (this.selectedTab === TabsComptabilite.JournalBanque) { return 'JournalBanque'; }
	}

	exporter() {
		this.loading = true;
		this.service.create(ApiUrl.journal + '/Export', this.getFiletrExport()).subscribe(
			value => {
				this.loading = false;
				// Get time stamp for fileName.
				const stamp = new Date().getTime();
				// file type
				const fileType = 'application/vnd.ms-excel';
				// file data
				const fileData = AppSettings._base64ToArrayBuffer(value.value);
				// file extension
				const extension = 'xlsx';
				AppSettings.setFile(fileData, (this.typeJournauxTranslate()) + stamp + '.' + extension, fileType, extension);

			},
			err => {
				console.log(err);
				this.loading = false;
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.server, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
			}
		)
	}

}
