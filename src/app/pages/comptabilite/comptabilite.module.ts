import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ComptabiliteRoutingModule } from './comptabilite-routing.module';
import { CommonModules } from 'app/common/common.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { DataTablesModule } from 'angular-datatables';
import { IndexListGlobalComponent } from 'app/components/form-data/index-list/index.component';
import { ListIndexModule } from 'app/components/form-data/index-list/index.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { MatIconModule, MatMenuModule, MatTabsModule } from '@angular/material';


export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/comptabilite/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' }
	]);
}

@NgModule({
	declarations: [IndexComponent],
	imports: [
		CommonModule,
		ComptabiliteRoutingModule,
		CommonModules,
		MatTabsModule,
		MatMenuModule,
		MatIconModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		DataTablesModule,
		ListIndexModule
	],
	exports: [
		IndexListGlobalComponent,
	],
	entryComponents: [IndexListGlobalComponent],
	providers: []
})
export class ComptabiliteModule { }
