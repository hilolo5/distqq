
import { NgModule, Injector, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { CommonModules } from './common/common.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { DashboardLayoutComponent } from './layouts/dashboard/dashboard.component';
import { loginLayoutComponent } from './layouts/login/login.component';
import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { MissingTranslationHandler, TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { repositoryConfig } from './shared/service/service-config';
import { InputTextareaModule } from './common/input-textarea/input-textarea.module';
import { MatTableModule, MatPaginatorModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MAT_DATE_LOCALE } from '@angular/material';
import { Router } from '@angular/router';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { CustomMatPaginatorIntl } from './libraries/pagination-personalise';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { MatSliderModule } from '@angular/material/slider';
import { IGenericRepository } from './shared/repository/igeneric-repository';
import { GenericRepository } from './shared/repository/generic-repository';
import { DatePipe } from '@angular/common';
import { ShowCurrencyPipe } from './common/Pipes/show-currency.pipe';
import { CustomMissingTranslationHandler } from './libraries/translation-handler';
import { TacheModule } from './pages/tache/tache.module';

export function Loader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
	]);
}
@NgModule({
	declarations: [
		AppComponent,
		DashboardLayoutComponent,
		loginLayoutComponent,
	],
	imports: [
		MatTableModule,
		MatPaginatorModule,
		MatDialogModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		SharedModule,
		CommonModules,
		MatSliderModule,
		HttpClientModule,
		InputTextareaModule,
		NgbModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: Loader,
				deps: [HttpClient]
			},
			missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomMissingTranslationHandler },
			isolate: true
		}),
		TacheModule,
	],
	providers: [
		{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
		TranslateStore,
		TranslateService,
		{
			provide: APP_INITIALIZER,
			useFactory: initialiseGlobalDependencies,
			multi: true,
			deps: [TranslateService, HttpClient]
		},
		AuthService,
		AuthGuard,
		repositoryConfig,
		DatePipe,
		ShowCurrencyPipe
	],
	bootstrap: [AppComponent]
})


export class AppModule {

	constructor(
		private router: Router,
		public datePipe: DatePipe,
		public currencyPipe: ShowCurrencyPipe
	) {
		// tslint:disable-next-line: no-use-before-declare
		GlobalInstances.router = this.router;
		GlobalInstances.genericService = new GenericRepository();
		GlobalInstances.datePipe = this.datePipe;
		GlobalInstances.currencyPipe = this.currencyPipe;
	}
}

export function initialiseGlobalDependencies(
	translateService: TranslateService,
	httpClient: HttpClient,
): () => Promise<any> {
	GlobalInstances.translateService = translateService;
	GlobalInstances.httpClient = httpClient;
	return () => Promise.resolve(true);
}

export class GlobalInstances {
	public static translateService: TranslateService;
	public static httpClient: HttpClient;
	public static location: Location;
	public static router: Router;
	public static genericService: IGenericRepository<any>;
	public static datePipe: DatePipe;
	public static currencyPipe: ShowCurrencyPipe;


}

