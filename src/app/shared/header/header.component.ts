import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService } from 'app/services/menu/menu.service';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { LocalElements } from '../utils/local-elements';
import { HeaderService } from 'app/services/header/header.service';
import { DialogHelper } from 'app/libraries/dialog';
import { MatDialog } from '@angular/material';
import { ChangePasswordComponent } from 'app/common/change-password/change-password.component';



@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

	//#region Properties

	title = '';
	icon = '';

	notificationsOpened = false;

	user: any;
	subscriptions: Subscription = new Subscription();

	//#endregion

	//#region Lifecycle
	constructor(
		private router: Router,
		public menu: MenuService,
		public header: HeaderService,
		private dialog: MatDialog,

	) {
		this.loadUser();
	}

	ngOnInit(): void {
		this.subscriptions.add(
			this.router.events
				.pipe(filter(e => e instanceof NavigationEnd))
				.subscribe(e => {
					const activeGroup = this.menu.menuItems.filter((group) => this.menu.isGrouActive(group))[0];

					if (activeGroup) {
						this.title = activeGroup['label'];
						this.icon = activeGroup['iconStale'];
					}
				})
		);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	//#endregion

	//#region Events

	onProfile(): void {
		this.loadUser();
		this.router.navigate([`utilisateurs/detail/${this.user.id}`])
	}

	onChangePassword(): void {
		this.loadUser();
		localStorage.setItem('SBP_changePassword_UserId', this.user.id.toString());
		DialogHelper.openDialog(
			this.dialog,
			ChangePasswordComponent,
			DialogHelper.SIZE_MEDIUM,
			{ userId: this.user.id }
		).subscribe(async response => {
		});
	}

	onLogout(): void {
		this.router.navigate(['/login']).then(() => {
			localStorage.removeItem(LocalElements.TOKEN);
			localStorage.removeItem(LocalElements.PDB_USER);
		});
	}

	onNotificationsTriggered(): void {
		this.notificationsOpened = !this.notificationsOpened;
	}

	onBackdropClicked(): void {
		this.notificationsOpened = false;
	}

	//#endregion

	//#region Methods

	loadUser(): void {
		if (!this.user) {
			this.user = JSON.parse(localStorage.getItem(LocalElements.PDB_USER));
		}
	}

	generateAvatar = () => this.user ? `${this.user.lastName[0]}${this.user.firstName[0]}` : '';

	//#endregion
}
