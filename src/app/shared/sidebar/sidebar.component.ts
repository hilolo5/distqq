import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ROUTES } from './sidebar-routes.config';


declare var $: any;
@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {

	public menuItems: any[];
	i = 0;

	constructor(private translate: TranslateService) { }


	ngOnInit() {

		$.getScript('./assets/app/js/core/app.min.js');
		$.getScript('./assets/app/js/core/app-menu.min.js');

		this.translate.setDefaultLang(AppSettings.lang)
		this.translate.use(AppSettings.lang);
		// cas  : user
		// ROUTES.filter(r => r.title === 'Users')[0].actif = (RoleUtils.isAdmin()) ? true : false;

		// // cas  : parametres
		// ROUTES.filter(r => r.title === 'parameteres')[0].actif = (RoleUtils.isAdmin()) ? true : false;

		this.menuItems = ROUTES.filter(menuItem => menuItem.actif === true);
		// this.menuItems = ROUTES.filter(menuItem => menuItem);

	}


	clickSubMenu(index, e) {
		const className = e.srcElement.offsetParent.className
		this.i++;
		if (this.menuItems[index].submenu.length > 0 && this.i > 1) {
			if (className.includes('open') && (this.menuItems[index].open == null || !this.menuItems[index].open)) {
				this.menuItems[index].open = true;
			} else if (this.menuItems[index].open == null || !this.menuItems[index].open) {
				this.menuItems[index].open = true;
				this.menuItems[index].class += ' open'
			} else {
				this.menuItems[index].open = false;
				const classe = this.menuItems[index].class as string;
				this.menuItems[index].class = classe.replace(' open', '');
			}
		}
	}

}
