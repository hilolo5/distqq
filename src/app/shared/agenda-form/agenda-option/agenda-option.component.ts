import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MatDateFormats, MAT_NATIVE_DATE_FORMATS, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { AgendaTypeFilter } from 'app/Enums/AgendaTypeFIlter';
import { Color } from 'app/Enums/color';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { MissionKind } from 'app/Enums/missionKinds';
import { StatutMission } from 'app/Enums/StatutMission .Enum';
import { DialogHelper } from 'app/libraries/dialog';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';

export const GRI_DATE_FORMATS: MatDateFormats = {
	...MAT_NATIVE_DATE_FORMATS,
	display: {
		...MAT_NATIVE_DATE_FORMATS.display,
		dateInput: {
			year: 'numeric',
			month: 'short',
			day: 'numeric',
		} as Intl.DateTimeFormatOptions,
	}
};

export enum priorite {
	urgent,
	moyen,
	normal,
	basic
}

@Component({
	selector: 'app-agenda-option',
	templateUrl: './agenda-option.component.html',
	styleUrls: ['./agenda-option.component.scss'],
	providers: [
		{ provide: MAT_DATE_FORMATS, useValue: GRI_DATE_FORMATS },
		{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' }
	]
})

export class AgendaoptionComponent implements OnInit {

	//#region Properties

	AgendaTypeFilter: typeof AgendaTypeFilter = AgendaTypeFilter;
	type: AgendaTypeFilter = AgendaTypeFilter.Taches;
	today: Date = new Date();
	clients = [];
	listClients = [];
	technichiens = [];
	descripion;
	clientId;
	technicienId;
	date;
	form;
	listtechnichiens = [];
	listchantiers = [];
	chantiers = [];
	types = [];
	listDure = [];
	listpriorite = [];
	listContacts = [];
	listAdresse = [];
	contactsList = [];
	adresseList = [];
	color: typeof Color = Color;
	time;
	//#endregion
	selectedStatus = StatutMission.InProgress;
	status = [];
	isAdd = true;

	dures = ['00:30', '01:00', '01:30', '02:00', '02:30', '03:00'
		, '03.30', '04:00', '04:30', '05:00', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00'];


	constructor(
		private router: Router,
		private fb: FormBuilder,
		private dialog: MatDialog,
		public dialogRef: MatDialogRef<AgendaoptionComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { hasBackdrop, dateSelectd, res, selected, type, edit },
		@Inject('IGenericRepository') private service: IGenericRepository<any>
	) {
		this.form = this.fb.group({
			title: ['', [Validators.required]],
			object: [''],
			technicianId: ['', [Validators.required]],
			clientId: [''],
			date: [this.data.dateSelectd, [Validators.required]],
			time: [''],
			allDayLong: [false],
			workshopId: [''],
			type: [''],
			dure: [''],
			priorite: [''],
			phoneNumber: [''],
			adresse: [''],
			contact: [''],
		});

		this.type = this.data.type;
		this.getType();
		if (this.data.res) {
			this.isAdd = false;

			this.setData(this.data.res);
		}
		this.isRequired();
	}


	ngOnInit(): void {
		this.status = [StatutMission.InProgress, StatutMission.Finished, StatutMission.Annulee];

		this.getClients();
		this.getTechniciens();
		this.getChantiers();

		this.listpriorite = Object.entries(priorite).map(doc => ({
			label: doc[0], value: doc[1]
		})).filter(e => Number.isInteger(e.value) === true);

	}

	getPhone(e) {
		const client = this.clients.find(x => x.id === e);
		if (client) {
			this.form.controls['phoneNumber'].setValue(client.phoneNumber);
			this.listAdresse = client.addresses;
			this.listContacts = client.contactInformations;
		}
	}


	getColor(status) { return this.color[status] };

	//#region Lifecycle
	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}


	getTelepone(e) {

		const client = this.clients.find(x => x.id === e);
		if (client) {

			this.listAdresse = client.addresses;
			this.listContacts = client.contactInformations;
		}

		if (this.data.selected && this.type === AgendaTypeFilter.Evenements) {
			this.form.controls['adresse'].setValue(this.data.selected.additionalInfo);
			this.adresseList = this.data.selected.additionalInfo;
		}
		if (this.data.selected && this.type === AgendaTypeFilter.Rappels) {
			this.form.controls['contact'].setValue(this.data.selected.additionalInfo);
			this.contactsList = this.data.selected.additionalInfo;
		}
	}

	urlType() {
		if (this.type === AgendaTypeFilter.Evenements) { return 'Configuration/mission_appointment_types'; }
		if (this.type === AgendaTypeFilter.Rappels) { return 'Configuration/mission_call_types'; }
		if (this.type === AgendaTypeFilter.Taches) { return 'Configuration/mission_technician_task_types'; }
	}

	isRequired() {
		if (this.type === AgendaTypeFilter.Evenements) { this.form.get('clientId').setValidators([Validators.required]) }
		if (this.type === AgendaTypeFilter.Rappels) {
			this.form.get('clientId').setValidators([Validators.required]);
			this.form.get('phoneNumber').setValidators([Validators.required])
		}
	}

	getType() {
		const filter = {
			missionKinds: [this.getMisionKind()],
			searchQuery: '',
			page: 1,
			ignorePagination: true,
			pageSize: 0,
			sortDirection: 'descending',
			orderBy: 'value'
		};
		this.service.getAllPagination(ApiUrl.typesMission, filter).subscribe(res => {
			this.types = res.value;
		});
	}

	filterclient(e) {
		if (e !== '') {
			this.clients = [];
			this.clients = this.listClients.filter(x => x.name.includes(e) || x.reference.includes(e));
		} else {
			this.clients = this.listClients;
		}

	}

	filterchantier(e) {
		if (e !== '') {
			this.chantiers = [];
			this.chantiers = this.listchantiers.filter(x => x.name.includes(e) || x.reference.includes(e));
		} else {
			this.chantiers = this.listchantiers;
		}

	}

	filterTechnicien(e) {
		if (e !== '') {
			this.technichiens = [];
			this.technichiens = this.listtechnichiens.filter(x => x.lastName.includes(e) || x.firstName.includes(e));
		} else {
			this.technichiens = this.listtechnichiens;
		}
	}

	compareWithFn(item1, item2) {
		return item1 && item2 ? item1.designation === item2.designation : item1 === item2;
	}

	compareWithContact(item1, item2) {
		return item1 && item2 ? item1.firstName === item2.firstName && item1.lastName === item2.lastName : item1 === item2;
	}


	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients();
				this.clients.unshift(client);
				this.form.controls['clientId'].setValue(client.id);
			}
		});
	}

	get f() { return this.form.controls; }

	//#endregion

	//#region Events

	onClose(): void {
		this.dialogRef.close();
	}


	getMisionKind() {
		if (this.type === AgendaTypeFilter.Evenements) { return MissionKind.Appointment; }
		if (this.type === AgendaTypeFilter.Rappels) { return MissionKind.Call; }
		if (this.type === AgendaTypeFilter.Taches) { return MissionKind.TechnicianTask; }
	}


	async onValidate() {

		if (this.form.valid) {
			this.form.value.time = this.time ? (this.time.hour && +this.time.hour < 9 ? '0' + this.time.hour : this.time.hour)
				+ ':' + (this.time.minute && +this.time.minute < 9 ? '0' + this.time.minute : this.time.minute) : '';
			if (this.data.edit === 'update') {
				this.data.selected['title'] = this.form.value.title;
				this.data.selected['status'] = this.selectedStatus;
				this.data.selected['object'] = this.form.value.object;
				this.data.selected['clientId'] = this.form.value.clientId;
				this.data.selected['workshopId'] = this.form.value.workshopId;
				this.data.selected['technicianId'] = this.form.value.technicianId;
				this.data.selected['allDayLong'] = this.form.value.allDayLong;
				this.data.selected['duration'] = this.form.value.dure;
				this.data.selected['type'] = this.form.value.type;
				this.data.selected['phoneNumber'] = this.form.value.phoneNumber;
				this.data.selected['additionalInfo'] = this.getAdditional();
				this.data.selected['startingDate'] = AppSettings.formaterDatewithTime(this.form.value.date, !this.form.value.allDayLong ? this.form.value.time : '');
				await this.service.update(ApiUrl.Mission, this.data.selected, this.data.selected.id).toPromise();
				this.dialogRef.close(this.data.selected);
			} else {
				const res = {
					missionKind: this.getMisionKind(),
					duration: this.form.value.dure,
					type: this.form.value.type,
					title: this.form.value.title,
					object: this.form.value.object,
					status: this.selectedStatus,
					startingDate: AppSettings.formaterDatewithTime(this.form.value.date, !this.form.value.allDayLong ? this.form.value.time : ''),
					endingDate: null,
					clientId: this.form.value.clientId,
					workshopId: this.form.value.workshopId,
					technicianId: this.form.value.technicianId,
					allDayLong: this.form.value.allDayLong,
					phoneNumber: this.form.value.phoneNumber,
					additionalInfo: this.getAdditional()
				}
				await this.service.create(ApiUrl.Mission + ACTION_API.create, res).toPromise();
				this.dialogRef.close(res);
			}
		}
	}

	getAdditional() {
		if (this.type === AgendaTypeFilter.Evenements) {
			return this.form.value.adresse;
		}
		if (this.type === AgendaTypeFilter.Rappels) { return this.form.value.contact; }
		if (this.type === AgendaTypeFilter.Taches) { return [{ value: this.form.value.priorite }]; }
	}

	getTime(date) {
		const res = date.split('T');
		this.time = {
			'hour': +res[1].split(':')[0],
			'minute': +res[1].split(':')[1],
		}
		return res[1];
	}

	setData(doc) {
		this.getTelepone(doc.clientId);
		this.selectedStatus = doc.status;
		if (!doc.allDayLong && doc.time && doc.time !== '') {
			this.time = {
				'hour': +doc.time.split(':')[0],
				'minute': +doc.time.split(':')[1],
			}
		}

		if (this.data.edit === 'update') {

			this.form.patchValue({
				title: doc.title,
				object: doc.object,
				allDayLong: doc.allDayLong,
				date: doc.date,
				time: !doc.allDayLong ? doc.time : '',
				status: doc.statut,
				endingDate: null,
				clientId: doc.clientId,
				phoneNumber: doc.phoneNumber,
				technicianId: doc.technicianId,
				dure: this.data.selected.duration,
				type: this.data.selected.type,
				workshopId: this.data.selected.workshop ? this.data.selected.workshop.id : ''
			});

			if (this.type === AgendaTypeFilter.Taches) {
				this.form.controls['priorite'].setValue(this.data.selected.additionalInfo[0] ? this.data.selected.additionalInfo[0].value : '');
			}
		}
		if (this.data.edit === 'create') {
			this.form.patchValue({
				title: doc.title,
				object: doc.object,
				allDayLong: doc.allDayLong,
				date: doc.date,
				time: !doc.allDayLong ? doc.time : '',
				status: '',
				phoneNumber: doc.phoneNumber,
				endingDate: null,
				clientId: doc.clientId,
				technicianId: doc.technicianId,
				dure: doc.dure,
			});
		}
	}


	//#endregion

	//#region Methods

	async getClients(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.clients = res.value;
				this.listClients = res.value;
				this.getTelepone(this.form.value.clientId);
				resolve(res.value);
			}, (err: any) => reject(err));
		});
	}

	async getTechniciens(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.service.getAllPagination(ApiUrl.User, {}).subscribe(res => {
				this.technichiens = res.value;
				this.listtechnichiens = res.value;
				resolve(res.value);
			}, (err: any) => reject(err));
		});
	}

	async getChantiers(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.service.getAllPagination(ApiUrl.Chantier, {}).subscribe(res => {
				this.chantiers = res.value;
				this.listchantiers = res.value;
				resolve(res.value);
			}, (err: any) => reject(err));
		});
	}


	//#endregion
}
