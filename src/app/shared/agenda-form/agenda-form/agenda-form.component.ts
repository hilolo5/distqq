import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, DateAdapter, MatDateFormats, MAT_NATIVE_DATE_FORMATS, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { AgendaTypeFilter } from 'app/Enums/AgendaTypeFIlter';
import { Color } from 'app/Enums/color';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { MissionKind } from 'app/Enums/missionKinds';
import { StatutMission } from 'app/Enums/StatutMission .Enum';
import { DialogHelper } from 'app/libraries/dialog';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { LocalElements } from 'app/shared/utils/local-elements';
import { AgendaoptionComponent } from '../agenda-option/agenda-option.component';

export const GRI_DATE_FORMATS: MatDateFormats = {
	...MAT_NATIVE_DATE_FORMATS,
	display: {
		...MAT_NATIVE_DATE_FORMATS.display,
		dateInput: {
			year: 'numeric',
			month: 'short',
			day: 'numeric',
		} as Intl.DateTimeFormatOptions,
	}
};

@Component({
	selector: 'app-agenda-form',
	templateUrl: './agenda-form.component.html',
	styleUrls: [
		'./agenda-form.component.scss',
		'./../../../../assets/scss/components/input.scss'
	],
	providers: [
		{ provide: MAT_DATE_FORMATS, useValue: GRI_DATE_FORMATS },
		{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' }
	]
})
export class AgendaFormComponent implements OnInit {
	color: typeof Color = Color;

	//#region Properties
	MissionKind: typeof MissionKind = MissionKind;
	AgendaTypeFilter: typeof AgendaTypeFilter = AgendaTypeFilter;
	type: AgendaTypeFilter = AgendaTypeFilter.Taches;
	today: Date = new Date();
	clients = [];
	listClients = [];
	technichiens = [];
	descripion;
	clientId;
	clientName = '';
	techName = '';
	technicienId;
	date;
	form;
	listtechnichiens = [];
	//#endregion
	readOnly = false;
	//#region Lifecycle
	isMission = false;
	selectedStatus = StatutMission.InProgress;
	status = [];
	isAdd = true;
	dures = ['00:30', '01:00', '01:30', '02:00', '02:30', '03:00'
		, '03.30', '04:00', '04:30', '05:00', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00'];
	time;
	hasMain = true;
	constructor(
		private router: Router,
		private fb: FormBuilder,
		private dialog: MatDialog,
		public dialogRef: MatDialogRef<AgendaFormComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { hasBackdrop, dateSelectd, res, readOnly, isMission, hasMain },
		@Inject('IGenericRepository') private service: IGenericRepository<any>
	) {

		this.createForm();
		this.readOnly = this.data.readOnly === undefined ? false : this.data.readOnly;
		this.hasMain = this.data.hasMain === undefined ? true : this.data.hasMain;
		if (this.data.res) {
			this.isAdd = false
			this.setData(this.data.res);
		}
	}

	ngOnInit(): void {
		this.status = [StatutMission.InProgress, StatutMission.Finished, StatutMission.Annulee];

		this.getClients();
		this.getTechniciens();

		this.isMission = this.data.isMission === undefined ? false : this.data.isMission;
	}
	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}

	getColor(status) { return this.color[status] };

	createForm() {
		this.form = this.fb.group({
			dure: [''],
			title: ['', [Validators.required]],
			object: [''],
			technicianId: ['', [Validators.required]],
			clientId: [''],
			date: [this.data.dateSelectd, [Validators.required]],
			time: [''],
			allDayLong: [false],
			phoneNumber: [''],
			status: this.selectedStatus
		});
		this.isRequired();
	}

	getTelepone(e) {
		const client = this.clients.find(x => x.id === e);
		if (client) {
			this.form.controls['phoneNumber'].setValue(client.phoneNumber);
		}
	}

	filterclient(e) {
		if (e !== '') {
			this.clients = [];
			this.clients = this.listClients.filter(x => x.name.includes(e) || x.reference.includes(e));
		} else {
			this.clients = this.listClients;
		}

	}

	getMisionKind() {
		if (this.type === AgendaTypeFilter.Evenements) { return MissionKind.Appointment; }
		if (this.type === AgendaTypeFilter.Rappels) { return MissionKind.Call; }
		if (this.type === AgendaTypeFilter.Taches) { return MissionKind.TechnicianTask; }
	}

	isRequired() {
		if (this.type === AgendaTypeFilter.Evenements) { this.form.get('clientId').setValidators([Validators.required]) }
		if (this.type === AgendaTypeFilter.Rappels) {
			this.form.get('clientId').setValidators([Validators.required]);
			this.form.get('phoneNumber').setValidators([Validators.required])
		}
	}


	getActive(type) {
		if (type === MissionKind.Appointment) { return AgendaTypeFilter.Evenements; }
		if (type === MissionKind.Call) { return AgendaTypeFilter.Rappels; }
		if (type === MissionKind.TechnicianTask) { return AgendaTypeFilter.Taches; }
	}

	otherOption() {
		const edit = this.data.res ? 'update' : 'create';
		DialogHelper.openDialog(
			this.dialog,
			AgendaoptionComponent,
			DialogHelper.SIZE_LARGE,
			{ res: this.form.value, selected: this.data.res, type: this.type, edit: edit }
		).subscribe(async response => {
			if (response) {
				// this.router.navigate(['/agendaglobal', { refresh: (new Date).getTime() }]);
				this.dialogRef.close(response);
			}
		});
	}

	filterTechnicien(e) {
		if (e !== '') {
			this.technichiens = [];
			this.technichiens = this.listtechnichiens.filter(x => x.lastName.includes(e) || x.firstName.includes(e));
		} else {
			this.technichiens = this.listtechnichiens;
		}
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients();
				this.clients.unshift(client);
				this.form.controls['clientId'].setValue(client.id);
			}
		});
	}

	get f() { return this.form.controls; }

	//#endregion

	//#region Events

	onClose(): void {
		this.dialogRef.close();
	}

	async onValidate() {

		if (this.form.valid) {
			this.form.value.time = this.time ? (this.time.hour && +this.time.hour < 9 ? '0' + this.time.hour : this.time.hour)
				+ ':' + (this.time.minute && +this.time.minute < 9 ? '0' + this.time.minute : this.time.minute) : '';
			if (this.data.res) {
				this.data.res['duration'] = this.form.value.dure;
				this.data.res['status'] = this.selectedStatus;
				this.data.res['title'] = this.form.value.title;
				this.data.res['object'] = this.form.value.object;
				this.data.res['clientId'] = this.form.value.clientId;
				this.data.res['technicianId'] = this.form.value.technicianId;
				this.data.res['phoneNumber'] = this.form.value.phoneNumber;
				this.data.res['allDayLong'] = this.form.value.allDayLong;
				this.data.res['startingDate'] = AppSettings.formaterDatewithTime(this.form.value.date, !this.form.value.allDayLong ?
					this.form.value.time : '');
				await this.service.update(ApiUrl.Mission, this.data.res, this.data.res.id).toPromise();
				this.dialogRef.close(this.data.res);
			} else {
				const res = {
					phoneNumber: this.form.value.phoneNumber,
					missionKind: this.getMisionKind(),
					duration: this.form.value.dure,
					type: this.selectedStatus,
					title: this.form.value.title,
					object: this.form.value.object,
					status: this.selectedStatus,
					startingDate: AppSettings.formaterDatewithTime(this.form.value.date, !this.form.value.allDayLong ? this.form.value.time : ''),
					endingDate: null,
					clientId: this.form.value.clientId,
					technicianId: this.form.value.technicianId,
					allDayLong: this.form.value.allDayLong,
					additionalInfo: []
				}
				await this.service.create(ApiUrl.Mission + ACTION_API.create, res).toPromise();
				this.dialogRef.close(res);
			}
		}
	}

	getTime(date) {
		const res = date.split('T');
		this.time = {
			'hour': +res[1].split(':')[0],
			'minute': +res[1].split(':')[1],
		}
		return res[1];
	}

	setData(doc) {
		this.type = this.getActive(doc.missionKind);
		this.selectedStatus = doc.status;
		console.log(doc);
		this.form.patchValue({
			dure: doc.duration,
			title: doc.title,
			object: doc.object,
			allDayLong: doc.allDayLong,
			date: doc.startingDate,
			time: !doc.allDayLong ? this.getTime(doc.startingDate) : '',
			status: this.selectedStatus,
			phoneNumber: doc.phoneNumber,
			endingDate: null,
			clientId: doc.client && doc.client !== null ? doc.client.id : null,
			technicianId: doc.technician && doc.technician !== null ? doc.technician.id : null,
		});
		this.clientName = doc.client && doc.client !== null ? doc.client.reference + ' : ' + doc.client.name : '';
		this.techName = doc.technician && doc.technician !== null ? doc.technician.lastName + '  ' + doc.technician.firstName : '';
		if (this.readOnly) {
			this.form.controls['allDayLong'].disable()
		}

	}

	onTypeClicked(type: AgendaTypeFilter): void {
		this.type = type;
		this.createForm();
		if (this.type === AgendaTypeFilter.InterventionsChantier) {
			localStorage.setItem(LocalElements.docType, ApiUrl.FicheIntervention);
			this.router.navigate(['/ficheintervention/create']).finally(() => this.dialogRef.close());
		}

		if (this.type === AgendaTypeFilter.InterventionsMaintenance) {
			this.router.navigate(['/planification']).finally(() => this.dialogRef.close());
		}
	}

	//#endregion

	//#region Methods

	async getClients(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.clients = res.value;
				this.listClients = res.value;

				// if (this.data.res) {
				// 	this.getTelepone(this.data.res.client.id);
				// }

				resolve(res.value);
			}, (err: any) => reject(err));
		});
	}

	async getTechniciens(): Promise<void> {
		return new Promise((resolve, reject) => {
			this.service.getAllPagination(ApiUrl.User, {}).subscribe(res => {
				this.technichiens = res.value;
				this.listtechnichiens = res.value;
				resolve(res.value);
			}, (err: any) => reject(err));
		});
	}

	//#endregion
}
