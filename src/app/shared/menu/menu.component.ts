import { Component } from '@angular/core';
import { MenuService } from 'app/services/menu/menu.service';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

	//#region Properties

	interacted = false;

	//#endregion

	//#region Lifecycle

	constructor(
		public menu: MenuService
	) { }

	//#endregion

	//#region Events

	onToggle(): void {
		this.menu.opened = !this.menu.opened;
	}

	onChange(): void {
		this.interacted = true;
	}

	onGroupClicked(): void {
		this.menu.opened = true;
	}

	//#endregion
}
