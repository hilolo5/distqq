import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-notifications',
	templateUrl: './notifications.component.html',
	styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

	//#region Properties

	notifications = [];

	//#endregion

	//#region Lifecycle

	constructor() { }

	ngOnInit(): void {
		this.prepareNotifications();
	}

	//#endregion

	//#region Methods

	prepareNotifications(): void {
		this.notifications = [];
	}

	//#endregion
}
