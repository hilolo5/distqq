import { Routes } from '@angular/router';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

export const FIXED_NAVBAR_FOOTER_ROUTES: Routes = [

	// {
	// 	path: 'dashboard',
	// 	loadChildren: './pages/dashboard/dashboard-page.module#DashboardPagesModule'
	// },

	{
		path: 'dashboard',
		loadChildren: './pages/dashboardaxiobat/dashboardaxio-page.module#DashboardAxioPagesModule'
	},

	{
		path: 'utilisateurs',
		loadChildren: './pages/users/users.module#UtilisateurModule',
		data: {
			docType: ApiUrl.User
		}
	},
	{
		path: 'clients',
		loadChildren: './pages/clients/clients.module#ClientsModule',
		data: {
			docType: ApiUrl.Client
		}
	},
	{
		path: 'fournisseurs',
		loadChildren: './pages/fournisseur/fournisseur.module#FournisseurModule',
		data: {
			docType: ApiUrl.Fournisseur
		}
	},
	{
		path: 'devis',
		// loadChildren: './pages/devis/devis.module#DevisModule',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.Devis
		}
	},
	{
		path: 'produits',
		loadChildren: './pages/Produits/produit.module#ProduitModule'
	},
	{
		path: 'chantiers',
		loadChildren: './pages/chantier/chantier.module#ChantiersModule',
		data: {
			docType: ApiUrl.Chantier
		}
	},
	{
		path: 'ficheintervention',
		loadChildren: './pages/ficheintervention/ficheintervention.module#FicheinterventionModule',
		data: {
			docType: ApiUrl.FicheIntervention
		}
	},
	{
		path: 'factures',
		loadChildren: './pages/factures/factures.module#FacturesModule',
		data: {
			docType: ApiUrl.Invoice
		}
	},
	{
		path: 'paiements',
		loadChildren: './pages/paiements/paiements.module#PaiementsModule',
		data: {
			docType: ApiUrl.Payment
		}
	},
	{
		path: 'avoirs',
		loadChildren: './pages/avoirs/avoirs.module#AvoirsModule',
		data: {
			docType: ApiUrl.avoir
		}
	},
	{
		path: 'parameteres',
		loadChildren: './pages/parameteres/parameteres.module#ParameteresModule'
	},
	{
		path: 'lots',
		loadChildren: './pages/lots/lots.module#LotsModule',
		data: {
			docType: ApiUrl.Lot
		}
	},
	{
		path: 'groupes',
		loadChildren: './pages/Groupe/Groupe.module#GroupesModule',
		data: {
			docType: ApiUrl.Groupe
		}
	},
	{
		path: 'bonCommandeFournisseur',
		loadChildren: './pages/bonCommandeFournisseur/bonCommandeFournisseur.module#BonCommandeFournisseurModule',
		data: {
			docType: ApiUrl.BonCommandeF
		}
	},
	{
		path: 'depense',
		loadChildren: './pages/depense/depense.module#DepenseModule',
		data: {
			docType: ApiUrl.Depense
		}
	},
	{
		path: 'comptabilite',
		loadChildren: './pages/comptabilite/comptabilite.module#ComptabiliteModule'
	},
	{
		path: 'gammemaintenanceequipements',
		loadChildren: './pages/gamme-maintenance-equipement/gamme-maintenance-equipement.module#GammeMaintenanceEquipementModule',
		data: {
			docType: ApiUrl.GME
		}

	},
	{
		path: 'contratentretiens',
		loadChildren: './pages/contartEntretien/contratEntretien.module#ContratEntretienModule',
		data: {
			docType: ApiUrl.MaintenanceContrat
		}

	},
	{
		path: 'agendaglobal',
		loadChildren: './pages/agenda-global/agenda-global.module#AgendaGlobalModule'

	},
	{
		//C:\Users\Admin\Documents\Projet Axiobat\webadmin-axiobat-1\src\app\pages\tache\tache.module.ts

		path: 'tache',
		loadChildren: './pages/tache/tache.module#TacheModule'

	},

	{
		path: 'visitesMaintenance',
		loadChildren: './pages/visiteMaintenence/visiteMaintenance.module#VisiteMaintenanceModule',
		data: {
			docType: ApiUrl.VisitMaintenance
		}
	},

	{
		path: 'planification',
		loadChildren: './pages/planification/planification.module#PlanificationModule'

	},
	{
		path: 'ficheinterventionmaintenance',
		loadChildren: './pages/ficheinterventionmaintenance/ficheinterventionmaintenance.module#FicheinterventionmaintenanceModule',
		data: {
			docType: ApiUrl.MaintenanceOperationSheet
		}
	},
	{
		path: 'factureReccurente',
		loadChildren: './pages/factureReccurente/facture-reccurente.module#FactureReccurenteModule',
		data: {
			docType: ApiUrl.Recurring
		}
	}
];
