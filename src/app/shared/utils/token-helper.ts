import { JwtHelperService } from '@auth0/angular-jwt';

export class TokenHelper {
	public static TypeUser = null;
	public static getToken = () => localStorage.getItem('token');

	public static hasValidToken = () => typeof TokenHelper.getToken() === 'string' && TokenHelper.getToken().length > 0;

	public static decodeToken = () => new JwtHelperService().decodeToken(TokenHelper.getToken());

	public static getExpirationDate = () => new JwtHelperService().getTokenExpirationDate(TokenHelper.getToken());

	public static isExpired = () => new JwtHelperService().isTokenExpired(TokenHelper.getToken());

	public static getPackType = () => TokenHelper.decodeToken()['PackType'];

	/**
	 * This function will return the user information stored in token,
	 * if no user is logged in null will be returned
	 *
	 * @returns IUserToken | null
	 */
	public static GetUserToken(): IUserToken | null {
		const tokenData = this.decodeToken();
		return tokenData ? {
			id: tokenData[ClaimTypes.UserId],
			roleId: tokenData[ClaimTypes.RoleId],
			roleType: tokenData[ClaimTypes.RoleType],
			roleName: tokenData[ClaimTypes.RoleName],
			userName: tokenData[ClaimTypes.UserName],
			email: tokenData[ClaimTypes.UserEmail],
			permissions: JSON.parse(tokenData[ClaimTypes.UserPermissions]),
		} : null;
	}
}

/**
 * this interface describe the user information inside the token
 */
export interface IUserToken {
	id: string;
	roleId: number;
	roleType: string;
	roleName: string;
	userName: string;
	email: string;
	permissions: number[];
}


export enum ClaimTypes {
	UserId = 'userId',
	UserName = 'userName',
	UserEmail = 'userEmail',
	RoleId = 'roleId',
	RoleName = 'roleName',
	RoleType = 'roleType',
	UserPermissions = 'permissions',
	SecurityStamp = 'securityStamp'
}

