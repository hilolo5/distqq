import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FournisseurCardComponent } from './fournisseur-card.component';
import { MatIconModule, MatSelectModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [FournisseurCardComponent],
	imports: [
		CommonModule,
		MatIconModule,
		FormsModule,
		MatSelectModule
	],
	exports: [FournisseurCardComponent]
})
export class FournisseurCardModule { }
