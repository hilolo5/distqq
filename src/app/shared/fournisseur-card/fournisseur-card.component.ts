import { Component, Inject, Input, OnChanges, OnInit } from '@angular/core';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from '../repository/igeneric-repository';

@Component({
	selector: 'fournisseur-card',
	templateUrl: './fournisseur-card.component.html',
	styleUrls: ['./fournisseur-card.component.scss']
})
export class FournisseurCardComponent implements OnInit, OnChanges {

	//#region Properties
	edit = false;
	supplier;
	priceInput = 0;
	supplierEdit;
	priceInputEdit = 0;
	default = false;
	listFournisseurs = [];
	listChoixSupplie = [];
	supplierEditnow
	@Input() ListPrixParFournisseur;
	defaultEdit = false;
	@Input() readOnly = false;

	//#endregion

	//#region Lifecycle

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
	) { }

	ngOnInit(): void {
		this.getFournisseurList();
	}

	ngOnChanges() {
	}

	//#endregion

	//#region Events

	onDefault(): void {
		this.default = !this.default;
	}

	onDefaultEdit(fournisseur) {
		if (!this.readOnly) {
			const index = this.ListPrixParFournisseur.findIndex(x => x.supplierId === fournisseur.supplier.id);
			if (index >= 0) {
				this.ListPrixParFournisseur.map(x => x.isDefault = false);
				this.ListPrixParFournisseur[index].isDefault = !fournisseur.isDefault;
			}
		}
	}

	close() {
		this.priceInput = 0;
		this.supplier = {};
	}

	onDelete(supplier): void {
		this.ListPrixParFournisseur = this.ListPrixParFournisseur.filter(x => x.supplierId !== supplier.supplierId);
		this.listChoixSupplie.push(supplier);
	}

	onEdit(fournisseur): void {
		this.edit = true;
		this.priceInputEdit = fournisseur.price;
		this.listChoixSupplie.push(fournisseur.supplier);
		this.supplierEditnow = this.listChoixSupplie.find(x => x.id === fournisseur.supplier.id);
		this.supplierEdit = fournisseur.supplier;
	}

	onValidate(): void {
		if (this.supplier && this.supplier !== {} && this.supplier.id) {
			if (this.default) { this.ListPrixParFournisseur.map(x => x.isDefault = false); }
			this.ListPrixParFournisseur.push({
				supplier: this.supplier,
				supplierId: this.supplier.id,
				price: this.priceInput,
				isDefault: this.default
			});
			this.listChoixSupplie = this.listChoixSupplie.filter(x => x.id !== this.supplier.id);
			this.priceInput = 0;
			this.supplier = {};
			this.default = false;
		}

		if (this.supplierEditnow && this.supplierEditnow !== {} && this.supplierEditnow.id) {
			const index = this.ListPrixParFournisseur.findIndex(x => x.supplierId === this.supplierEdit.id);
			if (index >= 0) {
				this.ListPrixParFournisseur[index] = {
					supplier: this.supplierEditnow,
					supplierId: this.supplierEditnow.id,
					price: this.priceInputEdit,
					isDefault: this.supplierEdit.isDefault
				}
				this.listChoixSupplie = this.listChoixSupplie.filter(x => x.id !== this.supplierEditnow.id);
				this.priceInputEdit = 0;
				this.supplierEdit = {};
				this.supplierEditnow = {};
			}
		}
	}


	getFournisseurList() {
		this.service.getAll(ApiUrl.Fournisseur).subscribe(res => {
			this.listFournisseurs = res.value;
			this.listChoixSupplie = res.value;
			if (this.ListPrixParFournisseur && this.ListPrixParFournisseur.length >= 0) {
				this.ListPrixParFournisseur.map(element => {
					this.listChoixSupplie = this.listChoixSupplie.filter(x => x.id !== element.supplierId);
				});
			}
		});
	}

	//#endregion
}
