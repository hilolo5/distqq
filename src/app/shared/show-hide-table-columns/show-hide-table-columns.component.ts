import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'table-show-hide-table-columns',
	templateUrl: './show-hide-table-columns.component.html',
	styleUrls: ['./show-hide-table-columns.component.scss']
})
export class TableShowHideTableColumnsComponent {

	@Output()
	selectColumn = new EventEmitter();

	@Input()
	columns: never[];

	@Input()
	tableName: string;

	@Input()
	set checkedColumns(value: boolean[]) {
		this._checkedColumns = value;
		setTimeout(() => {
			if (this._checkedColumns && this._checkedColumns.length === this.columns.length) {
				for (let index = 0; index < this.columns.length; index++) {
					this.ColumnSelected(index, this._checkedColumns[index] == null ? false : this._checkedColumns[index]);
					this.showAll = this._checkedColumns[index] == null ? false : true;
				}
			}
		}, 600);
	}

	showAll = true;
	_checkedColumns: boolean[] = [];

	ColumnSelected(itemIndex: number, operation: boolean, notifyParent = false) {

		// extract the same length of number column
		// from checked column stored in local storage
		this._checkedColumns = this._checkedColumns.slice(0, this.columns.length);

		// get table by id
		const table = document.getElementById(this.tableName);

		// get th of current column
		const th = table.getElementsByTagName('th')[itemIndex];

		// display or hidden header
		this._checkedColumns[itemIndex] = operation;
		th.style.display = operation ? 'none' : '';

		// get rows of table
		const rows = table.getElementsByTagName('tr');

		for (let index = 1; index < rows.length; index++) {

			const rowItem = table.getElementsByTagName('tr')[index];
			if (rowItem === undefined) { return; }

			const cells = rowItem.getElementsByTagName('td')[itemIndex];
			if (cells !== undefined) {
				cells.hidden = operation;
			}
		}

		// if notify parent is true emit action to parent component
		if (notifyParent) { this.setCheckedColumn(); }
	}

	/** operation show and hide columns */
	operations() {
		if (this.showAll) {
			for (let index = 0; index < this.columns.length; index++) {
				this.ColumnSelected(index, true, true);
			}
			this.showAll = !this.showAll;
			this.setCheckedColumn();
		} else {
			for (let index = 0; index < this.columns.length; index++) {
				this.ColumnSelected(index, false, false);
			}
			this.showAll = !this.showAll;
			this.setCheckedColumn();
		}

	}

	setCheckedColumn() {
		this.selectColumn.emit(this._checkedColumns.slice(0, this.columns.length));
	}

}
