import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { LocalElements } from '../utils/local-elements';

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(private router: Router) { }

	canActivate() {
		if (!this.isAuthenticated()) {
			this.router.navigate(['/login']);
			return false;
		}
		return true;
	}

	isAuthenticated() {
		return localStorage.getItem(LocalElements.TOKEN) != null ? true : false;
	}
}
