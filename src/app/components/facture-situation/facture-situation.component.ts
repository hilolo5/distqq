import { Component, Input, OnInit, OnChanges, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { ParametrageContenuDevis } from 'app/Models/Entities/Parametres/Numerotation';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { Quote, QuoteSituations } from 'app/Models/Entities/Documents/Quote';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ColumnType } from '../form-data/data-table/data-table.component';
import { StatusManagement } from 'app/libraries/document-helper';
import { ManageDataTable, DataTableRowActions } from 'app/libraries/manage-data-table';
import { SortDirection } from 'app/Models/Model/filter-option';
import { HeaderService } from 'app/services/header/header.service';

declare var toastr: any;
@Component({
	selector: 'app-facture-situtaion',
	templateUrl: './facture-situation.component.html',
	styleUrls: ['./facture-situation.component.scss'],
	providers: []
})
export class FactureSitutaion implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('devis') devisInfos: Quote;
	// acomptes: Acompte[] = [];
	factureTableColumns: any[];
	statutFacture: typeof StatutFacture = StatutFacture;
	statutDevis: typeof StatutDevis = StatutDevis;
	showBtnAdd = true;
	showBtnAddCloture = true;
	resteAPayer = 0;
	totalPourcentage = 0;
	listSituation: QuoteSituations[] = [];
	typeFacture: typeof TypeFacture = TypeFacture;

	/**
 * @description la fonction principale pour sauvegarder la nouvelle facture
 * @param statut statut du facture qui on veut sauvegarder (cloture/brouillon/en cours)
 */
	processing = false;
	prestations: any;
	emitter: any = {};
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	columnType: typeof ColumnType = ColumnType;
	sortDirection: typeof SortDirection = SortDirection;
	actions: DataTableRowActions[];
	selected: any;
	docType = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private header: HeaderService
	) { }

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);


		this.factureTableColumns = [
			{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
			{
				name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
					value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
					appear: (doc) => true
				}, appear: true
			},
			{ name: 'purpose', nameTranslate: 'LABELS.OBJECT', isOrder: true, type: ColumnType.any, appear: true },

			{
				name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
				appear: true
			},
			{
				name: 'dueDate', nameTranslate: 'LABELS.DATE_ECHEANCE', isOrder: true, type: ColumnType.Date,
				appear: true
			},
			// {
			// 	name: 'situation', nameTranslate: 'labels.pourcentage', isOrder: false, type: ColumnType.Progressbar,
			// 	appear: true
			// },
			{ name: 'totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
			// {
			// 	name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, list: [
			// 		{ name: 'restePayer', nameTranslate: 'LABELS.RESTEPAYER', isOrder: false, type: ColumnType.Currency }], appear: true
			// },
		];
	}
	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.factureTableColumns.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}

	onMenuOpened(element: any): void {
		this.selected = element;
		this.actions = this.getActions(this.selected);
	}

	public getActions(detail: boolean): DataTableRowActions[] {

		return [
			{
				id: 1,
				icon: './assets/app/images/Icons/eye.svg',
				label: 'LABELS.SHOW',
				default: true,
				color: 'accent',
				action: (element: any) => {
					this.navigateToDetailFacture(element.invoiceId);
				},
				appear: () => true
			}
		];


	}
	ngOnChanges() {

		if (this.devisInfos && this.devisInfos.situations.length === 0) {
			this.resteAPayer = this.devisInfos.orderDetails.totalTTC;
		}
		const condition = this.devisInfos === undefined ? false : (this.devisInfos.situations.length === 0 ? false : true);

		if (condition) {

			//	const condi = (): boolean => {
			//	let showOrNot = true;
			let restePayer = this.devisInfos.orderDetails.totalTTC;
			this.totalPourcentage = this.devisInfos.situations.filter(z => z.typeInvoice === TypeFacture.Situation).reduce((x, y) => x + y.situation, 0);
			this.listSituation = this.devisInfos.situations.filter(e => e.typeInvoice === TypeFacture.Acompte || e.typeInvoice === TypeFacture.Situation ||
				e.typeInvoice === TypeFacture.Cloturer);
			const factureCloture = this.devisInfos.situations.filter(e =>
				e.typeInvoice === TypeFacture.Cloturer);
			const factureGeneral = this.devisInfos.situations.filter(e =>
				e.typeInvoice === TypeFacture.None);
			if (this.devisInfos.status === this.statutDevis.Facture && this.devisInfos.situations.length === 0
				&& this.devisInfos.associatedDocuments.find(d => d.typeInvoice === TypeFacture.None)) {
				//	showOrNot = false;
			}
			this.devisInfos.situations.forEach(facture => {
				// if (facture.status === this.statutFacture.Cloture || facture.typeInvoice === TypeFacture.None) {
				// 	showOrNot = false;
				// 	this.showBtnAddCloture = false;
				// }
				if (facture.typeInvoice === TypeFacture.Cloturer) {
					restePayer = 0;
				} else {
					restePayer -= facture.totalTTC;
				}
				facture['restePayer'] = restePayer;
			});
			if (factureGeneral.length !== 0 || factureCloture.length !== 0) {
				this.resteAPayer = 0;
				//	showOrNot = false;
			} else {
				this.resteAPayer = + this.devisInfos.orderDetails.totalTTC - + this.devisInfos.situations.reduce((x, y) => x + y.totalTTC, 0);
				//	showOrNot = this.resteAPayer <= 0.2 ? false : true;
			}
			//	return showOrNot;
			//	};
			//	this.showBtnAdd = condi() && this.totalPourcentage < 90;


			// this.devisInfos.facture = this.devisInfos.facture.map((facture, index) => {
			// TODO Facture
			// if (facture.typeInvoice == TypeFacture.Situation) {
			// 	const situations = JSON.parse(this.devisInfos.situation) as { idFacture: number, pourcentage: number, resteAPayer: number }[];
			// 	const situationFActureIndex: number = situations.findIndex(X => X.idFacture == facture.id);
			// 	const { pourcentage, resteAPayer } = situations[situationFActureIndex];
			// 	facture.pourcentage = pourcentage;
			// 	facture.restePayer = resteAPayer;
			// 	const length = this.devisInfos.facture.length - 1;
			// 	if (index == length) {
			// 		if (facture.status == this.statutFacture.Cloture) {
			// 			this.resteAPayer = this.devisInfos.total - this.devisInfos.total * 0.95;
			// 		} else {
			// 			this.resteAPayer = resteAPayer == 0 ? this.devisInfos.total : resteAPayer;

			// 		}
			// 	}
			// }
			// return facture;
			// });
		}

	}

	navigateTo() {

		this.route.params.subscribe(params => {
			const url = params['idChantier'] === undefined ? `devis/factureSituation/${this.devisInfos.id}` :
				`/chantiers/${params['idChantier']}/documents/3/${params['id']}/factureSituation`
				;
			this.router.navigate([url]);
		});
	}
	navigateToCloture() {
		this.route.params.subscribe(params => {
			const url = params['idChantier'] === undefined ? `devis/factureSituationCloture/${this.devisInfos.id}` :
				`/chantiers/${params['idChantier']}/documents/3/${params['id']}/factureSituationCloture`
				;
			this.router.navigate([url]);
		});
	}
	navigateToDetailFacture(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		this.route.params.subscribe(params => {
			//    let url = this.CallFromOutside ? `/chantiers/${this.idChantier}/documents/devis/detail/${idDevis}` : `/devis/detail/${idDevis}`;
			const url = params['idChantier'] === undefined ? `/devis/${this.devisInfos.id}/factures/detail/${id}` :
				`/chantiers/${params['idChantier']}/documents/3/${this.devisInfos.id}/factures/detail/${id}`;

			this.router.navigate([url]);

		});
	}

	async addCloture() {
		const statut = StatutFacture.Cloture;
		// afficher l'animation de chargement des données
		this.processing = true;
		// vérifier si tous les champs obligatoir est remplie
		// if (!this.checkFormIsValid(statut)) {
		//   //récupère la traduction des messages d'erreurs
		//   const translatation = await this.getTranslateByKey('errors');
		//   //afficher un message d'erreur
		//   toastr.warning(translatation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
		//   //cacher l'animation de chargement des données
		//   this.processing = false;
		//   //sortir de la fonction
		//   return;
		// }
		// création le corp de la requête du creation
		this.prestations = await this.getDataFromArticlesComponet(StatutFacture.Cloture);
		if (this.prestations.totalHt <= 0) {
			const translatation = await this.getTranslateByKey('errors');
			toastr.warning(translatation.factureSituationTotalHt, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			this.processing = false;
			return;
		}
		const facturecreationBody = await this.createBodyRequest(statut);
		// send request to the server
		// this.factureService.CreateFactureSituation(facturecreationBody).subscribe(
		// 	async res => {
		// 		if (res) {
		// 			//incrémenter la référence
		// 			this.IncremenetRefernce();
		// 			//récupère la traduction des messages d'ajout
		// 			const translatation = await this.getTranslateByKey('addFacture');
		// 			//affcihe un message du succès
		// 			toastr.success(translatation.msg, translatation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 			//redirigé au detail de la nouvelle insertion
		// 			//   this.navigateToDevisDetail();

		// 		} else {
		// 			//récupère la traduction des messages d'erreurs
		// 			const translatation = await this.getTranslateByKey('errors');
		// 			toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
		// 		}
		// 	},
		// 	async err => {
		// 		//récupère la traduction des messages d'erreurs
		// 		const translatation = await this.getTranslateByKey('errors');
		// 		toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
		// 	}, () => {
		// 		//cacher l'animation de chargement des données
		// 		this.processing = false;
		// 	});
	}

	generateReference(): Promise<string> {
		return new Promise((reslove, reject) => {
			// this.paramteresService.Generate(this.typeNumerotation.facture as number).subscribe(
			// 	res => {
			// 		reslove(res['data'] as string);
			// 	},
			// 	err => {
			// 		(err);
			// 	}
			// );
		});
	}
	IncremenetRefernce() {
		// this.paramteresService.Increment(this.typeNumerotation.facture as number).subscribe(res => { });
	}

	/*---------------------------------------------------------------*/
	/**
	 * @section  fonctions d'aide
	 */
	/*---------------------------------------------------------------*/
	getTranslateByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translatation => {
				resolve(translatation);
			});
		});
	}
	getDataFromArticlesComponet(status: StatutFacture,): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave(status, res => {
				resolve(res);
			});
		});
	}

	getStatutTosave(status: StatutFacture, resteAPayer): StatutFacture {
		// Brouillon Cloture Encours
		if (status === this.statutFacture.Encours) {
			if (resteAPayer === 0) {
				return this.statutFacture.Cloture;
			} else {
				return this.statutFacture.Encours;
			}
		} else if (status === this.statutFacture.Cloture || status === this.statutFacture.Brouillon) {
			return status;
		}
	}
	/**
	* @description return the defaults values from the configuration
	*/
	getDefaultsValues(): Promise<ParametrageContenuDevis> {
		return new Promise((resolve, reject) => {
			// this.paramteresService.Get(TypeParametrage.parametrageDevis)
			// 	.subscribe((response: Parametrage) => {
			// 		resolve(response != null ? (JSON.parse(response.contenu) as ParametrageContenuDevis) : new ParametrageContenuDevis());
			// 	}, error => reject(error));
		});
	}

	async createBodyRequest(status: StatutFacture): Promise<any> {

		// const defaultsValues: ParametrageContenuDevis = await this.getDefaultsValues();
		// if (defaultsValues.validite_facture) {
		//     var dateToday = new Date();
		//     dateToday.setDate(dateToday.getDate() + parseInt(defaultsValues.validite_facture.toString()));

		// }
		// //const formValue = this.creationForm.value;
		const facture: Invoice = new Invoice();
		// let reference = await this.generateReference();
		// facture.reference = reference;
		// facture.dateCreation = new Date();
		// facture.idChantier = this.devisInfos.chantier.id;
		// facture.status = this.getStatutTosave(status, this.prestations.resteAPayerTTC);
		// facture.typeInvoice = TypeFacture.Situation;

		// facture.emails = '[]';
		// facture.memos = '[]';
		// //facture.dateCreation = formValue.dateCreation;
		// facture.dateEcheance = dateToday;
		// facture.purpose = defaultsValues['objet_facture'];
		// facture.note = defaultsValues['note_facture'];
		// facture.conditionRegelement = defaultsValues['conditions_facture'];
		// const pourcentage = 95;
		// this.prestations.prestations.description = 'Situation  d'avencement' + pourcentage + ' % sur le devis ' + this.devisInfos.reference;
		// facture.orderDetails.lineItems = JSON.stringify(this.prestations.prestations);
		// facture.orderDetails.globalDiscount.value = 0;
		// facture.orderDetails.globalDiscount.type = TypeValue.Amount;
		// facture.orderDetails.holdbackDetails.holdback = this.prestations.retenueGarantie;
		// facture.orderDetails.holdbackDetails.warrantyPeriod = this.prestations.delaiGarantie;
		// facture.statusGarantie = StatutRetenueGarantie.encours;
		// facture.orderDetails.proportion = this.prestations.prorata;
		// facture.orderDetails.puc = this.prestations.puc;
		// facture.idDevis = this.devisInfos.id


		// const client: Client = this.devisInfos.chantier.client;
		// facture.idClient = client.id;
		// let infoClient: infoClientModel = new infoClientModel();
		// infoClient.code = client.code;
		// infoClient.nom = client.firstName;
		// infoClient.adresseFacturation = client.addresses.filter(X => X.isDefault == true)[0];
		// facture.infoClient = JSON.stringify(infoClient);

		// facture.searchTerms = reference + ' ' + this.prestations.totalTtc + ' ' + this.prestations.totalHt + ' ' + this.devisInfos.chantier.client.firstName;;

		// /*    if (status == this.statutFacture.Cloture) {
		//       facture.totalHt = this.prestations.situationCumulleeHT;
		//       facture.orderDetails.totalTTC = this.prestations.situationCumulleeTTC;
		//       facture.tvaGlobal = ((this.prestations.situationCumulleeTTC - this.prestations.situationCumulleeHT) / this.prestations.situationCumulleeHT) * 100;
		//     } else {*/
		// // const totalHt = this.calcTotalHtToSave(status, this.prestations.totalHt);
		// // facture.totalHt = totalHt;
		// facture.totalHt = this.prestations.totalHt;
		// // facture.orderDetails.totalTTC = this.calcTotalTtcToSave(status, this.prestations.totalTtc, totalHt, this.prestations.tva);
		// facture.orderDetails.totalTTC = this.prestations.totalTtc;
		// facture.tvaGlobal = null;
		// facture.tva = JSON.stringify([{ 'tva': this.prestations.tva, 'totalHT': facture.totalHt, 'totalTVA': facture.orderDetails.totalTTC - facture.totalHt, 'totalTTC': facture.orderDetails.totalTTC }]);

		// //}
		// facture.transmise = StatutTransmisefacture.notmarque;
		return {
			idDevis: this.devisInfos.id,
			// situations: this.createFacturesSituationInfos(status, pourcentage), // TODO Facture
			situations: this.createFacturesSituationInfos(status, 100),
			facture: facture
		}
	}
	createFacturesSituationInfos(status: StatutFacture, pourcentage: any) {
		throw new Error('Method not implemented.');
	}
}
