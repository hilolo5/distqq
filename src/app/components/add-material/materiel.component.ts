import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'materiel',
	templateUrl: './materiel.component.html',
	styleUrls: ['./materiel.component.scss']
})
export class MaterielComponent implements OnInit {

	title: string;
	public form: FormGroup;
	show = true;

	constructor(
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<MaterielComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
		this.show = this.data.show;
		if (this.data.selected) {
			this.setData();
			this.title = 'Modifier';
		} else {
			this.title = 'Ajouter'
		}
	}

	close() { this.dialogRef.close() }

	submit() {
		this.form.value.id = this.data.selected && this.data.selected.id ? this.data.selected.id : Math.random().toString(36).substr(2, 3);
		this.dialogRef.close(this.form.value)
	}

	createForm(): void {
		this.form = this.fb.group({
			id: [''],
			serialNumber: [''],
			model: [''],
			brand: [''],
			designation: [''],
		});
	}

	setData() {
		this.form.patchValue({
			serialNumber: this.data.selected.serialNumber,
			model: this.data.selected.model,
			brand: this.data.selected.brand,
			designation: this.data.selected.designation
		});
	}

	get f() { return this.form.controls; }
}
