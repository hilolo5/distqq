import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { User } from '../../Models/Entities/User';
import { Client } from '../../Models/Entities/Contacts/Client';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TypeClientUser } from '../../Enums/TypeClientUser.Enum';
declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'client-Auth',
	templateUrl: './clientAuth.component.html',
	styleUrls: ['./clientAuth.component.scss']
})
export class ClientAuthComponent implements OnInit, OnChanges {


	// tslint:disable-next-line: no-input-rename
	// @Input('client') public client: Client;
	// tslint:disable-next-line: no-output-rename
	@Output('userClientIsActif') public userClientIsActif = new EventEmitter();
	typeClientUser: typeof TypeClientUser = TypeClientUser;
	public insertOrUpdate = false;
	public form: FormGroup;
	public defaultUsername = '';
	public userClient: User;
	public loading = false;
	processing = false;

	typeClient = null;
	statutDiasplayClient = false;
	clientChantier = false;
	clientMaintenance = false;
	clientChantierMaintenance = false;

	constructor(
		private fb: FormBuilder,
		// private utilisateurService: UtilisateurService,
		// private clientService: ClientService,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<ClientAuthComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any

	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm(false, '', '', 1);
	}

	ngOnChanges() {
		// if (this.client !== undefined) {
		//   this.getUserClientInfos(this.client.id);
		// }
	}


	changedispalyToClient(value) {
		this.typeClient = value;
		if (value === 1) {
			this.clientChantierMaintenance = true;
		} else {
			this.clientChantierMaintenance = false;
		}
		if (value === 2) {
			this.clientChantier = true
		} else {
			this.clientChantier = false;
		}
		if (value === 3) {
			this.clientMaintenance = true
		} else {
			this.clientMaintenance = false;

		}

	}

	/**  créer un formulaire initialiser a partir du base de donnée  */
	createForm(actifV: boolean, usernameV: string, passwordV: string, typeClient: number): void {
		this.form = this.fb.group({
			actif: [actifV],
			username: [usernameV, [Validators.minLength(4), Validators.required], this.CheckUniqueUserName.bind(this)],
			password: [passwordV, [Validators.minLength(6), Validators.required]],
			typeClient: [typeClient, [Validators.required]]
		});
	}

	CheckUniqueUserName(control: FormControl) {
		/* try {
			// tslint:disable-next-line: triple-equals
			if (control.value != '' && control.value != this.defaultUsername) {
				const promise = new Promise((resolve, reject) => {
					this.utilisateurService
						.CheckUniqueUserName(control.value)
						.subscribe(res => {
							if (res === true) {
								resolve({ CheckUniqueUserName: true });
							} else {
								resolve(null);
							}
						});
				});
				return promise;
			} else {
				return new Promise((resolve, reject) => { resolve(null) })
			}
		} catch (err) {
			console.log(err);
		} */
	}

	getUserClientInfos(clientId) {
		/* this.loading = true;
		this.utilisateurService.getUserClientInfos(clientId).subscribe((Response: User) => {
			this.loading = false;
			if (Response == null) {
				this.userClientIsActif.emit(false);
				this.insertOrUpdate = true;
				return;
			}
			this.insertOrUpdate = false;
			this.userClient = Response;
			this.defaultUsername = Response.userName;
			const isActif = Response.statut;
			this.userClientIsActif.emit(isActif);
			this.form.controls.actif.setValue(isActif);
			this.form.controls.username.setValue(Response.userName);
			this.form.controls.typeClient.setValue(Response.role.type);
			this.changedispalyToClient(Response.role.type);
		}, error => {
			this.loading = false;
			console.log(error);
		}) */
	}

	saveInfos() {
		if (this.form.invalid) {
			return;
		}
		// if (this.insertOrUpdate) {
		//   this.addUser();
		// }
		this.addUser();
		// else {
		//   this.updateUser(this.userClient.id);
		// }
	}

	addUser() {
		this.loading = true;
		/* this.utilisateurService.Add(this.createBodyRequest()).subscribe(res => {
			this.translate.get('update').subscribe(text => {
				swal(text.msg, '', 'success');
				this.insertOrUpdate = false;
				this.userClient = res;
				this.defaultUsername = res.username;
				const isActif = res.actif === 0 ? false : true;
				this.userClientIsActif.emit(isActif);
				this.form.controls.actif.setValue(isActif);
				this.form.controls.username.setValue(res.username);
				this.form.controls.typeClient.setValue(res.typeClient);
				this.changedispalyToClient(res.typeClient);
			
				this.loading = false;
			})
			
		}, err => {
			this.translate.get('delete').subscribe(text => {
				swal(text.error, '', 'error');
				this.dialogRef.close('');
				this.loading = false;
			})
		}); */

		this.dialogRef.close('');
	}

	updateUser(idUser: number) {
		/* this.loading = true;
		this.utilisateurService.Update(this.createBodyRequest(idUser)).subscribe(res => {
			this.translate.get('update').subscribe(text => {
				swal(text.msg, '', 'success');
				this.loading = false;
			})
		}, err => {
			this.translate.get('delete').subscribe(text => {
				swal(text.error, '', 'error');
				this.loading = false;
			})
		}); */
	}

	createBodyRequest(id?: number) {
		const date = new Date();
		const user = {
			id: (id == null ? 0 : id),
			username: this.form.value.username,
			password: this.form.value.password,
			actif: this.form.value.actif ? 1 : 0,
			nom: this.data.nom,
			email: this.data.email,
			prenom: '',
			codecomptable: this.data.codeComptable,
			accessfailedcount: 0,
			phonenumber: this.data.telephone,
			userProfile: [{
				Idprofile: UserProfile.client,
				IdprofileNavigation: null,
				Iduser: 0,
				IduserNavigation: null
			}],
			historique: '[]',
			dernierecon: null,
			matricule: '',
			joinDate: date,
			idClient: this.data.id,
			idProfile: UserProfile.client,
			typeClient: this.typeClient,
		};
		return user;
	}
	get f() { return this.form.controls; }
}
