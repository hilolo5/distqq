import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AddFactureClotureComponent } from './add-facture-cloture.component';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { Routes, RouterModule } from '@angular/router';
import { ShowCurrencyPipe } from 'app/common/Pipes/show-currency.pipe';
import { CommonModules } from 'app/common/common.module';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { FullCalendarModule } from '@fullcalendar/angular';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { SliderModule } from 'app/common/slider/slider.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';


// export function createTranslateLoader(http: HttpClient) {
// 	return new TranslateHttpLoader(http, './assets/i18n/chantier/', '.json');
// }
export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },
	]);
}

const routes: Routes = [
	{ path: '', component: AddFactureClotureComponent, pathMatch: 'full' }
];

@NgModule({
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		NgSelectModule,
		// TranslateModule.forChild({
		// 	loader: {
		// 		provide: TranslateLoader,
		// 		useFactory: createTranslateLoader,
		// 		deps: [HttpClient]
		// 	},
		// 	isolate: true
		// }),
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		CommonModules,
		InputTextareaModule,
		CalendarModule,
		FullCalendarModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatCheckboxModule,
		TableArticleModule,
		MatAutocompleteModule,
		MatSelectModule,
		ReactiveFormsModule,
		DataTablesModule,
		TableArticleModule,
		SliderModule,

	],
	exports: [
		AddFactureClotureComponent,

	],
	declarations: [AddFactureClotureComponent,
	],
	entryComponents: []
})
export class AddFactureClotureModule { }

