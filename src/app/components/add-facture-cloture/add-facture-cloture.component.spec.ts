import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFactureClotureComponent } from './add-facture-cloture.component';

describe('AddFactureClotureComponent', () => {
  let component: AddFactureClotureComponent;
  let fixture: ComponentFixture<AddFactureClotureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFactureClotureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFactureClotureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
