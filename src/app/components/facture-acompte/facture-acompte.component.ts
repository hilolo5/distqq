import { Component, Input, OnInit, OnChanges, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { QuoteSituations, Quote } from 'app/Models/Entities/Documents/Quote';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ColumnType } from '../form-data/data-table/data-table.component';
import { StatusManagement } from 'app/libraries/document-helper';
import { ManageDataTable, DataTableRowActions } from 'app/libraries/manage-data-table';
import { SortDirection, IFilterOption } from 'app/Models/Model/filter-option';
import { AppSettings } from 'app/app-settings/app-settings';
import { StringHelper } from 'app/libraries/string';
import { HeaderService } from 'app/services/header/header.service';

declare var swal: any;
declare var toastr: any;
@Component({
	selector: 'app-facture-acompte',
	templateUrl: './facture-acompte.component.html',
	styleUrls: ['./facture-acompte.component.scss'],
	providers: []
})
export class FactureAcompte implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('devis') devisInfos: Quote;
	factureTableColumns: any[];
	statutFacture: typeof StatutFacture = StatutFacture;
	statutDevis: typeof StatutDevis = StatutDevis;
	typeFacture: typeof TypeFacture = TypeFacture;
	showBtnAdd = true;
	resteAPayer = 0;
	totalPourcentage = 0;
	listAcompte: QuoteSituations[] = [];
	columnType: typeof ColumnType = ColumnType;
	sortDirection: typeof SortDirection = SortDirection;
	actions: DataTableRowActions[];
	selected: any;
	docType = '';

	constructor(
		private router: Router,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private header: HeaderService) { }

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);

		this.factureTableColumns = [
			{ name: 'reference', nameTranslate: 'LABELS.REFERENCE', isOrder: true, type: ColumnType.any, appear: true },
			{
				name: 'status', nameTranslate: 'LABELS.STATUS', isOrder: false, type: ColumnType.Status, status: {
					value: (doc) => StatusManagement.controlStatus(doc, ManageDataTable.docType),
					appear: (doc) => true
				}, appear: true
			},
			{
				name: 'creationDate', nameTranslate: 'LABELS.CREATION_DATE', isOrder: true, type: ColumnType.Date,
				appear: true
			},
			{
				name: 'dueDate', nameTranslate: 'LABELS.DATE_ECHEANCE', isOrder: true, type: ColumnType.Date,
				appear: true
			},
			{
				name: 'situation', nameTranslate: 'labels.pourcentage', isOrder: false, type: ColumnType.Progressbar,
				appear: true
			},
			{ name: 'totalHT', nameTranslate: 'LABELS.TOTAL_HT', isOrder: true, type: ColumnType.Currency, appear: true },
			{
				name: 'totalTTC', nameTranslate: 'LABELS.TOTAL_TTC', isOrder: true, type: ColumnType.Currency, list: [
					{ name: 'restePayer', nameTranslate: 'LABELS.RESTEPAYER', isOrder: false, type: ColumnType.Currency }], appear: true
			},
		];
	}
	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.factureTableColumns.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}
	onMenuOpened(element: any): void {
		this.selected = element;
		this.actions = this.getActions(this.selected);
	}

	public getActions(detail: boolean): DataTableRowActions[] {
		return [
			{
				id: 1,
				icon: './assets/app/images/Icons/eye.svg',
				label: 'LABELS.SHOW',
				default: true,
				color: 'accent',
				action: (element: any) => {
					this.navigateToDetailFacture(element.invoiceId);
				},
				appear: () => true
			}
		];
	}

	async ngOnChanges() {
		const condition = this.devisInfos === undefined ? false : true;
		if (condition) {
			const condi = async (): Promise<boolean> => {
				let showOrNot = true;
				let restePayer = this.devisInfos.orderDetails.totalTTC;
				this.listAcompte = this.devisInfos.situations.filter(e => e.typeInvoice === TypeFacture.Acompte);
				this.totalPourcentage = this.listAcompte.reduce((x, y) => x + y.situation, 0);
				if (this.devisInfos.status === this.statutDevis.Facture && this.devisInfos.situations.length === 0
					&& this.devisInfos.associatedDocuments.find(d => d.typeInvoice === TypeFacture.None)) {
					showOrNot = false;
				}
				for (const facture of this.devisInfos.situations) {
					if (facture.typeInvoice === TypeFacture.Cloturer) {
						showOrNot = false;
					}
					if (facture.typeInvoice === TypeFacture.Situation) {
						if (facture.status !== this.statutFacture.Brouillon && facture.status !== this.statutFacture.Annule) {
							showOrNot = false;
						}
					} else if (facture.typeInvoice === TypeFacture.Acompte) {
						if (facture.status === this.statutFacture.Cloture) {
							showOrNot = false;
						}
					} else if (facture.typeInvoice === TypeFacture.None) {
						showOrNot = false;
					}
					restePayer -= facture.totalTTC;
					if (this.listAcompte.length > 0) {
						const factureAcompte = this.listAcompte.find(f => f.invoiceId === facture.invoiceId);
						if (factureAcompte !== undefined) {
							factureAcompte.restePayer = restePayer;
						}
					}
				}
				this.resteAPayer = restePayer;

				return showOrNot;
			};

			this.showBtnAdd = await condi() && this.totalPourcentage < 90;
		}
	}

	navigateTo() {
		this.route.params.subscribe(params => {
			const url = params['idChantier'] === undefined ?
				`devis/factureAcompte/${this.devisInfos.id}` : `/chantiers/${params['idChantier']}/documents/3/${params['id']}/factureAcompte`
				;
			this.router.navigate([url]);
		});
	}

	HasNoBills(): boolean {
		const condition = this.devisInfos === undefined ? false : (this.devisInfos.situations.length === 0 ? false : true);
		if (condition) {
			return this.devisInfos.situations.filter(x => x.typeInvoice === TypeFacture.Acompte).length === 0;
		}
	}

	navigateToDetailFacture(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		this.route.params.subscribe(params => {
			const url = params['idChantier'] === undefined ?
				`/devis/${this.devisInfos.id}/factures/detail/${id}` : `/chantiers/${params['idChantier']}/documents/3/${this.devisInfos.id}/factures/detail/${id}`;
			this.router.navigate([url]);

		});
	}
}
