import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { HttpClient } from '@angular/common/http';
import { IndexListGlobalComponent } from './index.component';
import { MatDialogModule, MatTableModule, MatPaginatorModule, MatSortModule, MatTooltipModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatMenuModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE, MatCheckboxModule } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CommonModules } from 'app/common/common.module';
import { TableShowHideTableColumnsComponent } from 'app/shared/show-hide-table-columns/show-hide-table-columns.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { FacturationGroupeFTComponent } from 'app/components/facturation-groupe-ft/facturation-groupe-ft.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { exportFacturesComponent } from 'app/components/exportFactures/export-factures';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { DepenseGroupeBCFComponent } from 'app/components/depense-groupe-bcf/depense-groupe-bcf.component';
import { PaiementGroupeComponent } from 'app/pages/paiements/index/paiement-groupe/paiement-groupe.component';
import { MouvementCompteComponent } from 'app/pages/paiements/index/mouvement-compte/mouvement-compte.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { DataTableComponent } from 'app/components/form-data/data-table/data-table.component';
import { SelectFactureComponent } from 'app/pages/paiements/index/paiement-groupe/select-facture/select-facture.component';

const routes: Routes = [
	{ path: '', component: IndexListGlobalComponent, pathMatch: 'full' }
];


export function HttpLoaderFactory(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/factures/', suffix: '.json' },
		{ prefix: './assets/i18n/clients/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/depenses/', suffix: '.json' },
		{ prefix: './assets/i18n/paiements/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' }
	]);
}

@NgModule({
	declarations: [
		IndexListGlobalComponent,
		DataTableComponent,
		DepenseGroupeBCFComponent,
		exportFacturesComponent,
		TableShowHideTableColumnsComponent,
		PaiementGroupeComponent,
		MouvementCompteComponent,
		FacturationGroupeFTComponent,
		SelectFactureComponent],
	exports: [DataTableComponent, TableShowHideTableColumnsComponent,
		IndexListGlobalComponent, PaiementGroupeComponent, SelectFactureComponent
	],
	imports: [
		CommonModule,
		CommonModules,
		ReactiveFormsModule,
		FormsModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			isolate: true
		}),
		RouterModule.forChild(routes),
		MatDialogModule,
		MatTableModule,
		OverlayModule,
		MatSortModule,
		NgSelectModule,
		CalendarModule,
		MatCheckboxModule,
		NgbTooltipModule,
		MatPaginatorModule,
		InfiniteScrollModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatMenuModule,
		ShowHidePasswordModule,
		MatDatepickerModule,
		MatNativeDateModule,

	],
	entryComponents: [IndexListGlobalComponent, FacturationGroupeFTComponent,
		exportFacturesComponent, DepenseGroupeBCFComponent, PaiementGroupeComponent, MouvementCompteComponent,
		SelectFactureComponent],
	providers: [
		{ provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
	],
})
export class ListIndexModule { }
