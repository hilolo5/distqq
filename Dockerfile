# stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

# stage 2
##FROM nginx:alpine

## Copy our  nginx config
##COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf

## Remove default nginx website
##RUN rm -rf /usr/share/nginx/html/*

##COPY --from=node /app/dist /usr/share/nginx/html

# stage 2
FROM httpd:alpine



COPY --from=node /app/dist /usr/local/apache2/htdocs/
RUN sed -i -e 's/^#\(LoadModule .*mod_rewrite.so\)/\1/' -e 's/AllowOverride None/AllowOverride All/' /usr/local/apache2/conf/httpd.conf
COPY apache2/.htaccess /usr/local/apache2/htdocs/